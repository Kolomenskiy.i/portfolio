See "MIA_algorithm_eng.png".

MIA algorithm is realised on Binance and ItBit exchanges (in "buyer.py" and "seller.py" + "order_book.py" in "Trading" folder).
Almost realized MIA algorithm on Apifiny platform is presented in "apifiny.py" files.
Unrealized MIA algorithm on Crosstower platform is presented in "rest_api.py" files.

Notifications of results and portfolio status via telegram are realised in "telegram.py", in "messaging" folder.
