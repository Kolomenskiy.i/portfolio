import requests
import time
session = requests.session()


def get_trades(symbol_code, from_id):
    return session.get("https://api.crosstower.com/api/2/public/trades/%s?sort=ASC&by=id&from=%d&limit=1000" % (symbol_code, from_id)).json()


def get_symbol():
    return session.get("https://api.crosstower.com/api/2/public/symbol").json()


# symbol = 'ETHDAI'
symbol = 'BTCUSDC'
from_trade_id = 1

# print(get_symbol())

while True:
    trades = get_trades(symbol, from_trade_id)
    # print(trades)
    if len(trades) > 0:
        from_trade_id = trades[-1]['id'] + 1
        # print('Loaded trades till', trades[-1]['id'], trades[-1]['timestamp'])
        if len(trades) < 1000:
            time.sleep(5)
    else:
        time.sleep(30)
