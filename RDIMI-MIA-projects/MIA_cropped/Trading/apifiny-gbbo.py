import requests
import hashlib
import jwt
import json
import time


accountId = "-"  # cropped
API_key = "-"  # cropped
API_secret = "-"  # cropped
apifiny_URL = 'https://api.apifiny.com/gx/openApi/v1?signature={signature}'

method = "order.listOpenOrderLite"
# method = "account.queryAccountInfoLite"
# method = "utils.currentTimeMillis"

expiration_unix_time = 1615000000

RequestBody = {"accountId": accountId}

digest = hashlib.sha256(json.dumps(RequestBody).encode()).hexdigest()

payload = {
    "accountId": accountId,
    "secretKeyId": API_key,
    "digest": digest,
    "method": method,
    "exp": int(round(time.time() * 1000)) + 5000,
}

signature = jwt.encode(payload=payload, key=API_secret, algorithm='HS256')
# -----------------GET--------------------------
# signature = jwt.encode({"accessKey": API_key}, API_secret, algorithm='HS256')
# headers = {'signature': signature}
# # headers = {'signature': str(signature, encoding='utf-8')}
# GET_request_URL = "https://api.apifiny.com/gx/orderbook/v1/ETHBTC/GBBO?signature={signature}"
# GET_request_URL = GET_request_URL.format(signature=signature)
# print(requests.get(GET_request_URL).json())
# -----------------GET--------------------------
# -----------------POST-------------------------
# signature encoding as UTF-8
apifiny_URL = apifiny_URL.format(signature=str(signature, encoding="utf-8"))
# apifiny_URL = apifiny_URL.format(signature=signature.encode(encoding="utf-8"))
print(apifiny_URL)
# request headers
headers = {"Content-Type": "application/json;charset=UTF-8"}
# data to json
print(requests.post(url=apifiny_URL, json=RequestBody, headers=headers).json())
# -----------------POST-------------------------
