import websocket
import json
from collections import OrderedDict
from binance.websockets import BinanceSocketManager
from binance.client import Client
from abc import ABC, abstractmethod
import time
import copy


class Exchange(ABC):
    def __init__(self, order, pair):
        self._ask_order_book = {}
        self._bid_order_book = {}
        self._ws = None
        self._pair = pair
        self._order = order
        self.websocket_connection()
        self.init_order_book()

    @abstractmethod
    def websocket_connection(self):
        pass

    @abstractmethod
    def init_order_book(self):
        pass

    @abstractmethod
    def update_order_book(self):
        pass

    def sort_order_book(self):
        self._ask_order_book = OrderedDict(sorted(self._ask_order_book.items()))
        self._bid_order_book = OrderedDict(sorted(self._bid_order_book.items(), reverse=True))

    def remove_filled_orders(self):
        for i in set(self._ask_order_book):
            if self._ask_order_book[i] == 0:
                del self._ask_order_book[i]
        for i in set(self._bid_order_book):
            if self._bid_order_book[i] == 0:
                del self._bid_order_book[i]

    def print_order_book(self):
        try:
            print('ask: ', list(self._ask_order_book)[0])
            print('bid: ', list(self._bid_order_book)[0])
        except:
            pass


class ItBitExchange(Exchange):
    def __init__(self, conn_ask_order_book, conn_bid_order_book, order='market', pair='BTCUSD'):
        self._url = 'wss://ws.paxos.com/marketdata/'
        self._method = 'subscribeOrderbook'
        self._order = order
        self.conn_ask_order_book = conn_ask_order_book
        self.conn_bid_order_book = conn_bid_order_book
        super().__init__(order, pair)
        self.update_order_book()

    def websocket_connection(self):
        self._ws = websocket.create_connection(self._url + self._pair)
        self._ws.send(json.dumps({'jsonrpc': '2.0', 'method': self._method, 'id': 123}))

    def init_order_book(self):
        self._ask_order_book = OrderedDict(self._ask_order_book)
        self._bid_order_book = OrderedDict(self._bid_order_book)
        result = json.loads(self._ws.recv())
        # print(result)
        for ask_dict in result['asks']:
            self._ask_order_book.update({float(ask_dict['price']): float(ask_dict['amount'])})
        for bid_dict in result['bids']:
            self._bid_order_book.update({float(bid_dict['price']): float(bid_dict['amount'])})

    def update_order_book(self):
        while True:
            try:
                # print('Updated')
                result = json.loads(self._ws.recv())
                # print('result:',result)
                if result['side'] == 'SELL':
                    target_order_book = self._ask_order_book
                elif result['side'] == 'BUY':
                    target_order_book = self._bid_order_book
                else:
                    raise NameError('incorrect side name')

                if float(result['price']) in target_order_book:
                    target_order_book[float(result['price'])] = float(result['amount'])
                else:
                    target_order_book.update({float(result['price']): float(result['amount'])})
            except:
                self.websocket_connection()
                self.init_order_book()

            self.remove_filled_orders()
            self.sort_order_book()
            self.conn_ask_order_book.send(dict(self._ask_order_book.items()))
            self.conn_bid_order_book.send(dict(self._bid_order_book.items()))
    def get_order_book(self):
        self.websocket_connection()
        self.init_order_book()


class BinanceExchange(Exchange):
    def __init__(self, conn_ask_order_book, conn_bid_order_book, order='market', pair='BTCUSDT'):
        self._order_book_for_update = None
        self.conn_ask_order_book = conn_ask_order_book
        self.conn_bid_order_book = conn_bid_order_book
        super().__init__(order, pair)
        self.update_order_book()

    def websocket_connection(self, api_key='-', api_secret='-'):  # cropped
        self._ws = Client(api_key, api_secret)

    def init_order_book(self):
        self._ask_order_book = OrderedDict(self._ask_order_book)
        self._bid_order_book = OrderedDict(self._bid_order_book)
        bm = BinanceSocketManager(self._ws)
        bm.start_depth_socket(self._pair, self.get_order_book_for_update, depth=bm.WEBSOCKET_DEPTH_20)
        bm.start()
        while self._order_book_for_update is None:
            pass

    def update_order_book(self):
        while True:
            a = self._order_book_for_update['asks']
            self.conn_ask_order_book.send(a)
            self.conn_bid_order_book.send(self._order_book_for_update['bids'])

    def get_order_book_for_update(self, msg):
        self._order_book_for_update = msg

    def get_order_book(self):
        self.websocket_connection()
        self.init_order_book()


# if __name__ == "__main__":
#     main_ask_order_book = {}
#     main_bid_order_book = {}
#     exchange_object = BinanceExchange(main_ask_order_book, main_bid_order_book)
#     exchange_object.websocket_connection()
#     exchange_object.get_order_book()
