import requests
import hashlib
import jwt
import json

accountId = "-"  # cropped
API_key = "-"  # cropped
API_secret = "-"  # cropped
apifiny_URL = 'https://api.apifiny.com/gx/openApi/v1?signature={signature}'

method = "order.listOpenOrderLite"
# method = "account.queryAccountInfoLite"
# method = "utils.currentTimeMillis"

expiration_unix_time = 1615000000

RequestBody = {"accountId": accountId}

digest = hashlib.sha256(json.dumps(RequestBody).encode()).hexdigest()

payload = {
    "accountId": accountId,
    "secretKeyId": API_key,
    "digest": digest,
    "method": method,
    "exp": expiration_unix_time
}


signature = jwt.encode(payload=payload, key=API_secret, algorithm='HS256')

# -----------------GET--------------------------
# signature = jwt.encode({"accessKey": API_key}, API_secret, algorithm='HS256')
# headers = {'signature': signature}
# # headers = {'signature': str(signature, encoding='utf-8')}
# GET_request_URL = "https://api.apifiny.com/gx/orderbook/v1/ETHBTC/GBBO?signature={signature}"
# GET_request_URL = GET_request_URL.format(signature=signature)
# print(requests.get(GET_request_URL).json())
# -----------------GET--------------------------

# -----------------POST-------------------------
apifiny_URL = apifiny_URL.format(signature=signature)
print(requests.post(url=apifiny_URL, data=RequestBody).json())
# -----------------POST-------------------------
