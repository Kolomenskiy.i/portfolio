import hashlib
import json
import jwt
from urllib.parse import urlencode
import requests
import random

import websocket
from threading import Thread
import time
import ssl
import yaml

# Ver 1.4.2

with open("Apifiny_API_props.yaml", 'r') as stream:
    params = yaml.safe_load(stream)


class API_CONFIG:
    ACCOUNT_ID = params['ACCOUNT_ID']
    API_KEY_ID = params['API_KEY_ID']
    SECRET_KEY = params['SECRET_KEY']
    
    SYMBOL = 'BTCUSD'
    venue = 'VENUE1'  # Calling api requires specifying the name of the exchange
    VENUES = ["VENUE1", "VENUE2"]   # Specify the name of the exchange, subscribe to the market can subscribe to multiple exchanges

    API_HTTP = "https://api.apifiny.com/gx/openApi/v1"
    MD_HTTP = "https://api.apifiny.com/gx/orderbook/v1"
    MD_WS = "wss://api.apifiny.com/gx/orderbook/v1"


class StoreMsg:
    last_msg = None

    def save_message(self, msg):
        self.last_msg = msg
        print(self.last_msg)


BYTE = {
    'LF': '\x0A',
    'NULL': '\x00'
}

VERSIONS = '1.0,1.1'


class Stomp:
    def __init__(self, host, sockjs=False, wss=True, headers=None):
        """
        Initialize STOMP communication. This is the high level API that is exposed to clients.

        Args:
            host: Hostname
            sockjs: True if the STOMP server is sockjs
            wss: True if communication is over SSL

        attention:  only support sockjsjs is True
        """
        # websocket.enableTrace(True)
        ws_host = host if sockjs is False else host + "/websocket"
        protocol = "ws://" if wss is False else "wss://"

        self.url = protocol + ws_host
        self.dispatcher = WSClient(self.url, stomp=self, headers=headers)

        # maintain callback registry for subscriptions -> topic (str) vs callback (func)
        self.callback_registry = {}
        # set flag to false
        self.connected = False

    def connect(self):
        """
        Connect to the remote STOMP server
        """
        # attempt to connect
        self.dispatcher.connect_ws()

        # wait until connected
        while self.connected is False:
            time.sleep(.50)

        return self.connected

    def close(self):
        self.dispatcher.disconnect()

    def subscribe(self, destination, callback):
        """
        Subscribe to a destination and supply a callback that should be executed when a message is received on that destination
        """
        # create entry in registry against destination
        self.callback_registry[destination] = callback
        self.dispatcher.subscribe_topic(destination)

    def unsubscribe(self, destination):
        # remove destination callback
        self.callback_registry.pop(destination)
        self.dispatcher.unsubscribe_topic(destination)

    def send(self, destination, message):
        """
        Send a message to a destination
        """
        self.dispatcher.send_msg(destination, message)


class WSClient(websocket.WebSocket):

    def __init__(self, url, msg=[], subscribe=None, stomp=None, headers=None):
        self.url = url.replace("http", 'ws')
        self.stomp = stomp
        websocket.enableTrace(True)
        if stomp:
            self.ws = websocket.WebSocketApp(self.stomp.url, header=headers)
        else:
            self.ws = websocket.WebSocketApp(self.url, header=headers)

        # register websocket callbacks
        self.ws.on_open = self._on_open
        self.ws.on_message = self._on_message
        self.ws.on_error = self._on_error
        self.ws.on_close = self._on_close
        self.msg = msg
        self.subscribe = subscribe  # send message for subscribe channel

        # run event loop on separate thread
        if self.url.startswith('wss'):
            sslopt = {'cert_reqs': ssl.CERT_NONE}
        else:
            sslopt = None
        self.t = Thread(target=self.ws.run_forever, args=(None, sslopt))

        self.t.setDaemon(True)
        self.t.start()

        self.opened = False

        while self.opened is False:
            time.sleep(.50)

    def _on_message(self, message):
        """
        Executed when WS received message
        """
        if isinstance(self.msg, list):
            self.msg.append(message)
        else:
            self.msg = message
        if not self.subscribe:
            command, headers, body = self._parse_message(message)

            # if connected, let Stomp know
            if command == "CONNECTED":
                self.stomp.connected = True

            if command == "DISCONNECTED":
                self.stomp.connected = False

            # if message received, call appropriate callback
            if command == "MESSAGE":
                self.stomp.callback_registry[headers['destination']](body)

    def _on_error(self, error):
        """
        Executed when WS connection errors out
        """
        print(error)

    def _on_close(self):
        """
        Executed when WS connection is closed
        """
        print("### closed ###")
        # self.ws.close()

    def _on_open(self):
        """
        Executed when WS connection is opened
        """
        self.opened = True
        if self.subscribe:
            # 订阅channel 需要向服务器发送订阅消息
            if isinstance(self.subscribe, dict):
                self.ws.send(json.dumps(self.subscribe))
            elif isinstance(self.subscribe, str):
                self.ws.send(self.subscribe)

    def _transmit(self, command, headers, msg=None):
        """
        Marshalls and transmits the frame
        """
        # Contruct the frame
        lines = []
        lines.append(command + BYTE['LF'])
        # add headers
        for key in headers:
            lines.append(key + ":" + headers[key] + BYTE['LF'])

        lines.append(BYTE['LF'])

        # add message, if any
        if msg is not None:
            lines.append(msg)

        # terminate with null octet
        lines.append(BYTE['NULL'])

        frame = ''.join(lines)
        # transmit over ws
        self.ws.send(frame)

    def _parse_message(self, frame):
        """
        Returns:
            command
            headers
            body

        Args:
            frame: raw frame string
        """
        print(frame)
        return None, None, None
        lines = frame.split(BYTE['LF'])

        command = lines[0].strip()
        headers = {}

        # get all headers
        i = 1
        while lines[i] != '':
            # get key, value from raw header
            (key, value) = lines[i].split(':')
            headers[key] = value
            i += 1

        # set body to None if there is no body
        body = None if lines[i + 1] == BYTE['NULL'] else lines[i + 1]

        return command, headers, body

    def connect_ws(self):
        """
        Transmit a CONNECT frame
        """

        headers = dict()
        headers['host'] = self.stomp.url
        headers['accept-version'] = VERSIONS
        headers['heart-beat'] = '10000,10000'

        self._transmit('CONNECT', headers)

    def subscribe_topic(self, destination):
        """
        Transmit a SUBSCRIBE frame
        """
        headers = dict()

        # TODO id should be auto generated
        headers['id'] = 'sub-1'
        headers['ack'] = 'client'
        headers['destination'] = destination

        self._transmit('SUBSCRIBE', headers)

    def unsubscribe_topic(self, destination):
        """
        Transmit a SUBSCRIBE frame
        """
        headers = dict()

        # TODO id should be auto generated
        headers['id'] = 'sub-1'
        headers['ack'] = 'client'
        headers['destination'] = destination

        self._transmit('UNSUBSCRIBE', headers)

    def send_msg(self, destination, message):
        """
        Transmit a SEND frame
        """
        headers = dict()

        headers['destination'] = destination
        headers['content-length'] = str(len(message))

        self._transmit('SEND', headers, msg=message)

    def disconnect(self):

        headers = {}

        self._transmit('DISCONNECT', headers)


class ExchangeAPI:
    def __init__(self, base_url, account_id, api_key_id, secret_key, be_exception=True):
        self.secret_key_id = api_key_id
        self.secret_key = secret_key
        self.account_id = account_id
        self.base_url = base_url
        self.be_exception = be_exception

    def http_request(self, method, params, stream=False):
        params_json_string = json.dumps(params)
        # print(params_json_string)
        digest = hashlib.sha256(params_json_string.encode()).hexdigest()
        millis = int(round(time.time() * 1000)) + 5000
        signature = jwt.encode({
            'accountId': self.account_id,
            'secretKeyId': self.secret_key_id,
            'digest': digest,
            'method': method,
            'exp': millis,
        }, self.secret_key, algorithm='HS256')

        url = self.base_url + "?" + urlencode({
            "signature": signature
        })
        # print(method)
        # print(params)
        # print(json.dumps(params, indent=4, sort_keys=True))
        if stream:
            url = self.base_url + "?" + urlencode({
                "signature": signature,
                "body": params_json_string,
            })
            print(url)
            r = requests.get(url, stream=True)
            # print(r.text)
            return r
        else:
            print(url), params_json_string
            r = requests.post(
                url, data=params_json_string,
                headers={'Content-Type': 'application/json; charset=utf-8'})
            if r.status_code == 200:
                return json.loads(r.text)
            else:
                return json.loads(r.text)

    def ws_client(self, method, params):
        params_json_string = json.dumps(params)
        digest = hashlib.sha256(params_json_string.encode()).hexdigest()
        millis = int(round(time.time() * 1000)) + 30000
        signature = jwt.encode({
            'accountId': self.account_id,
            'secretKeyId': self.secret_key_id,
            'digest': digest,
            'method': method,
            'exp': millis,
        }, self.secret_key, algorithm='HS256')

        url = self.base_url + "?" + urlencode({
            "signature": signature,
            "body": params_json_string,
            'p': 'webSocket'
        })
        ws = WSClient(url)
        return ws

    def current_time_millis(self):
        return self.http_request("utils.currentTimeMillis", {})

    def list_currency(self):
        return self.http_request("utils.listCurrency", {})

    def query_account_info(self, venue):
        return self.http_request("account.queryAccountInfoLite", {
            "accountId": self.account_id,
            "venue": venue
        })

    def list_venue_info_lite(self):
        return self.http_request('utils.listVenueInfoLite', {})

    def list_symbol_info_lite(self, venue):
        return self.http_request('utils.listSymbolInfoLite', {
            "accountId": self.account_id,
            "venue": venue
        })

    def list_balance(self, venue):
        return self.http_request("asset.listBalanceByLite", {
            "accountId": self.account_id,
            "venue": venue
        })

    def creat_withdraw_tickert(self, venue):
        return self.http_request("asset.createWithdrawTicketLite", {
            "accountId": self.account_id,
            "venue": venue
        })

    def query_address_lite(self, venue, currency):
        return self.http_request('asset.queryAddressLite', {
            "accountId": self.account_id,
            "venue": venue,
            "currency": currency,
        })

    def withdraw_lite(self, venue, currency, address, amount, ticket_id, memo=""):
        return self.http_request('asset.withdrawLite', {
            "accountId": self.account_id,
            "venue": venue,
            "currency": currency,
            "amount": amount,
            "address": address,
            "memo": memo,
            "ticket": ticket_id
        })

    def transfer_between_venues(self, venue, currency, amount, target_venue):
        return self.http_request('asset.transferToVenue', {
            "accountId": self.account_id,
            "venue": venue,
            "currency": currency,
            "amount": amount,
            "targetVenue": target_venue,
        })

    def query_asset_activity_list_lite(self, start_time, end_time, limit):
        return self.http_request('asset.queryAssetActivityListLite', {
            "accountId": self.account_id,
            "startTimeDate": str(start_time),
            "endTimeDate": str(end_time),
            "page": '1',
            "limit": str(limit)
        })

    def new_order(self, order_id, symbol, order_type, quantity, limit_price, order_side, venue, time_inforce):
        return self.http_request("order.insertOrderLite", {
            "accountId": self.account_id,
            "orderId": order_id,
            "venue": venue,
            "orderInfo": {
                "symbol": symbol,
                "orderType": order_type,
                "quantity": quantity,
                "limitPrice": limit_price,
                "orderSide": order_side,
                "timeInForce": time_inforce
            },
        })

    def cancel_order(self, order_id, venue):
        return self.http_request("order.cancelOrderLite", {
            "accountId": self.account_id,
            "venue": venue,
            "orderId": order_id,
        })

    def list_open_order(self):
        return self.http_request("order.listOpenOrderLite", {
            "accountId": self.account_id,
        })

    def query_order_info(self, order_id, venue):
        return self.http_request("order.queryOrderInfoLite", {
            "accountId": self.account_id,
            "venue": venue,
            "orderId": order_id,
        })

    def list_order_info(self, order_id_set):
        return self.http_request("order.listOrderInfoLite", {
            "accountId": self.account_id,
            "orderIdList": order_id_set,
        })

    def list_completed_order(self, start_time, end_time, limit, venue):
        return self.http_request("order.listCompletedOrderLite", {
            "accountId": self.account_id,
            'startTime': str(start_time),
            'endTime': str(end_time),
            "limit": limit,
            "venue": venue,
            'forward': True,
            'baseAsset': None,
            'quoteAsset': None,
            'orderSide': None,
            'orderStatus': None,
        })

    def stream_order(self):
        ws = self.ws_client("order.streamDetail", {
            "accountId": self.account_id,
            "startDateTime": None
        })
        return ws

    def stream_order_sse(self):
        client = self.http_request("order.streamDetail", {
            "accountId": self.account_id,
            "startDateTime": None
        }, stream=True)
        return client

    def generate_orderid(self):
        return self.account_id.split('-')[-1] + str(int(time.time() * 1000)) + str(random.randint(100, 999))

    def get_order_fees_in_two_minutes(self, venue, start_time):
        return self.http_request("order.listDetailLite", {
            "accountId": self.account_id,
            'startDateTime': start_time,
            "venue": venue,
        })

    def get_order_fee(self, venue, order_id):
        return self.http_request("order.readerOrderDetail", {
            "accountId": self.account_id,
            "venue": venue,
            'orderId': order_id,
        })


def http_get_market_data():
    res = requests.get(API_CONFIG.MD_HTTP + API_CONFIG.SYMBOL)
    print(res.json())


def ws_get_market_data(url, sub={"channel": "orderbook", "symbol": 'BTCUSDT', "venues": ["BVNEX","BINANCE"],"action": "sub"}, header=None):
    ws = WSClient(url, '', subscribe=sub, headers=header)
    time.sleep(2)
    count = 0
    while True:
        print(ws.msg)
        count += 1
        if count > 20:
            break
    ws.ws.close()


def ws_get_market_data_stomp(url, symbol, venue, headers=None):
    import traceback
    ws_base_url = url.split('://')[1]
    wss = False if url.split('://')[0] == "ws" else True
    try:
        sm1 = StoreMsg()
        b_t = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
        print("begin time: {}".format(b_t))
        st = Stomp(ws_base_url, True, wss, headers=headers)
        topic = "/topic/orderbook/{symbol}/{venue}".format(symbol=symbol, venue=venue)
        st.connect()
        st.subscribe(topic, sm1.save_message)
        print('subscribe {} {}'.format(ws_base_url, topic))
    except Exception:
        traceback.print_exc()
        print('error time:{}'.format(time.time()))

    while not sm1.last_msg:
        pass


def get_md_signature(accessKey, secretKey):
    claims = {'accessKey': accessKey}
    signature = jwt.encode(claims, secretKey, algorithm='HS256')

    # return str(signature, encoding='utf-8')  # original code
    return signature


if __name__ == '__main__':
    exchange_api = ExchangeAPI(API_CONFIG.API_HTTP, API_CONFIG.ACCOUNT_ID, API_CONFIG.API_KEY_ID, API_CONFIG.SECRET_KEY)

    print("get market data using http")
    headers = {'signature': get_md_signature(API_CONFIG.API_KEY_ID, API_CONFIG.SECRET_KEY)}
    res = requests.get(API_CONFIG.MD_HTTP + "/BTCUSD", headers=headers)
    print(res.json())

    # print('get current time millis')
    # res = exchange_api.current_time_millis()
    # print(res)
    #
    # print('list currency')
    # c_res = exchange_api.list_currency()
    # for i in c_res['result']:
    #     print(i)
    #
    # print('query venue info')
    # v_res = exchange_api.list_venue_info_lite()
    # print(v_res)
    #
    # print('query symbole info')
    # res = exchange_api.list_symbol_info_lite(API_CONFIG.venue)
    # print(res)
    #
    # print('query address ')
    # res = exchange_api.query_address_lite(API_CONFIG.venue, 'BTC')
    # print(res)
    # address = res['result']['address']
    #
    # print('create ticket ')
    # res = exchange_api.creat_withdraw_tickert(API_CONFIG.venue)
    # print(res)
    # ticket = res['result']['ticket']
    # # print(json.dumps(res['result'], indent=4, sort_keys=True))
    #
    # print('withdraw lite')
    # memo = ""  # if currency is eos, the value of memo is the memo field returned by queryAddressLite
    # res = exchange_api.withdraw_lite(API_CONFIG.venue, 'BTC', address, 0.01, ticket, memo)
    # print(res)
    #
    # print('query_asset_activity_list_lite')
    # now_time = int(time.time() * 1000)
    # begin_time = int(now_time - 3 * 24 * 3600 * 1000)
    # res = exchange_api.query_asset_activity_list_lite(begin_time, now_time, 10)
    # print(res)
    #
    # print('send order')
    # order_id = exchange_api.generate_orderid()
    # res = exchange_api.new_order(order_id, "BTCUSDC", "LIMIT", "0.02", "100", "BUY", API_CONFIG.venue, 3)
    # print(res)
    #
    # print('list open order')
    # res = exchange_api.list_open_order()
    # print(res)
    #
    # print('list order info')
    # res = exchange_api.list_order_info([order_id, '123'])
    # print(res)
    #
    # print("query single order")
    # res = exchange_api.query_order_info(order_id, API_CONFIG.venue)
    # print(res)
    #
    # print('cancel order')
    # res = exchange_api.cancel_order(order_id, API_CONFIG.venue)
    # print(res)
    #
    # print('list completed order')
    # now_time = int(time.time() * 1000)
    # begin_time = int(now_time - 3 * 24 * 3600 * 1000)
    # res = exchange_api.list_completed_order(begin_time, now_time, 10, API_CONFIG.venue)
    # for i in res['result']:
    #     print(i['accountId'], i['orderId'], i['venue'], i['orderStatus'])
    # print(res)
    #
    # print("query account info")
    # res = exchange_api.query_account_info(API_CONFIG.venue)
    # print(res)
    #
    # print("query account balance")
    # res = exchange_api.list_balance(API_CONFIG.venue)
    # print(res)
    # # print(json.dumps(res['result'], indent=4, sort_keys=True))
    # for i in v_res['result']:
    #     venue = i['exchange']
    #     res = exchange_api.query_account_info(venue)
    #     res2 = exchange_api.list_balance(venue)
    #     print(i)
    #     print(res)
    #     print(res2)
    # # print("websocket order update")
    # ws = exchange_api.stream_order()
    # order_id = exchange_api.generate_orderid()
    # res = exchange_api.new_order(order_id, API_CONFIG.SYMBOL, "LIMIT", "0.02", "100", "BUY")
    # while len(ws.msg) == 0:
    #     time.sleep(0.5)
    # for m in ws.msg:
    #     res = json.loads(m)['result']
    #     print(res)
    #
    # print("get market data using http")
    # headers = {'signature': get_md_signature(API_CONFIG.API_KEY_ID, API_CONFIG.SECRET_KEY)}
    # res = requests.get(API_CONFIG.MD_HTTP + "/BTCUSD", headers=headers)
    # print(res.json())
    # print("get market data using websocket")
    # # time.sleep(5)
    # msg = {"channel": "orderbook", "symbol": 'BTCUSDT', "venues": API_CONFIG.VENUES,
    #        'collect': True, "action": "sub"}
    #
    # def sub_md(message):
    #     from websocket import create_connection
    #     import ssl
    #     sslopt = {"cert_reqs": ssl.CERT_NONE}
    #     con = create_connection(API_CONFIG.MD_WS, sslopt=sslopt, header=headers)
    #     con.send(json.dumps(message))
    #     while True:
    #         response = con.recv()
    #         if 'ping' in response:
    #             pong = response.replace('ping', 'pong')
    #             con.send(pong)
    #         print(response)
    # sub_md(msg)
