# -*- encoding: utf-8 -*-

import pandas as pd

pd.core.common.is_list_like = pd.api.types.is_list_like


import re
from os.path import abspath

import logging

from telegram import Bot
from telegram import Update
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, InlineQueryHandler
from telegram.ext import Filters
from telegram.utils.request import Request

from messaging.core import MessageClient

# from trading.core import TradingClient


class TelegramMessageClient(MessageClient):

    def __init__(self, name, id, hash, default_chat, user_dict, teleg=True, log_file=None):
        self._log_file = log_file
        self._default_chat = default_chat
        self._order_pattern = '^(?i)(buy|sell)\s*([0-9\.]*)\s*([a-z]*)\s*([a-z]*)$'
        self._order_regex = re.compile(self._order_pattern)
        self._liquidate_pattern = '^(?i)(sell)\s*(all)\s*([a-z]*)$'
        self._liquidate_regex = re.compile(self._liquidate_pattern)
        self._admin_list = user_dict['Admins']
        self._client_list = user_dict['Clients']
        self._bot = Bot(
            token=':'.join([str(id), str(hash)]),
            base_url='https://telegg.ru/orig/bot' if teleg else None,  # vpn for telegram
            request=Request(con_pool_size=8)
        )
        self._updater = Updater(
            bot=self._bot,
        )
        start_handler = CommandHandler("run", self.start)
        stop_handler = CommandHandler("pause", self.stop)
        balance_chandler = CommandHandler("balance", self.balance)
        log_chandler = CommandHandler("log", self.send_log)
        hi_handler = MessageHandler(Filters.regex('^(?i)hello$'), self.say_hi)
        balance_handler = MessageHandler(Filters.regex('^(?i)balance$'), self.balance)
        order_handler = MessageHandler(Filters.regex(self._order_pattern), self.buy_sell)
        liquidate_handler = MessageHandler(Filters.regex(self._liquidate_pattern), self.liquidate)
        inline_start_handler = InlineQueryHandler(self.say_hi, pattern='.*')
        set_default_chat_handler = CommandHandler("setdefault", self.set_default_chat)

        self._updater.dispatcher.add_handler(start_handler)
        self._updater.dispatcher.add_handler(stop_handler)
        self._updater.dispatcher.add_handler(hi_handler)
        self._updater.dispatcher.add_handler(balance_chandler)
        self._updater.dispatcher.add_handler(log_chandler)
        self._updater.dispatcher.add_handler(balance_handler)
        self._updater.dispatcher.add_handler(order_handler)
        self._updater.dispatcher.add_handler(liquidate_handler)
        self._updater.dispatcher.add_handler(inline_start_handler)
        self._updater.dispatcher.add_handler(set_default_chat_handler)

        self._updater.start_polling()

        self._trading_client = None

        self.send_message("Telegram client was launched", self._default_chat)

    @property
    def client(self):
        return self._client

    @property
    def trading_client(self):
        return self._trading_client

    @property
    def log_file(self):
        return self._log_file

    # @trading_client.setter
    # def trading_client(self, client: TradingClient):
    #     self._trading_client = client
    #     self.send_message("Trading client was set")

    def send_message(self, msg, chat_id=None):
        # print('Chat', chat_id)
        try:
            self._bot.send_message(
                chat_id=self._default_chat if chat_id is None else chat_id,
                text=msg,
            )
        except:
            pass

    def set_default_chat(self, bot: Bot, update: Update):
        self._default_chat = update.message.chat_id
        bot.send_message(
            chat_id=update.message.chat_id,
            text='Chat '
                 + update.effective_chat.title if update.effective_chat.title is not None else update.effective_chat.username
                 + ' was set as default chat.'
        )

    def say_hi(self, bot: Bot, update: Update):
        bot.send_message(
            chat_id=update.message.chat_id,
            text='Hello!',
        )

    def start(self, bot: Bot, update: Update):
        if update.message.chat_id not in self._admin_list:
            bot.send_message(
                chat_id=update.message.chat_id,
                text="Access denied",
            )
            self._bot.send_message(
                chat_id=self._default_chat,
                text='A stranger is trying to talk with me! His ID: '+str(update.message.chat_id),
            )
            return
        if self.trading_client is not None:
            bot.send_message(
                chat_id=update.message.chat_id,
                text="Trying to start it!",
            )
            self.trading_client.start()
        else:
            bot.send_message(
                chat_id=update.message.chat_id,
                text="Nothing to start, my Lord",
            )

    def stop(self, bot: Bot, update: Update):
        if update.message.chat_id not in self._admin_list:
            bot.send_message(
                chat_id=update.message.chat_id,
                text="Access denied",
            )
            self._bot.send_message(
                chat_id=self._default_chat,
                text='A stranger is trying to talk with me! His ID: '+str(update.message.chat_id),
            )
            return
        if self.trading_client is not None:
            self.send_message("Trying to stop it!")
            self.trading_client.stop()
        else:
            self.send_message("Nothing to stop, my Lord")

    def balance(self, bot: Bot, update: Update):
        if update.message.chat_id not in self._admin_list and update.message.chat_id not in self._client_list:
            bot.send_message(
                chat_id=update.message.chat_id,
                text="Access denied",
            )
            self._bot.send_message(
                chat_id=self._default_chat,
                text='A stranger is trying to talk with me! His ID: '+str(update.message.chat_id),
            )
            return
        balances = self.trading_client.get_balance()
        data = dict()
        if len(balances) > 0:
            for bname, bdata in balances.items():
                for currency, cdata in bdata.items():
                    currency = currency.upper()
                    bname = bname.capitalize()
                    tmp_dict = data.get(currency, dict())
                    tmp_dict[bname] = cdata
                    data[currency] = tmp_dict
        msg = 'Account balances:\n'
        prices = self.trading_client.get_prices(list(data.keys()))
        prices['USD'] = 1
        total = 0
        for currency, balance in data.items():
            msg += currency + ':\n'
            nothing = True
            for name, value in balance.items():
                if name == 'Total':
                    total += value * prices.get(currency, 0)
                if float(value) != 0:
                    msg += '\t{:7}{:<9.7f} : ${:.2f}\n'.format(name, value, value * prices.get(currency, 0))
                    nothing = False
            if nothing:
                msg = msg[:-5]
        msg += '\n'+'Overall: ' + str("%.2f" % total)
        self.send_message(msg, update.effective_chat.id)

    def buy_sell(self, bot: Bot, update: Update):
        if update.message.chat_id not in self._admin_list:
            bot.send_message(
                chat_id=update.message.chat_id,
                text="Access denied",
            )
            self._bot.send_message(
                chat_id=self._default_chat,
                text='A stranger is trying to talk with me! His ID: '+str(update.message.chat_id),
            )
            return
        try:
            side, amount, currency, quote = self._order_regex.findall(update.message.text)[0]
            side = side.lower()
            amount = float(amount)
            currency = currency.upper()
            quote = quote.upper()
            order = self.trading_client.market_order(amount, currency, quote, side, True)
        except Exception as e:
            self.send_message('Unable to create order. Check logs.\n{}'.format(e), update.effective_chat.id)

    def liquidate(self, bot: Bot, update: Update):
        if update.message.chat_id not in self._admin_list:
            bot.send_message(
                chat_id=update.message.chat_id,
                text="Access denied",
            )
            self._bot.send_message(
                chat_id=self._default_chat,
                text='A stranger is trying to talk with me! His ID: '+str(update.message.chat_id),
            )
            return
        try:
            _, _, quote = self._order_regex.findall(update.message.text)[0]
            self.trading_client.liquidate(quote)
        except Exception as e:
            self.send_message('Unable to liquidate position. Check logs.\n{}'.format(e), update.effective_chat.id)

    def send_log(self, bot: Bot, update: Update):
        if update.message.chat_id not in self._admin_list:
            bot.send_message(
                chat_id=update.message.chat_id,
                text="Access denied",
            )
            self._bot.send_message(
                chat_id=self._default_chat,
                text='A stranger is trying to talk with me! His ID: '+str(update.message.chat_id),
            )
            return
        try:
            if self.log_file is not None:
                bot.send_document(
                    chat_id=update.effective_chat.id,
                    # filename=self.log_file,
                    document=open(abspath(self.log_file), 'rb')
                )
            else:
                bot.send_message("I don't know where is log file.")
        except Exception as e:
            logging.exception(e)
            self.send_message('Unable to send log file.\n{}'.format(e), update.effective_chat.id)
