# -*- encoding: utf-8 -*-

import pandas as pd

pd.core.common.is_list_like = pd.api.types.is_list_like

from abc import ABC, abstractmethod


class MessageClient(ABC):

    @abstractmethod
    def send_message(self, msg):
        pass
