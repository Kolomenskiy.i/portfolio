import statistics
from multiprocessing import Process, Manager, Pipe
from messaging.telegram import TelegramMessageClient
from Trading.order_book import BinanceExchange, ItBitExchange

import ccxt

import time


class arbitraje():

    def __init__(self, exchangeA, exchangeB, msg_client):
        self.exchangeA = exchangeA
        self.exchangeB = exchangeB
        self._msg_client = msg_client

    def start(self):
        # self._msg_client = TelegramMessageClient()
        # self._msg_client = TelegramMessageClient('anon', TG_ID, TG_HASH, TG_DCHAT, USER_DICT)
        # self.manager = Manager()
        self.inside_spread = 0.0009
        self.ask_price = float(0)
        self.bid_price = float(0)
        self.sell_hedge_opportunity = 0
        # self.buy_hedge_opportunity = 0
        self.prev_sell_hedge_opportunity = 0
        # self.prev_buy_hedge_opportunity = 0
        self.bookA_ask = None
        self.bookA_bid = None
        self.bookB_ask = None
        self.bookB_bid = None
        self.done_order = False
        # self.Binance = BinanceExchange(self.bookB_ask, self.bookB_bid)
        self.conn_askA_parent, conn_askA_child = Pipe(duplex=False)
        self.conn_bidA_parent, conn_bidA_child = Pipe(duplex=False)
        self.conn_askB_parent, conn_askB_child = Pipe(duplex=False)
        self.conn_bidB_parent, conn_bidB_child = Pipe(duplex=False)
        p1 = Process(target=ItBitExchange, args=(conn_askA_child,conn_bidA_child))
        p2 = Process(target=BinanceExchange, args=(conn_askB_child, conn_bidB_child))
        p1.start(), p2.start(), conn_askA_child.close(), conn_bidA_child.close()
        self._wallet_id = '-'  # cropped

    def run(self):
        self.start()
        # self._sched.start()
        while True:
            self.book_update()
            if self.upd == False:
                continue
            if self.bookB_ask is None or self.bookB_bid is None or self.bookB_ask is None or self.bookB_ask is None:
                continue
            self.target_price_calculator()
            self.func_buy_hedge_opportunity()
            if self.buy_hedge_opportunity > 0.001:
                self.buy_hedge_logic()

    def book_update(self):
        self.uod = False
        try:
            self.bookA_ask = dict(self.conn_askA_parent.recv())
            self.upd = True
        except:
            pass
        try:
            self.bookA_bid = dict(self.conn_bidA_parent.recv())
            self.upd = True
        except:
            pass
        try:
            self.bookB_ask = dict(self.conn_askB_parent.recv())
            self.upd = True
        except:
            pass
        try:
            self.bookB_bid = dict(self.conn_bidB_parent.recv())
            self.upd = True
        except:
            pass

    def func_buy_hedge_opportunity(self):
        self.buy_hedge_opportunity = 0
        for price, amount in self.bookB_bid.items():
            price = float(price)
            amount = float(amount)
            if price > self.bid_price*1.0007 and amount > 0.1:
                self.buy_hedge_opportunity += amount
            else:
                self.buy_hedge_opportunity = min([0.005, self.buy_hedge_opportunity/2])
                break

    def create_order(self):
        pass

    def target_price_calculator(self):
        if float(list(self.bookA_ask.keys())[0]) < float(self.ask_price) or float(
                list(self.bookA_bid.keys())[0]) > float(self.bid_price):
            mid_price = statistics.median([float(list(self.bookA_ask.keys())[0]), float(list(self.bookA_bid.keys())[0])])
            self.ask_price = round(max(mid_price * (1+self.inside_spread) - ((mid_price * (1+self.inside_spread)) % 0.25),
                                       (float(list(self.bookA_ask.keys())[0])*1.0002)), 2)
            self.bid_price = round(min(mid_price * (1-self.inside_spread) - ((mid_price * (1-self.inside_spread)) % 0.25),
                                       (float(list(self.bookA_bid.keys())[0])*0.9998)), 2)

    def buy_hedge_logic(self):
        try:
            # self.orderA = self.exchangeA.create_order(symbol='BTC/USD', type='limit', side='buy', amount=self.buy_hedge_opportunity,
            #                                 price=self.bid_price, params={'walletId': self._wallet_id,'postOnly': True})['id']
            self.orderA = self.exchangeA.create_order(symbol='BTC/USD', type='limit', side='buy',
                                                      amount=self.buy_hedge_opportunity, price=self.bid_price,
                                                      params={'walletId': self._wallet_id})['id']
            time.sleep(1)
            order_amount = self.buy_hedge_opportunity
        except Exception as e:
            self._msg_client.send_message('Failed to create a buy order on itbit'+str(e))
            return
        while True:
            update = False
            try:
                order_status = self.exchangeA.fetch_order_status(id=self.orderA, symbol='BTC/USD',
                                                                 params={'walletId': self._wallet_id})
                if order_status == 'cancelled':
                    print('Crashed3')
                    return
            except Exception as e:
                self._msg_client.send_message('Failed to get an order status on itbit. Ex: ', e)
                print('Crashed2')
                break

            if order_status == 'filled' or order_status == 'closed':
                try:
                    self.orderB = self.exchangeB.create_order(symbol='BTC' + '/'+'USDT', type='market', side='sell',
                                                              amount=self.buy_hedge_opportunity)['id']
                except Exception as e:
                    self._msg_client.send_message('Failed to create sell order on Binance'+str(e))
                    break
                update = False
                for _ in range(3):
                    try:
                        self.after_trade_notification()
                        update = True
                        self.done_order = True
                        print('Crashed5')
                        return
                    except Exception as e:
                        time.sleep(0.1)
                        ex = e
                if update == False:
                    print('Crashed6')
                    # self._msg_client.send_message('Failed to get info on Binance market order. Exception: '+str(ex))
                    return
                break
            else:
                self.book_update()
                self.target_price_calculator()
                self.func_buy_hedge_opportunity()
                if self.buy_hedge_opportunity<order_amount:
                    try:
                        self.exchangeA.cancel_order(id=self.orderA, symbol='BTC/USD', params={'walletId': self._wallet_id})
                        return
                    except:
                        self._msg_client.send_message('Failed to cancel itBit order. Order status: '+str(order_status))
                        return

    def after_trade_notification(self):
        rebate = float(exchangeA.fetch_my_trades(params={'walletId':walletId})[-1]['info']['rebatesApplied'])
        bin_com = float(exchangeB.fetch_my_trades('BTC/USDT')[-1]['info']['commission'])
        itbit_com = float(exchangeA.fetch_my_trades(params={'walletId':walletId})[-1]['info']['commissionPaid'])
        sell_price = self.exchangeB.fetch_order(self.orderB, 'BTC/USDT')['price']
        np = (self.buy_hedge_opportunity*(sell_price-self.bid_price))-bin_com+rebate-itbit_com
        self._msg_client.send_message('Bought on itbit for: ' + str(self.bid_price) + '\n' + 'Sold on Binance for: ' +
                                      str(sell_price) + '\n' + 'Amount: ' + str(self.buy_hedge_opportunity) + '\n' +
                                      'Net profit: $' + str(np))

    def informer(self):
        try:
            self._msg_client.send_message('Update: ')
            self._msg_client.send_message('Best ask on itBit: '+str(list(self.bookA_ask.keys())[0])+'\n')
            self._msg_client.send_message('Our ask: ' + str(self.ask_price)+'\n')
            self._msg_client.send_message('Best bid on Binance: ' + str(list(self.bookB_bid.keys())[0]) + '\n')
            self._msg_client.send_message('Best bid on itBit: '+str((list(self.bookA_bid.keys())[0]))+'\n')
            self._msg_client.send_message('Our bid: ' + str((self.bid_price))+'\n')
            self._msg_client.send_message('Best ask on Binance: ' + str(list(self.bookB_ask.keys())[0]) + '\n')
        except:
            pass
        # self._msg_client.send_message('Binance ask: ' + str(list(self.bookB_ask)) + '\n')
        # self._msg_client.send_message('Binance bid: ' + str(list(self.bookB_bid)) + '\n')


if __name__ == "__main__":
    TG_ID = 0  # cropped
    TG_HASH = '-'  # cropped
    TG_DCHAT = '-'  # cropped
    # TG_DCHAT = '-'  # cropped
    USER_DICT = {'Admins': 0, 'Clients': 0}  # cropped

    msg_client = TelegramMessageClient('anon', TG_ID, TG_HASH, TG_DCHAT, USER_DICT)

    BITSTAMP_USERNAME = '-'  # cropped
    BITSTAMP_KEY = '-'  # cropped
    BITSTAMP_SECRET = '-'  # cropped

    BINANCE_KEY = '-'  # cropped
    BINANCE_SECRET = '-'  # cropped

    itbit_UserId = '-'  # cropped
    itbit_ClientKey = '-'  # cropped
    itbit_ClientSecret = '-'  # cropped

    itbit_params = {
        'UserId': itbit_UserId,
        'apiKey': itbit_ClientKey,
        'secret': itbit_ClientSecret,
        # 'nonce': lambda: ccxt.Exchange.microseconds()
    }

    binance_params = {
        'apiKey': BINANCE_KEY,
        'secret': BINANCE_SECRET,
        # 'nonce': lambda: ccxt.Exchange.microseconds()
    }
    bitstamp_params = {
        'apiKey': BITSTAMP_KEY,
        'secret': BITSTAMP_SECRET,
        'uid': BITSTAMP_USERNAME,
        'nonce': lambda: ccxt.Exchange.microseconds()
    }
    walletId = '-'  # cropped
    exchangeA = ccxt.itbit(itbit_params)
    exchangeB = ccxt.binance(binance_params)
    arbitraje(exchangeA, exchangeB, msg_client).run()
