This repository contains 2 projects: RDIMI and MIA. 

RDIMI is the main project. Its purpose is to build a profitable and 
risk-adjusted portfolio, develop trading strategies, find the best of them
with proper and not overfitted parameters and maintain its work in real conditions 
executing trades with the best prices and without slippage. 

MIA is a secondary project. Its purpose is to build market making and arbitrage algorithms
with a zero risk using hedge opportunities.
