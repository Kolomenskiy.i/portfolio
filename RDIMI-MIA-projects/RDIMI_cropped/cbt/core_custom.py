# -*- encode: utf-8 -*-

import copy
from enum import Enum

import pandas as pd
import numpy as np

pd.core.common.is_list_like = pd.api.types.is_list_like
from bt import Backtest, Strategy
from bt.core import StrategyBase, SecurityBase
from ffn import fmtn, fmtp

from trading.params import TradingParams


def fee_func(q, p, ff, pf):  # Цена, объем, фикс-фи, percent fee
    return np.max([ff, pf * p * np.abs(q)])


def stock_market_fee_func(q, p, ff, pf):  # ff per share quantity, pf - spread
    return np.abs(q) * ff + np.abs(q) * p * pf


class StrategyOHLC(Strategy):

    def __init__(self, name, algos=None, children=None, sec_amount_rounding=None):
        super().__init__(name, algos, children, sec_amount_rounding)
        logger = TradingParams().get_logger()
        logger.debug('StrategyOHLC is inited')

        self._data_dict = None
        self._open = None
        self._high = None
        self._low = None
        self._close = None
        self._volume = None

    @property
    def data_dict(self):
        return self._data_dict

    @data_dict.setter
    def data_dict(self, data_dict):
        self._data_dict = data_dict

        self._open = pd.DataFrame({k: v.Open for k, v in data_dict.items()}).fillna(method='pad').fillna(
            method='bfill')
        self._high = pd.DataFrame({k: v.High for k, v in data_dict.items()}).fillna(method='pad').fillna(
            method='bfill')
        self._low = pd.DataFrame({k: v.Low for k, v in data_dict.items()}).fillna(method='pad').fillna(
            method='bfill')
        self._close = pd.DataFrame({k: v.Close for k, v in data_dict.items()}).fillna(method='pad').fillna(
            method='bfill')
        # self._volume = pd.DataFrame({k: v.Volume for k, v in data_dict.items()}).fillna(method='pad').fillna(
        #     method='bfill')

        logger = TradingParams().get_logger()
        logger.debug('StrategyOHLC data is set')

    def add_data(self, dt, data):
        logger = TradingParams().get_logger()
        logger.debug(f"add_data:\ndt: {dt}\ndata:\n{data}")
        open = dict()
        high = dict()
        low = dict()
        close = dict()
        volume = dict()
        for currency, prices in data.items():
            self._data_dict[currency].loc[dt] = prices
            logger.debug(f"add_data:\ncurrency: {currency}\ndata_dict last dt: {self._data_dict[currency].index[-1]}")

            open[currency] = prices.Open
            high[currency] = prices.High
            low[currency] = prices.Low
            close[currency] = prices.Close
            # volume[currency] = prices.Volume

        self._open.loc[dt] = pd.Series(open)
        self._high.loc[dt] = pd.Series(high)
        self._low.loc[dt] = pd.Series(low)
        self._close.loc[dt] = pd.Series(close)
        # self._volume.loc[dt] = pd.Series(volume)

        logger.debug('\n'.join([
            '\nadd_data last OHLCV dates',
            f'add_data: open last: {self._open.index[-1]}',
            f'add_data: high last: {self._high.index[-1]}',
            f'add_data: low last: {self._low.index[-1]}',
            f'add_data: close last: {self._close.index[-1]}',
            # f'add_data: volume last: {self._volume.index[-1]}',
        ]))

    @property
    def open_price(self):
        return self._open

    @property
    def high_price(self):
        return self._high

    @property
    def low_price(self):
        return self._low

    @property
    def close_price(self):
        return self._close

    @property
    def volume(self):
        return self._volume

    @property
    def withdrawals(self):
        return pd.Series(self.perm.get('withdrawals', {}))

    @property
    def success_fee(self):
        return pd.Series(self.perm.get('success_fee', {}))

    def consume_withdrawals(self):
        for dt, value in self.perm.get('withdrawals', {}).items():
            self._values.loc[self._values.index >= dt] += value
        self._prices = (self._values / self.values.shift()).fillna(1.0).cumprod() * 100


class StrategyOHLCCap(StrategyOHLC):

    def __init__(self, name, algos=None, children=None):
        super().__init__(name, algos, children)

        self._market_cap = None

    @property
    def market_cap(self):
        return self._market_cap

    @market_cap.setter
    def market_cap(self, market_cap):
        self._market_cap = market_cap


class PriceData(Enum):

    Open = 'Open'
    High = 'High'
    Low = 'Low'
    Close = 'Close'
    Volume = 'Volume'


class BacktestOHLC(Backtest):

    def __init__(self, strategy: StrategyOHLC, data_dict, name=None, initial_capital=1000000.0, commissions=None, integer_positions=True,
                 progress_bar=True, price_type: PriceData=PriceData.Open, **kwargs):

        if not isinstance(strategy, StrategyOHLC):
            raise TypeError('strategy must be StrategyOHLC')

        if not isinstance(price_type, PriceData):
            raise TypeError('price_type must be PriceData')

        self._price_type = price_type.value

        data = pd.DataFrame({c: v[self._price_type] for c, v in data_dict.items()}).fillna(method='pad').fillna(method='bfill')

        super().__init__(strategy, data, name, initial_capital, commissions, integer_positions, progress_bar)

        self.strategy.data_dict = copy.deepcopy(data_dict)

        logger = TradingParams().get_logger()
        logger.debug('BacktestOHLC is inited')
        logger.debug(f'BacktestOHLC last date: {self.dates[-1]}')

    @property
    def price_type(self):
        return self._price_type

    def run(self):
        super().run()
        self.strategy.consume_withdrawals()
        self.stats = self.strategy.prices.calc_perf_stats()
        self._original_prices = self.strategy.prices


class BacktestOHLCCap(BacktestOHLC):

    def __init__(self, strategy: StrategyOHLCCap, data_dict, cap_column, name=None, initial_capital=1000000.0,
                 commissions=None, integer_positions=True, progress_bar=True, **kwargs):

        if not isinstance(strategy, StrategyOHLCCap):
            raise TypeError('strategy must be StrategyOHLC')

        super().__init__(strategy, data_dict, name, initial_capital, commissions, integer_positions, progress_bar)

        self.strategy.market_cap = pd.DataFrame({k: v[cap_column] for k, v in data_dict.items()})


class Online(BacktestOHLC):

    def run(self):
        """
        Runs the Backtest.
        """
        if self.has_run:
            return

        # set run flag to avoid running same test more than once
        self.has_run = True

        # setup strategy
        self.strategy.setup(self.data)

        # adjust strategy with initial capital
        self.strategy.adjust(self.initial_capital)

        # since there is a dummy row at time 0, start backtest at date 1.
        # we must still update for t0
        self.strategy.update(self.dates[0])

        # and for the backtest loop, start at date 1
        for dt in self.dates[1:]:

            # update strategy
            self.strategy.update(dt)

            if not self.strategy.bankrupt:
                self.strategy.run()
                # need update after to save weights, values and such
                self.strategy.update(dt)

    def update_with_data(self, dt, data):
        logger = TradingParams().get_logger()
        if self.dates[-1] >= dt:
            logger.debug(f'last date: {self.dates.loc[-1]}\nupdate date: {dt}')
            return
        logger.debug(f'update_with_data:\ndt: {dt}\ndata\n{data}')
        self.strategy.add_data(dt, data)

        prices = pd.Series({currency: prices[self.price_type] for currency, prices in data.items()})
        logger.debug(f'update_with_data: prices:\n{prices}')
        self.data.loc[dt] = prices
        self.dates = self.dates.append(pd.DatetimeIndex([dt]))

        logger.debug(f'update_with_data: data:\n{self.data.iloc[-2:]}')
        logger.debug(f'update_with_data: dates:\n{self.dates[-2:]}')


        def update_child(child):
            if isinstance(child, SecurityBase):
                logger.debug(f'update_with_data: update child:\n')
                condition = dt > child.data.index[-1]
                logger.debug(
                    f'update_with_data:\ndt: {dt}\nchild dt: {child.data.index[-1]}\ndt > strategy.dt: {condition}')
                child.add_data(dt, prices[child.name])
                condition = dt == child.data.index[-1]
                logger.debug(
                    f'update_with_data:\ndt: {dt}\nchild dt: {child.data.index[-1]}\ndt == strategy.dt: {condition}')
            elif isinstance(child, StrategyBase):
                child.update_universe(dt, prices)
                if child.children is not None:
                    for name, sub_child in child.children.items():
                        update_child(sub_child)

        condition = dt > self.strategy.data.index[-1]
        logger.debug(f'update_with_data:\ndt: {dt}\nstrategy dt: {self.strategy.data.index[-1]}\ndt > strategy.dt: {condition}')
        self.strategy.update_universe(dt, prices)
        condition = dt == self.strategy.data.index[-1]
        logger.debug(f'update_with_data:\ndt: {dt}\nstrategy dt: {self.strategy.data.index[-1]}\ndt == strategy.dt: {condition}')
        for name, child in self.strategy.children.items():
            update_child(child)

        self.strategy.update(dt)
        self.strategy.run()
        self.strategy.update(dt)


def make_stats(res):
    names = list(res.keys())
    values = list(res.values())

    stats = [('start', 'Start', 'dt'),
             ('end', 'End', 'dt'),
             ('rf', 'Risk-free rate', 'ps'),
             (None, None, None),
             ('total_return', 'Total Return', 'p'),
             ('daily_sharpe', 'Daily Sharpe', 'n'),
             ('daily_sortino', 'Daily Sortino', 'n'),
             ('cagr', 'CAGR', 'p'),
             ('max_drawdown', 'Max Drawdown', 'p'),
             ('calmar', 'Calmar Ratio', 'n'),
             (None, None, None),
             ('wtd', 'WTD', 'p'),
             ('three_week', '3w', 'p'),
             # ('six_week', '6w', 'p'),
             ('mtd', 'MTD', 'p'),
             ('three_month', '3m', 'p'),
             # ('six_month', '6m', 'p'),
             ('ytd', 'YTD', 'p'),
             ('one_year', '1Y', 'p'),
             ('three_year', '3Y (ann.)', 'p'),
             ('five_year', '5Y (ann.)', 'p'),
             ('ten_year', '10Y (ann.)', 'p'),
             ('incep', 'Since Incep. (ann.)', 'p'),
             (None, None, None),
             ('daily_sharpe', 'Daily Sharpe', 'n'),
             ('daily_sortino', 'Daily Sortino', 'n'),
             ('daily_mean', 'Daily Mean (ann.)', 'p'),
             ('daily_vol', 'Daily Vol (ann.)', 'p'),
             ('daily_skew', 'Daily Skew', 'n'),
             ('daily_kurt', 'Daily Kurt', 'n'),
             ('best_day', 'Best Day', 'p'),
             ('worst_day', 'Worst Day', 'p'),
             (None, None, None),
             ('weekly_sharpe', 'Weekly Sharpe', 'n'),
             ('weekly_sortino', 'Weekly Sortino', 'n'),
             ('weekly_mean', 'Weekly Mean (ann.)', 'p'),
             ('weekly_vol', 'Weekly Vol (ann.)', 'p'),
             ('weekly_skew', 'Weekly Skew', 'n'),
             ('weekly_kurt', 'Weekly Kurt', 'n'),
             ('best_week', 'Best Week', 'p'),
             ('worst_week', 'Worst Week', 'p'),
             (None, None, None),
             ('monthly_sharpe', 'Monthly Sharpe', 'n'),
             ('monthly_sortino', 'Monthly Sortino', 'n'),
             ('monthly_mean', 'Monthly Mean (ann.)', 'p'),
             ('monthly_vol', 'Monthly Vol (ann.)', 'p'),
             ('monthly_skew', 'Monthly Skew', 'n'),
             ('monthly_kurt', 'Monthly Kurt', 'n'),
             ('best_month', 'Best Month', 'p'),
             ('worst_month', 'Worst Month', 'p'),
             (None, None, None),
             ('yearly_sharpe', 'Yearly Sharpe', 'n'),
             ('yearly_sortino', 'Yearly Sortino', 'n'),
             ('yearly_mean', 'Yearly Mean', 'p'),
             ('yearly_vol', 'Yearly Vol', 'p'),
             ('yearly_skew', 'Yearly Skew', 'n'),
             ('yearly_kurt', 'Yearly Kurt', 'n'),
             ('best_year', 'Best Year', 'p'),
             ('worst_year', 'Worst Year', 'p'),
             (None, None, None),
             ('avg_drawdown', 'Avg. Drawdown', 'p'),
             ('avg_drawdown_days', 'Avg. Drawdown Days', 'n'),
             ('avg_up_month', 'Avg. Up Month', 'p'),
             ('avg_down_month', 'Avg. Down Month', 'p'),
             ('win_year_perc', 'Win Year %', 'p'),
             ('twelve_month_win_perc', 'Win 12m %', 'p')]

    data = pd.DataFrame(columns=names)
    empty = pd.Series(np.full(len(names), ''), index=names)
    for stat in stats:
        k, n, f = stat
        # blank row
        if k is None:
            data.loc[str(len(data))] = empty
            continue

        try:
            raw = pd.Series(map(lambda v: v.__getattribute__(k), values), index=names)
            if k == 'rf':
                raw = pd.Series({nm: raw[nm].mean() for nm in raw.index})
            if f is None:
                raw = empty
            elif f == 'p':
                raw = raw.map(fmtp)
            elif f == 'n':
                raw = raw.map(fmtn)
            elif f == 'dt':
                raw = raw.map(lambda val: val.strftime('%Y-%m-%d'))
            elif f == 'ps':
                pass
            else:
                raise NotImplementedError('unsupported format %s' % f)

            data.loc[n] = raw.values
        except Exception as e:
            print(e)

    return data