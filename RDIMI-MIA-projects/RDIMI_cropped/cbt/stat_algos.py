# -*- encode: utf-8 -*-

from bt.algos import Algo

from indicator import MA


class CapMAStat(Algo):

    def __init__(self, period, ma_type='simple'):
        super().__init__()

        self._period = period
        self._ma_type = ma_type

        self._ma_cap = None

    @property
    def period(self):
        return self._period

    @property
    def ma_type(self):
        return self._ma_type

    def __call__(self, target):

        if self._ma_cap is None:
            self._ma_cap = MA(target.market_cap, self.period, self.ma_type)

        target.temp['stat'] = self._ma_cap.loc[target.now]

        return True