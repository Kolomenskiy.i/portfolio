# -*- encoding: utf-8 -*-

from copy import deepcopy
import operator
from abc import abstractmethod
from types import MethodType

import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
import sklearn
from scipy.cluster.hierarchy import linkage, dendrogram

import bt
from bt.algos import Algo, Rebalance, RunPeriod
from pyti.directional_indicators import average_directional_index as ADX, positive_directional_index as PDI, negative_directional_index as NDI


# TODO does not work
class BaseAlgo(Algo):

    def __init__(self, name=None):
        super().__init__(name)

        self.__call__ = self._base_init_call

    @abstractmethod
    def _init_call(self, target):
        pass

    @abstractmethod
    def _regular_call(self, target):
        pass

    def _base_init_call(self, target):
        self._init_call(target)
        self.__call__ = self._regular_call


class WithdrawalAlgo(Algo):

    def __init__(self, limit=0, sf=0):
        super().__init__()

        assert 0 <= limit <= 1
        assert 0 <= sf <= 1

        self._limit = limit
        self._sf = sf

        self._start_capital = 0
        self._last_capital = 0
        self._withdrawals = pd.Series()
        self._success_fee = pd.Series()

        self._inited = False

    @property
    def withdrawals(self):
        return self._withdrawals

    @abstractmethod
    def __call__(self, target):
        pass


class OnPeriodEndWithdrawal(WithdrawalAlgo):

    def __init__(self, period=0, limit=0, sf=0):
        super().__init__(limit, sf)

        assert period in ['Y', 'Q', 'M']

        self._period = period
        self._next_time = None

    def __call__(self, target):
        if self._inited:
            if target.now >= self._next_time and self._limit > 0:
                target.update(target.now)
                self._next_time = target.now.to_period(self._period).to_timestamp(self._period) + pd.Timedelta('1D')
                if target.value > self._last_capital:
                    profit = target.value - self._last_capital
                    success_fee = self._sf * profit
                    withdrawal = self._limit * (profit - success_fee)
                    outflow = withdrawal + success_fee
                    if outflow > target.capital:
                        target.allocate(-outflow)
                        target.update(target.now)
                        # target.temp['cash'] = np.round(withdrawal / target.value, 2) + 0.01
                        # securities = target.securities
                        # weights = pd.Series({s.name: np.round(s.value / target.value, 2) for s in securities})
                        # if weights.sum() > 1.0:
                        #     idx = weights.idxmax()
                        #     weights.loc[idx] = weights.loc[idx] + (1.0 - weights.sum())
                        # target.temp['weights'] = weights.to_dict()
                        # Rebalance()(target)
                        # target.update(target.now)

                    self._withdrawals.loc[target.now] = withdrawal
                    target.perm['withdrawals'][target.now] = target.perm['withdrawals'].get(target.now, 0) + withdrawal
                    self._success_fee.loc[target.now] = success_fee
                    target.perm['success_fee'][target.now] = target.perm['success_fee'].get(target.now, 0) + success_fee
                    target.adjust(-outflow, update=True, flow=False)
                    self._last_capital = target.value
        else:
            target.perm['withdrawals'] = dict()
            target.perm['success_fee'] = dict()
            self._start_capital = target.values.iloc[0]
            self._last_capital = target.values.iloc[0]
            self._next_time = target.now.to_period(self._period).to_timestamp(self._period) + pd.Timedelta('1D')
            self._inited = True

        return True


class FalseAlgo(Algo):

    def __call__(self, target):
        return False


class PutAttrAlgo(Algo):

    def __init__(self, dict_name, param_name, attr_name):
        super().__init__()

        assert dict_name in ['temp', 'perm'], "Specify temp or perm"

        self.attr_name = attr_name
        self.dict_name = dict_name
        self.param_name = param_name

    def __call__(self, target):
        dict_obj = getattr(target, self.dict_name)
        dict_obj[self.param_name] = getattr(target, self.attr_name)


class CompareAttrsAlgo(Algo):

    def __init__(self, dict_name, param_name, attr_name, op_type='ne'):
        super().__init__()

        assert dict_name in ['temp', 'perm'], "Specify temp or perm"
        assert op_type in ['eq', 'ne', 'gt', 'qe', 'lt', 'le'], "Specify 'eq', 'ne', 'gt', 'qe', 'lt', 'le'"

        self.attr_name = attr_name
        self.dict_name = dict_name
        self.param_name = param_name
        self.operation = getattr(operator, op_type)

    def __call__(self, target):

        dict_obj = getattr(target, self.dict_name)
        value1 = dict_obj.get(self.param_name, None)
        value2 = getattr(target, self.attr_name)

        return self.operation(value1, value2)


class SymbolicRoundingAlgo(Algo):

    '''
        Base Algo for indicators based on one symbol.
        Rounding parameter is needed to round return value
    '''

    def __init__(self, symbol, params, rounding=1):
        super().__init__()

        self._symbol = symbol
        self._params = {} if params is None else params.copy()
        self._rounding = rounding
        self.__inited = False

    @property
    def symbol(self):
        return self._symbol

    @property
    def params(self):
        return self._params

    @property
    def rounding(self):
        return self._rounding

    @property
    def inited(self):
        return self.__inited

    def set_inited(self):
        self.__inited = True


class SignalAlgo(Algo):
    def __init__(self, generator, symbol):
        super().__init__()
        self._generator = deepcopy(generator)
        self._symbol = symbol
        self.__inited = False
        self._signals = {}

    def __call__(self, target):
        if not self.__inited:
            self._signals = self._generator(target.data_dict[self._symbol])
            self.__inited = True
        else:
            target.temp['signal'] = self._signals.get(target.now, 0)

        return True

    @property
    def signals(self):
        return self._signals

    @signals.setter
    def signals(self, signals):
        self._signals = signals.copy()


class MultiSignalAlgo(Algo):

    def __init__(self, **params):
        super().__init__()
        self._curr_generators = {k: deepcopy(v) for k, v in params['curr_generators'].items()}
        self.__inited = False
        self._last_signals = dict()

    def __call__(self, target):
        if not self.__inited:
            target.perm['trade_types'] = {}
            for currency, generator in self._curr_generators.items():
                target.perm['trade_types'][currency] = generator.params.get('trade_types', ['long'])
            self._last_date = np.max([data.index[-1] for data in target.data_dict.values()])
            self._signals = pd.DataFrame({symbol: generator(target.data_dict[symbol])
                                          for symbol, generator
                                          in self._curr_generators.items()}).fillna(int(0))

            for generator in self._curr_generators.values():
                self._charts = generator.charts
            if isinstance(self._signals, pd.DataFrame):
                for symbol in self._signals.columns:
                    data = self._signals[symbol]
                    data = data[data != 0]
                    signal = 0 if len(data) == 0 else data.iloc[-1]
                    self._last_signals[symbol] = signal
                self._signals = self._signals.T.to_dict()
            else:
                # TODO проверить, реально ли этот код вызывается
                self._signals = self._signals.to_dict()
            self.__inited = True
        elif self._last_date < target.now:
            self._last_date = target.now
            temp_signals = dict()
            for symbol, generator in self._curr_generators.items():
                data = target.data_dict[symbol].iloc[-generator.max_period - 5:]
                signal = generator.generate_signals(data).iloc[-1]
                if signal != 0 and signal != self._last_signals[symbol]:
                    temp_signals[symbol] = signal
                    self._last_signals[symbol] = signal
                else:
                    temp_signals[symbol] = 0
            self._signals[target.now] = temp_signals
            target.temp['signals'] = temp_signals
        else:
            target.temp['signals'] = self._signals.get(target.now, {})

        return True

    @property
    def signals(self):
        return self._signals

    @signals.setter
    def signals(self, signals):
        self._signals = signals.copy()

    @property
    def charts(self):
        return self._charts


class CheckSignal(Algo):

    def __init__(self):
        super().__init__()

    def __call__(self, target):

        return bool(target.temp.get('signals', 0))


class OpenSignal(Algo):

    def __init__(self):
        super().__init__()

    def __call__(self, target):

        signal = target.temp.get('signal', 0)
        if signal != 0 and 'weights' in target.temp.keys():
            target.temp['weights'] = {k: v * signal for k, v in target.temp['weights'].items()}

            return True
        else:
            return False


class CloseSignal(Algo):

    def __init__(self):
        super().__init__()

    def __call__(self, target):
        signals = target.temp.get('signals', 0)
        if signals != 0:
            target.temp['selected'] = {}
            target.temp['weights'] = {}

            return True
        else:
            return False


class CheckMarginCall(Algo):

    '''
        Algo to check if strategy is bankrupt because of margin call
    '''

    def __init__(self, leverage=1):
        super().__init__()

        self.leverage = leverage

    def __call__(self, target):
        if target.bankrupt:
            return True

        positioned = [v for v in target.children if target.children[v].position < 0]
        if len(positioned) == 0:
            return True

        highs = target.high_price.loc[target.now]
        prof = 0.0
        for name in positioned:
            prof += target.children[name].position * highs.loc[name] / self.leverage

        if prof + target.value <= 0:
            target.bankrupt = True
            target.temp.pop('weights', None)
        return True


class CheckFeeBankrupt(Algo):

    '''
        Algo to check if strategy will be bankrupt next to open position operation;
        insufficient money to pay fee
    '''

    def __init__(self, fee_func):
        super().__init__()

        self._fee_func = fee_func

    def __call__(self, target):

        if target.bankrupt:
            return True

        selected = target.temp.get('selected')
        weights = target.temp.get('weights')

        if selected is not None and weights is not None:

            strat = deepcopy(target)
            Rebalance()(strat)
            if np.any(strat.positions < 0):
                target.bankrupt = True
                target.temp.pop('weights', None)

            # weights = pd.Series(weights)
            # prices = target.universe[selected].loc[target.now].copy()
            #
            # fee = 0.0
            # for w, p in pd.concat((weights, prices[weights.index]), axis=1).values:
            #     fee += self._fee_func(target.value * np.abs(w) / p, p)
            #
            # target.bankrupt = np.round(fee / target.value, 2) >= .4 or target.value < 0
            # if target.bankrupt:
            #     target.temp.pop('weights', None)
        return True


class InPosition(Algo):

    '''
        Algo to check if strategy is in position
    '''

    def __call__(self, target):
        return target.perm.get('position', False)


class OutPosition(Algo):

    '''
        Algo to check if strategy is out of position
    '''

    def __call__(self, target):
        return not target.perm.get('position', False)


class CashClose(Algo):

    '''
        Algo for closing all positions because of cash parameter
    '''

    def __call__(self, target):

        if target.temp.get('cash', 0.0) == 1.0:
            target.temp['weights'] = {c: 0.0 for c in target.universe.columns}
            Rebalance()(target)

        return True


class ConditionalRebalance(Algo):

    '''
        Algo to close position because of weights, 'stopped' flag or key
    '''

    def __init__(self, key):
        super().__init__()

        self._key = key

    @property
    def key(self):
        return self._key

    def __call__(self, target):

        weights = target.temp.get('weights')
        stopped = target.perm.get('stopped')

        if weights is None:
            return True

        if stopped is not None:
            weights = {k: v for k, v in weights.items() if k not in stopped}

        yesterday = target.now - relativedelta(days=1)
        if yesterday not in target.positions.index:
            return True

        pos = target.positions.loc[yesterday]

        if target.temp[self.key]:

            if len(weights) > 0:
                target.temp['weights'] = weights.copy()
                Rebalance()(target)
        else:
            symbols = [s for s in pos.index.values if pos[s] == 0.0]
            weights = {k: v for k, v in weights.items() if k in symbols}

            base = target.value
            if 'cash' in target.temp:
                base = base * (1 - target.temp['cash'])

            for child, weight in weights.items():
                target.rebalance(weight, child=child, base=base)

        return True


class RunHourly(RunPeriod):
    def compare_dates(self, now, date_to_compare):
        now_copy = now.timestamp()
        if now_copy % 3600 == 0:
            return True
        return False


class RunDataTimeframe(RunPeriod):
    def compare_dates(self, now, date_to_compare):
        return True


class SelectAll(Algo):

    """
    Sets temp['selected'] with all securities (based on universe).

    Selects all the securities and saves them in temp['selected'].
    By default, SelectAll does not include securities that have no
    data (nan) on current date or those whose price is zero.

    Args:
        * include_no_data (bool): Include securities that do not have data?

    Sets:
        * selected

    """

    def __init__(self, include_no_data=False):
        super(SelectAll, self).__init__()
        self.include_no_data = include_no_data

    def __call__(self, target):
        if self.include_no_data:
            target.temp['selected'] = target.assets
            # target.temp['selected'] = target.universe.columns
        else:
            universe = target.universe.loc[target.now].dropna()
            target.temp['selected'] = list(universe[universe > 0].index)
        return True


class CheckForAction(Algo):

    def __call__(self, target):

        return np.abs(list(target.temp.get('signals', {}).values())).sum() != 0


class AddSavedSignals(Algo):
    def __init__(self, trade_types):
        super().__init__()
        self._trade_types = trade_types

    def __call__(self, target):
        temp_dict = {'long': -1, 'short': 1}
        dropping_signal = temp_dict[self._trade_types[0]] if len(self._trade_types) == 1 else 0
        saved_signals = target.perm.get('saved_signals', {})
        if len(saved_signals.keys()) != 0:
            for k, v in saved_signals.items():
                signal_value = target.temp['signals'].get(k, 0)
                if signal_value == dropping_signal:
                    target.temp['signals'][k] = 0
                else:
                    target.temp['signals'][k] = signal_value

            target.perm.pop('saved_signals', None)

        return True


class SelectSignalAlgo(Algo):

    def __init__(self):
        super().__init__()

    def __call__(self, target):
        target.temp['selected'] = [k for k, v in target.temp.get('signals', {}).items() if v != 0]

        return True


class AddSelectedPosition(Algo):

    def __call__(self, target):

        target.temp['selected'] = target.temp.get('selected', [])
        target.temp['selected'].extend([k for k, v in target.current_position.items() if v != 0])

        return True


class DropSelectedSignal(Algo):
    def __init__(self):
        super().__init__()

    def __call__(self, target):
        for currency, trade_types in target.perm['trade_types'].items():

            temp_dict = {'long': -1, 'short': 1}
            if len(trade_types) > 1:
                dropping_signal = 0
            else:
                if target.temp.get('signals', {})[currency] == 0:
                    dropping_signal = 0
                else:
                    dropping_signal = temp_dict[trade_types[0]]

            if dropping_signal != 0:
                selection = [k for k, v in target.temp.get('signals', {}).items() if v == dropping_signal]

                target.temp['weights'] = target.temp.get('weights', {})
                for currency_2 in selection:
                    target.temp['weights'][currency_2] = 0
                target.temp['selected'] = [v for v in target.temp.get('selected', []) if v not in selection]

        return True


class DistinctSelectedAlgo(Algo):

    def __call__(self, target):
        target.temp['selected'] = list(set(target.temp.get('selected', [])))

        return True


class HRPWeightCap(Algo):

    def __init__(self, **params):
        super().__init__()

        self._robust = params.get('robust', True)
        self._ddev = params.get('downside_deviation', False)
        self._dist_type = params.get('dist_type', 'ward')
        self._minw = params.get('min_weight', 0.0)
        self._maxw = params.get('max_weight', 1.0)
        self._rounding = params.get('rounding', 4)
        self._stat_length = params.get('stat_length', 26)

    def __call__(self, target):

        selected = target.temp.get('selected', [])

        if len(selected) == 0:
            return True
        elif len(selected) == 1:
            target.temp['weights'] = {selected[0]: 1}
        else:
            # print('hrp start')
            closes = target.close_price.loc[:target.now].iloc[-self._stat_length:]
            closes = closes[selected]

            var = np.log(closes / closes.shift()).fillna(0.0).var(0)
            currencies = var[var != 0].index
            if len(currencies) == 0:
                target.temp['weights'] = {}
                return True

            closes = closes[currencies]

            if closes.shape[1] == 1:
                target.temp['weights'] = target.temp.get('weights', {})
                target.temp['weights'][closes.columns[0]] = 1

            ret = np.log(closes / closes.shift()).fillna(0.0)
            # print('hrp cov')
            corr = self.__cov2cor(self.__cov_robust(ret) if self._robust else ret.cov())
            # print('hrp distance')
            dist = self.__distance(corr)
            # print('hrp linkage start')
            link = linkage(dist, self._dist_type)
            # print('hrp dendogram')
            quasiIdx = np.array(dendrogram(link)['leaves'])
            clusters = quasiIdx
            # print('hrp recursive bipart')
            weights = self.__getRecBipart(closes, self._ddev, clusters, self._minw, self._maxw)
            weights.index = closes.columns[weights.index]

            target.temp['weights'] = weights.round(2).to_dict()
        # print('hrp stop')
        return True

    def __cov2cor(self, X):
        D = np.zeros_like(X)
        d = np.sqrt(np.diag(X))
        np.fill_diagonal(D, d)
        DInv = np.linalg.inv(D)
        R = np.dot(np.dot(DInv, X), DInv)
        return pd.DataFrame(R, index=X.index, columns=X.columns)

    def __distance(self, corr):
        columns = corr.columns
        corr = np.sqrt((1 - corr) / 2.).values
        np.fill_diagonal(corr, 0.0)
        return pd.DataFrame(corr, index=columns, columns=columns)

    def __getQuasiDiag(self, link):
        # Sort clustered items by distance
        link = link.astype(int)
        sortIx = pd.Series([link[-1, 0], link[-1, 1]])
        numItems = link[-1, 3]  # number of original items
        while sortIx.max() >= numItems:
            sortIx.index = range(0, sortIx.shape[0] * 2, 2)  # make space
            df0 = sortIx[sortIx >= numItems]  # find clusters
            i = df0.index
            j = df0.values - numItems
            sortIx[i] = link[j, 0]  # item 1
            df0 = pd.Series(link[j, 1], index=i + 1)
            sortIx = sortIx.append(df0)  # item 2
            sortIx = sortIx.sort_index()  # re-sort
            sortIx.index = range(sortIx.shape[0])  # re-index

        return sortIx.tolist()

    def __getRecBipart(self, closes, ddev, sortIx, minw, maxw):
        # Compute HRP alloc
        w = pd.Series(1, index=sortIx)
        cItems = [sortIx]  # initialize all items in one cluster
        while len(cItems) > 0:
            cItems = [i[j:k] for i in cItems for j, k in ((0, len(i) // 2),
                                                          (len(i) // 2, len(i))) if len(i) > 1]  # bi-section
            for i in range(0, len(cItems), 2):  # parse in pairs
                cItems0 = cItems[i]  # cluster 1
                cItems1 = cItems[i + 1]  # cluster 2
                cVar0 = self.__getClusterVar(closes, ddev, cItems0)
                cVar1 = self.__getClusterVar(closes, ddev, cItems1)
                alpha = 1 - cVar0 / (cVar0 + cVar1)
                lb = np.max((1 - len(cItems1) * maxw / np.sum(w[cItems1]), len(cItems0) * minw / np.sum(w[cItems0])))
                ub = np.min((1 - len(cItems1) * minw / np.sum(w[cItems1]), len(cItems0) * maxw / np.sum(w[cItems0])))
                alpha = np.max((lb, np.min((alpha, ub))))
                w[cItems0] *= alpha  # weight 1
                w[cItems1] *= 1 - alpha  # weight 2
        return w

    def __getClusterVar(self, closes, ddev, cItems):
        # Compute variance per cluster

        closes = closes[closes.columns[cItems]]
        ret = np.log(closes / closes.shift()).fillna(0.0)
        quant = 1000000 * self.__getIVP(ret.var(0).values) / closes.iloc[-1].values
        price = np.sum(closes * quant, 1)
        ret = np.log(price / price.shift()).fillna(0.0).values

        if ddev:
            ret[ret > 0] = 0.0
            cVar = np.power(ret, 2).sum() / len(ret)
        else:
            cVar = np.var(ret)

        return cVar

    def __getIVP(self, variance, **kargs):
        # Compute the inverse-variance portfolio
        ivp = 1. / variance
        ivp /= ivp.sum()
        return ivp

    def __cov_robust(self, X):
        oas = sklearn.covariance.OAS()
        oas.fit(X)
        return pd.DataFrame(oas.covariance_, index=X.columns, columns=X.columns)


class ADXSecurityWeight(Algo):

    def __init__(self, **params):
        super(ADXSecurityWeight, self).__init__()
        self._params = params
        self._rounding = params.get('rounding', 2)
        self._inited = False
        self._adx_data = {}

    @property
    def params(self):
        return self._params

    @property
    def adx_data(self):
        return self._adx_data

    def __call__(self, target):

        if not self._inited:
            closes = target.close_price
            highs = target.high_price
            lows = target.low_price

            for currency, params in self.params.items():
                weights = 0
                adx_dirs = []
                for prm in params:
                    prm['pdr'] = prm['pdr'] if 'pdr' in prm.keys() else 0.0
                    prm['ndr'] = prm['ndr'] if 'ndr' in prm.keys() else 0.0

                    adx = pd.Series(ADX(closes[currency], highs[currency], lows[currency], prm['period'])).fillna(
                        0.0)
                    pdi = pd.Series(PDI(closes[currency], highs[currency], lows[currency], prm['period'])).fillna(
                        0.0)
                    ndi = pd.Series(NDI(closes[currency], highs[currency], lows[currency], prm['period'])).fillna(
                        0.0)
                    dir = (pdi - ndi).replace([np.inf, -np.inf, np.nan], 0.0)

                    adx_dir = np.round(
                        (np.sign(dir) * np.tanh(prm['coef'] * (adx / 100.0))).replace([np.inf, -np.inf],
                                                                                      np.nan).fillna(
                            0.0).values, 1)
                    adx_dir[adx_dir > 1] = 1.0
                    adx_dir[(adx_dir > 0) & (adx_dir <= prm['pdr'])] = 0.0
                    adx_dir[adx_dir < -1] = -1.0
                    adx_dir[(adx_dir < 0) & (adx_dir >= prm['ndr'])] = 0.0

                    weights += prm['weight']
                    adx_dirs.append(adx_dir)

                self._adx_data[currency] = np.round(np.array(adx_dirs).sum(0) / weights, self._rounding)

            self._adx_data = pd.DataFrame(self._adx_data).T.to_dict()
            self._inited = True
        else:
            saved_signals = {}
            adx_weights = self.adx_data.get(target.now, {})
            weights = target.temp['weights'].copy()
            for currency in weights.keys():
                w = adx_weights.get(currency, 0)
                if w != 0:
                    target.temp['weights'][currency] = np.round(w * target.temp['weights'][currency], self._rounding)
                else:
                    target.temp['weights'].pop(currency, None)
                    if currency in target.temp['signals'].keys():
                        saved_signals[currency] = target.temp['signals'][currency]

            if len(saved_signals.keys()) != 0:
                target.perm['saved_signals'] = saved_signals

        # print('adx stop')
        return True


class SkipPeriod(Algo):

    def __init__(self, periods):
        super().__init__()
        self._periods = periods

    def __call__(self, target):

        if self._periods > 0:
            self._periods -= 1
            return False

        return True


class WeighEqually(Algo):

    def __init__(self):
        super(WeighEqually, self).__init__()

    def __call__(self, target):
        for currency, trade_types in target.perm['trade_types'].items():

            selected = target.temp['selected']
            n = len(selected)

            if currency not in selected:
                continue

            if n == 0:
                target.temp['weights'] = {}
            else:
                w = 1.0 / n
                # w = 0.2

                if 'short' in trade_types:
                    target.temp['weights'] = {x: target.temp['signals'][x] * w for x in selected}
                else:
                    target.temp['weights'] = {x: w for x in selected}

        return True

class WeighEqually_No_Buy_on_Sell(Algo):

    def __init__(self):
        super(WeighEqually_No_Buy_on_Sell, self).__init__()

    def __call__(self, target):
        for currency, trade_types in target.perm['trade_types'].items():
            sold_currencies = []
            if -1 in target.temp['signals'].values():
                target.temp['weights'] = target.perm['weights']
                for x in list(target.temp['signals'].keys()):
                    if target.temp['signals'][x]==-1:
                        sold_currencies.append(x)
            for i in sold_currencies:
                target.temp['weights'][i] = 0


            if 1 in target.temp['signals'].values():
                selected = target.temp['selected']

                n = len(selected)
                # n = len(selected) + list(target.temp['signals'].values()).count(-1)
                # n = len(selected) + list(target.temp['signals'].values()).count(-1) + list(
                #     target.temp['signals'].values()).count(1)
                # if list(target.temp['signals'].values()).count(-1) > 0:
                    # target.temp[]

                if currency not in selected:
                    continue

                if n == 0:
                    target.temp['weights'] = {}

                else:
                    w = 1.0 / n

                    if 'short' in target.perm['trade_types'].items():
                        target.temp['weights'] = {x: target.temp['signals'][x] * w for x in selected}
                    else:
                        target.temp['weights'] = {x: w for x in selected}
        target.perm['weights'] = target.temp['weights']

        return True


class WeighEqually_Sell_All_if_sell(Algo):

    def __init__(self):
        super(WeighEqually_Sell_All_if_sell, self).__init__()

    def __call__(self, target):
        for currency, trade_types in target.perm['trade_types'].items():
            selected = target.temp['selected']
            if -1 in target.temp['signals'].values():
                target.temp['weights'] = {x: 0 for x in selected}

            if 1 in target.temp['signals'].values():
                n = len(selected)
                if currency not in selected:
                    continue

                if n == 0:
                    target.temp['weights'] = {}

                else:
                    w = 1.0 / n

                    if 'short' in target.perm['trade_types'].items():
                        target.temp['weights'] = {x: target.temp['signals'][x] * w for x in selected}
                    else:
                        target.temp['weights'] = {x: w for x in selected}
        target.perm['weights'] = target.temp['weights']

        return True

class WeighInvVol_Sell_All_if_sell(Algo):

    def __init__(self,lookback = pd.DateOffset(months=3),lag = pd.DateOffset(days=0)):
        super(WeighInvVol_Sell_All_if_sell, self).__init__()
        self.lookback = lookback
        self.lag = lag

    def __call__(self, target):
        selected = target.temp['selected']
        if -1 in target.temp['signals'].values():
            target.temp['weights'] = {x: 0 for x in selected}

        if 1 in target.temp['signals'].values():
            selected = target.temp['selected']

            if len(selected) == 0:
                target.temp['weights'] = {}
                return True

            if len(selected) == 1:
                target.temp['weights'] = {selected[0]: 1.}
                return True

            t0 = target.now - self.lag
            prc = target.universe[selected].loc[t0 - self.lookback:t0]
            tw = bt.ffn.calc_inv_vol_weights(
                prc.to_returns().dropna())
            target.temp['weights'] = tw.dropna()
        return True


class WeighEqually_Sell_on_BTC(Algo):

    def __init__(self):
        super(WeighEqually_Sell_on_BTC, self).__init__()

    def __call__(self, target):
        for currency, trade_types in target.perm['trade_types'].items():
            selected = target.temp['selected']
            if target.temp['signals']['BTC'] == -1:
                target.temp['weights'] = {x: 0 for x in selected}
            else:
                sold_currencies = []
                if -1 in target.temp['signals'].values():
                    target.temp['weights'] = target.perm['weights']
                    for x in list(target.temp['signals'].keys()):
                        if target.temp['signals'][x] == -1:
                            sold_currencies.append(x)
                for i in sold_currencies:
                    target.temp['weights'][i] = 0

                if 1 in target.temp['signals'].values():
                    selected = target.temp['selected']

                    n = len(selected)
                    if currency not in selected:
                        continue

                    if n == 0:
                        target.temp['weights'] = {}

                    else:
                        w = 1.0 / n

                        if 'short' in target.perm['trade_types'].items():
                            target.temp['weights'] = {x: target.temp['signals'][x] * w for x in selected}
                        else:
                            target.temp['weights'] = {x: w for x in selected}
            target.perm['weights'] = target.temp['weights']
        return True
