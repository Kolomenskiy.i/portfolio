# -*- encoding: utf-8 -*-

import numpy as np
import pandas as pd

from bt.core import Algo

from cbt.base_algos import SymbolicRoundingAlgo, SignalAlgo
from indicator import ADX, MA, indicators as ind
from scipy.signal import argrelextrema

pd.options.display.max_rows = 40000


class FlowADX(SymbolicRoundingAlgo):

    def __init__(self, symbol, params, rounding=1):
        super().__init__(symbol, params, rounding)

        self._weight = None

    @property
    def weight(self):
        return self._weight

    def __call__(self, target):

        if self._weight is None:
            weights = 0
            adx_dirs = []
            for prm in self.params:
                prm['pdr'] = prm['pdr'] if 'pdr' in prm.keys() else np.inf
                prm['ndr'] = prm['ndr'] if 'ndr' in prm.keys() else -np.inf

                quotes = ADX(target.data_dict[self.symbol].copy(), prm['period'])
                quotes[['MDI', 'PDI']] = quotes[['MDI', 'PDI']].replace([np.inf, -np.inf, np.nan, 0.0], 0.01)
                quotes['DIR'] = (quotes.PDI - quotes.MDI).replace([np.inf, -np.inf], np.nan).fillna(0.0)
                quotes['PC'] = quotes.Close.shift().values
                quotes.fillna(0.0, inplace=True)

                adx_dir = np.round(
                    (np.sign(quotes.DIR) * np.tanh(prm['coef'] * (quotes.ADX / 100.0))).replace([np.inf, -np.inf],
                                                                                                np.nan).fillna(
                        0.0).values, 1)
                adx_dir[(adx_dir > 0) & (adx_dir <= prm['pdr'])] = 0.0
                adx_dir[adx_dir > 1] = 1.0
                adx_dir[(adx_dir < 0) & (adx_dir >= prm['ndr'])] = 0.0
                adx_dir[adx_dir < -1] = -1.0

                weights += prm['weight']
                adx_dirs.append(adx_dir)

            self._weight = pd.Series(np.round(np.array(adx_dirs).sum(0) / weights, self.rounding),
                                     index=target.data_dict[self.symbol].index).fillna(0.0)

        target.temp['cash'] = np.round(1 - self._weight.loc[target.now], self.rounding)

        return True


class FlowSingleADX(FlowADX):

    def __init__(self, symbol, period, coef, pdr=np.inf, ndr=-np.inf, rounding=1):
        super().__init__(symbol, [{
            'period': period,
            'coef': coef,
            'pdr': pdr,
            'ndr': ndr,
            'weight': 1.0
        }], rounding)


class FlowADXChange(Algo):

    def __init__(self):
        super().__init__()

        self._change = None

    @property
    def change(self):
        return self._change

    def __call__(self, target):

        if self._change is None:
            adx_algo = None
            for child in target.children:
                if isinstance(target.children[child], FlowADX):
                    adx_algo = target.children[child]
                    break
            if adx_algo is not None and adx_algo.weight is not None:
                self._change = (adx_algo.weight - adx_algo.weight.shift()).fillna(0.0)
                self._change.iloc[0] = self._change.iloc[1]

        if self._change is not None:
            target.temp['wchange'] = self._change.loc[target.now]
        else:
            target.temp['wchange'] = 0.0

        return True


class BBSignalAlgo(SymbolicRoundingAlgo, SignalAlgo):
    '''
        Bollinger Bands Signal Algo.
        up_period: float
            Upper period for BB
        low_period: float
            Lower period for BB
        green_zone: float
            Green zone in % for signal
    '''

    def __init__(self, symbol, params=None, rounding=1, type='long'):
        SymbolicRoundingAlgo.__init__(self, symbol, params, rounding)
        SignalAlgo.__init__(self, type)

    def __call__(self, target):

        if not self.inited:

            quotes = target.data_dict[self.symbol].copy()
            # bb_tl = ind.up_bb(quotes.Close.values, self.params.get('up_period', 20))
            # bb_bl = ind.low_bb(quotes.Close.values, self.params.get('low_period', 20))
            # green_zone = self._params.get('green_zone', 0)
            if len(self.params) == 0:
                up_bb, low_bb, green_zone = ind.up_bb(quotes.Close.values, 20), ind.low_bb(quotes.Close.values, 20), 0
            else:
                up_bb, low_bb, green_zone = ind.up_bb(quotes.Close.values, self.params[0]), ind.low_bb(
                    quotes.Close.values, self.params[1]), self.params[2]

            self.signals = pd.Series(0, index=quotes.index.values)

            buy_list = np.where(quotes.Low.values * (1 + green_zone) > up_bb)[0] + 1
            buy_list = buy_list[buy_list < len(quotes)]
            self.signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))

            sell_list = np.where(quotes.High.values * (1 - green_zone) < low_bb)[0] + 1
            sell_list = sell_list[sell_list < len(quotes)]
            self.signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

            self.match_type()

            self.set_inited()
        else:
            target.temp['signal'] = self.signals.get(target.now, 0)

        return True

    def param_list(self):
        return [[15, 25], [15, 25], [0.0001, 0.001]]


class MASignalAlgo(SymbolicRoundingAlgo, SignalAlgo):
    '''
        Moving Average Signal Algo.
        up_period: float
            Upper period for BB
        low_period: float
            Lower period for BB
        green_zone: float
            Green zone in % for signal
    '''

    def __init__(self, symbol, params=None, rounding=1, type='long'):
        SymbolicRoundingAlgo.__init__(self, symbol, params, rounding)
        SignalAlgo.__init__(self, type)

    def __call__(self, target):

        if not self.inited:

            quotes = target.data_dict[self.symbol].copy()
            ma = MA(quotes[self.params.get('price', 'Close')],
                    period=self.params['period'],
                    ma_type=self.params['type'])
            green_zone = self._params.get('green_zone', 0)
            self.signals = pd.Series(0, index=quotes.index.values)

            buy_list = np.where(ma > quotes.Close.values * (1 + green_zone))[0] + 1
            buy_list = buy_list[buy_list < len(quotes)]
            self.signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))

            sell_list = np.where(ma < quotes.Close.values * (1 - green_zone))[0] + 1
            sell_list = sell_list[sell_list < len(quotes)]
            self.signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))
            self.match_type()
            self.set_inited()
        else:
            target.temp['signal'] = self._signals.loc[target.now]

        return True

    def param_list(self):
        return [[1, 25], [0, 25]]


class TrianglesSignalAlgo(SymbolicRoundingAlgo, SignalAlgo):

    def __init__(self, symbol, params=None, rounding=1, type='long'):
        SymbolicRoundingAlgo.__init__(self, symbol, params, rounding)
        SignalAlgo.__init__(self, type)

    def __call__(self, target):

        if not self.inited:
            quotes = target.data_dict[self.symbol].copy()

            self.signals = pd.Series(0, index=quotes.index.values)

            if len(self.params) == 0:
                ema_period, area, stock_min_order, stock_max_order = 10, 500, 15, 15
            else:
                ema_period, area, stock_min_order, stock_max_order = self.params[0], int(self.params[1]), self.params[2], self.params[3]

            ema_list = np.array(ind.ema(list(quotes.Close.values), ema_period))

            print(ema_period, area, stock_min_order, stock_max_order)

            p = 0
            i = 0
            sell_list = []
            buy_list = []
            sell = True
            buy = False
            while i < len(ema_list):
                if i > area:
                    p = i - area
                max_order = stock_max_order
                min_order = stock_min_order
                max_list = list(argrelextrema(ema_list[p:i], np.greater, order=int(max_order))[0])
                min_list = list(argrelextrema(ema_list[p:i], np.less, order=int(min_order))[0])
                min_list = list(np.array(min_list) + p)
                max_list = list(np.array(max_list) + p)
                id_list = max_list + min_list
                id_list.sort()
                check = False
                if len(max_list) > 2 and len(min_list) > 2:

                    if max_list[-1] == id_list[-1] and min_list[-1] == id_list[-2] and max_list[-2] == id_list[-3] and \
                            min_list[-2] == id_list[-4]:
                        check = True
                    if min_list[-1] == id_list[-1] and max_list[-1] == id_list[-2] and min_list[-2] == id_list[-3] and \
                            max_list[-2] == id_list[-4]:
                        check = True
                    j = -2
                    m = -1
                    k = -2
                    n = -1
                    if check and ema_list[max_list[j]] > ema_list[max_list[m]] and ema_list[min_list[n]] > ema_list[min_list[k]]:
                        if ema_list[i] > (ema_list[max_list[m]]):
                            buy_list.append(i)
                        elif ema_list[i] < (ema_list[min_list[n]]):
                            sell_list.append(i)

                i += 1
            self.signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
            self.signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

            self.match_type()

            self.set_inited()
        else:
            target.temp['signal'] = self._signals.loc[target.now]
        return True

    def param_list(self):
        return [[1, 150], [50, 1000], [3, 50], [3, 50]]
