# -*- encoding: utf-8 -*-

import numpy as np
import pandas as pd

from bt.core import Algo, AlgoStack

from indicator import MA


class RemoveStopped(Algo):

    def __call__(self, target):
        target.perm.pop('stopped', None)


def _sltp_std_ma(data_dict, price, period, ma_type, mult):
    sltp = dict()
    for symbol, df in data_dict.items():
        df['Return'] = np.log(df[price] / df.Open).fillna(0.0)

        mean = MA(df.Return, period, ma_type)
        std = MA(df.Return, period, ma_type, 'std')
        move = mean + mult * std
        sltp[symbol] = df.Close * (1 + move.fillna(method='bfill'))

    return pd.DataFrame(sltp).shift().fillna(method='bfill')


class SLTPStdMa(Algo):

    def __init__(self, direction, sltp_type, period=26, price='Close', ma_type='ewma'):
        super().__init__('_'.join([direction, sltp_type]))

        self._direction = direction
        self._sltp_type = sltp_type
        self._period = period
        self._price = price
        self._ma_type = ma_type

        self._mult = (1 if direction == 'long' else -1) * (-1 if sltp_type == 'sl' else 1)

        self._prices = None

    @property
    def direction(self):
        return self._direction

    @property
    def sltp_type(self):
        return self._sltp_type

    @property
    def period(self):
        return self._period

    @property
    def price(self):
        return self._price

    @property
    def ma_type(self):
        return self._ma_type

    def __call__(self, target):

        if self._prices is None:
            self._prices = _sltp_std_ma(target.data_dict, self.price, self.period, self.ma_type, self._mult)

        target.perm[self.name] = self._prices.loc[target.now]

        return True


class CloseSLTP(Algo):

    def __init__(self, direction, sltp_type, lower):
        super().__init__('_'.join([direction, sltp_type]))

        self._lower = lower

    def __call__(self, target):

        if self.name not in target.perm:
            return True

        positioned = [v for v in target.children if target.children[v].position != 0]
        if len(positioned) == 0:
            return True

        tprices = target.perm[self.name][positioned]

        if self._lower:
            idx = target.low_price[tprices.index].loc[target.now] <= tprices
        else:
            idx = target.high_price[tprices.index].loc[target.now] >= tprices

        if ~np.any(idx):
            return True

        names = tprices.index[np.where(idx)[0]]
        target.temp['weights'] = {k:v for k, v in target.temp['weights'].items() if k not in names}

        for name in names:
            child = target.children[name]
            outlay = -child.position * tprices.loc[name]
            fee = child.commission(child.position, tprices.loc[name])

            child.allocate_predef(-child.position, outlay, fee)

            target.perm['stopped'] = names

        return True