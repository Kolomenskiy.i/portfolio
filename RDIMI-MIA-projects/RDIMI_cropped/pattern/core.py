# -*- encoding: utf-8 -*-

import numpy as np
import pandas as pd
from scipy.signal import savgol_filter, argrelmax, argrelmin, argrelextrema, medfilt, find_peaks_cwt, find_peaks, daub
from scipy.signal import detrend, peak_prominences
from scipy.signal import filtfilt, butter, cheby1, cheby2

from matplotlib import pyplot as plt

from pywt import dwt, idwt

from data.utils import get_data
from pattern import peakdetect as pkdtk

n = 0

def plot_prices(prices):
    plt.plot(prices, 'black')
    plt.show(dpi=1600)


def dwt_smooth(data, level, db='db20', mode='constant'):
    data = np.array(data)
    size = len(data)
    for n in range(level):
        data = dwt(data, db, mode)[0]
        # drop = int(np.floor(np.abs(size - len(data) * np.power(2, n + 1)) / np.power(2, n + 2)))
        # data = data[drop:-drop]
    for _ in range(level):
        data = idwt(data, None, db, mode)
    return data


def savgol_smooth(data, level, window, polyorder):
    data = np.array(data)
    for n in range(level):
        data = savgol_filter(data, window, polyorder)
    return data


def split_for_ext_trends(prices: pd.Series, drop_ratio=0.125, min_trend=30*24, verbose=False):

    rets = np.log(prices / prices.shift()).fillna(0.0)
    crets = rets.cumsum().values

    b, a = cheby1(3, 2, 0.0007)
    smoothed = filtfilt(b, a, rets.values)

    plt.plot(crets)
    plt.plot(np.cumsum(smoothed))
    plt.show()

    peaks = find_peaks(smoothed, distance=10)[0]
    hollows = find_peaks(-smoothed, distance=10)[0]
    exts = np.unique(np.hstack((peaks, hollows)))
    exts.sort()

    highest_peak = np.argmax(crets[peaks])
    peak_zone = peaks[max(0, highest_peak - 1)], peaks[min(len(peaks) - 1, highest_peak + 1)]
    highest_peak = range(peak_zone[0], peak_zone[1])[np.argmax(crets[peak_zone[0]:peak_zone[1]])]
    if np.max(crets) > crets[highest_peak]:
        highest_peak = np.argmax(crets)
    deepest_hollow = np.argmin(crets[hollows])
    hollow_zone = hollows[max(0, deepest_hollow - 1)], hollows[min(len(hollows) - 1, deepest_hollow + 1)]
    deepest_hollow = range(hollow_zone[0], hollow_zone[1])[np.argmin(crets[hollow_zone[0]:hollow_zone[1]])]
    if np.min(crets) < crets[deepest_hollow]:
        deepest_hollow = np.argmin(crets)
    hpdh = np.array([highest_peak, deepest_hollow])
    hpdh.sort()

    drop_size = max(int(len(prices) * drop_ratio), min_trend)

    trend_candidates = [
        prices.iloc[0:hpdh[0]],
        prices.iloc[hpdh[0]:hpdh[1]],
        prices.iloc[hpdh[1]:]
    ]

    if len(trend_candidates[0]) < drop_size:
        trend_candidates[1] = pd.concat((trend_candidates[0], trend_candidates[1]))
        del trend_candidates[0]

    if len(trend_candidates[-1]) < drop_size:
        trend_candidates[1] = pd.concat((trend_candidates[-2], trend_candidates[-1]))
        del trend_candidates[-1]

    if len(trend_candidates) == 1:
        return trend_candidates

    if verbose:
        colors = ['r', 'b', 'g']
        for tc in trend_candidates:
            plt.plot(tc, colors.pop(0))
        plt.show()

    trends = []
    for tc in trend_candidates:
        trends.extend(split_for_ext_trends(tc, drop_ratio, min_trend, verbose))

    return trends


def split_for_smooth_trends(prices: pd.Series, verbose=False):

    rets = np.log(prices / prices.shift()).fillna(0.0)
    crets = rets.cumsum().values

    # rets = prices.diff().fillna(0.0)
    # crets = rets.cumsum().values

    # if verbose:
    #     plot_prices(crets)

    for level in range(2, 51):
        # smoothed = dwt_smooth(rets, level)
        smoothed = savgol_smooth(rets, level, 21, 5)
        # cumrets = smoothed
        cumrets = smoothed.cumsum()
        # cumrets = cumrets[int(np.round(np.abs(len(rets) - len(cumrets)) / 2)):]
        # cumrets = cumrets[:len(rets)]
        # peaks = find_peaks_cwt(smoothed, np.arange(1, 10))
        # hollows = find_peaks_cwt(-smoothed, np.arange(1, 10))
        # cumrets = smoothed.cumsum()
        peaks = find_peaks(cumrets, distance=24)[0]
        hollows = find_peaks(-cumrets, distance=24)[0]
        exts = np.unique(np.hstack((peaks, hollows)))
        exts.sort()
        if verbose:
            plt.title('level ' + str(level))
            plt.plot(range(len(crets)), crets, 'red')
            plt.plot(range(len(cumrets)), cumrets, 'green')
            # plt.plot(range(len(cumrets)), detrend(cumrets), 'orange')
            if len(exts) > 0:
                plt.plot(peaks, cumrets[peaks], 'b^')
                plt.plot(hollows, cumrets[hollows], 'kv')
            plt.show()
        if len(exts) <= 2:
            break
    level -= 1
    smoothed = dwt_smooth(rets, level)
    cumrets = smoothed.cumsum()
    cumrets = cumrets[int(np.round(np.abs(len(rets) - len(cumrets)) / 2)):]
    cumrets = cumrets[:len(rets)]
    # cumrets = smoothed.cumsum()
    if verbose:
        plt.title('level ' + str(level))
        plt.plot(crets, 'red')
        plt.plot(cumrets, 'blue')
        plt.show()
    peaks = find_peaks(cumrets)[0]
    hollows = find_peaks(-cumrets)[0]
    exts = np.unique(np.hstack((0, peaks, hollows, len(prices))))
    exts.sort()

    exts = exts[exts <= len(prices)]

    split_points = []
    for i in range(1, len(exts) - 1):
        f, l = exts[i-1], exts[i+1]
        if cumrets[exts[i]] > cumrets[f]:
            split_points.append(np.argmax(prices.iloc[f:l]))
        elif cumrets[exts[i]] < cumrets[f]:
            split_points.append(np.argmin(prices.iloc[f:l]))

    plt.plot(prices)
    plt.show()
    splits = np.hstack((prices.index[0], split_points, prices.index[-1]))
    trends = []
    for i in range(1, len(splits)):
        tmp_trend = prices.loc[splits[i-1]:splits[i]]
        if len(tmp_trend) > 0:
            trends.append(tmp_trend)

    new_trends = []
    for trend in trends:
        rets = np.log(trend / trend.shift()).fillna(0.0)
        smoothed = dwt_smooth(rets, 2).cumsum()
        plt.title(str(trend.index[0]) + ' - ' + str(trend.index[-1]))
        plt.plot(rets.cumsum().values)
        plt.plot(smoothed)
        plt.show(dpi=600)
        arg_func = np.argmax if trend.iloc[-1] > trend.iloc[0] else np.argmin
        if arg_func(smoothed) < len(trend) and not 0.95 < trend.loc[arg_func(trend)] / trend.iloc[-1] < 1.05:
            inner_trends = split_for_trends(trend, verbose)
            new_trends.extend(inner_trends)
        else:
            new_trends.append(trend)

    if verbose:
        for trend in new_trends:
            plt.plot(trend)
            plt.show(dpi=600)

    return new_trends


def find_pattern_candidates(prices: pd.Series, verbose=False):

    rets = np.log(prices / prices.shift()).fillna(0.0)
    crets = rets.cumsum().values

    # rets = prices.diff().fillna(0.0)
    # crets = rets.cumsum().values

    if verbose:
        plot_prices(crets)

    for level in range(2, 51):
        smoothed = dwt_smooth(rets, level)
        cumrets = smoothed.cumsum()
        cumrets = cumrets[int(np.round(np.abs(len(rets) - len(cumrets)) / 2)):]
        cumrets = cumrets[:len(rets)]
        # peaks = find_peaks_cwt(smoothed, np.arange(1, 10))
        # hollows = find_peaks_cwt(-smoothed, np.arange(1, 10))
        # cumrets = smoothed.cumsum()
        peaks = find_peaks(cumrets)[0]
        hollows = find_peaks(-cumrets)[0]
        exts = np.unique(np.hstack((peaks, hollows)))
        exts.sort()
        if verbose:
            plt.title('level ' + str(level))
            plt.plot(range(len(crets)), crets, 'red')
            plt.plot(range(len(cumrets)), cumrets, 'green')
            if len(exts) > 0:
                plt.plot(exts, cumrets[exts], 'bx')
            plt.show()
        if len(exts) <= 2:
            break

if __name__ == '__main__':
    plt.rcParams["figure.figsize"] = [19.2, 10.8]

    prices = get_data('../data', 'LTC', 'USD', '1h', 'Bitstamp',
                      # start_date='2016-05-01', last_date='2016-12-31',
                      update_data=False).Close
    trends = split_for_ext_trends(prices, .125, 15*24, False)
    for t in trends:
        plt.plot(t, c=np.random.rand(3,))
    # plt.plot(pd.Series(savgol_smooth(prices.values, 3, 47, 11), index=prices.index), 'red')
    plt.show()
    # plt.plot(prices)
    # plt.savefig('./figs/eth.png', dpi=600)
    # plt.close()
    # for i in range(len(trends)):
    #     plt.plot(trends[i])
    #     plt.title(str(trends[i].index[0]) + ' - ' + str(trends[i].index[-1]))
    #     plt.savefig('./figs/' + str(i) + '.png', dpi=600)
    #     plt.close()
    # find_pattern_candidates(trends[0], True)
    # print(idxs)