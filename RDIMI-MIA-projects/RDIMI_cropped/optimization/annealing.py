# -*- encoding: utf-8 -*-

from collections import Iterable
from abc import abstractmethod
from copy import deepcopy

import numpy as np

import bt

from optimization.base import Optimizer, OptParam, IntOptParam, RoundingOptParam


class AnnealSimulation(Optimizer):

    def __init__(self, t=1, t_min=0.09, maxiter=1e10, drop_t=10, regen_n=50, auto_params=True, **kwargs):
        super().__init__(**kwargs)

        self._T = t
        self._start_T = t
        self._T_min = t_min
        self._maxiter = maxiter
        self._iteration = 0
        self._drop_T = drop_t
        self._regen_n = regen_n
        self._auto_params = auto_params

    @property
    def T(self):
        return self._T

    @property
    def T_min(self):
        return self._T_min

    @property
    def maxiter(self):
        return self._maxiter

    @maxiter.setter
    def maxiter(self, maxiter):
        self._maxiter = maxiter

    @property
    def iteration(self):
        return self._iteration

    def _init_param_value(self, param):
        if isinstance(param, IntOptParam):
            param.value = np.random.randint(param.minv, param.maxv)
        elif isinstance(param, RoundingOptParam):
            param.value = np.random.uniform(param.minv, param.maxv)

    @abstractmethod
    def _compute_T(self):
        pass

    def chance_of_change(self, new_value, old_value):
        return 1 / (1 + np.exp((new_value - old_value) / self._T))

    def _perform_test(self):
        key_value = self._gen_params_key()
        current_params = self.param_space.generate_params()
        if key_value in self.result_dict.keys():
            return self.result_dict[key_value], current_params, None

        current_test = self._test_generator(current_params, self._data)
        current_result = bt.run(current_test)
        balances = current_test.strategy.values
        target_value = self._target_func(balances)
        # trades = np.where(current_test.positions.sum(1) != 0)[0]
        # trades = trades[1:] - trades[:-1]
        # trades = np.sum(trades > 1) / 2.0
        # target_value = self._target_func(balances) * trades
        self.result_dict[key_value] = target_value
        self.stat_dict[key_value] = self._stat_func(balances)

        ret = np.round((balances.iloc[-1] / balances.iloc[0] - 1) * 100.0, 2)

        message = [
            '\nT: {}'.format(self._T),
            'I: {}'.format(self._iteration),
            'Best: {}'.format(self._best_result),
            'Result: {}'.format(target_value),
            'Profit: {}'.format(ret),
            'Params: {}\n'.format(key_value),
        ]
        self._print_message('\n'.join(message))

        return target_value, current_params, current_test

    def optimize(self):
        self._init_params()
        target_value, current_params, backtest = self._perform_test()

        self._best_result = target_value
        opt_target = target_value
        self._best_params = current_params
        opt_params = current_params
        self._best_backtest = backtest

        fails_counter = 0
        for rn in range(self._relaunch):
            while self._T > self._T_min and self._iteration < self._maxiter:
                self._iteration += 1

                if target_value > self.best_result:
                    self._best_result = target_value
                    self._best_params = current_params
                    self._best_backtest = backtest

                if self.chance_of_change(target_value, opt_target) > np.random.uniform():
                    opt_target = target_value
                    self._compute_T()
                else:
                    for param in self.param_space.opt_params:
                        param.value = param.prev_value

                    fails_counter += 1
                    if fails_counter >= 10:
                        self._init_params()
                        self._compute_T()
                        fails_counter = 0

                self._gen_params()
                target_value, current_params, backtest = self._perform_test()

            drop_t = np.power(self._drop_T, rn + 1)
            for param in self.param_space.opt_params:
                tmp_dict = self.best_params
                for path_key in param.path:
                    tmp_dict = tmp_dict[path_key]
                param.value = tmp_dict[param.name]
                param.value = tmp_dict[param.name]
                param.T = param.T_start / drop_t
                param.T_min /= drop_t
            self._T = self._start_T / drop_t
            self._T_min /= drop_t
            self._iteration = 0
            opt_target = self.best_result
            self._gen_params()
            target_value, current_params, backtest = self._perform_test()

        if target_value > self.best_result:
            self._best_result = target_value
            self._best_params = current_params
            self._best_backtest = backtest

        return self._best_result, self._best_params


class MultiDAnnealSimulation(AnnealSimulation):

    def __init__(self, t_d=None, t_min_d=None, **kwargs):
        super().__init__(**kwargs)

        self._D = len(self.param_space.opt_params)

        if t_d is None:
            t_d = np.full(self._D, self.T)
        elif np.isfinite(t_d):
            t_d = np.full(self._D, t_d)

        if t_min_d is None:
            t_min_d = np.full(self._D, self.T_min)
        elif np.isfinite(t_min_d):
            t_min_d = np.full(self._D, t_min_d)

        if len(t_d) != self._D or len(t_min_d) != self._D:
            raise Exception('Wrong params length: D: {} | T: {} | Tmin: {}'.format(self._D, len(t_d), len(t_min_d)))

        for param, t, t_min in zip(self.param_space.opt_params, t_d, t_min_d):
            param.__setattr__('T', t)
            param.__setattr__('T_start', t)
            param.__setattr__('T_min', t_min)

        # target_acceptance = 0.7
        # if self._auto_params:
        #     self._init_params()
        #     for param in self.param_space.opt_params:
        #         acceptance = 0
        #         level = acceptance / target_acceptance
        #         while level <= 0.67 or level >= 0.73:
        #             steps = 1000
        #             for _ in range(steps):
        #                 self._gen_param_value(param)
        #                 acceptance += 1 if param.minv <= param.value <= param.maxv else 0
        #                 self._init_param_value(param)
        #                 self._init_param_value(param)
        #             acceptance /= steps
        #             param.T = param.T * acceptance / target_acceptance
        #             acceptance = 0
        #             level = acceptance / target_acceptance


class VeryFastMultiAnnealing(MultiDAnnealSimulation):

    def __init__(self, p1, p2, **kwargs):
        super().__init__(**kwargs)

        if np.isfinite(p1):
            self._p1 = p1
            p1 = np.full(self._D, p1)
        elif isinstance(p1, Iterable):
            p1 = [v for v in p1]
            self._p1 = p1[0]
            p1 = p1[1:]

        if np.isfinite(p2):
            self._p2 = p2
            p2 = np.full(self._D, p2)
        elif isinstance(p2, Iterable):
            p2 = [v for v in p2]
            self._p2 = p2[0]
            p2 = p2[1:]

        if len(p1) != self._D or len(p2) != self._D:
            raise Exception('Wrong params length: D: {} | p1: {} | p2: {}'.format(self._D, len(p1), len(p2)))

        for param, p1_val, p2_val in zip(self.param_space.opt_params, p1, p1):
            param.__setattr__('p1', p1_val)
            param.__setattr__('p2', p2_val)

    @property
    def p1(self):
        return self._p1

    @property
    def p2(self):
        return self._p2

    def _gen_param_value(self, param: OptParam, valid=False):
        value = np.nan
        for _ in range(self._regen_n):
            a = np.random.uniform()
            z = np.sign(a - 0.5) * param.T * (np.power(1 + 1 / param.T, np.abs(2 * a - 1)) - 1)
            value = param.prev_value + (param.maxv - param.minv) * z

            if valid or param.minv <= value <= param.maxv:
                param.value = value
                return

        param.value = [param.minv, param.maxv][np.abs([value - param.minv, value - param.maxv]).argmin()]

    def _compute_T(self):
        self._T = self._T * np.exp(-self._p1 * np.exp(-self._p2 / self._D) * np.power(self._iteration, 1 / self._D))
        for param in self.param_space.opt_params:
            param.T = param.T * np.exp(-param.p1 * np.exp(-param.p2 / self._D) * np.power(self._iteration, 1 / self._D))


# class ModifiedVFMultiSA(VeryFastMultiAnnealing):
#
#     def __init__(self, **kwargs):
#         super().__init__(**kwargs)
#
#     def __select_params(self):
#         self._init_params()
#         target_value, current_params = self._perform_test()
#
#
class AnnealParamsSelector():

    def __init__(self, optimizer: AnnealSimulation, maxiter=2000):
        self._optimizer = optimizer
        self._maxiter = maxiter

    def execute_optimizer(self, optimizer):
        optimizer.optimize()
        return optimizer.best_result

    def fit(self):
        optimizer = deepcopy(self._optimizer)

#     def run(self, optimizer: AnnealSimulation, E, T, steps):
#         """Anneals a system at constant temperature and returns the state,
#         energy, rate of acceptance, and rate of improvement."""
#         prevEnergy = E
#         accepts, improves = 0, 0
#         for _ in range(steps):
#             E = optimizer.step_optimize()
#             dE = prevEnergy - E
#             if dE > 0.0 and math.exp(-dE / T) < random.random():
#                 E = prevEnergy
#             else:
#                 accepts += 1
#                 if dE < 0.0:
#                     improves += 1
#                 prevEnergy = E
#         return E, float(accepts) / steps, float(improves) / steps
#
#     def select_params(self):
#         """Explores the annealing landscape and
#         estimates optimal temperature settings.
#         Returns a dictionary suitable for the `set_schedule` method.
#         """
#
#         step = 0
#         self.start = time.time()
#
#         # Attempting automatic simulated anneal...
#         # Find an initial guess for temperature
#         E = self._optimizer.step_optimize()
#         T = np.abs(self._optimizer.step_optimize() - E)
#
#         # Search for Tmax - a temperature that gives 98% acceptance
#         E, acceptance, improvement = run(T, steps)
#
#         step += steps
#         while acceptance > 0.98:
#             T = round_figures(T / 1.5, 2)
#             E, acceptance, improvement = run(T, steps)
#             step += steps
#             self.update(step, T, E, acceptance, improvement)
#         while acceptance < 0.98:
#             T = round_figures(T * 1.5, 2)
#             E, acceptance, improvement = run(T, steps)
#             step += steps
#             self.update(step, T, E, acceptance, improvement)
#         Tmax = T
#
#         # Search for Tmin - a temperature that gives 0% improvement
#         while improvement > 0.0:
#             T = round_figures(T / 1.5, 2)
#             E, acceptance, improvement = run(T, steps)
#             step += steps
#             self.update(step, T, E, acceptance, improvement)
#         Tmin = T
#
#         # Calculate anneal duration
#         elapsed = time.time() - self.start
#         duration = round_figures(int(60.0 * minutes * step / elapsed), 2)
#
#         # Don't perform anneal, just return params
#         return {'tmax': Tmax, 'tmin': Tmin, 'steps': duration, 'updates': self.updates}