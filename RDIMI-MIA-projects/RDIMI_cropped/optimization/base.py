# -*- encoding: utf-8 -*-

from os import makedirs
from os.path import normpath, sep as path_separator
from time import strptime
from copy import deepcopy
import json
from abc import ABC, abstractmethod
from concurrent.futures import ProcessPoolExecutor
from functools import partial
import math

import tqdm

import pandas as pd
import numpy as np

import bt
import ffn

from strategy import signal_generators
from strategy.signal_generators import CombineByLastDates

from stats.stats import proc_return


class OptParamSpace():

    def __init__(self, default_dict, params_dict):
        self._default_dict = deepcopy(default_dict)
        self._params_dict = deepcopy(params_dict)

        self._opt_params = []
        self.__iterate_param(params_dict, [])

    @property
    def default_dict(self):
        return self._default_dict

    @property
    def params_dict(self):
        return self._params_dict

    @property
    def opt_params(self):
        return self._opt_params

    def __iterate_param(self, params_dict, key_path):
        for k, v in params_dict.items():
            vclass = v.__class__
            if issubclass(vclass, OptParam):
                v.path = key_path
                v.name = k
                self._opt_params.append(v)
            elif issubclass(vclass, dict):
                key_path.append(k)
                self.__iterate_param(v, deepcopy(key_path))
                key_path.pop(-1)

    def generate_params(self):
        new_params = deepcopy(self._default_dict)
        for param in self._opt_params:
            tmp_dict = new_params
            for path_key in param.path:
                tmp_dict = tmp_dict[path_key]
            tmp_dict[param.name] = param.value
        return new_params

    def set_default_param(self, key, value, v_path=None):
        if v_path is None:
            self._default_dict[key] = value
        else:
            for pkey in v_path:
                tmp_dict = self._default_dict[pkey]
            tmp_dict[key] = value


class OptParam(ABC):

    def __init__(self, minv, maxv):
        self._minv = minv
        self._maxv = maxv

        self._prev_value = np.nan
        self._value = np.nan
        self._path = []
        self._name = ''

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path_value):
        self._path = deepcopy(path_value)

    @property
    def minv(self):
        return self._minv

    @property
    def maxv(self):
        return self._maxv

    @abstractmethod
    def _adjust_value(self, value):
        pass

    @property
    def value(self):
        return self._adjust_value(self._value)

    @value.setter
    def value(self, value):
        self._prev_value = value
        self._value = value

    @property
    def prev_value(self):
        return self._adjust_value(self._value)

    @prev_value.setter
    def prev_value(self, prev_value):
        self._prev_value = prev_value


class IntOptParam(OptParam):

    def __init__(self, minv, maxv):
        super().__init__(minv, maxv)

    def _adjust_value(self, value):
        return int(np.round(value))


class RoundingOptParam(OptParam):

    def __init__(self, minv, maxv, rounding):
        super().__init__(minv, maxv)

        self._rounding = rounding

    @property
    def rounding(self):
        return self._rounding

    def _adjust_value(self, value):
        return np.round(value, self._rounding)


class Optimizer(ABC):

    def __init__(self, data, test_generator, param_space: OptParamSpace, target_func, stat_func, relaunch=3, verbose=True):
        self._test_generator = test_generator
        self._param_space = param_space
        self._target_func = target_func
        self._stat_func = stat_func
        self._data = data
        self._verbose = verbose
        self._relaunch = relaunch

        self._result_dict = {}
        self._stat_dict = {}
        self._best_result = np.nan
        self._best_params = None
        self._best_backtest = None

    @property
    def verbose(self):
        return self._verbose

    @verbose.setter
    def verbose(self, verbose):
        self._verbose = verbose
        self._param_space.set_default_param('progress_bar', verbose)

    @property
    def param_space(self) -> OptParamSpace:
        return self._param_space

    def _gen_params_key(self) -> str:
        key_dict = {}
        for param in self.param_space.opt_params:
            tmp_dict = key_dict
            for path_key in param.path:
                if path_key not in tmp_dict.keys():
                    tmp_dict[path_key] = {}
                tmp_dict = tmp_dict[path_key]
            tmp_dict[param.name] = param.value
        return json.dumps(key_dict)

    @abstractmethod
    def _perform_test(self):
        pass

    def _init_params(self):
        for param in self.param_space.opt_params:
            self._init_param_value(param)

    @abstractmethod
    def _init_param_value(self, param):
        pass

    def _gen_params(self):
        for param in self.param_space.opt_params:
            self._gen_param_value(param)

    @abstractmethod
    def _gen_param_value(self, param, valid=False):
        pass

    @abstractmethod
    def optimize(self) -> (float, dict):
        pass

    @property
    def test_generator(self):
        return self._test_generator

    @property
    def result_dict(self) -> dict:
        return self._result_dict

    @property
    def stat_dict(self) -> dict:
        return self._stat_dict

    @property
    def best_result(self):
        return self._best_result

    @property
    def best_params(self):
        return self._best_params

    def _print_message(self, message):
        if self._verbose:
            print(message)

    @property
    def stat_func(self):
        return self._stat_func

    @property
    def best_backtest(self):
        return self._best_backtest


def dump_generators(params, dates, fpath):

    def iterate_generator(gen_params):
        for key, value in gen_params.items():
            if key in ['combine', 'generator']:
                gen_params[key] = value.__name__
            elif isinstance(value, dict):
                iterate_generator(value)

    params_list = []
    for param, dt in zip(params, dates):
        params_dict = dict()
        params_dict['start_date'] = dt[0]
        params_dict['end_date'] = dt[1]
        params_dict['params'] = deepcopy(param)
        iterate_generator(params_dict['params'])
        params_list.append(params_dict)

    fpath = normpath(fpath)
    makedirs(fpath[:fpath.rfind(path_separator)])
    with open(fpath, 'w') as fobj:
        fobj.write(json.dumps(params_list))


def load_generators(fpath):

    def iterate_generator(gen_params):
        for key, value in gen_params.items():
            if key in ['combine', 'generator']:
                gen_params[key] = getattr(signal_generators, key)
            elif key in ['start_date', 'end_date']:
                gen_params[key] = strptime(gen_params[key])
            elif isinstance(value, dict):
                iterate_generator(value)

    with open(fpath, 'r') as fobj:
        params_list = json.load(fobj)

    for param in params_list:
        iterate_generator(param['params'])

    params, dates = [], []
    for param in params_list:
        dates.append((param['start_date'], param['end_date']))
        params.append(param['params'])

    return params, dates


class WindowsOptimizer:

    def __init__(self, name, data, optimizer: Optimizer, learn_period, test_period, period_type, proc=4):
        self._name = name
        self._data = data
        self._optimizer = optimizer
        self._test_generator = optimizer.test_generator
        self._learn_period = learn_period
        self._test_period = test_period
        self._period_type = period_type
        self._proc = proc

        self._learn_list = []
        self._test_list = []
        self._periods_dates = []

        self._backtest = None


    @property
    def backtest(self):
        return self._backtest

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, data):
        self._data = data

    @property
    def optimizer(self):
        return self._optimizer

    @optimizer.setter
    def optimizer(self, optimizer):
        self._optimizer = optimizer

    @property
    def learn_period(self):
        return self._learn_period

    @learn_period.setter
    def learn_period(self, learn_period):
        self._learn_period = learn_period

    @property
    def test_period(self):
        return self._test_period

    @test_period.setter
    def test_period(self, test_period):
        self._test_period = test_period

    @property
    def period_type(self):
        return self._period_type

    @period_type.setter
    def period_type(self, period_type):
        self._period_type = period_type

    @property
    def proc(self):
        return self._proc

    @proc.setter
    def proc(self, proc):
        self._proc = proc

    def _split_data(self):

        tmp_data = list(self._data.values())[0]
        ridx = [tmp_data.index[0]]
        ridx.extend(list(ffn.asfreq_actual(tmp_data, '1{}'.format(self.period_type)).index))
        ridx.append(tmp_data.index[-1])

        for i in range(1, int(np.ceil((len(ridx) - self.learn_period) / self._test_period))):
            iptp = int((i - 1) * self._test_period)
            tptplp = int(iptp + self._learn_period)
            itplp = min(int(i * self._test_period + self._learn_period), len(ridx))

            learn_dict = dict.fromkeys(self._data.keys(), [])
            test_dict = dict.fromkeys(self._data.keys(), [])
            for currency, data in self._data.items():
                learn_dict[currency] = data.loc[ridx[iptp]:ridx[tptplp]]
                test_dict[currency] = data.loc[ridx[tptplp]:ridx[itplp]]

            # print(i, ridx[iptp], ridx[tptplp], ridx[tptplp], ridx[itplp])

            self._periods_dates.append((ridx[tptplp], ridx[itplp]))
            self._learn_list.append(learn_dict)
            self._test_list.append(test_dict)

    def _optimize(self, params):
        optimizer = params[1]
        optimizer.verbose = False
        optimizer.data = params[0]
        optimizer.optimize()
        return optimizer.best_result, optimizer.best_params

    def _test(self, backtest):
        result = bt.run(backtest)
        return result, backtest

    def optimize(self):

        self._split_data()

        # for per in self._test_list:
        #     print(per['BTC'].index[0], per['BTC'].index[-1])

        optimizers = [deepcopy(self._optimizer) for _ in range(len(self._learn_list))]
        with ProcessPoolExecutor(self._proc) as executor:
            opt_results = [x for x in tqdm.tqdm(executor.map(self._optimize, zip(self._learn_list, optimizers)),
                                               desc='optimization processing',
                                               total=len(self._learn_list))]

        learn_scores = [v[0] for v in opt_results]

        strategies = [self._test_generator(p[1], d) for p, d in zip(opt_results, self._test_list)]
        with ProcessPoolExecutor(self._proc) as executor:
            test_results = [x for x in tqdm.tqdm(executor.map(self._test, strategies),
                                               desc='test processing',
                                               total=len(strategies))]

        test_scores = [proc_return(v[1].strategy.values, to_dict=False) for v in test_results]

        param_dict = {}
        CombineByLastDatesPartial = partial(CombineByLastDates, periods_dates=self._periods_dates)
        for currency in self._data.keys():
            param_dict[currency] = {'combine': CombineByLastDatesPartial}
            for i in range(len(opt_results)):
                gen_params = opt_results[i][1]['signal_params']['curr_generators'][currency]
                if 'combine' in gen_params.keys():
                    param_dict[currency]['param_' + str(i)] = gen_params
                else:
                    for key, item in gen_params.items():
                        param_dict[currency]['param_' + str(i) + key] = item
        test_params = self._optimizer.param_space.default_dict
        test_params['signal_params']['curr_generators'] = param_dict

        test_data = {k: self._data[k].loc[self._data[k].index >= v.index[0]] for k, v in self._test_list[0].items()}
        result, backtest = self._test(self._test_generator(test_params, test_data))
        self._backtest = backtest

        backtest.strategy.values.to_excel(self._name + '_BALANCE.xlsx')

        # print(learn_scores)
        # print(test_scores)
        # optimizer = deepcopy(self._optimizer)
        # optimizer.verbose = True
        # optimizer.data = self._test_list[-1]
        # print(optimizer.optimize())

        fileobj = open(self._name + '_results.txt', 'w')

        fileobj.write('\n==== results ====\n')
        print('\n==== results ====\n')
        for opt, test in zip(opt_results, test_results):
            print(opt[1], '\n')
            print(proc_return(test[1].strategy.values), '\n')
            stats = self.optimizer.stat_func(test[1].strategy.values)[0]
            print(stats, '\n')

            fileobj.write(str(opt[1]))
            fileobj.write('\n')
            fileobj.write(json.dumps(stats))
            fileobj.write('\n')

        print('\n==== whole ====\n')
        fileobj.write('\n==== whole ====\n')
        print(proc_return(backtest.strategy.values), '\n')
        print(self.optimizer.stat_func(backtest.strategy.values)[0])
        fileobj.write(json.dumps(self.optimizer.stat_func(backtest.strategy.values)[0]))

