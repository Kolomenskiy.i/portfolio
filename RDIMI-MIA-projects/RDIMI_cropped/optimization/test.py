# -*- encoding: utf-8 -*-

from functools import partial

import pandas as pd
pd.core.common.is_list_like = pd.api.types.is_list_like
import numpy as np
from copy import deepcopy
from importlib import import_module
from optimization.base import IntOptParam, OptParamSpace, WindowsOptimizer
from optimization.annealing import VeryFastMultiAnnealing

import json
import bt

from cbt.core_custom import BacktestOHLC, fee_func

from strategy.strategy_generators import get_multi_strategy, get_multi_test
from strategy import signal_generators as signals

from data.utils import get_data, get_report, get_yahoo_data, get_kaggle_minute_data

from stats.stats import get_balance_stat, proc_return, sortino, Charts, NewCharts

import pylab as plt
from enum import Enum
from datetime import datetime, timedelta

from data import finam


class TradeTypes(Enum):
    LONG = ['long']
    SHORT = ['short']
    LONG_SHORT = ['long', 'short']


if __name__ == '__main__':
    currency = 'BTC'
    data = get_data('./data', currency, 'USD', '1h', 'Bitstamp',
                    start_date='2020-01-01 00:00:00', last_date='2020-03-28 00:00:00', update_data=False)
    print(data.index[0], data.index[-1])

    gen_params = {
        'progress_bar': True,
        'timeframe': '1H',
        'skip_period': 0,
        'signal_params': {
            'curr_generators': {
                currency: {
                    # 'combine': signals.CombineFuncOR,
                    # 'first': {
                    #     'generator': signals.BBChannelSignalGenerator,
                    #     'params': {}
                    # },
                    'second': {
                        'generator': signals.BBSignalGenerator,
                        'params': {}
                    }
                }
            }
        },
    }

    opt_params = {
        'signal_params': {
            'curr_generators': {
                currency: {
                    # 'first': {
                    #     'params': {
                    #         'up_period': IntOptParam(5, 35),
                    #         'low_period': IntOptParam(5, 35),
                    #         # 'order_length': IntOptParam(50, 350),
                    #         # 'buy_n': IntOptParam(1, 3),
                    #         # 'sell_n': IntOptParam(1, 3),
                    #     }
                    # },
                    'second': {
                        'params': {
                            'up_period': IntOptParam(5, 35),
                            'low_period': IntOptParam(5, 35)
                        }
                    }
                }
            }
        }
    }

    # learn_data = data.loc[(data.index >= '2018-01-01') & (data.index < '2019-01-01')]
    # test_data = data.loc[(data.index >= '2018-12-01') & (data.index < '2020-01-01')]

    target_func = partial(sortino, to_dict=False, balance=True)
    stat_func = partial(get_balance_stat, rf_rate=0.16 / 365 / 24)
    optimizer = VeryFastMultiAnnealing(p1=0.25, p2=4, t=1e-3, t_min=1e-5, maxiter=150, drop_t=10,
                                       data={currency: data},
                                       test_generator=get_multi_test,
                                       param_space=OptParamSpace(gen_params, opt_params),
                                       target_func=target_func,
                                       stat_func=stat_func)
    window_optimizer = WindowsOptimizer(currency + '_bb_1_1_35', {currency: data}, optimizer, 1, 1, 'W', proc=6)  # 'M'
    window_optimizer.optimize()
