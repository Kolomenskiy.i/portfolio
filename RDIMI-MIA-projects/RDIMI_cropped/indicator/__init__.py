# -*- encode: utf-8 -*-

import numpy as np
# from talib.abstract import ADX as TADX, PLUS_DI, MINUS_DI, ADXR
#
#
# def ADX(quotes, period):
#     inputs = {
#         'open': quotes.Open,
#         'high': quotes.High,
#         'low': quotes.Low,
#         'close': quotes.Close
#     }
#     adx = TADX(inputs, timeperiod=period)
#     pdi = PLUS_DI(inputs, timeperiod=period)
#     mdi = MINUS_DI(inputs, timeperiod=period)
#     adxr = ADXR(inputs, timeperiod=period)
#
#     adx = np.nan_to_num(adx)
#     pdi = np.nan_to_num(pdi)
#     mdi = np.nan_to_num(mdi)
#     adxr = np.nan_to_num(adxr)
#
#     # index = quotes.index
#     # adx = adx.set_index(index)
#     # pdi = pdi.set_index(index)
#     # mdi = mdi.set_index(index)
#     # adxr = adxr.set_index(index)
#
#     new_quotes = quotes.copy()
#     new_quotes['ADX'] = adx
#     new_quotes['PDI'] = pdi
#     new_quotes['MDI'] = mdi
#     new_quotes['ADXR'] = adxr
#
#     return new_quotes
#
#
# def MA(data, period, ma_type, ma_func='mean'):
#     if ma_type == 'simple':
#         data = data.rolling(window=period, min_periods=1)
#     elif ma_type == 'ewma':
#         data = data.ewm(span=period, min_periods=1)
#
#     if ma_func == 'mean':
#         return data.mean()
#     elif ma_func == 'std':
#         return data.std()
#
