# -*- encoding: utf-8 -*-

import numpy as np
from pyti.smoothed_moving_average import smoothed_moving_average as smma
# from pyti.bollinger_bands import upper_bollinger_band as up_bb
# from pyti.bollinger_bands import lower_bollinger_band as low_bb
# from pyti.bollinger_bands import middle_bollinger_band as mid_bb
from pyti.bollinger_bands import bandwidth as bbw
from pyti import average_true_range as atr
from ta.volume import force_index as fi
import zigzag
from talib import SAR
import numpy as np
from talib.abstract import ADX, PLUS_DI, MINUS_DI, ADXR
import talib
from talib import SMA, EMA, RSI, TEMA, DEMA
from market_profile import MarketProfile
from finta.finta import TA  # IFT_RSI is not working BUT finta cointains lots of indicators


def get_mp(data, bar_size):
    mp = MarketProfile(data, tick_size=bar_size)
    # mp_slice = mp[data['BTC'].index.max() - pd.Timedelta(6.5, 'h'):data['BTC'].index.max()]
    mp_slice = mp[data.index.min():data.index.max()]

    # mp_data = mp_slice.profile
    # mp_data.plot(kind='bar')
    # plt.show()

    # print("Initial balance: %f, %f" % mp_slice.initial_balance())
    # print("Opening range: %f, %f" % mp_slice.open_range())
    # print("POC: %f" % mp_slice.poc_price)
    # print("Profile range: %f, %f" % mp_slice.profile_range)
    # print("Value area: %f, %f" % mp_slice.value_area)
    # print("Balanced Target: %f" % mp_slice.balanced_target)

    return mp_slice.value_area


def get_atr(high_list, low_list, close_list, period=14):
    return talib.ATR(high_list, low_list, close_list, period)


def get_natr(high_list, low_list, close_list, period=14):
    return talib.NATR(high_list, low_list, close_list, period)


def get_bbands(close_list, period=20, nbdevup=2, nbdevdn=2, matype=0):
    return talib.BBANDS(close_list, timeperiod=period, nbdevup=nbdevup, nbdevdn=nbdevdn, matype=matype)


def get_adx(quotes, period):
    inputs = {
        'open': quotes.Open,
        'high': quotes.High,
        'low': quotes.Low,
        'close': quotes.Close
    }
    adx = ADX(inputs, timeperiod=period)
    pdi = PLUS_DI(inputs, timeperiod=period)
    mdi = MINUS_DI(inputs, timeperiod=period)
    adxr = ADXR(inputs, timeperiod=period)

    adx = np.nan_to_num(adx)
    pdi = np.nan_to_num(pdi)
    mdi = np.nan_to_num(mdi)
    adxr = np.nan_to_num(adxr)

    # index = quotes.index
    # adx = adx.set_index(index)
    # pdi = pdi.set_index(index)
    # mdi = mdi.set_index(index)
    # adxr = adxr.set_index(index)

    new_quotes = quotes.copy()
    new_quotes['ADX'] = adx
    new_quotes['PDI'] = pdi
    new_quotes['MDI'] = mdi
    new_quotes['ADXR'] = adxr

    return new_quotes


def get_adxdir(raw_quotes, params):
    quotes = get_adx(raw_quotes, params['period'])
    quotes[['MDI', 'PDI']] = quotes[['MDI', 'PDI']].replace([np.inf, -np.inf, np.nan, 0.0], 0.01)
    quotes['DIR'] = np.sign((quotes.PDI - quotes.MDI).replace([np.inf, -np.inf], np.nan).fillna(0.0))
    quotes = quotes.fillna(0.0)

    adx_dir = (quotes.DIR * np.sin(params['coef'] * quotes.ADX / 100.0)).replace([np.inf, -np.inf], np.nan).fillna(0.0).values

    val_slice = adx_dir > 0
    adx_dir[val_slice] = np.round(adx_dir[val_slice] / params['pmx'], 1)
    adx_dir[val_slice & (adx_dir < params['pdr'])] = 0.0
    adx_dir[adx_dir > 1] = 1.0

    val_slice = adx_dir < 0
    adx_dir[val_slice] = np.round(adx_dir[val_slice] / params['nmx'], 1)
    adx_dir[val_slice & (adx_dir > params['ndr'])] = 0.0
    adx_dir[adx_dir < -1] = -1.0

    return adx_dir


def fill_for_noncomputable_vals(input_data, result_data):
    non_computable_values = np.repeat(
        np.nan, len(input_data) - len(result_data)
        )
    filled_result_data = np.append(non_computable_values, result_data)
    return filled_result_data


def rsi(data, period): #modified version of pyti #the more data, the more it's accurate 
    """
    Relative Strength Index.
    Formula:
    RSI = 100 - (100 / 1 + (prevGain/prevLoss))
    """
    period = int(period)
    changes = [data_tup[1] - data_tup[0] for data_tup in zip(data[::1], data[1::1])]

    filtered_gain = [val < 0 for val in changes]
    gains = [0 if filtered_gain[idx] == True else changes[idx] for idx in range(0, len(filtered_gain))] #use ==, not is!  

    filtered_loss = [val > 0 for val in changes]
    losses = [0 if filtered_loss[idx] == True else abs(changes[idx]) for idx in range(0, len(filtered_loss))] #use ==, not is! 

    avg_gain = np.mean(gains[:period])
    avg_loss = np.mean(losses[:period])

    rsi = []
    if avg_loss == 0:
        rsi.append(100)
    else:
        rs = avg_gain / avg_loss
        rsi.append(100 - (100 / (1 + rs)))

    for idx in range(1, len(data) - period):
        avg_gain = ((avg_gain * (period - 1) +
                    gains[idx + (period - 1)]) / period)
        avg_loss = ((avg_loss * (period - 1) +
                    losses[idx + (period - 1)]) / period)

        if avg_loss == 0:
            rsi.append(100)
        else:
            rs = avg_gain / avg_loss
            rsi.append(100 - (100 / (1 + rs)))

    rsi = fill_for_noncomputable_vals(data, rsi)

    return rsi


def ema(data, period):
    ema_list = np.empty(len(data))
    ema_list[0] = float(data[0])
    i = 1
    while i < len(data):
        ema_list[i] = ((data[i] - ema_list[i-1]) * (2/(period+1))) + ema_list[i-1]
        i += 1
    return ema_list
   

def macd(data):
    macd = ema(data, 12) - ema(data, 26)
    signal_macd = np.zeros(len(data))
    signal_macd = ema(macd, 9)
    histogram_macd = np.zeros(len(macd))
    histogram_macd = macd - signal_macd
    return histogram_macd


def momentum(data, period):
    mom = np.zeros(len(data))
    k = period
    while k < len(data):
        mom[k] = data[k] - data[k - period]
        k+=1
    return mom


def on_balance_volume(ohlc):
    obv = np.empty((len(ohlc)))
    obv[0] = ohlc.Volume[0]
    for i in range(1, len(obv)):
        if ohlc.Close.values[i] > ohlc.Close.values[i - 1]:
            obv[i] = obv[i - 1] + ohlc.Volume[i]
        elif ohlc.Close.values[i] < ohlc.Close.values[i - 1]:
            obv[i] = obv[i - 1] - ohlc.Volume.values[i]
        else:
            obv[i] = obv[i - 1]
    return obv


def get_fish_rsi(data):
    rsi_res = RSI(data['Close'])
    rsi_res = 0.1 * (rsi_res - 50)
    return (np.exp(2 * rsi_res) - 1) / (np.exp(2 * rsi_res) + 1)


def up_bb(data, period, std_mult=2.0, ma_type='sma'):
    """
    Upper Bollinger Band.

    Formula:
    u_bb = SMA(t) + STD(SMA(t-n:t)) * std_mult
    """
    functions = {'sma': SMA, 'ema': EMA, 'dema': DEMA, 'tema': TEMA}

    period = int(period)
    simple_ma = functions[ma_type](data, period)[period - 1:]

    upper_bb = []
    for idx in range(len(data) - period + 1):
        std_dev = np.std(data[idx:idx + period])
        upper_bb.append(simple_ma[idx] + std_dev * std_mult)
    upper_bb = fill_for_noncomputable_vals(data, upper_bb)

    return np.array(upper_bb)


def mid_bb(data, period, std=2.0, ma_type='sma'):
    """
    Middle Bollinger Band.

    Formula:
    m_bb = sma()
    """
    functions = {'sma': SMA, 'ema': EMA, 'dema': DEMA, 'tema': TEMA}

    period = int(period)
    mid_bb = functions[ma_type](data, period)

    return mid_bb


def low_bb(data, period, std=2.0, ma_type='sma'):
    """
    Lower Bollinger Band.

    Formula:
    u_bb = SMA(t) - STD(SMA(t-n:t)) * std_mult
    """
    functions = {'sma': SMA, 'ema': EMA, 'dema': DEMA, 'tema': TEMA}

    period = int(period)
    simple_ma = functions[ma_type](data, period)[period - 1:]

    lower_bb = []
    for idx in range(len(data) - period + 1):
        std_dev = np.std(data[idx:idx + period])
        lower_bb.append(simple_ma[idx] - std_dev * std)
    lower_bb = fill_for_noncomputable_vals(data, lower_bb)

    return np.array(lower_bb)
