# -*- encoding: utf-8 -*-

from functools import partial

import pandas as pd
pd.core.common.is_list_like = pd.api.types.is_list_like
import numpy as np
from copy import deepcopy
from importlib import import_module
from optimization.base import IntOptParam, OptParamSpace, WindowsOptimizer
from optimization.annealing import VeryFastMultiAnnealing

import json
import bt

from cbt.core_custom import BacktestOHLC, fee_func, stock_market_fee_func

from strategy.strategy_generators import get_multi_strategy, get_multi_test
from strategy import signal_generators as signals

from data.utils import get_data, get_report, get_yahoo_data, get_kaggle_minute_data, generator_list_reader

from stats.stats import get_balance_stat, proc_return, sortino, Charts, NewCharts

import pylab as plt
from enum import Enum
from datetime import datetime, timedelta

# from data import finam


class TradeTypes(Enum):
    LONG = ['long']
    SHORT = ['short']
    LONG_SHORT = ['long', 'short']


if __name__ == '__main__':
    max_period = 500  # exchange works from 9 am to 15 pm
    timeframe = '1h'
    params = {
        'skip_period': max_period,
        'commissions': partial(fee_func, ff=0, pf=0.003),  # 0.258
        'timeframe': '1H',
        'signal_params': {
            'curr_generators': {
                ('BTC',):
                    [{'combine': signals.Trend_Generator_Switch_Func, 'params': {'take_profit': 10}},
                     {'generator': signals.BBSignalGenerator},
                     {'generator': signals.EMA_and_Price_cross, 'params': {'ema_type': 'tema'}},
                     {'generator': signals.EMA_and_Price_cross, 'params': {'ema_type': 'tema'}},
                     ],
                ('ETH', 'XRP', 'BCH', 'LTC', ):  # 'XLM'
                [{'combine': signals.Trend_Generator_Switch_Func, 'params': {'take_profit': 20}},
                 {'generator': signals.BBSignalGenerator},
                 {'generator': signals.EMA_and_Price_cross, 'params': {'ema_type': 'tema'}},
                 {'generator': signals.EMA_and_Price_cross, 'params': {'ema_type': 'tema'}},
                 ],
                # ('AAPL', 'FB', 'AMZN', 'NVDA', 'F', 'ADBE'): [{'generator': signals.Test_strategy}],
                # ('BTC', 'ETH', 'BCH', 'XRP', 'LTC'):
                #     [{'generator': signals.BB_weak_trend_follow}, ],
                # ('AAPL', 'FB', 'AMZN', 'NVDA', 'F', 'ADBE'): [{'generator': signals.Test_strategy}],
                # ('YNDX', ): [{'generator': signals.Test_strategy}],  # 'MAIL', 'AFLT',
                # ('ETHUSD', 'XRPUSD', 'BCHUSD', 'LTCUSD',):
                #     [{'generator': signals.Test_strategy}],
                # ('BTCUSD',):
                #     [{'generator': signals.Test_strategy}],
                # ('AFLT',):
                #     [{'generator': signals.Test_strategy}],
            },
        }
    }

    start_date = datetime.strptime('2021-01-01 00:00:00', '%Y-%m-%d %H:%M:%S') - timedelta(hours=max_period)
    # TODO: timedelta(minutes=max_period) works wrong because lots of minute candles are missed in data files
    # start_date = datetime.strptime('2020-01-01 00:00:00', '%Y-%m-%d %H:%M:%S') - timedelta(minutes=max_period)
    last_date = None
    # last_date = datetime.strptime('2020-06-29 12:00:00', '%Y-%m-%d %H:%M:%S')
    # last_date = datetime.strptime('2020-07-23 00:00:00', '%Y-%m-%d %H:%M:%S')
    # LONG:
    # start_date = datetime.strptime('2019-03-26 00:00:00', '%Y-%m-%d %H:%M:%S') - timedelta(hours=max_period)
    # last_date = datetime.strptime('2019-06-27 00:00:00', '%Y-%m-%d %H:%M:%S')
    # SHORT:
    # start_date = datetime.strptime('2018-01-06 00:00:00', '%Y-%m-%d %H:%M:%S') - timedelta(hours=max_period)
    # last_date = datetime.strptime('2018-12-12 00:00:00', '%Y-%m-%d %H:%M:%S')
    if isinstance(list(params['signal_params']['curr_generators'].keys())[0], tuple):
        params['signal_params']['curr_generators'] = generator_list_reader(params['signal_params']['curr_generators'])

    data = dict.fromkeys(params['signal_params']['curr_generators'].keys())
    for currency in data.keys():
        data[currency] = get_data('./data', currency, 'USD', timeframe, 'Bitstamp',  # Finam/Bitstamp/Binance
                                  start_date=start_date, last_date=last_date, update_data=True)  # USD/USDT
        # data[currency] = get_yahoo_data(currency, timeframe='1h', start=start_date, end=last_date)
        # data[currency] = get_kaggle_minute_data(currency.lower(), start_date=start_date, last_date=last_date)
        # data[currency] = get_data('./data', currency, 'USD', timeframe, 'Finam',  # Finam/Bitstamp/Binance
        #                           start_date=start_date, last_date=last_date, update_data=True)  # USD/USDT
    # test = get_multi_test(params, data)
    # res = bt.run(test)
    # res.display()
    # plt.show()
    # Charts(**{'Backtest_result': res, 'Name': 'BTC'}).plot()
    NewCharts(params, data)

    # LONG:
    # start_date='2017-11-12', last_date='2017-12-08'
    # SHORT:
    # start_date='2017-12-17', last_date='2018-02-06'
    # FLAT:
    # start_date='2019-03-01', last_date='2019-04-01'
