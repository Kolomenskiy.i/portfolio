# -*- encode: utf-8 -*-
from functools import wraps
import bt
from ffn.core import *
from cbt.core_custom import BacktestOHLC
import sys
np.set_printoptions(threshold=sys.maxsize)
from strategy import strategy_generators as strat
from strategy.strategy_generators import get_multi_test
import plotly.graph_objs as go
from plotly.subplots import make_subplots
from plotly.offline import plot

import pandas as pd
from strategy import signal_generators as signals
import time

import plotly.express as px

def to_array(func):

    @wraps(func)
    def to_array_wrap(*args, **kwargs):
        args = list(args)
        args[0] = args[0].values if isinstance(args[0], pd.Series) else np.array(args[0])
        return func(*args, **kwargs)

    return to_array_wrap

def balance_to_ret(func):

    @wraps(func)
    def balance_to_ret_wrap(*args, **kwargs):
        if kwargs.get('balance', False):
            args = list(args)
            args[0] = args[0].values if isinstance(args[0], pd.Series) else np.array(args[0])
            args[0] = args[0][1:] / args[0][:-1] - 1
            args[0][~np.isfinite(args[0])] = 0
        return func(*args, **kwargs)

    return balance_to_ret_wrap


@to_array
@balance_to_ret
def sharp(ret_series, r=.16/365, to_dict=True, balance=False):
    val = np.round((ret_series - r).mean() / (ret_series - r).std(), 4)
    return {'Sharp': val} if to_dict else val


@to_array
@balance_to_ret
def sortino(ret_series, r=.16/365, to_dict=True, balance=False):
    down_ret = np.where(ret_series < 0)[0]
    if len(down_ret) >= 2:
        down_ret = (r - ret_series[down_ret]).std()
    else:
        down_ret = ret_series.std() * .8
    val = np.round((ret_series.mean() - r) / down_ret, 6)
    return {'Sortino': val} if to_dict else val


@to_array
@balance_to_ret
def volatility(ret_series, year=False, to_dict=True, balance=False):
    volat = (np.exp(ret_series.std()) - 1.0) * 100.0
    val = np.round(volat * np.sqrt(365) if year else volat, 4)
    return {'{}Volatility'.format('Y' if year else ''): val} if to_dict else val


@to_array
def abs_return(balance_series, to_dict=True):
    val = np.round(balance_series[-1] - balance_series[0], 4)
    return {'Return': val} if to_dict else val


@to_array
def proc_return(balance_series, year=False, to_dict=True):
    if year:
        balance_series = np.log(balance_series[1:] / balance_series[:-1])[1:]
        val = (np.exp(np.mean(balance_series)) - 1) * np.sqrt(365)
    else:
        val = balance_series[-1] / balance_series[0] - 1
    val = np.round(val * 100.0, 4)
    return {'{}PReturn'.format('Y' if year else ''): val} if to_dict else val


@to_array
def trade_len(trade_days):

    trade_days = np.where(trade_days == 1)[0]
    trade_days = trade_days[1:] - trade_days[:-1]

    if len(trade_days) > 0:
        return {
            'MinTradeLen': np.min(trade_days).round(4),
            'MaxTradeLen': np.max(trade_days).round(4),
            'MeanTradeLen': np.mean(trade_days).round(4)
        }
    else:
        return {
            'MinTradeLen': 0,
            'MaxTradeLen': 0,
            'MeanTradeLen': 0
        }


@to_array
def max_drop(balance_series):

    result = dict()
    drops = []
    min_b, max_b = balance_series[0], balance_series[0]
    indrop = False
    for balance in balance_series[1:]:
        if balance > max_b:
            if indrop:
                drops.append([
                    np.round(min_b - max_b, 2),
                    np.round(min_b / max_b - 1.0, 6)])
                indrop = False
            min_b = balance
            max_b = balance
        elif balance < max_b:
            indrop = True
            min_b = balance if balance < min_b else min_b

    if indrop:
        drops.append([
            np.round(min_b - max_b, 2),
            np.round(min_b / max_b - 1.0, 6)])

    if len(drops) > 0:
        drops = np.array(drops)
        result['MaxAbsDrop'] = drops.min(axis=0)[0]
        result['MeanAbsDrop'] = drops[:, 0].mean()
        result['MaxRelDrop'] = np.round(drops.min(axis=0)[1] * 100.0, 4)
        result['MeanRelDrop'] = np.round(drops[:, 1].mean() * 100.0, 4)
    else:
        result['MaxAbsDrop'] = 0.0
        result['MeanAbsDrop'] = 0.0
        result['MaxRelDrop'] = 0.0
        result['MeanRelDrop'] = 0.0

    return result


def get_stat_columns():
    return ['AbsReturn', 'PReturn', 'PMeanReturn', 'PVolatility', 'PYearVolatility', 'Sharp',
            'MaxAbsDrop', 'MeanAbsDrop', 'MaxRelDrop', 'MeanRelDrop']


def get_balance_stat(prices, rf_rate=0.16/365):
    result = dict()
    rets = np.log(prices / prices.shift()).fillna(0.0)
    columns = ['AbsReturn', 'PReturn', 'PMeanReturn', 'PVolatility', 'PYearVolatility', 'Sharp',
               'MaxAbsDrop', 'MeanAbsDrop', 'MaxRelDrop', 'MeanRelDrop']
    result['AbsReturn'] = np.round(prices.iloc[-1] - prices.iloc[0], 2)
    result['PReturn'] = np.round((prices.iloc[-1] / prices.iloc[0] - 1) * 100.0, 4)
    result['PMeanReturn'] = np.round((np.exp(rets.mean()) - 1.0) * 100.0, 4)
    result['PVolatility'] = np.round((np.exp(rets.std()) - 1.0) * 100.0, 4)
    result['PYearVolatility'] = np.round((np.exp(rets.std() * np.sqrt(365)) - 1.0) * 100.0, 4)
    result['Sharp'] = sharp(rets, rf_rate, to_dict=False)
    result['Sortino'] = sortino(rets, rf_rate, to_dict=False)
    drops = []
    min_b, max_b = prices.iloc[0], prices.iloc[0]
    indrop = False
    for balance in prices.iloc[1:]:
        if balance > max_b:
            if indrop:
                drops.append([
                    np.round(min_b - max_b, 2),
                    np.round(min_b / max_b - 1.0, 6)])
                indrop = False
            min_b = balance
            max_b = balance
        elif balance < max_b:
            indrop = True
            min_b = balance if balance < min_b else min_b

    if indrop:
        drops.append([
            np.round(min_b - max_b, 2),
            np.round(min_b / max_b - 1.0, 6)])

    if len(drops) > 0:
        drops = np.array(drops)
        result['MaxAbsDrop'] = drops.min(axis=0)[0]
        result['MeanAbsDrop'] = drops[:, 0].mean()
        result['MaxRelDrop'] = np.round(drops.min(axis=0)[1] * 100.0, 4)
        result['MeanRelDrop'] = np.round(drops[:, 1].mean() * 100.0, 4)
    else:
        result['MaxAbsDrop'] = 0.0
        result['MeanAbsDrop'] = 0.0
        result['MaxRelDrop'] = 0.0
        result['MeanRelDrop'] = 0.0

    return result, columns


def get_report(generator, data):
    s = strat.get_strategy('bb_strategy', generator, 'BTC', 0.03, '1H')
    test = BacktestOHLC(s, {'BTC': data})
    bt.run(test)

    signals = pd.Series(np.zeros(len(data)), index=data.index)
    signals.update(pd.Series(test.strategy.stack.algos[1].signals))
    pd.DataFrame({'Price': test.strategy.prices,
                  'Weight': test.weights[test.weights.columns[1]],
                  'Position': test.positions[test.positions.columns[0]],
                  'Signals': signals}).to_excel('results.xlsx')


def get_params(df, strategy, parameter='profit'):
    test = bt.Backtest(strategy, df) if type(strategy) == 'Strategy' else BacktestOHLC(strategy, {'BTC': df})
    bt.run(test)
    returns = test.strategy.prices
    return_dict = {'profit': calc_total_return(returns)*100, 'max_drawdown': calc_max_drawdown(returns)*100,
                   'sharpe': calc_sharpe(returns), 'sortino': calc_sortino_ratio(returns)}
    return return_dict.get(parameter, None)


class Charts:

    def __init__(self, **kwargs):
        self._backtest_result = kwargs.get('Backtest_result')

        signals = pd.Series(self._backtest_result.backtest_list[0].strategy.stack.algos[2].signals)
        if signals.empty:
            self._data = self._backtest_result.backtest_list[0].strategy.data_dict[
                next(iter(self._backtest_result.backtest_list[0].strategy.data_dict))]
            self._signals = pd.Series(0, index=self._data.index.values)
        else:
            self._signals = signals
            self._data = self._backtest_result.backtest_list[0].strategy.data_dict[list(self._signals[0].keys())[0]]

        self.balance_list = self._backtest_result.prices*self._data.Close.iloc[0]/self._backtest_result.prices.iloc[0]
        self.additional_charts = self._backtest_result.backtest_list[0].strategy.stack.algos[2].charts
        self.name = kwargs.get('Name', 'BTC')
        self.profitable_deals = 0
        self.loss_making_deals = 0
        self.bought_list = pd.Series(index=self._data.index)
        self.sold_list = pd.Series(index=self._data.index)
        self.profits = []
        prev_i = None
        for i in self._signals[self._signals != 0].index:
            if self._signals[i][self.name] == 1:
                self.bought_list[i] = self._data.Open.loc[i]
            elif prev_i is not None:
                self.sold_list[i] = self._data.Open[i]
                if self.balance_list[self.balance_list.index == i].values[0][0] >= self.balance_list[self.balance_list.index == prev_i].values[0][0]:
                    self.profitable_deals += 1
                else:
                    self.loss_making_deals += 1
                self.profits.append(float(self.balance_list[self.balance_list.index == i].values) / float(self.balance_list[self.balance_list.index == prev_i].values) - 1)
            prev_i = i
        self.profits = np.array(self.profits)


    def plot(self, name='strategy.png'):
        plt.figure(figsize=(24, 12))
        ax1 = plt.subplot2grid((6, 6), (0, 1), rowspan=5, colspan=5)
        ax2 = plt.subplot2grid((6, 6), (5, 1), rowspan=1, colspan=5, sharex=ax1)
        ax1.set_title(self.name + ' ')
        ax1.plot(self._data.index, self._data.Open, label='btc open list')
        ax1.plot(self.balance_list, label='strategy', color='orange')
        ax1.plot(self._data.index, self.sold_list, linestyle='', marker='v', label='sell',
                 color='red')
        ax1.plot(self._data.index,self.bought_list, linestyle='', marker='^', label='buy',
                 color='green')

        priority_ax = ax1
        if self.additional_charts is not None:
            for chart in self.additional_charts:
                if 'Priority' in chart:
                    if 'Secondary' in chart['Priority']:
                        priority_ax = ax2
                chart_params = {}
                if 'Special_params' in chart:
                    chart_params.update(chart['Special_params'])
                if 'Color' in chart:
                    chart_params.update({'Color': chart['Color']})
                if 'Type' in chart:
                    if 'Point_chart' in chart['Type']:
                        chart_params.update({'Linestyle': '', 'marker': '.'})
                        priority_ax.plot(self._data.index, chart['Data'], **chart_params)
                    elif 'Horizontal_chart' in chart['Type']:
                        for lines in chart['Data']:
                            priority_ax.axhline(lines, **chart_params)
                else:
                    priority_ax.plot(self._data.index, chart['Data'], **chart_params)

        ax1.xaxis_date()
        ax1.legend(loc='upper left', frameon=False)

        if priority_ax is ax1:
            ax2.fill_between(self._data.index, self._data.Volume, 0)

        ax1.xaxis_date()
        ax1.legend(loc='upper left', frameon=False)
        ax2.text(0.0, -0.3,
                 f'Profit: {(float(self.balance_list.iloc[-1]) / float(self.balance_list.iloc[0]) - 1) * 100}\nHolding profit: {(self._data.Close[-1] / self._data.Open[0] * 0.997 * 0.997 - 1) * 100}', verticalalignment='top', weight='heavy',
                 transform=ax2.transAxes)
        ax2.text(0.25, -0.3,
                 f'Profitable deals/loss-making deals: {str(self.profitable_deals)+"/"+str(self.loss_making_deals)}',
                 verticalalignment='top', transform=ax2.transAxes)
        # ax2.text(0.6, -0.3,
        # f'Max drawdown: {max_drawdown}\nAverage drawdown: {average_drawdown}', verticalalignment='top', transform=ax2.transAxes)
        ax2.text(0.85, -0.3,
                 f'Sharpe: {sharp(self.balance_list)["Sharp"]}\nSortino: {sortino(self.balance_list)["Sortino"]}\nVolatility: {volatility(self.balance_list)["Volatility"]}', verticalalignment='top',
                 transform=ax2.transAxes)

        ax3 = plt.axes([.025, .5, .2, .35])
        ax3.set_title('Histogram of profits')
        ax3.hist(self.profits, density=True, bins=100)
        ax3.axvline(0, color='red')

        ax4 = plt.axes([.025, .1, .2, .35])
        ax4.set_title('Chart of profits')
        ax4.plot(self.balance_list)
        ax4.axhline(0, color='red')

        ax1.grid()
        ax2.grid()
        ax3.grid()
        ax4.grid()

        #mng = plt.get_current_fig_manager()
        #mng.window.state('zoomed')
        if name is not None:
            plt.savefig(name, dpi=80)
        plt.show()


class NewCharts:

    def __init__(self, strategy_params, data):
        start_time = time.time()
        self.strategy_params = strategy_params
        self.data = data
        self.strategy_update(strategy_params, data)
        print('BT time:', time.time() - start_time)
        start_time = time.time()
        self._signals = pd.Series(self._res.backtest_list[0].strategy.stack.algos[2].signals)
        # self._data_calculator(signals)
        table = self._res.display()
        table.pop(4)
        table = table[1:10]

        # fig = make_subplots(
        #     rows=2, cols=2,
        #     shared_xaxes=True,
        #     specs=[[{"type": "Candlestick"}, None], [{"type": "Table"}, {"type": "histogram"}]])

        fig = make_subplots(
            rows=2, cols=1,
            shared_xaxes=True,
            specs=[[{"type": "Candlestick"}],
                   [{"type": "table"}]]
        )

        if len(self.strategy_params['signal_params']['curr_generators'].items()) == 1:
            self.data = data[list(self.strategy_params['signal_params']['curr_generators'].keys())[0]]
            fig.add_trace(go.Candlestick(x=self.data.index,
                                                 open=self.data['Open'],
                                                 high=self.data['High'],
                                                 low=self.data['Low'],
                                               close=self.data['Close'], name='OHLC for ' + str(list(self._signals[0])[0])), row=1, col=1)
            self.balance_list = self._res.prices * \
                                self.data.Close.iloc[
                                    0] / self._res.prices.iloc[0]

            self._bought_sold_profit(self.data, False)
            holding_profit = (self.data['Close'][-1]*100/self.data['Open'][0])-100

            fig.add_trace(go.Scatter(x=self.data.index, y=self.sold_list,
                                     mode='markers',
                                     name='Sold Points'),row=1, col=1)
            fig.add_trace(go.Scatter(x=self.data.index, y=self.bought_list,
                                     mode='markers',
                                     name='Bought points', text='Here'),row=1, col=1)
        else:
            self.balance_list = self._res._prices
            index = self.calc_of_index()
            self._bought_sold_profit(index, True)
            fig.add_trace(go.Scatter(x=index.index, y=index['strategy'], mode='lines', name='Index'),
                          row=1, col=1)
            fig.add_trace(go.Scatter(x=self.sold_list.index, y=self.sold_list,
                                     mode='markers',
                                     name='Sold Points'),row=1, col=1)
            fig.add_trace(go.Scatter(x=self.bought_list.index, y=self.bought_list,
                                     mode='markers',
                                     name='Bought points',text='Here'),row=1, col=1)
            # self.balance_list = self._res.prices * index.iloc[0] / self._res.prices.iloc[0]
            holding_profit = (index['strategy'][-1]*100/index['strategy'][0])-100

        table[2] = ['Index return', str(round(holding_profit,2))+'%']
        fig.add_trace(go.Scatter(x=self.balance_list.dropna().index, y=self.balance_list.dropna()['strategy'], mode='lines', name='Strategy ptice'), row=1, col=1)

        # fig.add_trace(go.Histogram(data=self.profits), row=2, col=2)


        fig.add_trace(
            go.Table(
                header=dict(
                    values=[table[i][0] for i in range(len(table))],
                    font=dict(size=10),
                    align="left"
                ),
                cells=dict(
                    values=[table[i][1] for i in range(len(table))],
                    align="left")
            ),
            row=2, col=1
        )

        fig.update_layout(xaxis_rangeslider_visible=False)  # , template='plotly_dark'
        print('Plotly time:', time.time()-start_time)
        # fig.show()
        plot(fig, filename='file.html')

    def strategy_update(self, strategy_params, data):
        test = get_multi_test(strategy_params, data)
        self._res = bt.run(test)

    def calc_of_index(self):
        if len(self.strategy_params['signal_params']['curr_generators'].items()) == 1:
            return self._res.prices * self._data.Close.iloc[0] / self._res.prices.iloc[0]

        else:
            params = self.strategy_params
            for x in list(self.strategy_params['signal_params']['curr_generators'].keys()):
                params['signal_params']['curr_generators'][x] = [{'generator': signals.Buy_n_hold, 'params': {}}]
            if 'skip_period' in list(params):
                period = int(params['skip_period'])
                params['skip_period'] = 0
                self.balance_list['strategy'] = self.balance_list['strategy'][period:]
                self.balance_list['strategy'].dropna()
                for x in self.data.keys():
                    self.data[x] = self.data[x].iloc[period:]

            test = get_multi_test(params, self.data)
            res = bt.run(test)
            return res._prices

    def _data_calculator(self, signals):
        if signals.empty:
            self._data = self._res.backtest_list[0].strategy.data_dict[
                next(iter(self._res.backtest_list[0].strategy.data_dict))]
            self._signals = pd.Series(0, index=self._data.index.values)
        else:
            self._signals = signals
            self._data = self._res.backtest_list[0].strategy.data_dict[list(self._signals[0].keys())[0]]

    def _bought_sold_profit(self, data, index):
        self.profitable_deals = 0
        self.loss_making_deals = 0
        if index:
            self.bought_list = pd.Series(index=data.index)
            self.sold_list = pd.Series(index=data.index)
        else:
            self.bought_list = pd.Series(index=data.index)
            self.sold_list = pd.Series(index=data.index)
        # self.profits = pd.DataFrame()
        self.profits = []
        prev_i = None
        for i in self._signals[self._signals != 0].index:
            if self._signals[i][list(self._signals[i])[0]] == 1:
                if index:
                    self.bought_list[i] = data['strategy'][i]
                else:
                    self.bought_list[i] = data.Open[i]
            elif prev_i is not None:
                if index:
                    self.sold_list[i] = data['strategy'][i]
                else:
                    self.sold_list[i] = data.Open[i]

                if self.balance_list[self.balance_list.index == i].values[0][0] >= self.balance_list[self.balance_list.index == prev_i].values[0][0]:
                    self.profitable_deals += 1
                else:
                    self.loss_making_deals += 1
                # self.profits[i] = float(self.balance_list[self.balance_list.index == i].values) / float(self.balance_list[self.balance_list.index == prev_i].values) - 1
                self.profits.append(float(self.balance_list[self.balance_list.index == i].values) / float(self.balance_list[self.balance_list.index == prev_i].values) - 1)
