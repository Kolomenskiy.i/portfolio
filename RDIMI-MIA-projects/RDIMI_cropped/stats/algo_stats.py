# -*- encode: utf-8 -*-

import numpy as np
import pandas as pd

from functools import wraps


def to_array(func):

    @wraps(func)
    def to_array_wrap(*args, **kwargs):
        args = list(args)
        args[0] = args[0].values if isinstance(args[0], pd.Series) else np.array(args[0].values)
        return func(*args, **kwargs)

    return to_array_wrap


@to_array
def sharp(ret_series, r=.16/365, to_dict=True):
    val = np.round((ret_series - r).mean() / (ret_series - r).std(), 4)
    return {'Sharp': val} if to_dict else val


@to_array
def sortino(ret_series, r=.16/365, to_dict=True):
    val = np.round((ret_series.mean() - r) / (r - ret_series[ret_series < 0]).std(), 6)
    return {'Sortino': val} if to_dict else val


@to_array
def volatility(ret_series, year=False, to_dict=True):
    volat = (np.exp(ret_series.std()) - 1.0) * 100.0
    val = np.round(volat * np.sqrt(365) if year else volat, 4)
    return {'{}Volatility'.format('Y' if year else ''): val} if to_dict else val


@to_array
def abs_return(balance_series, to_dict=True):
    val = np.round(balance_series[-1] - balance_series[0], 4)
    return {'Return': val} if to_dict else val


@to_array
def proc_return(balance_series, year=False, to_dict=True):
    if year:
        balance_series = np.log(balance_series[1:] / balance_series[:-1])[1:]
        val = (np.exp(np.mean(balance_series)) - 1) * np.sqrt(365)
    else:
        val = balance_series[-1] / balance_series[0] - 1
    val = np.round(val * 100.0, 4)
    return {'{}PReturn'.format('Y' if year else ''): val} if to_dict else val


@to_array
def trade_len(trade_days):

    trade_days = np.where(trade_days == 1)[0]
    trade_days = trade_days[1:] - trade_days[:-1]

    if len(trade_days) > 0:
        return {
            'MinTradeLen': np.min(trade_days).round(4),
            'MaxTradeLen': np.max(trade_days).round(4),
            'MeanTradeLen': np.mean(trade_days).round(4)
        }
    else:
        return {
            'MinTradeLen': 0,
            'MaxTradeLen': 0,
            'MeanTradeLen': 0
        }


@to_array
def max_drop(balance_series):

    result = dict()
    drops = []
    min_b, max_b = balance_series[0], balance_series[0]
    indrop = False
    for balance in balance_series[1:]:
        if balance > max_b:
            if indrop:
                drops.append([
                    np.round(min_b - max_b, 2),
                    np.round(min_b / max_b - 1.0, 6)])
                indrop = False
            min_b = balance
            max_b = balance
        elif balance < max_b:
            indrop = True
            min_b = balance if balance < min_b else min_b

    if indrop:
        drops.append([
            np.round(min_b - max_b, 2),
            np.round(min_b / max_b - 1.0, 6)])

    if len(drops) > 0:
        drops = np.array(drops)
        result['MaxAbsDrop'] = drops.min(axis=0)[0]
        result['MeanAbsDrop'] = drops[:, 0].mean()
        result['MaxRelDrop'] = np.round(drops.min(axis=0)[1] * 100.0, 4)
        result['MeanRelDrop'] = np.round(drops[:, 1].mean() * 100.0, 4)
    else:
        result['MaxAbsDrop'] = 0.0
        result['MeanAbsDrop'] = 0.0
        result['MaxRelDrop'] = 0.0
        result['MeanRelDrop'] = 0.0

    return result


def get_stat_columns():
    return ['AbsReturn', 'PReturn', 'Trades', 'PMeanReturn', 'PVolatility', 'PYearVolatility', 'Sharp', 'MaxAbsDrop', 'MeanAbsDrop', 'MaxRelDrop', 'MeanRelDrop']


def get_balance_stat(data, trades):
    result = dict()
    columns = ['AbsReturn', 'PReturn', 'Trades', 'PMeanReturn', 'PVolatility', 'PYearVolatility', 'Sharp', 'MaxAbsDrop', 'MeanAbsDrop', 'MaxRelDrop', 'MeanRelDrop']
    result['AbsReturn'] = np.round(data.Balance.iloc[-1] - data.Balance.iloc[0], 2)
    result['PReturn'] = np.round((data.Balance.iloc[-1] / data.Balance.iloc[0] - 1) * 100.0, 4)
    trade_days = np.where(data.Trades.values == 1)[0]
    trade_days = trade_days[1:] - trade_days[:-1]
    result['TradesMin'] = np.round(np.min(trade_days), 2)
    result['TradesMax'] = np.round(np.max(trade_days), 2)
    result['TradesMean'] = np.round(np.mean(trade_days), 2)
    result['PMeanReturn'] = np.round((np.exp(data.Return.mean()) - 1.0) * 100.0, 4)
    result['PVolatility'] = np.round((np.exp(data.Return.std()) - 1.0) * 100.0, 4)
    result['PYearVolatility'] = np.round((np.exp(data.Return.std() * np.sqrt(365)) - 1.0) * 100.0, 4)
    result['Sharp'] = sharp(data.Return, 0.16/365, to_dict=False)
    result['Sortino'] = sortino(data.Return, 0.16 / 365, to_dict=False)
    drops = []
    min_b, max_b = data.Balance.iloc[0], data.Balance.iloc[0]
    indrop = False
    for balance in data.Balance.iloc[1:]:
        if balance > max_b:
            if indrop:
                drops.append([
                    np.round(min_b - max_b, 2),
                    np.round(min_b / max_b - 1.0, 6)])
                indrop = False
            min_b = balance
            max_b = balance
        elif balance < max_b:
            indrop = True
            min_b = balance if balance < min_b else min_b

    if indrop:
        drops.append([
            np.round(min_b - max_b, 2),
            np.round(min_b / max_b - 1.0, 6)])

    if len(drops) > 0:
        drops = np.array(drops)
        result['MaxAbsDrop'] = drops.min(axis=0)[0]
        result['MeanAbsDrop'] = drops[:, 0].mean()
        result['MaxRelDrop'] = np.round(drops.min(axis=0)[1] * 100.0, 4)
        result['MeanRelDrop'] = np.round(drops[:, 1].mean() * 100.0, 4)
    else:
        result['MaxAbsDrop'] = 0.0
        result['MeanAbsDrop'] = 0.0
        result['MaxRelDrop'] = 0.0
        result['MeanRelDrop'] = 0.0

    return result, columns


def get_svr(data, kernel='rbf', degree=3, coef0=0.0, tol=0.001, C=1.0, epsilon=0.1,
                 shrinking=True, cache_size=200, verbose=True, max_iter=-1):

    Y = data.LogPProfit.as_matrix().reshape(len(data), 1)
    X = data[['Weight', 'Alpha', 'Period', 'Drop']].as_matrix()

    model = SVR(kernel=kernel, degree=degree, coef0=coef0, tol=tol, C=C, epsilon=epsilon,
                 shrinking=shrinking, cache_size=cache_size, verbose=verbose, max_iter=max_iter, gamma=1.0/X.shape[1])

    model.fit(X, Y)

    return model

def get_ridge(data, alpha=.5, copy_X=True, fit_intercept=True, max_iter=None,
      normalize=False, random_state=None, solver='auto', tol=0.001):

    Y = data.LogPProfit.as_matrix().reshape(len(data), 1)
    X = data[['Weight', 'Alpha', 'Period', 'Drop']].as_matrix()

    model = Ridge(alpha=alpha, copy_X=copy_X, fit_intercept=fit_intercept,
                  max_iter=max_iter, normalize=normalize, random_state=random_state, solver=solver, tol=tol)
    model.fit(X, Y)

    return model


# data = pd.read_csv('C:/Users/Danil/Documents/rdi_fund/portfolio_adx_tests.csv', sep=',', decimal='.')
# model = get_ridge(data)

# max_v, params = 0.0, []
# for w in range(1, 21, 1): # 20
#     for a in range(1, 101, 1): # 200
#         for p in range(5, 46, 1):
#             for d in range(75, 101, 1):
#                 rates = np.array([w / 20., a / 200., p, d / 100.])
#                 v = np.exp(model.predict(np.array([rates])))
#                 if v > max_v:
#                     max_v = v
#                     params = rates
#
#                 print(rates, v, max_v)
#
# print params, max_v