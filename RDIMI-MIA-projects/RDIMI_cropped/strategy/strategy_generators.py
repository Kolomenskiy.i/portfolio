# -*- encoding: utf-8 -*-

import bt.algos as btalgos

import cbt.base_algos as balgos
from cbt.core_custom import StrategyOHLC, BacktestOHLC
from strategy import signal_generators as signals

#from optimization.utils import load_generators


def get_multi_strategy(params, data_dict):
    timeframe_dict = {'1H': balgos.RunHourly, '1D': btalgos.RunDaily, '1W': btalgos.RunWeekly,
                      '1M': btalgos.RunMonthly}
    timeframe_algo = timeframe_dict.get(params['timeframe'].get('period', '1H'), balgos.RunHourly)(
        run_on_first_date=params['timeframe'].get('run_on_first_date', True),
        run_on_end_of_period=params['timeframe'].get('run_on_end_of_period', False),
        run_on_last_date=params['timeframe'].get('run_on_last_date', True)
    )

    algos = [
        btalgos.RunMonthly(),
        balgos.SelectAll(),
        btalgos.WeighEqually(),
        balgos.SkipPeriod(params['skip_period']),
        balgos.MultiSignalAlgo(**params['signal_params']),
        balgos.AddSavedSignals(),
        balgos.CheckForAction(),
        balgos.SelectSignalAlgo(),
        balgos.AddSelectedPosition(),
        balgos.DistinctSelectedAlgo(),
        balgos.DropSelectedSignal(),
        #balgos.HRPWeightCap(**params.get('hrp_params', {})),
        #balgos.ADXSecurityWeight(**params['adx_params']),
        btalgos.Rebalance()
    ]

    strategy = StrategyOHLC(params.get('name', 'strategy'), algos,
                            sec_amount_rounding=params.get('sec_amount_rounding', None))
    backtest = BacktestOHLC(strategy, data_dict,
                            integer_positions=params.get('integer_positions', True),
                            commissions=params.get('commissions', 0.0),
                            initial_capital=params.get('initial_capital', 100000.0),
                            progress_bar=params.get('progress_bar', False))


    return backtest


def get_multi_test(params, data_dict):
    timeframe_dict = {'1H': balgos.RunHourly(run_on_end_of_period=True),
                      '1D': btalgos.RunDaily(),
                      '1W': btalgos.RunWeekly(),
                      '1M': btalgos.RunMonthly(),
                      'DT': balgos.RunDataTimeframe()}
    timeframe_algo = timeframe_dict.get(params['timeframe'], 'DT')

    signal_params = {'curr_generators': {}}

    def iterate_generator(gen_params):
        generators = []
        combine = None
        combine_params = {}
        if isinstance(gen_params, dict):
            for key, current_dict in gen_params.items():
                try:
                    if 'combine' in current_dict.keys():
                        combine = current_dict['combine']
                        combine_params = current_dict.get('params', {})
                    else:
                        if isinstance(current_dict['generator'], list):
                            generators.append(iterate_generator(current_dict['generator']))
                        else:
                            generators.append(current_dict['generator'](current_dict.get('params', {})))
                except:  # TODO: try-except is dumb. Why functools.partial is in the current_dict?
                    print(key)
                    print(current_dict)
                    print(gen_params)
        elif isinstance(gen_params, list):
            for current_dict in gen_params:
                try:
                    if 'combine' in current_dict.keys():
                        combine = current_dict['combine']
                        combine_params = current_dict.get('params', {})
                    else:
                        if isinstance(current_dict['generator'], list):
                            generators.append(iterate_generator(current_dict['generator']))
                        else:
                            generators.append(current_dict['generator'](current_dict.get('params', {})))
                except:  # TODO: try-except is dumb. Why functools.partial is in the current_dict?
                    print(current_dict)
                    print(gen_params)
        else:
            print(gen_params)

        if combine is not None:
            generator = combine(generator_list=generators, params=combine_params)
        else:
            generator = generators[0]

        return generator

    for currency, gen_params in params['signal_params']['curr_generators'].items():
        signal_params['curr_generators'][currency] = iterate_generator(gen_params)

    algos = [
        timeframe_algo,
        balgos.SelectAll(),
        balgos.MultiSignalAlgo(**signal_params),
        balgos.CheckForAction(),
        balgos.SelectSignalAlgo(),  # algo for LONGS
        balgos.AddSelectedPosition(),
        balgos.DistinctSelectedAlgo(),
        balgos.DropSelectedSignal(),
        # btalgos.WeighMeanVar(),
        # balgos.WeighEqually(),
        balgos.WeighEqually_Sell_All_if_sell(),
        btalgos.Rebalance(),
        # btalgos.RebalanceOverTime(n=3000),
    ]

    strategy = StrategyOHLC(params.get('name', 'strategy'), algos,
                            sec_amount_rounding=params.get('sec_amount_rounding', None))
    backtest = BacktestOHLC(strategy, data_dict,
                            integer_positions=params.get('integer_positions', True),
                            commissions=params.get('commissions', None),
                            initial_capital=params.get('initial_capital', 100000.0),
                            progress_bar=params.get('progress_bar', False))

    return backtest


def get_predefined_strategy(params, signal_params_paths, data_dict):
    timeframe_dict = {'1H': balgos.RunHourly, '1D': btalgos.RunDaily, '1W': btalgos.RunWeekly,
                      '1M': btalgos.RunMonthly}
    timeframe_algo = timeframe_dict.get(params['timeframe'].get('period', '1H'), balgos.RunHourly)(
        run_on_first_date=params['timeframe'].get('run_on_first_date', True),
        run_on_end_of_period=params['timeframe'].get('run_on_end_of_period', False),
        run_on_last_date=params['timeframe'].get('run_on_last_date', True)
    )

    curr_signal_params_dict = {currency: load_generators(fpath) for currency, fpath in signal_params_paths.items()}

    def iterate_generator(gen_params):
        generators = []
        combine = None
        for key, value in gen_params.items():
            if key == 'combine':
                combine = value
            elif 'combine' in value.keys():
                generators.append(iterate_generator(value))
            else:
                generators.append(value['generator'](value.get('params', {})))

        if len(generators) == 1:
            generator = generators[0]
        else:
            generator = combine(generator_list=generators)

        return generator

    curr_generators = dict()
    for currency, signal_params_list in curr_signal_params_dict.items():
        generators_list = []
        dates_list = []
        for signal_params in signal_params_list:
            dates_list.append((signal_params['start_date'], signal_params['end_date']))
            generators_list.append(iterate_generator(signal_params['params'][currency]))

        curr_generators[currency] = signals.CombineByLastDates(generator_list=generators_list, periods_dates=dates_list)

    signal_params = {'curr_generators': curr_generators}

    algos = [
        timeframe_algo,
        balgos.SkipPeriod(params['skip_period']),
        balgos.MultiSignalAlgo(**signal_params),
        balgos.AddSavedSignalsLong(),
        balgos.CheckForAction(),
        balgos.SelectSignalAlgo(),
        balgos.AddSelectedPosition(),
        balgos.DistinctSelectedAlgo(),
        balgos.DropSelectedSignal(),
        btalgos.WeighEqually(),
        btalgos.Rebalance(),
        balgos.OnPeriodEndWithdrawal(**params.get('withdrawal', {'limit': 0.0, 'period': 'M'}))
    ]

    strategy = StrategyOHLC(params.get('name', 'strategy'), algos, sec_amount_rounding=params.get('sec_amount_rounding', None))
    backtest = BacktestOHLC(strategy, data_dict,
                            integer_positions=params.get('integer_positions', True),
                            commissions=params.get('commissions', None),
                            initial_capital=params.get('initial_capital', 100000.0),
                            progress_bar=params.get('progress_bar', False))

    return backtest
