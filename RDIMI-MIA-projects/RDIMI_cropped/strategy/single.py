from stats import stats as st
from strategy import strategy_generators as gen
from data import utils as ut
from strategy import signal_generators as sig
from optimiation import optimize as opt
import pylab as plt
from cbt.core_custom import BacktestOHLC
import bt
from strategy.strategy_generators import get_multi_strategy

if __name__ == '__main__':

    df = ut.get_data('../data/','BTC', 'USD', '1h', 'Bitstamp', start_date='2014-11-28 12:00:00', update_data=True)
    x = opt.AnnealSimulation(df, sig.BBSignalGenerator(), strategy_name='bb', learn_period='5W', test_period='3W')
    x.windows_optimising()
    #x.single_optimising()
    print('aaaa', x.final_params)
    print('bbb', x.final_result)
