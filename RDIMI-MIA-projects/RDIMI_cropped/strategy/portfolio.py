# -*- encode: utf-8 -*-

import bt.algos as btalgos

from cbt.core_custom import StrategyOHLCCap
import cbt.base_algos as balgos
import cbt.ind_algos as ialgos
import cbt.sltp_algos as stalgos
import cbt.stat_algos as saalgos

def adx_bt(name, cap_params, top_n, adx_params, fee_func, sltp_params=None):

    sltp = sltp_params is not None

    in_pos_stack = [
        balgos.InPosition(),
        balgos.CheckMarginCall()
    ]

    if sltp:
        in_pos_stack.append(stalgos.CloseSLTP(direction='long', sltp_type='sl', lower=True))

    in_pos_stack.extend([
        balgos.CheckFeeBankrupt(fee_func),
        balgos.CashClose(),
        balgos.ConditionalRebalance('wchange')
    ])

    if sltp:
        in_pos_stack.append(stalgos.RemoveStopped())

    in_pos_stack = btalgos.AlgoStack(*in_pos_stack)

    out_pos_stack = btalgos.AlgoStack(
        balgos.OutPosition(),
        balgos.CheckFeeBankrupt(fee_func),
        btalgos.Rebalance())

    algos = [
        btalgos.RunDaily(),
        saalgos.CapMAStat(**cap_params),
        btalgos.SelectN(top_n),
        btalgos.WeighEqually(),
        ialgos.FlowSingleADX(**adx_params),
        ialgos.FlowADXChange(),
        btalgos.Or([in_pos_stack, out_pos_stack])
    ]

    if sltp:
        algos.append(stalgos.SLTPStdMa(direction='long', sltp_type='sl', **sltp_params))

    strategy = StrategyOHLCCap(name, algos)

    return strategy