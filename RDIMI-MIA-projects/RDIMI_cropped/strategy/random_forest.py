from itertools import zip_longest

import numpy as np
import pandas as pd
import progressbar

df = pd.read_csv('1h_BTC_USD_Bitstamp.csv', sep=',')
open_list = df['open'].values
close_list = df['close'].values
min_list = df['low'].values
max_list = df['high'].values
time_list = df['time'].values
volume_list = df['volumefrom'].values

def no_repeat(div_list):#deletes first numbers 2 
    p = 0
    last_action = 0
    start = True
    start2 = True
    while p < len(div_list):
        if start:
            start = False
        else:
            if last_action == div_list[p]:
                div_list[p] = 0
        if div_list[p] != 0:
            if start2:
                if div_list[p] == 1:
                    start2 = False
                else:
                    div_list[p] = 0
            #else:
            last_action = div_list[p]
        p+=1
    return div_list

def adaptive_stop_loss(start_index, stop_index, step, stop_step, div_list, close_list1):
    max_profit = -100
    best_param = start_index
    while step >= stop_step:
        for i in np.arange(start_index, stop_index, step):
            balance = 1000000
            btc_balance = 0
            j = 0
            deals_count = 0
            div_list = no_repeat(div_list)
            bought_price = 0
            bought = False
            while j < len(div_list):
                if bought_price*i > close_list1[j] and bought == True:
                    balance = (float(balance) + float(btc_balance)*float(close_list1[j]))*0.997
                    btc_balance = 0
                    deals_count+=1
                    bought = False
                if div_list[j] == 1 and bought == False:#bought
                    btc_balance = (float(btc_balance) + float(balance)/float(close_list1[j]))*0.997
                    balance = 0
                    deals_count+=1
                    bought_price = close_list1[j]
                    bought = True
                if div_list[j] == 2 and bought == True:#sold
                    balance = (float(balance) + float(btc_balance)*float(close_list1[j]))*0.997
                    btc_balance = 0
                    deals_count+=1
                    bought = False
                j+=1     
            if balance == 0:
                balance = float(btc_balance) * float(open_list[len(open_list)-1])
            trading_profit = float(balance)/float(1000000)*float(100)
            profit = trading_profit-100

            if max_profit < profit:
                max_profit = profit
                best_param = i
            #print('max profit: ', max_profit)
            #print('i: ', i)
        start_index = best_param - step
        stop_index = best_param + step
        step/=10
    #print('Max profit: ', max_profit)
    #print('Best param: ', best_param)
    return best_param
def offline_for_rf(div_list): #auto remove of dublicates in div_list #0.3% commission = 0.1+0.2
    balance = 1000000
    btc_balance = 0
    i = 0
    deals_count = 0
    div_list = no_repeat(div_list)
    bought = False
    while i < len(div_list):
        if div_list[i] == 1 and bought == False:#bought
            btc_balance = (float(btc_balance) + float(balance)/float(close_list[i]))*0.997
            balance = 0
            deals_count+=1
            bought = True
        if div_list[i] == 2 and bought == True:#sold
            balance = (float(balance) + float(btc_balance)*float(close_list[i]))*0.997
            btc_balance = 0
            deals_count+=1
            bought = False
        i+=1    
    if (balance == 0):
        balance = float(btc_balance) * float(open_list[len(open_list)-1]) #!!!!!!!!len(open list) -> close list 
    trading_profit = float(balance)/float(1000000)*float(100)
    '''
    if deals_count > 300:
        return trading_profit - 100
    else: 
        return 0
    '''
    return trading_profit - 100
def pure_offline(div_list1): #dynamic_stop_loss is almost the same but better than this
    div_list = np.zeros(len(close_list))
    div_list[div_list1[1][div_list1[0]!=0].astype('uint16')] = div_list1[0][div_list1[0]!=0]
    div_list = no_repeat(div_list)
    balance = 1000000
    btc_balance = 0
    trading_profit = 0
    i = 0
    deals_count = 0
    bought_list = np.zeros(len(div_list))
    sold_list = np.zeros(len(div_list))
    bought_price = 0
    bought = False
    stop_loss = 0
    while i < len(close_list):
        
        if (i+1)%100 == 0:
            stop_loss = adaptive_stop_loss(0.7, 1.0, 0.01, 0.01, div_list[i-99:i+1], close_list[i-99:i+1])
            stop_loss = round(stop_loss, 2)
            if stop_loss == 0.7 or stop_loss == 1.0:
                stop_loss = 0.9
            #print('Stop loss: ', stop_loss)
        
        if bought_price*stop_loss > close_list[i] and bought == True: #BB: 1.0139 (1h) #1.09 (6h) stop is almost not needed (it happened only once) #stylus (1h) 1.008
            balance = (float(balance) + float(btc_balance)*float(close_list[i]))*0.997
            btc_balance = 0
            deals_count+=1
            bought = False
        
        if div_list[i] == 1 and bought == False:#bought
            btc_balance = (float(btc_balance) + float(balance)/float(close_list[i]))*0.997
            balance = 0
            bought_list[i] = close_list[i]
            deals_count+=1
            bought_price = close_list[i]
            bought = True
        if div_list[i] == 2 and bought == True:#sold
            balance = (float(balance) + float(btc_balance)*float(close_list[i]))*0.997
            btc_balance = 0
            sold_list[i] = close_list[i]
            deals_count+=1
            bought = False
        i+=1   
    if (balance == 0):
        balance = float(btc_balance) * float(open_list[len(open_list)-1]) #!!!!!!!!len(open list) -> close list 
    if deals_count>200:           
        trading_profit = float(balance)/float(1000000)*float(100)
    return trading_profit-100
 
def dynamic_stop_loss(name, div_list = [[-1],[-1]], index_list = -1, profit_flag = False):
    '''if profit_flag:
        div_list_copy = np.zeros(len(close_list))
        div_list_copy[div_list[1][div_list[0]!=0].astype('uint16')] = div_list[0][div_list[0]!=0]
        div_list = no_repeat(div_list_copy)'''
    if div_list[0] == -1:
        try:
            div_list = globals()[name]()
        except TypeError:
            div_list = globals()[name]
    elif profit_flag:
        div_list_copy = np.zeros(len(close_list))
        div_list_copy[div_list[1][div_list[0]!=0].astype('uint16')] = div_list[0][div_list[0]!=0]
        div_list = no_repeat(div_list_copy)
    balance = 1000000
    btc_balance = 0
    i = 0
    deals_count = 0
    div_list = no_repeat(div_list)
    bought_price = 0
    bought = False
    stop_loss = 0
    while i < len(close_list):
        if (i+1)%100 == 0:
            #prev_stop_loss = stop_loss
            stop_loss = adaptive_stop_loss(0.6, 1.0, 0.01, 0.01, div_list[i-99:i+1], close_list[i-99:i+1])
            stop_loss = round(stop_loss, 2)
            if stop_loss == 0.6 or stop_loss == 1.0:
                stop_loss = 0
            #print('Stop loss: ', stop_loss)
        
        if bought_price*stop_loss > close_list[i] and bought == True: #BB: 1.0139 (1h) #1.09 (6h) stop is almost not needed (it happened only once) #stylus (1h) 1.008
            balance = (float(balance) + float(btc_balance)*float(close_list[i]))*0.997
            btc_balance = 0
            deals_count+=1
            bought = False 
            div_list[i] = 2
        if div_list[i] == 1 and bought == False:#bought
            btc_balance = (float(btc_balance) + float(balance)/float(close_list[i]))*0.997
            balance = 0
            deals_count+=1
            bought_price = close_list[i]
            bought = True
        if div_list[i] == 2 and bought == True:#sold
            balance = (float(balance) + float(btc_balance)*float(close_list[i]))*0.997
            btc_balance = 0
            deals_count+=1
            bought = False
        i+=1    
    if balance == 0:
        balance = float(btc_balance) * float(open_list[len(open_list)-1])
    trading_profit = float(balance)/float(1000000)*float(100)
    if profit_flag:
        return trading_profit
    else:
        return div_list

def create_combinations2(big_list):
    all_combo_list = np.zeros((50000, 2, 20000))
    all_recepy_list = np.empty((50000, 16))
    list_number = 0
    i = 0

    tree_amount = len(big_list)

    while i<tree_amount:
        x = big_list[i][big_list[i]!=0]
        all_combo_list[list_number][0][:len(x)] = x
        all_combo_list[list_number][1][:len(x)] = list(np.argwhere(big_list[i]))
        all_recepy_list[list_number][0] = i+1
        print('ACL',x )
        i+=1
        list_number+=1

    comb_counter = 0
    comb_counter2 = 0
    amount_of_comb = 2
    variable_for_3d_array = 1
    same_list_check = -1
    amount_in_rest_sequence = list_number
    prev_amount_in_rest_sequence = 0
    print('Amount of lists: ', amount_in_rest_sequence)
    while amount_of_comb<=tree_amount:
        if amount_of_comb>2:
            prev_amount_in_rest_sequence = amount_in_rest_sequence
        amount_in_rest_sequence = list_number
        comb_counter = 0
        print(amount_of_comb,'in work')
        while comb_counter<tree_amount-1:
            div_list2 = all_combo_list[comb_counter]
            print('Adding tree #',comb_counter)
            comb_counter2 = prev_amount_in_rest_sequence   

            while comb_counter2 <= amount_in_rest_sequence:
                #print('cc2',comb_counter2)
                div_list1 = all_combo_list[comb_counter2]
                #same_index = np.intersect1d(div_list1[1], div_list2[1])
                #print('Same index: ', same_index)
                #print(div_list1[1][:10])
                #print(div_list2[1][:10])   
                check1 = False
                check2 = False
                almost_over_check = False         
                div_list1_active = False
                element = 0
                i = 0
                y = 0
                b = 0
                dlina = 0
                first = True
                if len(div_list1[1][div_list1[1]!=0])>len(div_list2[1][div_list2[1]!=0]):
                    dlina = len(div_list1[1][div_list1[1]!=0])
                else:
                    dlina = len(div_list2[1][div_list2[1]!=0])      
                while b<2*dlina:
                    if comb_counter2 == same_list_check:
                        list_number-=4 

                    if div_list1[1][i] > div_list2[1][y] and int(div_list2[1][y]) != 0 and first == False and almost_over_check == False:
                        y+=1

                    elif div_list2[1][y]>div_list1[1][i] and int(div_list1[1][i]) != 0 and first == False and almost_over_check == False:
                        i+=1

                    elif div_list1[1][i] == div_list2[1][y] and first == False and almost_over_check == False:
                        i+=1
                        y+=1    

                    if int(div_list1[1][i]) == 0 and int(div_list2[1][y]) !=0:
                        check2 = True

                    if int(div_list1[1][i]) != 0 and int(div_list2[1][y]) ==0:
                        check1 = True

                    first = False
                    if (div_list1[1][i]<=div_list2[1][y] or check1 == True) and check2 == False:
                        element = int(div_list1[1][i])
                        div_list1_active = True

                    elif (div_list1[1][i]>div_list2[1][y] or check2 == True) and check1 == False:
                        element = int(div_list2[1][y])
                        div_list1_active = False
                    
                    if div_list1_active:
                        all_combo_list[list_number][0][b] = div_list1[0][i]
                        all_combo_list[list_number][1][b] = element
                    else:
                        all_combo_list[list_number][0][b] = div_list2[0][y]
                        all_combo_list[list_number][1][b] = element      

                    all_recepy_list[list_number] = all_recepy_list[comb_counter2]
                    all_recepy_list[list_number][variable_for_3d_array] = 101
                    all_recepy_list[list_number][variable_for_3d_array+1] = comb_counter+1
                    list_number+=1   
                    
                    if div_list1_active and div_list1[0][i] == 1:
                            all_combo_list[list_number][0][b] = div_list1[0][i]
                            all_combo_list[list_number][1][b] = element 

                    elif (div_list1_active == False) or (div_list1[1][i]==div_list2[1][y] and div_list1[0][i] != 1): 
                        all_combo_list[list_number][0][b] = div_list2[0][y]
                        all_combo_list[list_number][1][b] = element          

                    all_recepy_list[list_number] = all_recepy_list[comb_counter2]
                    all_recepy_list[list_number][variable_for_3d_array] = 102
                    all_recepy_list[list_number][variable_for_3d_array+1] = comb_counter+1
                    list_number+=1   
                    
                    if div_list1_active and div_list1[0][i] == 2:
                        all_combo_list[list_number][0][b] = div_list1[0][i]
                        all_combo_list[list_number][1][b] = element 

                    elif (div_list1_active == False) or (div_list1[1][i]==div_list2[1][y] and div_list1[0][i] != 2): 
                        all_combo_list[list_number][0][b] = div_list2[0][y]
                        all_combo_list[list_number][1][b] = element

        

                    all_recepy_list[list_number] = all_recepy_list[comb_counter2]
                    all_recepy_list[list_number][variable_for_3d_array] = 103
                    all_recepy_list[list_number][variable_for_3d_array+1] = comb_counter+1
                    list_number+=1   
                    
                    if div_list1_active and div_list1[0][i] == 1:
                        all_combo_list[list_number][0][b] = div_list1[0][i]
                        all_combo_list[list_number][1][b] = element
                    elif (div_list1_active == False and div_list2[0][y] == 2) or (div_list1[1][i]==div_list2[1][y] and div_list1[0][i] != 1 and div_list2[0][y] == 2):
                            all_combo_list[list_number][0][b] = div_list2[0][y]
                            all_combo_list[list_number][1][b] = element 


                    all_recepy_list[list_number] = all_recepy_list[comb_counter2]
                    all_recepy_list[list_number][variable_for_3d_array] = 104
                    all_recepy_list[list_number][variable_for_3d_array+1] = comb_counter+1
                    list_number+=1 
                    
                    same_list_check = comb_counter2
                    b+=1



                comb_counter2+=1   
            comb_counter+=1
        amount_of_comb+=1
        variable_for_3d_array+=2
    all_combo_list = all_combo_list[:list_number]
    all_recepy_list = all_recepy_list[:list_number]
    max_percent = -100
    x = tree_amount
    bar = progressbar.ProgressBar(maxval=list_number).start() 
    while x<list_number:
        bar.update(x)
        #percent = pure_offline(all_combo_list[x])
        percent = dynamic_stop_loss('RF', all_combo_list[x], -1, True)
        if percent > max_percent:
            best_recepy = all_recepy_list[x]
            max_percent = percent 
        x+=1
    bar.finish()
       
    print('Best percent: ',max_percent)
    print('Recepy: ', best_recepy)
    '''
    f = open('list1.txt', 'w')
    f.write(str(all_combo_list))
    f.close()
    print('Here')
    f = open('list2.txt', 'w')
    f.write(str(all_recepy_list)) 
    f.close()
    print('Im here')      
    '''
    return best_recepy

def nearest_non_zero(lst, idx):
    if lst[idx] > 0:
        return -100
    before, after = lst[:idx], lst[idx+1:]
    for b_val, a_val in zip_longest(reversed(before), after, fillvalue=0):
        if b_val > 0:
            return b_val
        if a_val > 0:
            return -100
    else:
        return -100  # all zeroes in this list

def create_combinations3(big_list):
    all_combo_list = np.empty((5000000,len(close_list)))
    all_recepy_list = np.empty((5000000,10))
    cur_list_number = -1
    list_number = 0
    for list_number in range (len(big_list)):
        all_combo_list[list_number] = big_list[list_number]
        all_recepy_list[list_number] = list_number+1
    print('LN', list_number)    
    for lenght_of_cons in range (3):
        print('Making: ', lenght_of_cons)
        prev_list_number = cur_list_number+1
        cur_list_number = list_number
        for first in range (prev_list_number,cur_list_number):
            print('PLN', prev_list_number)
            print('CLN', cur_list_number)
            print('First: ', first)
            for second in range (prev_list_number,cur_list_number):
                print('Second: ', second)
                start = True
                for i in range (len(close_list)):
                    non_zero_element = False
                    if start == False:
                        first_time = False
                        list_number-=2
                    start = False    
                    if all_combo_list[first][i]!=0:
                        closest_value = nearest_non_zero(all_combo_list[second],i)
                        close = False
                        non_zero_element = True
                        if i - closest_value <= 50 and i - closest_value>=0:
                            print('ACL1', all_combo_list[first][i-50:i])
                            print('ACL2', all_combo_list[second][i-50:i])
                            close = True

                    if non_zero_element:
                        all_combo_list[list_number][i] = all_combo_list[first][i]
                    else:
                        all_combo_list[list_number][i] = all_combo_list[second][i]
                    all_recepy_list[list_number] = all_recepy_list[first]
                    np.append(all_recepy_list[list_number],101)
                    np.append(all_recepy_list[list_number], all_recepy_list[second]) 

                    list_number+=1

                    if non_zero_element and close and all_combo_list[first][i] == all_combo_list[second][closest_value]:
                        all_combo_list[list_number][i] = all_combo_list[first][i]
                    all_recepy_list[list_number] = all_recepy_list[first]
                    np.append(all_recepy_list[list_number],102)
                    np.append(all_recepy_list[list_number], all_recepy_list[second])     
                    list_number+=1
                    '''
                    if all_combo_list[first][i] == 1:
                        all_combo_list[list_number][i] = 1
                    elif all_combo_list[second][i] == 2:
                        all_combo_list[list_number] = 2
                    all_recepy_list[list_number] = all_recepy_list[first]
                    np.append(all_recepy_list[list_number],103)
                    np.append(all_recepy_list[list_number], all_recepy_list[second])       
                    list_number+=1 

                    if all_combo_list[first][i] == 1:
                        all_combo_list[list_number][i] = 1
                    elif all_combo_list[second][i] != 0:
                        all_combo_list[list_number] = all_combo_list[second][i]
                    all_recepy_list[list_number] = all_recepy_list[first]
                    np.append(all_recepy_list[list_number],104)
                    np.append(all_recepy_list[list_number], all_recepy_list[second])        
                    list_number+=1 

                    if all_combo_list[first][i] == 2:
                        all_combo_list[list_number][i] = 2
                    elif all_combo_list[second][i] != 0:
                        all_combo_list[list_number] = all_combo_list[second][i]
                    all_recepy_list[list_number] = all_recepy_list[first]
                    np.append(all_recepy_list[list_number],105)
                    np.append(all_recepy_list[list_number], all_recepy_list[second])       
                    list_number+=1 
                    '''



    all_combo_list = all_combo_list[:list_number]
    all_recepy_list = all_recepy_list[:list_number]
    max_percent = -100
    x = len(big_list)
    bar = progressbar.ProgressBar(maxval=list_number).start() 
    while x<list_number:
        bar.update(x)
        #percent = pure_offline(all_combo_list[x])
        percent = dynamic_stop_loss('RF', all_combo_list[x], -1, True)
        if percent > max_percent:
            best_recepy = all_recepy_list[x]
            max_percent = percent 
        x+=1
    bar.finish()
       
    print('Best percent: ',max_percent)
    print('Recepy: ', best_recepy)
    '''
    f = open('list1.txt', 'w')
    f.write(str(all_combo_list))
    f.close()
    print('Here')
    f = open('list2.txt', 'w')
    f.write(str(all_recepy_list)) 
    f.close()
    print('Im here')      
    '''
    return best_recepy

def recepy_recreator(final_list,tree_list):
    recepy = final_list[1][:10]
    recepy = recepy[:(np.where(recepy == 0)[0][0])]
    i = 2
    div_list1 = tree_list[int(recepy[0])]
    while i<len(recepy):
            div_list2 = tree_list[int(recepy[i])-1]
            div_list1 = combine_for_rf(div_list1,div_list2,recepy[i-1])
            i+=2
    return(div_list1)        
 
def combine_for_rf(div_list1,div_list2,method):
    div_list3 = np.empty(len(div_list1))
    i = 0   
    if method == 101:
        for i in range (len(close_list)):
            if div_list1[i] != 0:
                div_list3[i] = div_list1[i]
            elif div_list2[i] != 0:
                div_list3[i] = div_list2[i]          
    i = 0
    if method == 102:        
        for i in range (len(close_list)):
            close = False
            closest_value = nearest_non_zero(all_combo_list[second],i)
            if i - closest_value <= 50 and i - closest_value>=0:
                close = True
            if div_list1[i] != 0 and close and div_list1[i] == div_list2[closest_value]:
                div_list3[i] = div_list1[i]         
    
    i = 0
    if method == 103:        
        while i<len(close_list):
            if div_list1[i] == 1:
                div_list3[i] = div_list1[i]
            elif div_list2[i] == 2:
                div_list3[i] = div_list2[i]    
            i+=1         
        i = 0
    if method == 104:        
        while i<len(close_list):
            if div_list1[i] == 1:
                div_list3[i] = div_list1[i]
            elif div_list2[i] != 0:
                div_list3[i] = div_list2[i]    
            i+=1      
        i = 0
    if method == 105:        
        while i<len(close_list):
            if div_list1[i] == 2:
                div_list3[i] = div_list1[i]
            elif div_list2[i] != 0:
                div_list3[i] = div_list2[i]    
            i+=1         
            
    return div_list3

def recepy_recreator(recepy,tree_list):
    recepy = recepy[recepy!=0]
    print('Arr',recepy)
    i = 1
    div_list1 = tree_list[int(recepy[0])-1]
    while i<len(recepy):
            div_list2 = tree_list[int(recepy[i+1])-1]
            div_list1 = combine_for_rf(div_list1,div_list2,recepy[i])
            i+=2
    return(div_list1)        


def create_combinations4(big_list):
    acl = np.zeros((500000,2,2000))
    arl = np.empty((5000000,10))
    cur_list_number = -1
    rec_var = -1
    list_number = 0
    for a in range (len(big_list)):
        fill_in_list = np.nonzero(big_list[a])[0]
        #print('FIN1', fill_in_list)
        acl[a][0][:len(fill_in_list)] = fill_in_list
        acl[a][1][:len(fill_in_list)] = big_list[a][fill_in_list]
        arl[a][0] = a+1
        list_number+=1
    for lenght_of_cons in range (4):
        print('Making: ', lenght_of_cons+2)
        prev_list_number = cur_list_number+1
        cur_list_number = list_number
        rec_var+=2
        for first in range (len(big_list)):
            for second in range (prev_list_number,cur_list_number):
                start = True
                count1 = 0
                count2 = 0
                count3 = 0
                while True:
                    if start == False:
                        list_number-=1
                    if abs(acl[first][0][count1]-acl[second][0][count2])<50 and acl[first][1][count1]==acl[second][1][count2]:
                        a = int(acl[first][0][count1])
                        b = int(acl[second][0][count2])
                        if a > b:
                            acl[list_number][0][count3] = acl[first][0][count1]
                            acl[list_number][1][count3]  = acl[first][1][count1]
                        else:
                            acl[list_number][0][count3]  = acl[second][0][count1]
                            acl[list_number][1][count3]  = acl[first][1][count1]
                    if start:
                        arl[list_number] = arl[second]
                        arl[list_number][rec_var] = 101
                        arl[list_number][rec_var+1] = first+1
                    list_number+=1     

                    if acl[first][0][count1] <= acl[second][0][count2]:
                        acl[list_number][0][count3]  = acl[first][0][count1]
                        acl[list_number][1][count3]  = acl[first][1][count1]
                    else:
                        acl[list_number][0][count3]  = acl[second][0][count2]
                        acl[list_number][1][count3]  = acl[first][1][count1]
                    if start:      
                        arl[list_number] = arl[count1]
                        arl[list_number][rec_var] = 102
                        arl[list_number][rec_var+1] = second
                    count3+=1
                    start = False 
                    if acl[first][0][count1] == 0 and acl[second][0][count2] == 0:
                        break
                    elif acl[first][0][count1] == 0:
                        count2+=1
                    elif acl[second][0][count2] == 0:
                        count1+=1            
                    elif acl[first][0][count1]<=acl[second][0][count2]:
                        count1+=1
                    else:
                        count2+=1    
    print('arl', arl[:list_number])                    
    max_profit = -100 
    profit = -100                
    for x in range (list_number):
        div_list = np.zeros(len(close_list))
        for i in range (len(acl[x][0][acl[x][0]!=0])):
            div_list[int(acl[x][0][i])] = int(acl[x][1][i])
        profit = offline_for_rf(div_list)
        if profit > max_profit:
            max_profit = profit
            print('Max profit: ', max_profit)
            print('Best recepy: ',arl[x])
    print('MP', max_profit)        
            
def combine_for_rf(div_list1,div_list2,method):
    div_list3 = np.zeros(len(div_list1))
    i = 0
    method = int(method)
    if method == 101:
        while i<len(close_list):
            if div_list1[i] != 0:
                div_list3[i] = div_list1[i]
            if div_list2[i] != 0 and div_list1[i] == 0:
                div_list3[i] = div_list2[i]          
            i+=1
    i = 0
    if method == 102:        
        while i<len(close_list):
            if div_list1[i] == 1:
                div_list3[i] = div_list1[i]
            elif div_list2[i] != 0:
                div_list3[i] = div_list2[i]          
            i+=1 
   
    i = 0
    if method == 103:        
        while i<len(close_list):
            if div_list1[i] == 2:
                div_list3[i] = div_list1[i]
            elif div_list2[i] != 0:
                div_list3[i] = div_list2[i]    
            i+=1 
    i = 0

    if method == 104:        
        while i<len(close_list):
            if div_list1[i] == 1:
                div_list3[i] = div_list1[i]
            elif div_list2[i] == 2:
                div_list3[i] = div_list2[i]    
            i+=1             
    return div_list3
