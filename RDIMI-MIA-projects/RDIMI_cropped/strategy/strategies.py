import time

import numpy as np
import pandas as pd
import pylab as plt
import xlwt
from scipy.signal import argrelextrema

from indicator import indicators as ind
from stats import statistics_parameters as stat_params

np.set_printoptions(threshold=np.inf) #full output list

timeframe = '1H' 
df = pd.read_csv('1h_BTC_USD_Bitstamp.csv', sep=',', parse_dates=True, index_col=0)
df_ohlc = df
#df_ohlc = df['close'].resample(timeframe).ohlc()
#volume_list = df['volumefrom'].resample(timeframe).sum().values
open_list = df_ohlc['open'].values
close_list = df_ohlc['close'].values
min_list = df_ohlc['low'].values
max_list = df_ohlc['high'].values
volume_list = df_ohlc['volumefrom'].values



def offline(**kwargs):
    """
    Examples of params usage: name='divergence', div_list=[0, 1, 2, 0...0], stop_loss=0.9, take_profit=1.1, 
    get_profit=True, save_image=True, log_scale=True, dark_background=True, grid=False, get_excel_stats=True
    additional_charts=[
    {'data': close_list, 'color': 'grey', 'type': 'point_chart', 'special_params': {'alpha': 0.1, 'label': 'close_list'}}, 
    {'type': 'horizontal_chart', 'data': [3000, 19500], 'color': 'red'}, 
    {'data': ind.rsi(close_list, 14), 'priority': 'secondary', 'color': 'purple'}, 
    {'data': ind.rsi(close_list, 14)*2, 'priority': 'secondary', 'special_params': {'linewidth': 0.1}}]
    """
    start_time = time.time()
    
    stop_loss = kwargs.get('stop_loss', 0)
    take_profit = kwargs.get('take_profit', float('+inf'))
    additional_charts = kwargs.get('additional_charts', [])
    get_profit = kwargs.get('get_profit', False)
    get_excel_stats = kwargs.get('get_excel_stats', False)
    save_image = kwargs.get('save_image', False)
    log_scale = kwargs.get('log_scale', False)
    dark_background = kwargs.get('dark_background', False)
    grid = kwargs.get('grid', True)
    name = kwargs.get('name', 'strategy name')
    div_list = kwargs.get('div_list', [-1])
    if div_list[0] == -1:
        if name != 'strategy name':
            try:
                div_list = globals()[name]()
            except TypeError:
                div_list = globals()[name]
    div_list = no_repeat(div_list)

    bought_list = [None]*len(close_list)
    sold_list = [None]*len(close_list)
    stoploss_list = [None]*len(close_list)
    takeprofit_list = [None]*len(close_list)

    bought_price = 0
    bought_balance = 0

    r = []
    profit_list = []
    profitable_deals = 0
    loss_deals = 0

    balance_list = np.empty(len(close_list))
    balance_list[0] = open_list[0]
    bought = False

    last_year = df.index[0].year
    last_month = df.index[0].month
    start_balance = open_list[0]
    start_price = open_list[0]
    local_r = []
    local_profitable_deals = 0
    local_loss_deals = 0

    if get_excel_stats:
        wb = xlwt.Workbook()
        w_profit = wb.add_sheet(name)
        w_profit.write(0,0,'Date')
        w_profit.write(0,1,'BTC/USD price')
        w_profit.write(0,2,'Strategy price')
        w_profit.write(0,3,'Profit')
        w_profit.write(0,4,'Holding profit')
        w_profit.write(0,5,'Amount of deals')
        w_profit.write(0,6,'Profitable/loss-making deals')
        w_profit.write(0,7,'Max drawdown')
        w_profit.write(0,8,'Average drawdown')
        w_profit.write(0,9,'Sharpe')
        w_profit.write(0,10,'Sortino')
        w_profit.write(0,11,'Volatility')
        count=1
    
    for i in range(1, len(open_list)):
        if div_list[i] == 0:
            if bought:
                if open_list[i] < bought_price*stop_loss: #stoploss
                    balance_list[i] = open_list[i]/bought_price*bought_balance*0.997*0.997
                    bought = False
                    r.append(balance_list[i]/bought_balance-1)
                    local_r.append(balance_list[i]/bought_balance-1)
                    profit_list.append((balance_list[i]/balance_list[0]-1)*100)
                    if balance_list[i] > bought_balance:
                        profitable_deals+=1
                    stoploss_list[i] = open_list[i]
                elif open_list[i] > bought_price*take_profit: #takeprofit
                    balance_list[i] = open_list[i]/bought_price*bought_balance*0.997*0.997
                    bought = False
                    r.append(balance_list[i]/bought_balance-1)
                    local_r.append(balance_list[i]/bought_balance-1)
                    profit_list.append((balance_list[i]/balance_list[0]-1)*100)
                    if balance_list[i] > bought_balance:
                        profitable_deals+=1
                    takeprofit_list[i] = open_list[i]
                else:
                    balance_list[i] = open_list[i]/bought_price*bought_balance*0.997
                    r.append(balance_list[i]/bought_balance-1) #true stats!!!!!!!!!
                    local_r.append(balance_list[i]/bought_balance-1)
            else:
                balance_list[i] = balance_list[i-1]
        elif div_list[i] == 1 and not bought: #buy
            bought_balance = balance_list[i-1]
            bought_price = open_list[i]
            balance_list[i] = open_list[i]/bought_price*bought_balance*0.997
            bought = True
            bought_list[i] = open_list[i]
        elif div_list[i] == 2 and bought: #sell
            balance_list[i] = open_list[i]/bought_price*bought_balance*0.997*0.997
            bought = False
            r.append(balance_list[i]/bought_balance-1)
            local_r.append(balance_list[i]/bought_balance-1)
            profit_list.append((balance_list[i]/balance_list[0]-1)*100)
            if balance_list[i] > bought_balance: #compare (balance after sell) with (balance before buy without comission)
                profitable_deals+=1
                local_profitable_deals+=1
            else:
                loss_deals+=1
                local_loss_deals+=1
            sold_list[i] = open_list[i]
        elif div_list[i] == 2 and not bought:
            balance_list[i] = balance_list[i-1]

        if get_excel_stats: #makes excel monthly/yearly stats 
            current_year = df.index[i].year
            current_month = df.index[i].month
            if last_month != current_month or i == len(df.index)-1:
                local_amount_of_deals = local_profitable_deals+local_loss_deals
                local_holding_profit = round((close_list[i]/start_price*0.997*0.997-1)*100, 2)
                if local_amount_of_deals != 0:
                    local_r = np.array(local_r) 
                    local_e = np.mean(local_r)
                    f = 0.0 
                    if local_loss_deals != 0:
                        local_amount_of_profitable_to_loss_deals = round(local_profitable_deals/local_loss_deals, 2)
                    else:
                        local_amount_of_profitable_to_loss_deals = str(local_profitable_deals) + '/' + str(local_loss_deals)
                    local_profit = round((balance_list[i]/start_balance-1)*100, 2)
                    local_sharpe = round(stat_params.sharpe_ratio(local_e, local_r, f), 2)
                    local_sortino = round(stat_params.sortino_ratio(local_e, local_r, f), 2)
                    local_volatility = round(stat_params.vol(local_r), 2)
                    local_max_drawdown = round(stat_params.max_dd(local_r), 2)
                    local_average_drawdown = round(stat_params.average_dd(local_r, len(local_r)), 2)
                    w_profit.write(count,0,str(last_year) + '-' + str(last_month))
                    w_profit.write(count,1,str(open_list[i-1]))
                    w_profit.write(count,2,str(balance_list[i-1]))
                    w_profit.write(count,3,str(local_profit))
                    w_profit.write(count,4,str(local_holding_profit))
                    w_profit.write(count,5,str(local_amount_of_deals))
                    w_profit.write(count,6,str(local_amount_of_profitable_to_loss_deals))
                    w_profit.write(count,7,str(local_max_drawdown))
                    w_profit.write(count,8,str(local_average_drawdown))
                    w_profit.write(count,9,str(local_sharpe))
                    w_profit.write(count,10,str(local_sortino))
                    w_profit.write(count,11,str(local_volatility))
                    count+=1
                else:
                    w_profit.write(count,0,str(last_year) + '-' + str(last_month))
                    w_profit.write(count,1,str(open_list[i-1]))
                    w_profit.write(count,2,str(balance_list[i-1]))
                    w_profit.write(count,3,None)
                    w_profit.write(count,4,str(local_holding_profit))
                    w_profit.write(count,5,str(local_amount_of_deals))
                    w_profit.write(count,6,None)
                    w_profit.write(count,7,None)
                    w_profit.write(count,8,None)
                    w_profit.write(count,9,None)
                    w_profit.write(count,10,None)
                    w_profit.write(count,11,None)
                    count+=1
                last_month = current_month
                last_year = current_year
                start_balance = balance_list[i]
                start_price = close_list[i]
                local_e = np.mean(local_r)
                local_r = []
                local_profitable_deals = 0
                local_loss_deals = 0
            if i == len(df.index)-1:
                wb.save(name + '.xls')

    if get_profit: #returns profit and exit
        return (balance_list[-1]/balance_list[0]-1)*100

    r = np.array(r) #balance list in other format
    e = np.mean(r)
    f = 0.0 #risk-free procent
    
    profit = round((balance_list[-1]/balance_list[0]-1)*100, 2)
    holding_profit = round((close_list[-1]/open_list[0]*0.997*0.997-1)*100, 2)
    number_of_profitable_to_loss_deals = 0
    if loss_deals != 0:
        number_of_profitable_to_loss_deals = round(profitable_deals/loss_deals, 2)
    else:
        number_of_profitable_to_loss_deals = str(profitable_deals) + '/' + str(loss_deals)
    number_of_deals = profitable_deals+loss_deals
    sharpe = round(stat_params.sharpe_ratio(e, r, f), 2)
    sortino = round(stat_params.sortino_ratio(e, r, f), 2)
    volatility = round(stat_params.vol(r), 2)
    #max_drawdown = round(stat_params.max_dd(r), 2)
    #average_drawdown = round(stat_params.average_dd(r, len(r)), 2)
    time_spent = round(time.time()-start_time, 2)
    print(f'{name}\nProfit: {profit}\nHolding profit: {holding_profit}\n' + 
        f'Number of deals: {number_of_deals}\nProfitable deals/loss-making deals: {number_of_profitable_to_loss_deals}\n' +
        #f'Max drawdown: {max_drawdown}\nAverage drawdown: {average_drawdown}\n' + 
        f'Sharpe: {sharpe}\nSortino: {sortino}\nVolatility: {volatility}\nTime spent: {time_spent}')

    if dark_background:
        plt.style.use('dark_background')
    #plt.switch_backend('Qt5Agg') 
    plt.figure(figsize=(24, 12))
    ax1 = plt.subplot2grid((6,6), (0,1), rowspan=5, colspan=5)
    if log_scale:
        plt.yscale('log')
    ax2 = plt.subplot2grid((6,6), (5,1), rowspan=1, colspan=5, sharex=ax1)

    ax1.set_title(name + ' ' + timeframe)
    ax1.plot(df_ohlc.index, open_list, label='btc open list')
    ax1.plot(df_ohlc.index, balance_list, label='strategy', color='orange')
    ax1.plot(df_ohlc.index, takeprofit_list, linestyle='', marker='v', label='take profit sell', color='hotpink')
    ax1.plot(df_ohlc.index, stoploss_list, linestyle='', marker='v', label='stop loss sell', color='gold')
    ax1.plot(df_ohlc.index, sold_list, linestyle='', marker='v', label='sell', color='red')
    ax1.plot(df_ohlc.index, bought_list, linestyle='', marker='^', label='buy', color='green')

    priority_ax = ax1
    for chart in additional_charts:
        if 'priority' in chart:
            if 'secondary' in chart['priority']:
                priority_ax = ax2
        chart_params = {}
        if 'special_params' in chart:
            chart_params.update(chart['special_params'])
        if 'color' in chart:
            chart_params.update({'color': chart['color']})
        if 'type' in chart:
            if 'point_chart' in chart['type']:
                chart_params.update({'linestyle': '', 'marker': '.'})
                priority_ax.plot(df_ohlc.index, chart['data'], **chart_params)
            elif 'horizontal_chart' in chart['type']:
                for lines in chart['data']:
                    priority_ax.axhline(lines, **chart_params)
        else:
            priority_ax.plot(df_ohlc.index, chart['data'], **chart_params)

    ax1.xaxis_date()
    ax1.legend(loc='upper left', frameon=False)

    if priority_ax is ax1:
        ax2.fill_between(df_ohlc.index, volume_list, 0)

    ax2.text(0.0, -0.3, 
    f'Profit: {profit}\nHolding profit: {holding_profit}', verticalalignment='top', weight='heavy', transform=ax2.transAxes)
    ax2.text(0.25, -0.3, 
    f'Number of deals: {number_of_deals}\nProfitable deals/loss-making deals: {number_of_profitable_to_loss_deals}', verticalalignment='top', transform=ax2.transAxes)
    #ax2.text(0.6, -0.3, 
    #f'Max drawdown: {max_drawdown}\nAverage drawdown: {average_drawdown}', verticalalignment='top', transform=ax2.transAxes)
    ax2.text(0.85, -0.3, 
    f'Sharpe: {sharpe}\nSortino: {sortino}\nVolatility: {volatility}', verticalalignment='top', transform=ax2.transAxes)
    
    ax3 = plt.axes([.025, .5, .2, .35])
    ax3.set_title('Histogram of profits')
    ax3.hist(r, density=True, bins=100)
    ax3.axvline(0, color='red')

    ax4 = plt.axes([.025, .1, .2, .35])
    ax4.set_title('Chart of profits')
    ax4.plot(profit_list)
    ax4.axhline(0, color='red')

    mng = plt.get_current_fig_manager()
    mng.window.state('zoomed') 

    ax1.grid(grid)
    ax2.grid(grid)
    ax3.grid(grid)
    ax4.grid(grid)

    if save_image:
        plt.savefig(name + '.png', bbox_inches='tight')
    plt.show()

    return (balance_list[-1]/balance_list[0]-1)*100

def no_repeat(div_list):#deletes first sells (i.e. 2) and last buys (i.e. 1)
    p = 0
    last_action = 0
    last_action_index = 0
    start = True
    start2 = True
    while p < len(div_list):
        if start:
            start = False
        else:
            if last_action == div_list[p]:
                div_list[p] = 0
        if div_list[p] != 0:
            if start2:
                if div_list[p] == 1:
                    start2 = False
                else:
                    div_list[p] = 0
            #else:
            last_action = div_list[p]
            last_action_index = p
        p+=1
    if last_action == 1:
        div_list[last_action_index] = 0
    return div_list

def rsi_level_strategy(): 
    rsi_list = ind.rsi(close_list, 14)
    i = 0
    div_list=[0 for x in range(len(close_list))]
    while i < len(close_list):
        if (rsi_list[i] > 80.0 and rsi_list[i] < rsi_list[i-1]):#sell
            div_list[i] = 2
        if (rsi_list[i] < 20.0 and rsi_list[i] > rsi_list[i-1]):#buy
            div_list[i] = 1
        i+=1
    return div_list
      
def marked_list():
    div_list=[0 for x in range(len(close_list))]
    local_max = 0
    local_max_index = 0
    local_min = 0
    local_min_index = 0
    p = 1
    while p < len(close_list)-1:
        if close_list[p-1] < close_list[p] and close_list[p] > close_list[p+1]:#local max
            local_max = close_list[p]
            local_max_index = p
        if close_list[p-1] > close_list[p] and close_list[p] < close_list[p+1]:#local min
            local_min = close_list[p]
            local_min_index = p
        if local_max != 0 and local_min != 0:
            if local_max/local_min*0.9965**2 > 1:
                div_list[local_min_index] = 1
                div_list[local_max_index] = 2
        p+=1
    p = 0
    last_action = 0
    last_action_index = 0
    start = True
    while p < len(close_list)-1:
        if start:
            start = False
        else:
            if last_action == div_list[p]:
                if div_list[p] == 1:
                    if close_list[last_action_index] > close_list[p]:
                        div_list[last_action_index] = 0
                    else:
                        div_list[p] = 0
                if div_list[p] == 2:
                    if close_list[last_action_index] > close_list[p]:
                        div_list[p] = 0
                    else:
                        div_list[last_action_index] = 0
        if div_list[p] != 0:
            last_action = div_list[p]
            last_action_index = p
        p+=1
    return div_list
   
def rsi_trend(): #too good to be true
    rsi_list = ind.rsi(close_list, 14)
    k = 1
    div_list=[0 for x in range(len(close_list))]
    while k < len(close_list):
        if rsi_list[k] > 50:
            div_list[k-1] = 1
        else:
            div_list[k-1] = 2
        k+=1
    return div_list
     
def simple_trade():
    div_list=[0 for x in range(len(close_list))]
    k = 1
    start = True
    price = 1000000
    while k < len(close_list):
        if start and close_list[k-1] < close_list[k]:
            price = close_list[k]
            div_list[k] = 1
            start = False
        if close_list[k] > price*(0.999**(-1))*1.001 or close_list[k]*1.05 < price:
            div_list[k] = 2
            start = True
        k+=1
    return div_list
     
def waves():
    div_list=[0 for x in range(len(close_list))]
    k = 1
    while k < len(close_list):
        if close_list[k-1] < close_list[k]:
            div_list[k] = 1
        else:
            div_list[k] = 2
        k+=1
    return div_list
   
def divergence(): #sees th future
    rsi_list = ind.rsi(close_list, 14)
    k = 1
    div_list = np.zeros(len(close_list))
    uptrend = False
    downtrend = False
    if (rsi_list[k-1] < rsi_list[k]) and (close_list[k-1] < close_list[k]): #rsi_list[k] sees the future
        uptrend = True
    if (rsi_list[k-1] > rsi_list[k]) and (close_list[k-1] > close_list[k]):
        downtrend = True
    max_count = 0
    min_count = 0
    start_max = True
    start_min = True
    while k < len(close_list):
        if ((rsi_list[k-1] > rsi_list[k]) and uptrend and (close_list[k-1] > close_list[k])):#local max
            if (start_max):
                max_count = k-1
                start_max = False
            elif (k-1 - max_count > 5):
                if ((rsi_list[k-1] > rsi_list[max_count]) and (close_list[k-1] < close_list[max_count])) or ((rsi_list[k-1] < rsi_list[max_count]) and (close_list[k-1] > close_list[max_count])):
                    div_list[k] = 2
                max_count = k-1
        if ((rsi_list[k-1] < rsi_list[k]) and downtrend and (close_list[k-1] < close_list[k])):#local min
            if (start_min):
                min_count = k-1
                start_min = False
            elif (k-1 - min_count > 5):
                if ((rsi_list[k-1] > rsi_list[min_count]) and (close_list[k-1] < close_list[min_count])) or ((rsi_list[k-1] < rsi_list[min_count]) and (close_list[k-1] > close_list[min_count])):
                    div_list[k] = 1
                min_count = k-1
        uptrend = False
        downtrend = False
        if (rsi_list[k-1] < rsi_list[k]) and (close_list[k-1] < close_list[k]):
            uptrend = True
        elif (rsi_list[k-1] > rsi_list[k]) and (close_list[k-1] > close_list[k]):
            downtrend = True
        k+=1
    return div_list
   
def old_divergence(): #doesn't see the future
    rsi_list = ind.rsi(close_list, 14)
    deals_count = 0
    k = 1
    div_list=[0 for x in range(len(close_list))]
    uptrend = False
    if (rsi_list[k-1] < rsi_list[k]):
        uptrend = True
    max_count = 0
    min_count = 0
    start_max = True
    start_min = True
    while k < len(close_list):
        if ((rsi_list[k-1] > rsi_list[k]) and uptrend):#local max
            if (start_max):
                max_count = k
                start_max = False
            elif (k - max_count > 5):
                if ((rsi_list[k] > rsi_list[max_count]) and (close_list[k] < close_list[max_count])) or ((rsi_list[k] < rsi_list[max_count]) and (close_list[k] > close_list[max_count])):
                    if (rsi_list[k]!=0):
                        div_list[k+1] = 2
                        deals_count+=1
                max_count = k
        if ((rsi_list[k-1] < rsi_list[k]) and not uptrend):#local min
            if (start_min):
                min_count = k
                start_min = False
            elif (k - min_count > 5):
                if ((rsi_list[k] > rsi_list[min_count]) and (close_list[k] < close_list[min_count])) or ((rsi_list[k] < rsi_list[min_count]) and (close_list[k] > close_list[min_count])):
                    if (rsi_list[k]!=0):
                        div_list[k+1] = 1
                        deals_count+=1
                min_count = k
        if (rsi_list[k-1] > rsi_list[k]):
            uptrend = False
        else:
            uptrend = True
        k+=1
    #print('Deals count: ', deals_count)
    return div_list
   
def newest_divergence(): #idk whether it sees the future 
    rsi_list = ind.rsi(close_list, 14)
    k = 1
    div_list = np.zeros(len(close_list))
    local_max = np.zeros(len(close_list))
    local_min = np.zeros(len(close_list))
    uptrend = False
    downtrend = False
    if (rsi_list[k-1] < rsi_list[k]) and (close_list[k-1] < close_list[k]):
        uptrend = True
    if (rsi_list[k-1] > rsi_list[k]) and (close_list[k-1] > close_list[k]):
        downtrend = True
    start_max = True
    start_min = True
    while k < len(close_list):
        if ((rsi_list[k-1] > rsi_list[k]) and uptrend and (close_list[k-1] > close_list[k])):#local max
            local_max[k-1] = rsi_list[k-1]
            if start_max:
                start_max = False
            else:
                i = k-1
                first_max_index = k-1
                max_rsi = 0
                max_index = k-1
                pre_max_index = k-1
                while i >= 0:
                    if local_max[i] != 0:
                        if first_max_index - i > 2 and first_max_index - i < 4:
                            if max_rsi < rsi_list[i]:
                                if rsi_list[i] == max(local_max):
                                    max_index = i
                                else:
                                    pre_max_index = i
                                    max_rsi = rsi_list[i]
                    i-=1
                if ((rsi_list[max_index] > rsi_list[pre_max_index]) and (close_list[max_index] < close_list[pre_max_index])) or ((rsi_list[max_index] < rsi_list[pre_max_index]) and (close_list[max_index] > close_list[pre_max_index])):
                    div_list[k] = 2
                    #print(max_index, pre_max_index, k)
        if ((rsi_list[k-1] < rsi_list[k]) and downtrend and (close_list[k-1] < close_list[k])):#local min
            local_min[k-1] = rsi_list[k-1]
            if start_min:
                start_min = False
            else:
                i = k-1
                first_min_index = k-1
                min_rsi = 100
                min_index = k-1
                pre_min_index = k-1
                while i >= 0:
                    if local_min[i] != 0:
                        if first_min_index - i > 2 and first_min_index - i < 4:
                            if min_rsi > rsi_list[i]:
                                if rsi_list[i] == min(local_min):
                                    min_index = i
                                else:
                                    pre_min_index = i
                                    min_rsi = rsi_list[i]
                    i-=1
                if ((rsi_list[min_index] > rsi_list[pre_min_index]) and (close_list[min_index] < close_list[pre_min_index])) or ((rsi_list[min_index] < rsi_list[pre_min_index]) and (close_list[min_index] > close_list[pre_min_index])):
                    div_list[k] = 1
                    #print(min_index, pre_min_index, k)
        uptrend = False
        downtrend = False
        if (rsi_list[k-1] < rsi_list[k]) and (close_list[k-1] < close_list[k]):
            uptrend = True
        elif (rsi_list[k-1] > rsi_list[k]) and (close_list[k-1] > close_list[k]):
            downtrend = True
        k+=1
    return div_list
   
def fibonacci_retracement():
    div_list=[0 for x in range(len(close_list))]
    local_min = close_list[1]
    local_max = close_list[1]
    min_counter = 0
    max_counter = 0
    i = 0
    while i<len(close_list):
        if  close_list[i] < local_min:
            local_min = close_list[i]
            min_counter = i
            max_counter = 0
            local_max = 0
            div_list[i] = 2
        if close_list[i] > local_max:
            local_max = close_list[i]    
            max_counter = i  
        if (i-min_counter<(len(close_list)/4)):
            if (min_counter<max_counter) and (local_min*1.02<local_max):
                line=[0 for x in range(5)]
                delta = local_max-local_min
                k = 1
        
                line[1] = float(local_max - (delta*0.786))
                line[2] = float(local_max - (delta*0.618))
                line[3] = float(local_max - (delta*0.5))
                line[4] = float(local_max - (delta*0.382))
                   
                while k<4:
                    if (close_list[i] > line[k]) and (close_list[i]<line[k]*1.003) and (close_list[i]>close_list[i-1]): 
                        div_list[i] = 1
                    k = k+1
                k = 0
                while k<4:
                    if (close_list[i] < line[k]) and (close_list[i]>line[k]*0.995) and (close_list[i]<close_list[i-1]): 
                        div_list[i] = 2
                    k = k+1
        else:
            local_min = close_list[i]
            min_counter = i
            local_max = close_list[i]    
            max_counter = i
            div_list[i] = 2
        i = i+1
    '''#is it doing anything?
    if close_list[i-4]*1.03<close_list[i-2] and close_list[i]<close_list[i-1] and div_list[i] == 0:
        div_list[i] = 2
    '''
    return div_list
     
def stylus():
    mom = ind.momentum(close_list, 10) #10
    mac = ind.macd(close_list) 
    sm = ind.sma(close_list, 7) #7
    div_list = np.zeros(len(close_list))
    i = 1
    while i<len(mac):
        if close_list[i-1]<sm[i-1] and close_list[i]>sm[i] and mac[i] > 0 and mom[i-1]<mom[i] and mom[i]>28: #For 1h down - 5 and * #For 6h all - 0 and 28 For 1h all - dunno For 15m all - 7.3 and 16
            div_list[i] = 2
        elif close_list[i-1]>sm[i-1] and close_list[i]<sm[i] and mac[i] < 14 and mom[i-1]>mom[i] and mom[i]<11: #For 1h down - 6 and *  For 6h all - 14 and 11 For 1h all - 10.4 and 48 For 15m all - 10.4 and -20
            div_list[i] = 1
        i+=1 
    return div_list      
   
def average_price_action_v1():
    uptrend = False
    downtrend = False
    max_count = 0
    min_count = 0
    start_max = True
    start_min = True
    buy_price = 0.0
    sell_price = 0.0
    div_list=[0 for x in range(len(close_list))]
    k = 1
    while k < len(close_list):
        if close_list[k-1] > close_list[k] and uptrend:#local max
            if (start_max):
                max_count = k-1
                start_max = False
            else:
                if close_list[k-1] < close_list[max_count]:
                    if close_list[k] > buy_price*1.0011:
                        div_list[k] = 2
                        sell_price = close_list[k]
                max_count = k-1
        if close_list[k-1] < close_list[k] and downtrend:#local min
            if (start_min):
                min_count = k-1
                start_min = False
            else:
                if close_list[k-1] > close_list[min_count]:
                    if close_list[k] < sell_price*0.9989:
                        div_list[k] = 1
                        buy_price = close_list[k]
                min_count = k-1
        uptrend = False
        downtrend = False
        if close_list[k-1] < close_list[k]:
            uptrend = True
        elif close_list[k-1] > close_list[k]:
            downtrend = True
        k+=1
    return div_list

def triangles(): 
    n=len(close_list)
    min_list=[0 for x in range(2)]
    max_list=[0 for x in range(2)]
    #div_list=[0 for x in range(n)]
    div_list = np.zeros(n)
    i=1
    op=40
    max_counter=0
    min_counter=0
    drctn = -1
    ln = 0
    if close_list[0] < close_list[1]:
        drctn = 1
    check_index_min=0
    check_index_max=0
    while ln+op < n-1:
        while i < ln+op:
            if drctn == 1 and close_list[i-1] > close_list[i]:
                if max_counter>=1 and abs(max_list[max_counter-1]/close_list[i-1]-1.0)<0.005 and max_list[max_counter-1]<close_list[i-1]:
                    max_list[max_counter-1]=close_list[i-1]
                elif max_counter<=1:
                    max_list[max_counter]=close_list[i-1]                   
                    if max_list[check_index_max]>max_list[max_counter]:
                        check_index_max=max_counter
                    max_counter=max_counter+1
                elif close_list[i-1]>max_list[check_index_max]:
                    j=check_index_max
                    while j<1:
                        max_list[j]=max_list[j+1]
                        j=j+1
                    max_list[1]=close_list[i]
                    j=0
                    check_index_max=0
                    while j<2:
                        if max_list[check_index_max]>max_list[j]:
                            check_index_max=j
                        j=j+1
                drctn=-1
            elif close_list[i-1] < close_list[i]:
                if min_counter>=1 and abs(1.0-min_list[min_counter-1]/close_list[i-1])<0.005:
                    if close_list[i-1]<min_list[min_counter-1]:
                        min_list[min_counter-1]=close_list[i-1]
                elif min_counter<=1:
                    min_list[min_counter]=close_list[i-1]                   
                    if min_list[check_index_min]<min_list[min_counter]:
                        check_index_min=min_counter
                    min_counter=min_counter+1
                elif close_list[i-1]<min_list[check_index_min]:
                    j=check_index_min
                    while j<1:
                        min_list[j]=min_list[j+1]
                        j=j+1
                    min_list[1]=close_list[i-1]
                    j=0
                    check_index_min=0
                    while j<2:
                        if min_list[check_index_min]<min_list[j]:
                            check_index_min=j
                        j=j+1
                drctn=1
            #print(min_list+max_list)
            if max_counter==2 and min_counter==2 and i-ln>=9:
                fuck=0.007
                if i-ln <=14:
                    fuck =0.004
                if abs(1.0-((max_list[1]/max_list[0]*max_list[1] + min_list[1]/min_list[0]*min_list[1])/2.0)/close_list[i])<=fuck:
                    if max_list[1]/max_list[0] < max_list[1]/(min_list[0]+max_list[1]-min_list[1]) and max_list[1]/max_list[0]>=1.0 and min_list[1]/close_list[i]>0.95:
                        div_list[i]=1
                        ln=i
                        max_counter=0
                        min_counter=0
                        check_index_min=0
                        check_index_max=0
                    if max_list[1]/max_list[0] < max_list[1]/(min_list[0]+max_list[1]-min_list[1]) and min_list[0]/min_list[1]>=1.0 and max_list[1]/close_list[i]<1.05:
                        div_list[i]=2
                        ln=i   
                        max_counter=0
                        min_counter=0
                        check_index_min=0
                        check_index_max=0
            '''             
            if close_list[i-4]*1.03<close_list[i-2] and close_list[i]<close_list[i-1] and div_list[i] == 0:
                div_list[i] = 2  
                ln=i   
                max_counter=0
                min_counter=0
                check_index_min=0
                check_index_max=0 
            '''                           
            i=i+1
            if i>=n:
                i=ln+op
        ln=ln+1
        j=ln-op
        while j<ln:
            if close_list[j]==min_list[0]:
                min_list[0]=min_list[1]
                min_list[1]=0
                min_counter=0
                check_index_min=0
            if close_list[j]==max_list[0]:
                max_list[0]=min_list[1]
                max_list[1]=0
                max_counter=0
                check_index_max=0
            j=j+1
        #print (max_list+min_list)
        #print(ln)
    return div_list
   
def BB_strategy(): 
    div_list = np.zeros(len(close_list))
    BB_TL = ind.up_bb(close_list, 20)
    BB_BL = ind.low_bb(close_list, 20)
    for i in range(len(close_list)-1):
        if min_list[i] > BB_TL[i]:
            div_list[i+1] = 1
        if max_list[i] < BB_BL[i]:
            div_list[i+1] = 2
    return div_list
  
def BB_strategy_new(): 
    div_list = np.zeros(len(close_list))
    BB_TL = ind.up_bb(close_list, 20)
    BB_BL = ind.low_bb(close_list, 20)
    dif = len(close_list)-len(BB_TL)
    i = dif
    green_zone = 0.0005
    #green_zone = 0
    while i < len(BB_TL)-1: 
        if min_list[i]*(1+green_zone) > BB_TL[i-dif]:
            div_list[i+1] = 1
        if max_list[i]*(1-green_zone) < BB_BL[i-dif]:
            div_list[i+1] = 2
        i+=1
    return div_list
  
def bull_dog():
    #div_list = [0 for i in range(len(close_list))]
    div_list = np.zeros(len(close_list))
    max1_ind = 0
    max2_ind = 0
    the_max1 = 0
    the_max2 = 0
    max1 = False
    bought = False
    bought_price = 0
    min1_ind = 0
    min2_ind = 0
    the_min1 = 30000
    the_min2 = 30000
    min1 = False
    i = 0
    while i < len(close_list):
         
        if close_list[i]>the_max2 and max1 == True:
            the_max2 = close_list[i] 
            max2_ind = i
            #print('a')
 
        if close_list[i]>the_max1:
            if close_list[i]>the_max1*1.03 or i<max1_ind+7:
                the_max1 = close_list[i]
                max1 = True
                max1_ind = i
            #print('b')
 
        if i>max1_ind+10:
            max1_ind = 0
            max2_ind = 0
            the_max1 = 0
            the_max2 = 0
            max1 = False
            #print('c')
             
        #if abs(max2_ind - max1_ind)<15 and max1 == True and abs(the_max1-the_max2)<close_list[i]*0.07 and max2_ind>max1_ind+3 and abs(i-min1_ind)>5:
        if abs(max2_ind - max1_ind)<10 and max1 == True and abs(the_max1-the_max2)<close_list[i]*0.05 and max2_ind>max1_ind+3 and abs(i-min1_ind)>10:  
            div_list[i] = 2
            max1_ind = 0
            max2_ind = 0
            the_max1 = 0
            the_max2 = 0
            max1 = False
            bought_price = 0
            #print('d')
 
        if close_list[i]<the_min2 and min1 == True:
            the_min2 = close_list[i] 
            min2_ind = i
            #print('e')
 
        if close_list[i]<the_min1:
            if close_list[i]<the_min1*0.97 or i<min1_ind+7:
                the_min1 = close_list[i]
                min1 = True
                min1_ind = i
           #print('f')
         
        #15
        if i>min1_ind+10:
            min1_ind = 0
            min2_ind = 0
            the_min1 = close_list[i]*5
            the_min2 = close_list[i]*5
            min1 = False
            #print('g')
 
        #if abs(min2_ind - min1_ind)< 10 and min1 == True and abs(the_max1-the_max2)<close_list[i]*0.1 and min2_ind>min1_ind+5 and abs(i-max2_ind)>10:
        if abs(min2_ind - min1_ind)< 20 and min1 == True and abs(the_max1-the_max2)<close_list[i]*0.1 and min2_ind>min1_ind+5 and abs(i-max2_ind)>10:    
            div_list[i] = 1
            min1_ind = 0
            min2_ind = 0
            the_min1 = 0
            the_min2 = 0
            min1 = False
            bought_price = close_list[i]   
        '''
        if i>1:
            if close_list[i-1]>close_list[i]*1.15 and div_list[i] == 0:
                div_list[i] = 1
                agr_price = close_list[i]
                agr = 1
                bought = True
                #print('a')
                 
            if close_list[i] > close_list[i-1]*1.05 and agr == 1 and div_list[i] == 0:
                div_list[i] = 2
                agr = 0
                bought = False
                #print('b')
        '''
        if bought_price*1.<close_list[i] and bought == True:
            div_list[i] = 2
            bought_price = 0
            bought = False
            #print('c')
         
        if close_list[i-4]>close_list[i-2]*1.15 and abs(close_list[i]-close_list[i-2])<100:
            div_list[i] = 1
            bought_price = close_list[i]   
            bought = True
            #print('d')
        i+=1    
    return div_list


def market_profile_big():
    min_price = int(min(min_list))
    max_price = int(max(max_list))
    price_count = 0
    total_price_count = 0
    price_index = 0
    price_list = np.empty(int((max_price-min_price)/5)+1)
    price_count_list = np.empty(int((max_price-min_price)/5)+1)
    for i in range(min_price, max_price, 5):
        for j in range(len(close_list)):
            if max_list[j] >= i and min_list[j] <= i+5:
                price_count+=1
        total_price_count+=price_count
        price_list[price_index] = i
        price_count_list[price_index] = price_count
        price_count = 0
        price_index+=1
        
    total_price_count*=0.7
    indices = price_count_list.argsort()
    price_list = price_list[indices]
    price_count_list = price_count_list[indices]
    #np.savetxt('price_list.txt', price_list)
    i = price_count_list[0]
    count = len(price_count_list)-1
    min_price_area = price_list[count]
    max_price_area = price_list[count]

    while i < total_price_count:
        if price_list[count] < min_price_area:
            min_price_area = price_list[count]
        elif price_list[count] > max_price_area:
            max_price_area = price_list[count]
        count-=1
        i+=price_count_list[count]

    div_list = [0 for x in range(len(close_list))]
    for i in range(len(close_list)):
        if close_list[i] > max_price_area:
            div_list[i] = 2
        if close_list[i] < min_price_area:
            div_list[i] = 1
    return div_list

def market_profile():
    min_list_copy = df['low'].values
    max_list_copy = df['high'].values
    div_list = [0 for x in range(len(close_list))]


    max_price_area_list = [None]*len(close_list)
    min_price_area_list = [None]*len(close_list)
    mp_range = 1000

    for q in range(0, len(div_list)-2*mp_range, 1000): #len(div_list)-1000
        min_list = min_list_copy[q:q+mp_range]
        max_list = max_list_copy[q:q+mp_range]
        min_price = int(min(min_list))
        max_price = int(max(max_list))
        price_count = 0
        total_price_count = 0
        price_index = 0
        step = 1
        price_list = np.empty(int((max_price-min_price)/step)+1)
        #print('len(price_list):', len(price_list))
        price_count_list = np.empty(int((max_price-min_price)/step))
        for i in range(min_price, max_price, step):
            for j in range(len(min_list)):
                if max_list[j] >= i and min_list[j] <= i+step:
                    price_count+=1
            #print('price_index:', price_index, 'i:', i)
            total_price_count+=price_count
            price_list[price_index] = i
            #price_list = np.delete(price_list, 0)          #whats this???
            price_count_list[price_index] = price_count
            price_count = 0
            price_index+=1
        #print('old price_list:', price_list)
        total_price_count*=0.7
        indices = price_count_list.argsort()        #time spent? 
        price_list = price_list[indices]
        price_count_list = price_count_list[indices]
        #print('new price_list:', price_list)
        i = price_count_list[0]
        count = len(price_count_list)-1
        min_price_area = price_list[count]
        max_price_area = price_list[count]
        while i < total_price_count and count >= 0:
            if price_list[count] < min_price_area:
                min_price_area = price_list[count]
            elif price_list[count] > max_price_area:
                max_price_area = price_list[count]
            count-=1
            i+=price_count_list[count]


        '''if not flag:
            flag = True
        else:'''
        for i in range(mp_range, 2*mp_range):
            if close_list[q+i] > max_price_area:
                div_list[q+i] = 2
            if close_list[q+i] < min_price_area:
                div_list[q+i] = 1
        #flag = False

        '''if close_list[q] > max_price_area:
            div_list[q] = 2
            max_price_area_list[q] = max_price_area
            max_price_area_list[q+1] = max_price_area
            min_price_area_list[q] = min_price_area
            min_price_area_list[q+1] = min_price_area
        if close_list[q] < min_price_area:
            div_list[q] = 1
            max_price_area_list[q] = max_price_area
            max_price_area_list[q+1] = max_price_area
            min_price_area_list[q] = min_price_area
            min_price_area_list[q+1] = min_price_area'''

        for i in range(mp_range):
            max_price_area_list[q+i] = max_price_area
            min_price_area_list[q+i] = min_price_area


    #offline('market_profile', div_list=div_list, maximize_window=True, additional_charts=[max_price_area_list, min_price_area_list]) 
    return div_list

"""
def accuracy(div_list):#auto remove of dublicates in div_list
    i = 0
    deals_count = 0
    div_list = no_repeat(div_list)
    bought_price = 0
    pos_deals_count = 0
    while i < len(div_list):
        if div_list[i] == 1:#bought
            deals_count+=1
            bought_price = close_list[i]
        if div_list[i] == 2:#sold
            deals_count+=1
            if close_list[i]*0.997 > bought_price:
                pos_deals_count+=2
        i+=1     
    return pos_deals_count/deals_count 

def voting_gain():
    gain_list = np.empty(len(functions))
    for i in range(tree_amount):
        gain_list[i] = offline_for_rf(tree_list[i])
    div_list = np.zeros(len(close_list))
    average_gain = sum(gain_list)/len(gain_list)
    for i in range(len(close_list)):
        buy_sum = 0
        buy_count = 0
        sell_sum = 0
        sell_count = 0
        for j in range(len(tree_list)):
            if tree_list[j][i] == 1:
                buy_sum+=offline_for_rf(tree_list[j])
                buy_count+=1
            if tree_list[j][i] == 2:
                sell_sum+=offline_for_rf(tree_list[j])
                sell_count+=1
        if buy_count != 0 and buy_sum/buy_count > average_gain:
            div_list[i] = 1
        if buy_count == 0:
            buy_count = 1
        if sell_count != 0 and sell_sum/sell_count > average_gain and buy_sum/buy_count < sell_sum/sell_count:
            div_list[i] = 2
    return div_list

def voting_accuracy():
    accuracy_list = np.empty(len(functions))
    for i in range(tree_amount):
        #print('Accuracy: ', accuracy(globals()[i]()))
        accuracy_list[i] = accuracy(tree_list[i])
    div_list = np.zeros(len(close_list))
    average_accuracy = sum(accuracy_list)/len(accuracy_list)
    for i in range(len(close_list)):
        buy_sum = 0
        buy_count = 0
        sell_sum = 0
        sell_count = 0
        for j in range(len(tree_list)):
            if tree_list[j][i] == 1:
                buy_sum+=accuracy_list[j]
                buy_count+=1
            if tree_list[j][i] == 2:
                sell_sum+=accuracy_list[j]
                sell_count+=1
        if buy_count != 0 and buy_sum/buy_count > average_accuracy+0.1:
            div_list[i] = 1
        if buy_count == 0:
            buy_count = 1
        if sell_count != 0 and sell_sum/sell_count > average_accuracy+0.1 and buy_sum/buy_count < sell_sum/sell_count:
            div_list[i] = 2
    return div_list

def voting_gain_no_average():
    gain_list = np.empty(len(functions))
    for i in range(tree_amount):
        gain_list[i] = offline_for_rf(tree_list[i])
    #average_gain = sum(gain_list)/len(gain_list)

    accuracy_list = np.empty(len(functions))
    for i in range(tree_amount):
        accuracy_list[i] = accuracy(tree_list[i])
    average_accuracy = sum(accuracy_list)/len(accuracy_list)

    div_list = np.zeros(len(close_list))
    for i in range(len(close_list)):
        max_percent = -1000
        for j in range(len(tree_list)):
            if gain_list[j] > max_percent and tree_list[j][i] != 0 and accuracy_list[j] > average_accuracy:
                max_percent = gain_list[j]
                div_list[i] = tree_list[j][i]
    return div_list
"""

def sma_strategy_new():
    div_list = np.zeros(len(close_list))
    sma_list = ind.sma(close_list, 200)
    for i in range(len(close_list)-1):
        if sma_list[i] > max_list[i]:#0.993 (280 deals 1502%) #0.935 (50 deals 2107%)
            div_list[i+1] = 2
        if sma_list[i] < min_list[i] and sma_list[i] != 0:
            div_list[i+1] = 1
    return div_list

def sma_strategy():
    div_list = np.zeros(len(close_list))
    sma_list = ind.sma(close_list, 200)
    for i in range(len(close_list)-1):
        if sma_list[i]*0.993 > close_list[i]:#0.993 (280 deals 1502%) #0.935 (50 deals 2107%)
            div_list[i+1] = 2
        if sma_list[i] < close_list[i]*0.993 and sma_list[i] != 0:
            div_list[i+1] = 1
    return div_list

"""
def candle_analyze():
    div_list = np.zeros(len(close_list))
    start = True
    prev_difference = 1000000
    for i in range(len(close_list)):
        if start:
            prev_difference = close_list[i]-open_list[i]
            start = False
        else:
            if abs(close_list[i]-open_list[i]) > abs(prev_difference)*1000:
                if close_list[i] > open_list[i]:
                    div_list[i] = 1
                else:
                    div_list[i] = 2
            prev_difference = close_list[i]-open_list[i]
    return div_list

def martingale():
    balance = 1000000
    btc_balance = 0
    start_btc_price = open_list[0]
    i = 1
    deals_count = 0
    bought_list = np.zeros(len(close_list))
    sold_list = np.zeros(len(close_list))
    bought_price = 0
    bought = False
    amount = 100
    amount_copy = amount
    balance-=amount
    while i < len(close_list) or balance > 0:
        #if amount == 0:
            #balance-=float(btc_balance)*float(open_list[i-1])

        if close_list[i-1] - open_list[i-1] > 0:#green
            btc_balance = float(amount_copy)/float(open_list[i])*0.997
            bought_list[i] = open_list[i]
            deals_count+=1
            bought_price = open_list[i]
            bought = True
            if close_list[i] - open_list[i] <= 0:
                balance-=2.5*amount_copy-amount_copy
                btc_balance*=2.5
            amount = 0
        if close_list[i-1] - open_list[i-1] <= 0:#red
            amount = float(btc_balance)*float(open_list[i])*0.997
            btc_balance = 0
            sold_list[i] = open_list[i]
            deals_count+=1
            bought = False
            if close_list[i] - open_list[i] > 0:
                balance-=2.5*amount-amount 
                amount*=2.5
        i+=1    
    print('Amount: ', amount)
    #balance+=amount
    
    print(name)
    holding_profit = float(close_list[len(close_list)-1])/float(start_btc_price)*float(100)
    print('Holding profit: ' + str(holding_profit-100))
    if balance == 0:
        balance = float(btc_balance) * float(open_list[len(open_list)-1]) #!!!!!!!!len(open list) -> close list 
    trading_profit = float(balance)/float(1000000)*float(100)
    print('Trading profit: ' + str(trading_profit-100))
    print('Deals count: ', deals_count, '\n')
    #plt.style.use('dark_background')
    #plt.ion()
    fig, ax = plt.subplots(figsize=(16,9))
    ax.set_title(name + ' ' + timeframe)
    ax.set_xlabel('Holding profit: ' + str(holding_profit-100) + '\n' + 'Trading profit: ' + str(trading_profit-100) + '\n' + 'Deals count: ' + str(deals_count)
    + '\n' + 'Index list: ' + str(index_list))
    #ax.plot(close_list, 'bo')
    ax.plot(sold_list, 'ro')
    ax.plot(sold_list_stoploss, 'yo')
    ax.plot(bought_list, 'go')
    ax.plot(close_list)
    #ax.plot(ind.sma(close_list, 200))
    ax.axis([0, len(close_list), min(close_list), max(close_list)])
    plt.show()
"""

def triangles_with_volume():
    n=len(close_list)
    min_list=[0 for x in range(2)]
    max_list=[0 for x in range(2)]
    div_list=[0 for x in range(n)]
    i=1
    op=35
    max_counter=0
    min_counter=0
    drctn = -1
    ln = 0
    if close_list[0] < close_list[1]:
        drctn = 1
    check_index_min=0
    check_index_max=0
    while ln+op < n-1:
        while i < ln+op:
            if drctn == 1 and close_list[i-1] > close_list[i]:
                if max_counter>=1 and abs(max_list[max_counter-1]/close_list[i-1]-1.0)<0.005 and max_list[max_counter-1]<close_list[i-1]:
                    max_list[max_counter-1]=close_list[i-1]
                elif max_counter<=1:
                    max_list[max_counter]=close_list[i-1]                   
                    if max_list[check_index_max]>max_list[max_counter]:
                        check_index_max=max_counter
                    max_counter=max_counter+1
                elif close_list[i-1]>max_list[check_index_max]:
                    j=check_index_max
                    while j<1:
                        max_list[j]=max_list[j+1]
                        j=j+1
                    max_list[1]=close_list[i]
                    j=0
                    check_index_max=0
                    while j<2:
                        if max_list[check_index_max]>max_list[j]:
                            check_index_max=j
                        j=j+1
                drctn=-1
            elif close_list[i-1] < close_list[i]:
                if min_counter>=1 and abs(1.0-min_list[min_counter-1]/close_list[i-1])<0.005:
                    if close_list[i-1]<min_list[min_counter-1]:
                        min_list[min_counter-1]=close_list[i-1]
                elif min_counter<=1:
                    min_list[min_counter]=close_list[i-1]                   
                    if min_list[check_index_min]<min_list[min_counter]:
                        check_index_min=min_counter
                    min_counter=min_counter+1
                elif close_list[i-1]<min_list[check_index_min]:
                    j=check_index_min
                    while j<1:
                        min_list[j]=min_list[j+1]
                        j=j+1
                    min_list[1]=close_list[i-1]
                    j=0
                    check_index_min=0
                    while j<2:
                        if min_list[check_index_min]<min_list[j]:
                            check_index_min=j
                        j=j+1
                drctn=1
            #print(min_list+max_list)
            if max_counter==2 and min_counter==2 and i-ln>=9:
                fuck=0.007
                if i-ln <=14:
                    fuck =0.004
                if abs(1.0-((max_list[1]/max_list[0]*max_list[1] + min_list[1]/min_list[0]*min_list[1])/2.0)/close_list[i])<=fuck:
                    if max_list[1]/max_list[0] < max_list[1]/(min_list[0]+max_list[1]-min_list[1]):
                        j=i-op
                        v=0
                        while j < i:
                            v = v + volume_list[j]
                            j += 1
                        v=v/(op)
                        if volume_list[i] > v:
                            if ((max_list[1]/max_list[0]*max_list[1] + min_list[1]/min_list[0]*min_list[1])/2.0)-close_list[i] > fuck/2.0 and min_list[1]/close_list[i]>0.95:
                                div_list[i]=1
                                ln=i
                                max_counter=0
                                min_counter=0
                                check_index_min=0
                                check_index_max=0
                            if abs(1.0 - min_list[1]/min_list[0]*min_list[1]/close_list[i]) < 0.006  and min_list[0]/min_list[1]>=1.0 and max_list[1]/close_list[i]<1.05:
                                div_list[i]=2
                                ln=i   
                                max_counter=0
                                min_counter=0
                                check_index_min=0
                                check_index_max=0                 
            i=i+1
            if i>=n:
                i=ln+op
        ln=ln+1
        j=ln-op
        while j<ln:
            if close_list[j]==min_list[0]:
                min_list[0]=min_list[1]
                min_list[1]=0
                min_counter=0
                check_index_min=0
            if close_list[j]==max_list[0]:
                max_list[0]=min_list[1]
                max_list[1]=0
                max_counter=0
                check_index_max=0
            j=j+1
        #print (max_list+min_list)
        #print(ln)
    return div_list

def triangles_with_ema():
    n=len(close_list)
    min_list=[0 for x in range(2)]
    max_list=[0 for x in range(2)]
    div_list=[0 for x in range(n)]
    i=1
    op=35
    max_counter=0
    min_counter=0
    drctn = -1
    ln = 0
    if close_list[0] < close_list[1]:
        drctn = 1
    check_index_min=0
    check_index_max=0
    while ln+op < n-1:
        while i < ln+op:
            if drctn == 1 and close_list[i-1] > close_list[i]:
                if max_counter>=1 and abs(max_list[max_counter-1]/close_list[i-1]-1.0)<0.005 and max_list[max_counter-1]<close_list[i-1]:
                    max_list[max_counter-1]=close_list[i-1]
                elif max_counter<=1:
                    max_list[max_counter]=close_list[i-1]                   
                    if max_list[check_index_max]>max_list[max_counter]:
                        check_index_max=max_counter
                    max_counter=max_counter+1
                elif close_list[i-1]>max_list[check_index_max]:
                    j=check_index_max
                    while j<1:
                        max_list[j]=max_list[j+1]
                        j=j+1
                    max_list[1]=close_list[i]
                    j=0
                    check_index_max=0
                    while j<2:
                        if max_list[check_index_max]>max_list[j]:
                            check_index_max=j
                        j=j+1
                drctn=-1
            elif close_list[i-1] < close_list[i]:
                if min_counter>=1 and abs(1.0-min_list[min_counter-1]/close_list[i-1])<0.005:
                    if close_list[i-1]<min_list[min_counter-1]:
                        min_list[min_counter-1]=close_list[i-1]
                elif min_counter<=1:
                    min_list[min_counter]=close_list[i-1]                   
                    if min_list[check_index_min]<min_list[min_counter]:
                        check_index_min=min_counter
                    min_counter=min_counter+1
                elif close_list[i-1]<min_list[check_index_min]:
                    j=check_index_min
                    while j<1:
                        min_list[j]=min_list[j+1]
                        j=j+1
                    min_list[1]=close_list[i-1]
                    j=0
                    check_index_min=0
                    while j<2:
                        if min_list[check_index_min]<min_list[j]:
                            check_index_min=j
                        j=j+1
                drctn=1
            #print(min_list+max_list)
            if max_counter==2 and min_counter==2 and i-ln>=9:
                fuck=0.007
                if i-ln <=14:
                    fuck =0.004
                if abs(1.0-((max_list[1]/max_list[0]*max_list[1] + min_list[1]/min_list[0]*min_list[1])/2.0)/close_list[i])<=fuck:
                    if max_list[1]/max_list[0] < max_list[1]/(min_list[0]+max_list[1]-min_list[1]):
                        j=i-op
                        v=0
                        while j < i:
                            v = v + volume_list[j]
                            j += 1
                        v=v/(op)
                        if volume_list[i] > v:
                            if ((max_list[1]/max_list[0]*max_list[1] + min_list[1]/min_list[0]*min_list[1])/2.0)-close_list[i] > fuck/2.0 and min_list[1]/close_list[i]>0.95:
                                div_list[i]=1
                                ln=i
                                max_counter=0
                                min_counter=0
                                check_index_min=0
                                check_index_max=0
                            if min_list[0]/min_list[1]>=1.0 and max_list[1]/close_list[i]<1.05:
                                div_list[i]=2
                                ln=i   
                                max_counter=0
                                min_counter=0
                                check_index_min=0
                                check_index_max=0  
  


            i=i+1
            if i>=n:
                i=ln+op
        ln=ln+1
        j=ln-op
        while j<ln:
            if close_list[j]==min_list[0]:
                min_list[0]=min_list[1]
                min_list[1]=0
                min_counter=0
                check_index_min=0
            if close_list[j]==max_list[0]:
                max_list[0]=min_list[1]
                max_list[1]=0
                max_counter=0
                check_index_max=0
            j=j+1
        #print (max_list+min_list)
        #print(ln)
    return div_list

def bull_tri():
    div_list2 = no_repeat(triangles())
    div_list1 = no_repeat(bull_dog())
    div_list = np.zeros(len(close_list))
    i = 0
    while i<len(close_list):
        if div_list1[i] != 0:
            div_list[i] = div_list1[i]
        if div_list2[i] != 0 and div_list1[i] == 0:
            div_list[i] = div_list2[i]          
        i+=1
    return div_list

def triangles_13dot12():
    n=len(close_list)
    min_list=[0 for x in range(2)]
    max_list=[0 for x in range(2)]
    div_list=[0 for x in range(n)]
    i=1
    op=35
    max_counter=0
    min_counter=0
    drctn = -1
    ln = 0
    if close_list[0] < close_list[1]:
        drctn = 1
    check_index_min=0
    check_index_max=0
    while ln+op < n-1:
        while i < ln+op:
            if drctn == 1 and close_list[i-1] > close_list[i]:
                if max_counter>=1 and abs(max_list[max_counter-1]/close_list[i-1]-1.0)<0.005 and max_list[max_counter-1]<close_list[i-1]:
                    max_list[max_counter-1]=close_list[i-1]
                elif max_counter<=1:
                    max_list[max_counter]=close_list[i-1]                   
                    if max_list[check_index_max]>max_list[max_counter]:
                        check_index_max=max_counter
                    max_counter=max_counter+1
                elif close_list[i-1]>max_list[check_index_max]:
                    j=check_index_max
                    while j<1:
                        max_list[j]=max_list[j+1]
                        j=j+1
                    max_list[1]=close_list[i]
                    j=0
                    check_index_max=0
                    while j<2:
                        if max_list[check_index_max]>max_list[j]:
                            check_index_max=j
                        j=j+1
                drctn=-1
            elif close_list[i-1] < close_list[i]:
                if min_counter>=1 and abs(1.0-min_list[min_counter-1]/close_list[i-1])<0.005:
                    if close_list[i-1]<min_list[min_counter-1]:
                        min_list[min_counter-1]=close_list[i-1]
                elif min_counter<=1:
                    min_list[min_counter]=close_list[i-1]                   
                    if min_list[check_index_min]<min_list[min_counter]:
                        check_index_min=min_counter
                    min_counter=min_counter+1
                elif close_list[i-1]<min_list[check_index_min]:
                    j=check_index_min
                    while j<1:
                        min_list[j]=min_list[j+1]
                        j=j+1
                    min_list[1]=close_list[i-1]
                    j=0
                    check_index_min=0
                    while j<2:
                        if min_list[check_index_min]<min_list[j]:
                            check_index_min=j
                        j=j+1
                drctn=1
            #print(min_list+max_list)
            if max_counter==2 and min_counter==2 and i-ln>=9:
                fuck=0.0070777
                if abs(1.0-((max_list[1]/max_list[0]*max_list[1] + min_list[1]/min_list[0]*min_list[1])/2.0)/close_list[i])<=fuck:
                    if max_list[1]/max_list[0] < max_list[1]/(min_list[0]+max_list[1]-min_list[1]):
                        j=i-op
                        v=0
                        while j < i:
                            v = v + volume_list[j]
                            j += 1
                        v=v/(op)
                        if volume_list[i] > v:
                            if ((max_list[1]/max_list[0]*max_list[1] + min_list[1]/min_list[0]*min_list[1])/2.0)-close_list[i] > fuck/2.0 and min_list[1]/close_list[i]>0.95:
                                div_list[i]=1
                                ln=i
                                max_counter=0
                                min_counter=0
                                check_index_min=0
                                check_index_max=0
                            if abs(1.0 - min_list[1]/min_list[0]*min_list[1]/close_list[i]) < 0.006  and min_list[0]/min_list[1]>=1.0 and max_list[1]/close_list[i]<1.05:
                                div_list[i]=2
                                ln=i   
                                max_counter=0
                                min_counter=0
                                check_index_min=0
                                check_index_max=0                                     
            i=i+1
            if i>=n:
                i=ln+op
        ln=ln+1
        j=ln-op
        while j<ln:
            if close_list[j]==min_list[0]:
                min_list[0]=min_list[1]
                min_list[1]=0
                min_counter=0
                check_index_min=0
            if close_list[j]==max_list[0]:
                max_list[0]=min_list[1]
                max_list[1]=0
                max_counter=0
                check_index_max=0
            j=j+1
        #print (max_list+min_list)
        #print(ln)
    return div_list

def flags():
    n=len(close_list)
    min_list=[0 for x in range(2)]
    max_list=[0 for x in range(2)]
    div_list=[0 for x in range(n)]
    i=1
    op=35
    max_counter=0
    min_counter=0
    drctn = -1
    ln = 0
    if close_list[0] < close_list[1]:
        drctn = 1
    check_index_min=0
    check_index_max=0
    while ln+op < n-1:
        while i < ln+op:
            if drctn == 1 and close_list[i-1] > close_list[i]:
                if max_counter>=1 and abs(max_list[max_counter-1]/close_list[i-1]-1.0)<0.005 and max_list[max_counter-1]<close_list[i-1]:
                    max_list[max_counter-1]=close_list[i-1]
                elif max_counter<=1:
                    max_list[max_counter]=close_list[i-1]                   
                    if max_list[check_index_max]>max_list[max_counter]:
                        check_index_max=max_counter
                    max_counter=max_counter+1
                elif close_list[i-1]>max_list[check_index_max]:
                    j=check_index_max
                    while j<1:
                        max_list[j]=max_list[j+1]
                        j=j+1
                    max_list[1]=close_list[i]
                    j=0
                    check_index_max=0
                    while j<2:
                        if max_list[check_index_max]>max_list[j]:
                            check_index_max=j
                        j=j+1
                drctn=-1
            elif close_list[i-1] < close_list[i]:
                if min_counter>=1 and abs(1.0-min_list[min_counter-1]/close_list[i-1])<0.005:
                    if close_list[i-1]<min_list[min_counter-1]:
                        min_list[min_counter-1]=close_list[i-1]
                elif min_counter<=1:
                    min_list[min_counter]=close_list[i-1]                   
                    if min_list[check_index_min]<min_list[min_counter]:
                        check_index_min=min_counter
                    min_counter=min_counter+1
                elif close_list[i-1]<min_list[check_index_min]:
                    j=check_index_min
                    while j<1:
                        min_list[j]=min_list[j+1]
                        j=j+1
                    min_list[1]=close_list[i-1]
                    j=0
                    check_index_min=0
                    while j<2:
                        if min_list[check_index_min]<min_list[j]:
                            check_index_min=j
                        j=j+1
                drctn=1
            #print(min_list+max_list)
            if max_counter==2 and min_counter==2 and i-ln>=9:
                fuck=0.007
                if abs(1.0-((max_list[1]/max_list[0]*max_list[1] + min_list[1]/min_list[0]*min_list[1])/2.0)/close_list[i])<=fuck:
                    if (1.0 - (max_list[1]-min_list[1])/(max_list[0]-min_list[0]))<0.001:
                        j=i-op
                        v=0
                        while j < i:
                            v = v + volume_list[j]
                            j += 1
                        v=v/(op)
                        if volume_list[i] > v:
                            if abs(1.0 - min_list[1]/min_list[0]*min_list[1]/close_list[i]) < 0.006  and min_list[0]/min_list[1]<=1.0 and min_list[1]/close_list[i]>0.95:
                                div_list[i]=1
                                ln=i
                                max_counter=0
                                min_counter=0
                                check_index_min=0
                                check_index_max=0
                            if abs(1.0 - min_list[1]/min_list[0]*min_list[1]/close_list[i]) < 0.006  and min_list[0]/min_list[1]>=1.0 and max_list[1]/close_list[i]<1.05:
                                div_list[i]=2
                                ln=i   
                                max_counter=0
                                min_counter=0
                                check_index_min=0
                                check_index_max=0                                         
            i=i+1
            if i>=n:
                i=ln+op
        ln=ln+1
        j=ln-op
        while j<ln:
            if close_list[j]==min_list[0]:
                min_list[0]=min_list[1]
                min_list[1]=0
                min_counter=0
                check_index_min=0
            if close_list[j]==max_list[0]:
                max_list[0]=min_list[1]
                max_list[1]=0
                max_counter=0
                check_index_max=0
            j=j+1
        #print (max_list+min_list)
        #print(ln)
    return div_list

"""
def unite_strategies():
    div_list_bull_tri = bull_tri()
    div_list_triangles = triangles_with_volume()
    ema_list = ind.ema(close_list, 200)
    div_list = np.empty(len(close_list))
    for i in range(len(close_list)):
        if ema_list[i] > close_list[i]:
            div_list[i] = div_list_triangles[i]
        else:
            div_list[i] = div_list_bull_tri[i]
    return div_list
"""

def rsi_strategy():
    rsi_list = ind.rsi(close_list, 14)
    #print(rsi_list)
    div_list = np.zeros(len(rsi_list))
    for i in range(len(rsi_list)):
        if rsi_list[i] < 20:
            div_list[i] = 1
        if rsi_list[i] > 80:
            div_list[i] = 2
    return div_list

#no fib graphs 
def new_fibonacci():
    div_list = np.zeros(len(close_list))
    emka = ind.ema(close_list, 9)
    stage1 = False
    stage2 = False
    ch1 = False
    ch2 = False
    #line1 = 0
    line2 = 0
    #line3 = 0
    line4 = 0
    line5 = 0
    #line6 = 0
    low_ind = 0
    high_ind = 0

    price = 0

    for x in range (len(close_list)):
        if emka[x-1]<close_list[x-1] and emka[x]>=close_list[x]:
            low_ind = x
            ch1 = True

        elif emka[x-1]>close_list[x-1] and emka[x]<=close_list[x]:
            high_ind = x  
            ch2 = True

        if low_ind+10<high_ind and ch1 and ch2:
            allowance = int(0.3*(high_ind-low_ind))
            minimum = np.amin(min_list[(low_ind-allowance):(low_ind)])
            maximum = np.amax(max_list[(high_ind-allowance):(high_ind)])
            delta = maximum-minimum
            #line1 = float(maximum - (delta*0.236))
            line2 = float(maximum - (delta*0.382))
            #line3 = float(maximum - (delta*0.5))
            line4 = float(maximum - (delta*0.618))
            line5 = float(maximum - (delta*0.65))
            #line6 = float(maximum - (delta*0.786))
            stage1 = True
        if stage1 and min_list[x-1]>line5*0.998 and min_list[x-1]<line4*1.002 and open_list[x]>min_list[x-1]:
            div_list[x] = 1
            stage1 = False
            stage2 = True
            
            price = min_list[x-1]

        if stage2:
            minimum = price
            delta = maximum-minimum
            #line1 = float(minimum + (delta*0.236))
            line2 = float(minimum + (delta*0.382))
            #line3 = float(minimum + (delta*0.5))
            line4 = float(minimum + (delta*0.618))
            line5 = float(minimum + (delta*0.65))
            #line6 = float(minimum + (delta*0.786))
            if max_list[x]>line2:
                div_list[x] = 2
                stage2 = False
                ch1 = False
                ch2 = False
    return div_list

'''
def new_fibonacci():
    div_list = np.zeros(len(close_list))
    emka = ind.ema(close_list, 9)
    stage1 = False
    stage2 = False
    ch1 = False
    ch2 = False
    line1 = 0
    line2 = 0
    line3 = 0
    line4 = 0
    line5 = 0
    line6 = 0
    low_ind = 0
    high_ind = 0
 
    price = 0
    line0_list = [None]*len(close_list)
    line1_list = [None]*len(close_list)
    line2_list = [None]*len(close_list)
    line3_list = [None]*len(close_list)
    line4_list = [None]*len(close_list)
    line5_list = [None]*len(close_list)
    line6_list = [None]*len(close_list)
    line7_list = [None]*len(close_list)
 
    for x in range (len(close_list)):
 
        flag1 = True
        flag2 = True
 
        if emka[x-1]<close_list[x-1] and emka[x]>=close_list[x]:
            low_ind = x
            ch1 = True
 
        elif emka[x-1]>close_list[x-1] and emka[x]<=close_list[x]:
            high_ind = x  
            ch2 = True
 
        if low_ind+10<high_ind and ch1 and ch2:
            allowance = int(0.3*(high_ind-low_ind))
            minimum = np.amin(min_list[(low_ind-allowance):(low_ind)])
            maximum = np.amax(max_list[(high_ind-allowance):(high_ind)])
            delta = maximum-minimum
             
            line0 = float(maximum)
 
            line1 = float(maximum - (delta*0.236))
            line2 = float(maximum - (delta*0.382))
            line3 = float(maximum - (delta*0.5))
            line4 = float(maximum - (delta*0.618))
            line5 = float(maximum - (delta*0.65))
            line6 = float(maximum - (delta*0.786))
 
            line7 = float(minimum)
 
            stage1 = True
 
            if flag1:
                line0_list[x-1] = line0
                line1_list[x-1] = line1
                line2_list[x-1] = line2
                line3_list[x-1] = line3
                line4_list[x-1] = line4
                line5_list[x-1] = line5
                line6_list[x-1] = line6
                line7_list[x-1] = line7
                flag1 = False
 
        if stage1 and min_list[x-1]>line5*0.998 and min_list[x-1]<line4*1.002 and open_list[x]>min_list[x-1]:
            div_list[x] = 1
            stage1 = False
            stage2 = True
             
            price = min_list[x-1]
 
        if stage2:
            minimum = price
            delta = maximum-minimum
 
            line0 = float(minimum)
 
            line1 = float(minimum + (delta*0.236))
            line2 = float(minimum + (delta*0.382))
            line3 = float(minimum + (delta*0.5))
            line4 = float(minimum + (delta*0.618))
            line5 = float(minimum + (delta*0.65))
            line6 = float(minimum + (delta*0.786))
 
            line7 = float(maximum)
 
            if max_list[x]>line2:
 
                if flag2:
                    line0_list[x] = line0
                    line1_list[x] = line1
                    line2_list[x] = line2
                    line3_list[x] = line3
                    line4_list[x] = line4
                    line5_list[x] = line5
                    line6_list[x] = line6
                    line7_list[x] = line7
                    flag2 = False
 
                div_list[x] = 2
                stage2 = False
                ch1 = False
                ch2 = False
    return [line0_list, line1_list, line2_list, line3_list, line4_list, line5_list, line6_list, line7_list, div_list]
'''
"""
def honest_triangles():
    div_list = [0 for x in range(len(close_list))]
    ema_list=ind.ema(close_list, 55)
    buy = False
    sell = False
    target = 0
    number = 10
    min_list = [0 for x in range(number)]
    max_list = [0 for x in range(number)]
    ranged_min_list = [0 for x in range(number)]
    ranged_max_list = [0 for x in range(number)]
    #global tri_list
    max_counter=0
    min_counter=0
    q=0
    i=2
    while i < len(close_list)-1: #previously len(close_list)
        if close_list[i] > close_list[i-1] and close_list[i-1] < close_list[i-2] : #and close_list[i] < close_list[min_list[max_counter - 1]]:
            if min_counter == number:
                if close_list[i] < close_list[min_list[0]]:
                    k = number - 1
                    while k > 0:
                        min_list[k]=min_list[k-1]  
                        k -= 1  
                    min_list[0]=i
                else:
                    j = 1
                    while j < number - 1:

                        if close_list[min_list[j-1]] < close_list[i] and close_list[i] < close_list[min_list[j+1]]:
                            k = number - 1
                            while k > j:
                                min_list[k]=min_list[k-1]  
                                k -= 1  
                            min_list[j]=i 
                        j +=1
            elif min_counter > 1:
                if close_list[i] < close_list[min_list[0]]:
                    k = min_counter
                    while k > 0:
                        min_list[k]=min_list[k-1]  
                        k -= 1  
                    min_list[0]=i
                    min_counter +=1
                else:
                    min_list[min_counter]=i
                    j = 1
                    while j < min_counter - 1:
                        if close_list[min_list[j-1]] < close_list[i] and close_list[i] < close_list[min_list[j+1]]:
                            k = min_counter - 1
                            while k > j:
                                min_list[k]=min_list[k-1]  
                                k -= 1  
                            min_list[j] = i
                        j +=1
                    min_counter += 1
            elif min_counter == 1:
                if close_list[min_list[0]] < close_list[i]:
                    min_list[1] = i
                else:
                    min_list[1] = min_list[0]
                    min_list[0] = i
                min_counter +=1
            else:
                min_list[0] = i
                min_counter +=1
        elif close_list[i-1] < close_list[i] and close_list[i] > close_list[i+1]:# and close_list[i] > close_list[max_list[max_counter-1]]:
            if max_counter == number:
                if close_list[i] > close_list[max_list[0]]:
                    k = number - 1
                    while k > 0:
                        max_list[k]=max_list[k-1]  
                        k -= 1  
                    max_list[0]=i
                else:
                    j = 1
                    while j < number - 1:
                        if close_list[max_list[j-1]] > close_list[i] and close_list[i] > close_list[max_list[j+1]]:
                            k = number - 1
                            while k > j:
                                max_list[k]=max_list[k-1]  
                                k -= 1  
                            max_list[j]=i 
                        j +=1
            elif max_counter > 1:
                if close_list[i] > close_list[max_list[0]]:
                    k = max_counter
                    while k > 0:
                        max_list[k]=max_list[k-1]  
                        k -= 1  
                    max_list[0]=i
                    max_counter +=1
                else:
                    max_list[max_counter]=i
                    j = 1
                    while j < max_counter:
                        if close_list[max_list[j-1]] > close_list[i] and close_list[i] > close_list[max_list[j+1]]:
                            k = max_counter
                            while k > j:
                                max_list[k]=max_list[k-1]  
                                k -= 1  
                            max_list[j] = i
                        j +=1
                    max_counter += 1
            elif max_counter == 1:
                if close_list[max_list[0]] > close_list[i]:
                    max_list[1] = i
                else:
                    max_list[1] = max_list[0]
                    max_list[0] = i
                max_counter +=1
            else:
                max_list[0] = i
                max_counter +=1
        '''
        print("gggggggggggggggggggggggggg")
        print(min_list)
        print(min_counter)
        print(max_list)
        print(max_counter)
        '''
        '''
        j = 0
        while j < max_counter:

            ranged_max_list[j] = max_list[j]
            j+=1
        j = 0
        k = 0
        while j < max_counter:
            k = 1
            while k < max_counter - j:
                if ranged_max_list[k] < ranged_max_list[k - 1]:
                    ranged_max_list[k] = ranged_max_list[k] + ranged_max_list[k-1]
                    ranged_max_list[k-1] = ranged_max_list[k] - ranged_max_list[k-1]
                    ranged_max_list[k] = ranged_max_list[k] - ranged_max_list[k-1]
                k +=1
            j +=1

        j = 0
        while j < min_counter:
            ranged_min_list[j] = min_list[j]
            j+=1
        j = 0
        k = 0
        while j < min_counter:
            k = 1
            while k < min_counter - j:
                if ranged_min_list[k] < ranged_min_list[k - 1]:
                    ranged_min_list[k] = ranged_min_list[k] + ranged_min_list[k-1]
                    ranged_min_list[k-1] = ranged_min_list[k] - ranged_min_list[k-1]
                    ranged_min_list[k] = ranged_min_list[k] - ranged_min_list[k-1]
                k +=1
            j +=1
        '''
        '''
        print("----------------------")
        print(min_list)
        print(ranged_min_list)
        '''
        
        x = 0
        y = 0

        j = 0
        
        if 0 > 1: 
        #if close_list[i] > target and buy:
            div_list[i] = 2
            buy = False
            sell = True
            '''    
        elif  close_list[i] < target and sell:
            div_list[i] = 1
            sell = False
            buy = True
            target = target*1.007
            '''
        else:
            while j < max_counter-1:
                k = 0
                while k < min_counter-1:
                    #if max_list[k] > max_list[j]:
                    m = j + 1
                    while m < max_counter:
                        n = k+1
                        while n < min_counter:
                            q=0
                            a1 = close_list[max_list[j]] - close_list[max_list[m]]
                            a2 = close_list[min_list[k]] - close_list[min_list[n]]
                            b1 = max_list[m] - max_list[j]
                            b2 = min_list[n] - min_list[k]
                            c1 = max_list[j]*close_list[max_list[m]] - max_list[m]*close_list[max_list[j]]
                            c2 = min_list[k]*close_list[min_list[n]] - min_list[n]*close_list[min_list[k]]
                            
                            if (b2*a1 - b1*a2) != 0 and a2 != 0 and max_list[j] < max_list[m] and min_list[k]< min_list[n]:
                                x = (b2*(c2*a1 - c1*a2)/(b2*a1 - b1*a2) - c2)/a2
                                y = (c1*a2 - c2*a1)/(b2*a1 - b1*a2)
                                if x > max_list[j] and x > max_list[m] and x > min_list[k] and x > min_list[n]:
                                    check = True
                                    v = 0
                                    l=max_list[j]
                                    if min_list[k] < l:
                                        l = min_list[k]
                                    ll=i-l
                                    while l < i:
                                        v = v+volume_list[l]
                                        l+=1
                                    v=v/ll
                                    l=0
                                    s =0.5*abs((max_list[j] - x)*(close_list[min_list[k]] - y) - (min_list[k] - x)*(close_list[max_list[j]] - y))
                                    while l < number and check:
                                        if l > max_list[j] and l <max_counter:
                                            s1 =0.5*abs((max_list[j] - max_list[l])*(close_list[min_list[k]] - close_list[max_list[l]]) - (min_list[k] - max_list[l])*(close_list[max_list[j]] - close_list[max_list[l]]))
                                            s2 =0.5*abs((max_list[l] - x)*(close_list[min_list[k]] - y) - (min_list[k] - x)*(close_list[max_list[k]] - y))
                                            s3 =0.5*abs((max_list[j] - x)*(close_list[max_list[l]] - y) - (max_list[l] - x)*(close_list[max_list[j]] - y))
                                            if s1+s2+s3 > s:
                                                check = False
                                        if l > min_list[k] and l < min_counter:
                                            s1 =0.5*abs((max_list[j] - min_list[l])*(close_list[min_list[k]] - close_list[min_list[l]]) - (min_list[k] - min_list[l])*(close_list[max_list[j]] - close_list[min_list[l]]))
                                            s2 =0.5*abs((max_list[j] - x)*(close_list[min_list[l]] - y) - (min_list[l] - x)*(close_list[max_list[j]] - y))
                                            s3 =0.5*abs((min_list[l] - x)*(close_list[min_list[k]] - y) - (min_list[k] - x)*(close_list[min_list[l]] - y))
                                            if s1+s2+s3 > s:
                                                check = False
                                        l+=1
                                    if i >= x and volume_list[i] > v*2 and check and ll>20:
                                        # and abs(1.0 - close_list[int(round(x))]/y) < 0.05:                                        
                                        if close_list[i] > close_list[max_list[j]] and close_list[i] < close_list[i-1]:
                                        #and close_list[i] < close_list[max_list[j]]*1.005: 
                                        #and ema_list[i] > close_list[max_list[j]]:
                                        #y + close_list[max_list[j]] - close_list[min_list[k]] and close_list[i]*0.995 < y + close_list[max_list[j]] - close_list[min_list[k]]:
                                            div_list[i] = 1
                                            target = close_list[max_list[j]] + close_list[max_list[j]] - close_list[min_list[k]]
                                            buy = True
                                            '''tri_list[max_list[j]] = close_list[max_list[j]]
                                            tri_list[min_list[k]] = close_list[min_list[k]]
                                            tri_list[int(round(x))] = y'''
                                            f=min_list[k]
                                            if max_list[j] < min_list[k]:
                                                f = max_list[j]
                                            '''for q in range(f, int(round(x))):
                                                tri_list[q] = close_list[q]'''
                                            n=number
                                            m=number
                                            k=number
                                            j=number
                                             
                                        elif close_list[i] < close_list[min_list[k]] and close_list[i]>close_list[i-1]:
                                            #y -  close_list[max_list[j]] + close_list[min_list[k]] and close_list[i]*1.005 > y - close_list[max_list[j]] + close_list[min_list[k]]:
                                            div_list[i] = 2
                                            sell = True
                                            '''tri_list[max_list[j]] = close_list[max_list[j]]
                                            tri_list[min_list[k]] = close_list[min_list[k]]
                                            tri_list[int(round(x))] = y'''
                                            f=min_list[k]
                                            if max_list[j] < min_list[k]:
                                                f = max_list[j]
                                            '''for q in range(f, int(round(x))):
                                                tri_list[q] = close_list[q]'''
                                            n=number
                                            m=number
                                            k=number
                                            j=number

                                            '''
                                            if sell == False:
                                                target = close_list[min_list[k]] -  close_list[max_list[j]] + close_list[min_list[k]]
                                            buy = False
                                            sell = True
                                            '''
                                            
                            n+=1
                        m+=1
                    k+=1
                j +=1






                    





        l = 50
        j=0
        while j < max_counter:
            if max_list[j] < i - l:
                k = j
                while k < max_counter - 1:
                    max_list[k] = max_list[k +1]
                    k +=1
                max_counter -=1
                max_list[max_counter] = 0
            j += 1
        
        j = 0
        while j < min_counter:
            if min_list[j] < i - l:
                k = j
                while k < min_counter -1:
                    min_list[k] = min_list[k+1]
                    k +=1
                min_counter -=1
                min_list[min_counter] = 0
            j +=1
            
        i += 1
    return div_list
"""
def trend_recognition():
    max_idx = list(argrelextrema(open_list, np.greater, order=20)[0])
    min_idx = list(argrelextrema(open_list, np.less, order=20)[0])
    idx = max_idx + min_idx
    idx.sort()
    extreme_prices = [None]*len(open_list)
    for i in idx:
        extreme_prices[i] = open_list[i]


    div_list = np.zeros(len(open_list))
    max_prices = []
    min_prices = []
    order_length = 20

    max_prices_copy = [None]*len(open_list)
    min_prices_copy = [None]*len(open_list)

    min_trend_line = [None]*len(open_list)
    max_trend_line = [None]*len(open_list)
    border_list = [None]*len(open_list)
    for i in range(2*order_length, len(open_list), 2*order_length):
        border_list[i] = open_list[i]
        temp_open_list = open_list[i-2*order_length:i]
        max_idx = list(argrelextrema(temp_open_list, np.greater, order=order_length)[0])
        min_idx = list(argrelextrema(temp_open_list, np.less, order=order_length)[0])
        for j in max_idx:
            max_prices.append([i-2*order_length+j, temp_open_list[j]])
            max_prices_copy[i-2*order_length+j] = temp_open_list[j]
        for j in min_idx:
            min_prices.append([i-2*order_length+j, temp_open_list[j]])
            min_prices_copy[i-2*order_length+j] = temp_open_list[j]
        if len(max_prices)>=2 and len(min_prices)>=2:
            '''#0
            if max_prices[-1][1] > max_prices[-2][1] and min_prices[-1][1] > min_prices[-2][1]:
                div_list[i] = 1
            if max_prices[-1][1] <= max_prices[-2][1] and min_prices[-1][1] <= min_prices[-2][1]:
                div_list[i] = 2
            '''

            '''#1  #250%
            if len(max_idx) != 0:
                for j in range(max_prices[-2][0], i+1):
                    #print('max_prices[-1][0]:', max_prices[-1][0], 'max_prices[-2][0]:', max_prices[-2][0])
                    max_trend_line[j] = (j-max_prices[-2][0])*(max_prices[-1][1]-max_prices[-2][1])/(max_prices[-1][0]-max_prices[-2][0])+max_prices[-2][1] #line equation
                if open_list[i] > max_trend_line[i]:
                    div_list[i] = 2
            if len(min_idx) != 0:
                for j in range(min_prices[-2][0], i+1):
                    #print('min_prices[-1][0]:', min_prices[-1][0], 'min_prices[-2][0]:', min_prices[-2][0])
                    min_trend_line[j] = (j-min_prices[-2][0])*(min_prices[-1][1]-min_prices[-2][1])/(min_prices[-1][0]-min_prices[-2][0])+min_prices[-2][1] #line equation
                if open_list[i] < min_trend_line[i]:
                    div_list[i] = 1'''

            #2  #285%    #using rule: the longer the trendline, the stronger it is  #need to fix line plotting
            if len(max_idx) != 0 or len(min_idx) != 0:
                max_lenght = max_prices[-1][0] - max_prices[-2][0]
                min_lenght = min_prices[-1][0] - min_prices[-2][0]
                if max_lenght > min_lenght:
                    k = (max_prices[-1][1]-max_prices[-2][1])/(max_prices[-1][0]-max_prices[-2][0])
                    for j in range(max_prices[-2][0], i+1):
                        max_trend_line[j] = k*(j-max_prices[-2][0])+max_prices[-2][1] #line equation
                    for j in range(min_prices[-2][0], i+1):
                        min_trend_line[j] = k*(j-min_prices[-2][0])+min_prices[-2][1] #line equation
                    if open_list[i] < min_trend_line[i]:
                        div_list[i] = 2
                    if open_list[i] > max_trend_line[i]:
                        div_list[i] = 1
    return div_list      
    """offline(name='trend', div_list=div_list, additional_charts=[
        {'data': extreme_prices, 'type': 'point_chart'}, 
        {'data': max_prices_copy, 'special_params': {'color': 'red'}, 'point_chart'}, [min_prices_copy, {'color': 'green'}, 'point_chart'], [border_list, {'color': 'blue', 'linestyle': '', 'marker': '|'}], [max_trend_line], [min_trend_line]])"""

def true_divergence():
    order_length = 20
    rsi_list = ind.rsi(close_list, 14)
    #rsi_list = ind.macd(close_list)
    div_list = np.zeros(len(open_list))

    max_prices = []
    min_prices = []
    max_prices_copy = [None]*len(open_list)
    min_prices_copy = [None]*len(open_list)

    min_trend_line = [None]*len(open_list)
    max_trend_line = [None]*len(open_list)
    border_list = [None]*len(open_list)

    max_rsi = []
    min_rsi = []
    max_rsi_copy = [None]*len(open_list)
    min_rsi_copy = [None]*len(open_list)
    max_rsi_trend_line = [None]*len(open_list)
    min_rsi_trend_line = [None]*len(open_list)
    for i in range(2*order_length, len(open_list), 2*order_length):
        border_list[i] = open_list[i]
        temp_open_list = open_list[i-2*order_length:i]
        temp_rsi_list = rsi_list[i-2*order_length:i]
        max_prices_idx = list(argrelextrema(temp_open_list, np.greater, order=order_length)[0])
        min_prices_idx = list(argrelextrema(temp_open_list, np.less, order=order_length)[0])

        max_rsi_idx = list(argrelextrema(temp_open_list, np.greater, order=order_length)[0])
        min_rsi_idx = list(argrelextrema(temp_open_list, np.less, order=order_length)[0])
        for j in max_rsi_idx:
            max_rsi.append([i-2*order_length+j, temp_rsi_list[j]])
            max_rsi_copy[i-2*order_length+j] = temp_rsi_list[j]
        for j in min_rsi_idx:
            min_rsi.append([i-2*order_length+j, temp_rsi_list[j]])
            min_rsi_copy[i-2*order_length+j] = temp_rsi_list[j]

        for j in max_prices_idx:
            max_prices.append([i-2*order_length+j, temp_open_list[j]])
            max_prices_copy[i-2*order_length+j] = temp_open_list[j]
        for j in min_prices_idx:
            min_prices.append([i-2*order_length+j, temp_open_list[j]])
            min_prices_copy[i-2*order_length+j] = temp_open_list[j]

        if len(max_prices)>=2 and len(min_prices)>=2 and len(max_rsi)>=2 and len(min_rsi)>=2:
            if len(max_prices_idx) != 0:
                for j in range(max_prices[-2][0], i+1):
                    max_trend_line[j] = (j-max_prices[-2][0])*(max_prices[-1][1]-max_prices[-2][1])/(max_prices[-1][0]-max_prices[-2][0])+max_prices[-2][1] #line equation
            if len(min_prices_idx) != 0:
                for j in range(min_prices[-2][0], i+1):
                    min_trend_line[j] = (j-min_prices[-2][0])*(min_prices[-1][1]-min_prices[-2][1])/(min_prices[-1][0]-min_prices[-2][0])+min_prices[-2][1] #line equation

            if len(max_rsi_idx) != 0:
                for j in range(max_rsi[-2][0], i+1):
                    max_rsi_trend_line[j] = (j-max_rsi[-2][0])*(max_rsi[-1][1]-max_rsi[-2][1])/(max_rsi[-1][0]-max_rsi[-2][0])+max_rsi[-2][1] #line equation

                max_rsi_dif = max_rsi_trend_line[max_rsi[-1][0]]-max_rsi_trend_line[max_rsi[-2][0]]
                max_prices_dif = max_trend_line[max_prices[-1][0]]-max_trend_line[max_prices[-2][0]]
                if (max_rsi_dif>0 and max_prices_dif<0) or (max_rsi_dif<0 and max_prices_dif>0):
                    div_list[i] = 2

            if len(min_rsi_idx) != 0:
                for j in range(min_rsi[-2][0], i+1):
                    min_rsi_trend_line[j] = (j-min_rsi[-2][0])*(min_rsi[-1][1]-min_rsi[-2][1])/(min_rsi[-1][0]-min_rsi[-2][0])+min_rsi[-2][1] #line equation

                min_rsi_dif = min_rsi_trend_line[min_rsi[-1][0]]-min_rsi_trend_line[min_rsi[-2][0]]
                min_prices_dif = min_trend_line[min_prices[-1][0]]-min_trend_line[min_prices[-2][0]]
                if (min_rsi_dif>0 and min_prices_dif<0) or (min_rsi_dif<0 and min_prices_dif>0):
                    div_list[i] = 1
    return div_list
    """offline(name='divergence', div_list=div_list, additional_charts=[
        {'data': border_list, 'color': 'blue', 'special_params': {'linestyle': '', 'marker': '|'}},
        {'data': max_prices_copy, 'color': 'red', 'type': 'point_chart'},
        {'data': max_trend_line},
        {'data': min_prices_copy, 'color': 'green', 'type': 'point_chart'}, 
        {'data': min_trend_line}, 
        {'data': rsi_list, 'color': 'purple', 'priority': 'secondary'},
        {'data': max_rsi_copy, 'color': 'red', 'type': 'point_chart', 'priority': 'secondary'},
        {'data': max_rsi_trend_line},
        {'data': min_rsi_copy, 'color': 'green', 'type': 'point_chart', 'priority': 'secondary'},
        {'data': min_rsi_trend_line}
        ])"""

def triangles_19_02():
    div_list = [0 for x in range(len(close_list))]
    ema_list=ind.ema(close_list, 55)
    buy = False
    sell = False
    target = 0
    number = 10
    min_list = [0 for x in range(number)]
    max_list = [0 for x in range(number)]
    ranged_min_list = [0 for x in range(number)]
    ranged_max_list = [0 for x in range(number)]
    tri_list = [None]*len(close_list)
    list1 = [None]*len(close_list)
    list2 = [None]*len(close_list)
    max_counter=0
    min_counter=0
    q=0
    i=2
    while i < len(close_list):
        if close_list[i] > close_list[i-1] and close_list[i-1] < close_list[i-2] : #and close_list[i] < close_list[min_list[max_counter - 1]]:
            if min_counter == number:
                if close_list[i-1] < close_list[min_list[0]]:
                    k = number - 1
                    while k > 0:
                        min_list[k]=min_list[k-1]  
                        k -= 1  
                    min_list[0]=i-1
                else:
                    j = 1
                    while j < number - 1:

                        if close_list[min_list[j-1]] < close_list[i-1] and close_list[i-1] < close_list[min_list[j+1]]:
                            k = number - 1
                            while k > j:
                                min_list[k]=min_list[k-1]  
                                k -= 1  
                            min_list[j]=i-1
                        j +=1
            elif min_counter > 1:
                if close_list[i-1] < close_list[min_list[0]]:
                    k = min_counter
                    while k > 0:
                        min_list[k]=min_list[k-1]  
                        k -= 1  
                    min_list[0]=i-1
                    min_counter +=1
                else:
                    min_list[min_counter]=i-1
                    j = 1
                    while j < min_counter - 1:
                        if close_list[min_list[j-1]] < close_list[i-1] and close_list[i-1] < close_list[min_list[j+1]]:
                            k = min_counter - 1
                            while k > j:
                                min_list[k]=min_list[k-1]  
                                k -= 1  
                            min_list[j] = i-1
                        j +=1
                    min_counter += 1
            elif min_counter == 1:
                if close_list[min_list[0]] < close_list[i-1]:
                    min_list[1] = i-1
                else:
                    min_list[1] = min_list[0]
                    min_list[0] = i-1
                min_counter +=1
            else:
                min_list[0] = i-1
                min_counter +=1
        elif close_list[i-1] > close_list[i] and close_list[i-1] > close_list[i-2]:# and close_list[i] > close_list[max_list[max_counter-1]]:
            if max_counter == number:
                if close_list[i-1] > close_list[max_list[0]]:
                    k = number - 1
                    while k > 0:
                        max_list[k]=max_list[k-1]  
                        k -= 1  
                    max_list[0]=i-1
                else:
                    j = 1
                    while j < number - 1:
                        if close_list[max_list[j-1]] > close_list[i-1] and close_list[i-1] > close_list[max_list[j+1]]:
                            k = number - 1
                            while k > j:
                                max_list[k]=max_list[k-1]  
                                k -= 1  
                            max_list[j]=i-1
                        j +=1
            elif max_counter > 1:
                if close_list[i-1] > close_list[max_list[0]]:
                    k = max_counter
                    while k > 0:
                        max_list[k]=max_list[k-1]  
                        k -= 1  
                    max_list[0]=i-1
                    max_counter +=1
                else:
                    max_list[max_counter]=i-1
                    j = 1
                    while j < max_counter:
                        if close_list[max_list[j-1]] > close_list[i-1] and close_list[i-1] > close_list[max_list[j+1]]:
                            k = max_counter
                            while k > j:
                                max_list[k]=max_list[k-1]  
                                k -= 1  
                            max_list[j] = i-1
                        j +=1
                    max_counter += 1
            elif max_counter == 1:
                if close_list[max_list[0]] > close_list[i-1]:
                    max_list[1] = i-1
                else:
                    max_list[1] = max_list[0]
                    max_list[0] = i-1
                max_counter +=1
            else:
                max_list[0] = i-1
                max_counter +=1
        '''
        print("gggggggggggggggggggggggggg")
        print(min_list)
        print(min_counter)
        print(max_list)
        print(max_counter)
        '''
        '''
        j = 0
        while j < max_counter:

            ranged_max_list[j] = max_list[j]
            j+=1
        j = 0
        k = 0
        while j < max_counter:
            k = 1
            while k < max_counter - j:
                if ranged_max_list[k] < ranged_max_list[k - 1]:
                    ranged_max_list[k] = ranged_max_list[k] + ranged_max_list[k-1]
                    ranged_max_list[k-1] = ranged_max_list[k] - ranged_max_list[k-1]
                    ranged_max_list[k] = ranged_max_list[k] - ranged_max_list[k-1]
                k +=1
            j +=1

        j = 0
        while j < min_counter:
            ranged_min_list[j] = min_list[j]
            j+=1
        j = 0
        k = 0
        while j < min_counter:
            k = 1
            while k < min_counter - j:
                if ranged_min_list[k] < ranged_min_list[k - 1]:
                    ranged_min_list[k] = ranged_min_list[k] + ranged_min_list[k-1]
                    ranged_min_list[k-1] = ranged_min_list[k] - ranged_min_list[k-1]
                    ranged_min_list[k] = ranged_min_list[k] - ranged_min_list[k-1]
                k +=1
            j +=1
        '''
        '''
        print("----------------------")
        print(min_list)
        print(ranged_min_list)
        '''
        
        x = 0
        y = 0

        j = 0
        
        if 0 > 1: 
        #close_list[i] > target and buy:
            div_list[i] = 2
            buy = False
            sell = True
            '''    
        elif  close_list[i] < target and sell:
            div_list[i] = 1
            sell = False
            buy = True
            target = target*1.007
            '''
        else:
            while j < max_counter-1:
                k = 0
                while k < min_counter-1:
                    #if max_list[k] > max_list[j]:
                    m = j + 1
                    while m < max_counter:
                        n = k+1
                        while n < min_counter:
                            q=0
                            a1 = close_list[max_list[j]] - close_list[max_list[m]]
                            a2 = close_list[min_list[k]] - close_list[min_list[n]]
                            b1 = max_list[m] - max_list[j]
                            b2 = min_list[n] - min_list[k]
                            c1 = max_list[j]*close_list[max_list[m]] - max_list[m]*close_list[max_list[j]]
                            c2 = min_list[k]*close_list[min_list[n]] - min_list[n]*close_list[min_list[k]]
                            
                            if (b2*a1 - b1*a2) != 0 and a2 != 0 and max_list[j] < max_list[m] and min_list[k]< min_list[n]:
                                x = (b2*(c2*a1 - c1*a2)/(b2*a1 - b1*a2) - c2)/a2
                                y = (c1*a2 - c2*a1)/(b2*a1 - b1*a2)
                                if x > max_list[j] and x > max_list[m] and x > min_list[k] and x > min_list[n]:
                                    check = True
                                    v = 0
                                    l=max_list[j]
                                    if min_list[k] < l:
                                        l = min_list[k]
                                    ll=i-l
                                    while l < i:
                                        v = v+volume_list[l]
                                        l+=1
                                    v=v/ll
                                    if max_list[j] > min_list[k]:
                                        l = max_list[j] +1
                                    else:
                                        l = min_list[k] + 1
                                    if max_list[m] > min_list[n]:
                                        d =max_list[m]
                                    else:
                                        d = min_list[n]
                                    s =0.5*abs((max_list[j] - x)*(close_list[min_list[k]] - y) - (min_list[k] - x)*(close_list[max_list[j]] - y))
                                    while l < d and check:
                                        s1 =0.5*abs((max_list[j] - l)*(close_list[min_list[k]] - close_list[l]) - (min_list[k] - l)*(close_list[max_list[j]] - close_list[l]))
                                        s2 =0.5*abs((l - x)*(close_list[min_list[k]] - y) - (min_list[k] - x)*(close_list[l] - y))
                                        s3 =0.5*abs((max_list[j] - x)*(close_list[l] - y) - (l - x)*(close_list[max_list[j]] - y))
                                        if (s1+s2+s3)*0.9995 > s:
                                            check = False
                                        '''
                                        if l > min_list[k] and l < min_counter:
                                            s1 =0.5*abs((max_list[j] - min_list[l])*(close_list[min_list[k]] - close_list[min_list[l]]) - (min_list[k] - min_list[l])*(close_list[max_list[j]] - close_list[min_list[l]]))
                                            s2 =0.5*abs((max_list[j] - x)*(close_list[min_list[l]] - y) - (min_list[l] - x)*(close_list[max_list[j]] - y))
                                            s3 =0.5*abs((min_list[l] - x)*(close_list[min_list[k]] - y) - (min_list[k] - x)*(close_list[min_list[l]] - y))
                                            if s1+s2+s3 > s:
                                                check = False
                                                '''
                                        l+=1
                                    if volume_list[i] > v*2 and check and ll>20:
                                        # and abs(1.0 - close_list[int(round(x))]/y) < 0.05:                                        
                                        if close_list[i] > close_list[max_list[j]] and close_list[i] < close_list[i-1] and buy == False:
                                        #and close_list[i] < close_list[max_list[j]]*1.005: 
                                        #and ema_list[i] > close_list[max_list[j]]:
                                        #y + close_list[max_list[j]] - close_list[min_list[k]] and close_list[i]*0.995 < y + close_list[max_list[j]] - close_list[min_list[k]]:
                                            div_list[i] = 1
                                            target = close_list[max_list[j]] + close_list[max_list[j]] - close_list[min_list[k]]
                                            buy = True
                                            sell = False
                                            tri_list[max_list[j]] = close_list[max_list[j]]
                                            tri_list[min_list[k]] = close_list[min_list[k]]
                                            tri_list[int(round(x))] = y
                                            f=min_list[k]
                                            if max_list[j] < min_list[k]:
                                                f = max_list[j]
                                            for q in range(f, int(round(x))):
                                                tri_list[q] = close_list[q]
                                            for q in range(max_list[j], max_list[m]):
                                                list1[q]=-1.0*(a1*q + c1)/b1
                                            for q in range(min_list[k], min_list[n]):
                                                list2[q] = -1.0*(a2*q +c2)/b2
                                            n=number
                                            m=number
                                            k=number
                                            j=number
                                            max_counter=0
                                            min_counter=0
                                            max_list=[0 for x in range(number)]
                                            min_list=[0 for x in range(number)]
                                             
                                        elif close_list[i] < close_list[min_list[k]] and close_list[i]>close_list[i-1] and buy:
                                            #y -  close_list[max_list[j]] + close_list[min_list[k]] and close_list[i]*1.005 > y - close_list[max_list[j]] + close_list[min_list[k]]:
                                            div_list[i] = 2
                                            sell = True
                                            buy = False
                                            tri_list[max_list[j]] = close_list[max_list[j]]
                                            tri_list[min_list[k]] = close_list[min_list[k]]
                                            tri_list[int(round(x))] = y
                                            f=min_list[k]
                                            if max_list[j] < min_list[k]:
                                                f = max_list[j]
                                            for q in range(f, int(round(x))):
                                                tri_list[q] = close_list[q]
                                            for q in range(max_list[j], max_list[m]):
                                                list1[q]=-1.0*(a1*q + c1)/b1
                                            for q in range(min_list[k], min_list[n]):
                                                list2[q] = -1.0*(a2*q +c2)/b2
                                            n=number
                                            m=number
                                            k=number
                                            j=number
                                            max_counter=0
                                            min_counter=0
                                            max_list=[0 for x in range(number)]
                                            min_list=[0 for x in range(number)]

                                            '''
                                            if sell == False:
                                                target = close_list[min_list[k]] -  close_list[max_list[j]] + close_list[min_list[k]]
                                            buy = False
                                            sell = True
                                            '''
                                            
                            n+=1
                        m+=1
                    k+=1
                j +=1






                    





        l = 50
        j=0
        while j < max_counter:
            if max_list[j] < i - l:
                k = j
                while k < max_counter - 1:
                    max_list[k] = max_list[k +1]
                    k +=1
                max_counter -=1
                max_list[max_counter] = 0
            j += 1
        
        j = 0
        while j < min_counter:
            if min_list[j] < i - l:
                k = j
                while k < min_counter -1:
                    min_list[k] = min_list[k+1]
                    k +=1
                min_counter -=1
                min_list[min_counter] = 0
            j +=1
            
        i += 1
    offline(name='triangles_19_02', div_list=div_list, additional_charts=[
        {'data': tri_list},
        {'data': list1},
        {'data': list2},
    ])
    return div_list

def BB_strategy_and_rsi(): 
    div_list = np.zeros(len(close_list))
    BB_TL = ind.up_bb(close_list, 20)
    BB_BL = ind.low_bb(close_list, 20)
    rsi_list = ind.rsi(close_list, 14)
    ema_list = ind.ema(close_list, 50)
    for i in range(len(close_list)-1):
        if min_list[i] < BB_BL[i] and rsi_list[i] < 30 and max_list[i] < ema_list[i]: 
            div_list[i+1] = 1
        if max_list[i] > BB_TL[i] and rsi_list[i] > 70 and min_list[i] > ema_list[i]:
            div_list[i+1] = 2
    offline(name='BB_strategy_and_rsi', div_list=div_list, additional_charts=[
        {'data': BB_TL, 'color': 'red'}, 
        {'data': BB_BL, 'color': 'green'}, 
        {'data': ema_list, 'color': 'orange'}, 
        {'data': rsi_list, 'priority': 'secondary', 'color': 'purple'}, 
        {'data': [30, 70], 'priority': 'secondary', 'type': 'horizontal_chart', 'color': 'red'}
        ])
    return div_list

def macd_strategy():
    macd_list = ind.macd(close_list)
    div_list = np.zeros(len(close_list))
    macd_trend = np.empty(len(close_list))
    macd_trend[0] = 1
    for i in range(1, len(macd_trend)):
        if macd_list[i] > macd_list[i-1]:
            macd_trend[i] = 1
        else:
            macd_trend[i] = 2

    for i in range(len(open_list)-1):
        '''if macd_list[i-1] < macd_list[i-2] and macd_list[i-1] < macd_list[i] and macd_list[i-1] < 0:
            div_list[i+1] = 1
        if macd_list[i-1] > macd_list[i-2] and macd_list[i-1] > macd_list[i] and macd_list[i-1] > 0:
            div_list[i+1] = 2'''
        '''if macd_trend[i-3] == 2 and macd_trend[i-2] == 1 and macd_trend[i-1] == 1 and macd_trend[i] == 1 and macd_list[i-2] < 0:
            div_list[i+1] = 1
        if macd_trend[i-3] == 1 and macd_trend[i-2] == 2 and macd_trend[i-1] == 2 and macd_trend[i] == 2 and macd_list[i-2] > 0:
            div_list[i+1] = 2'''
        if macd_list[i] > 0:
            div_list[i+1] = 1
        if macd_list[i] < 0:
            div_list[i+1] = 2
    return div_list

def extrema_stop(div_list):
    div_list = no_repeat(div_list)

    min_prices = []
    min_prices_copy = [None]*len(open_list)
    order_length = 20

    border_list = [None]*len(open_list)
    for i in range(2*order_length, len(open_list)-2, 2*order_length):
        border_list[i] = open_list[i]
        temp_min_list = min_list[i-2*order_length:i]
        min_idx = list(argrelextrema(temp_min_list, np.less, order=order_length)[0])
        for j in min_idx:
            min_prices.append([i-2*order_length+j, temp_min_list[j]])
            min_prices_copy[i-2*order_length+j] = temp_min_list[j]
        if min_prices[-1][1] > open_list[i+1]:
            div_list[i+2] = 2
    offline(name='sma_and_extrema', div_list=div_list, additional_charts=[
        {'data': border_list, 'color': 'blue', 'special_params': {'marker': '|'}},
        {'data': min_prices_copy, 'color': 'orange', 'type': 'point_chart'},
        {'data': ind.sma(close_list, 200), 'color': 'blue'}
        ])
    return div_list


def triangles_06_03():
    div_list = [0 for x in range(len(close_list))]
    ema_list = ind.ema(close_list, 10)
    ema55 = ind.ema(close_list, 55)
    max_list = []
    min_list = []
    maximum_list = []
    minimum_list = []
    max_z_list = []
    min_z_list = []
    g=-1
    h=-1
    f=-1
    i = 0
    v = 0
    t=0
    d = -1
    while i < len(close_list):
        if close_list[i-2] > close_list[i-1] and close_list[i-1] < close_list[i]:
            min_list.append(i-1)
        elif close_list[i-2] < close_list[i-1] and close_list[i-1] > close_list[i]:
            max_list.append(i-1)
        
        q = g+1
        while q < len(max_list)-1:
            if close_list[max_list[q-1]] < close_list[max_list[q]] and close_list[max_list[q]] > close_list[max_list[q+1]]:
                max_z_list.append(max_list[q])
                g = q
            q += 1
        q = h+1
        while q < len(min_list)-1:
            if close_list[min_list[q-1]] > close_list[min_list[q]] and close_list[min_list[q]] < close_list[min_list[q+1]]:
                min_z_list.append(min_list[q])
                h = q
            q+=1
        q = d+1
        while q < len(max_z_list)-1:
            if close_list[max_z_list[q-1]] < close_list[max_z_list[q]] and close_list[max_z_list[q]] > close_list[max_z_list[q+1]]:
                maximum_list.append(max_z_list[q])
                d = q
            q += 1
        q = f+1
        while q < len(min_z_list)-1:
            if close_list[min_z_list[q-1]] > close_list[min_z_list[q]] and close_list[min_z_list[q]] < close_list[min_z_list[q+1]]:
                minimum_list.append(min_z_list[q])
                f = q
            q+=1
        v = v + volume_list[i]
        v = 0
        for q in range(i-5, i):
            v=v + volume_list[q]
        v=v/5
        if volume_list[i] > v * 1.5 and len(maximum_list) > 2 and len(minimum_list) > 2:
            j = len(maximum_list) - 2
            m = len(maximum_list) - 1
            k = len(minimum_list) - 2
            n = len(minimum_list) - 1
            a1 = close_list[maximum_list[j]] - close_list[maximum_list[m]]
            a2 = close_list[minimum_list[k]] - close_list[minimum_list[n]]
            b1 = maximum_list[m] - maximum_list[j]
            b2 = minimum_list[n] - minimum_list[k]
            c1 = maximum_list[j]*close_list[maximum_list[m]] - maximum_list[m]*close_list[maximum_list[j]]
            c2 = minimum_list[k]*close_list[minimum_list[n]] - minimum_list[n]*close_list[minimum_list[k]]
            x = (b2*(c2*a1 - c1*a2)/(b2*a1 - b1*a2) - c2)/a2
            y = (c1*a2 - c2*a1)/(b2*a1 - b1*a2)
            '''for q in range(maximum_list[j], maximum_list[m]+1):
                list1[q]=-1.0*(a1*q + c1)/b1
            for q in range(minimum_list[k], minimum_list[n]+1):
                list2[q] = -1.0*(a2*q +c2)/b2'''
            w=False
            if ema55[i-1] < ema_list[i-1] and ema55[i] >ema_list[i]:
                w = True
            elif ema55[i-1] > ema_list[i-1] and ema55[i] < ema_list[i]:
                w = True
            if x > maximum_list[m] and x > minimum_list[n] and w:    
                if close_list[i] < y :
                    div_list[i]=1               
                elif close_list[i] > y :
                    div_list[i]=2              
            t += 1


        i += 1
    return div_list

# if __name__ == '__main__':

#extrema_stop(sma_strategy())
offline(name='triangles_13dot12')
#offline(name='BB_and_extrema', div_list=true_divergence()) #old_main.extrema_stop(true_divergence(), open_list)

'''print(len(df.index))
print(df.index[-10:-1])
print(len(ind.macd(close_list)))
print(ind.macd(close_list)[-10:-1])'''

#triangles_19_02()
#BB_strategy_and_rsi()
"""df = pd.read_csv('1h_BTC_USD_Bitstamp.csv', sep=',', parse_dates=True, index_col=0)
best_funcion = '1'
max_profit = -100
functions = ['rsi_level_strategy', 'fibonacci_retracement', 'stylus', 'average_price_action_v1', 'triangles', 'BB_strategy', 'BB_strategy_new', 'bull_dog', 'market_profile', 'sma_strategy', 'sma_strategy_new', 'triangles_with_volume', 'triangles_with_ema', 'triangles_13dot12', 'flags', 'trend_recognition', 'true_divergence']
for t in range(1, 25):
    local_max_profit = -100
    local_best_function = '1'
    print('timeframe:', str(t) + 'H')
    timeframe = '1H' 
    df_ohlc = df['close'].resample(str(t) + 'H').ohlc()
    volume_list = df['volumefrom'].resample(str(t) + 'H').sum().values
    open_list = df_ohlc['open'].values
    close_list = df_ohlc['close'].values
    min_list = df_ohlc['low'].values
    max_list = df_ohlc['high'].values
    for i in functions:
        profit = offline(name=i, get_profit=True)
        if max_profit < profit:
            max_profit = profit
            best_funcion = i
            local_max_profit = profit
            local_best_function = i
    print('max_profit = {} has {}'.format(local_max_profit, local_best_function))
print('Ultimate max_profit = {} has {}'.format(max_profit, best_funcion))
offline(name=best_funcion)"""
#functions = ['BB_strategy', 'market_profile', 'sma_strategy', 'triangles_with_volume', 'triangles_with_ema', 'triangles_13dot12', 'flags', 'true_divergence']
