# -*- encoding: utf-8 -*-

import time

import bitstamp.client
import numpy as np
import pandas as pd

pd.core.common.is_list_like = pd.api.types.is_list_like
from datetime import datetime
import bt

from data.utils import get_data
from apscheduler.schedulers.blocking import BlockingScheduler
from cbt.core_custom import Online

#from optimization.utils import load_generators
from abc import ABC, abstractmethod
import ccxt

import re
from os.path import abspath

import logging

from telegram import Bot
from telegram import Update
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters


BITSTAMP_USERNAME = '-'  # cropped
BITSTAMP_KEY = '-'  # cropped
BITSTAMP_SECRET = '-'  # cropped

TG_ID = 0  # cropped
TG_HASH = '-'  # cropped

log_file = 'trade_log.log'
logging.basicConfig(
    filename='./' + log_file,
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S',
    format='%(levelname)s - %(asctime)s - ' + BITSTAMP_USERNAME + ':\n%(message)s\n'
)


class MessageClient(ABC):

    @abstractmethod
    def send_message(self, msg):
        pass


class DefaultMessageClient(MessageClient):

    def send_message(self, msg, chat_id=None):
        pass


class TradingClient(ABC):

    @abstractmethod
    def get_balance(self):
        pass


class DefaultTradingClient(TradingClient):

    def get_balance(self):
        return {'Total': {'USD': 0.0}}


class MarketTradingCllient(TradingClient):

    def __init__(self, exchnage: ccxt.Exchange, online: Online, data_params, **kwargs):

        if not isinstance(online, Online):
            raise TypeError('online must be Online type')

        self._symbols_to_execute = dict()
        self._update_pos_tries = kwargs.get('update_pos_tries', 5)
        self._update_pos_delay = kwargs.get('update_pos_delay', 300)

        # TODO надо передавать параметры и создавать объект из параметров тут
        self._data_params = data_params
        self._online = online
        bt.run(self._online)
        self.exchange = exchnage
        self._msg_client = DefaultMessageClient()

        markets = self.exchange.fetch_markets()
        self._symbols_precision = dict()
        for market in markets:
            if market['quote'] == 'USD':
                self._symbols_precision[market['base'].upper()] = market['precision']['amount']

    def get_amount_precision(self, base, quote='USD'):
        return self._symbols_precision.get(base.upper(), 2)

    def run(self):
        sched = BlockingScheduler()
        sched.add_job(self._execute, 'cron', minute=0)
        logging.info('Trading bot is launched')
        print('Trading bot is launched')
        sched.start()

    def _execute(self):
        try:
            self._data_params['start_date'] = datetime.now().strftime('%Y-%m-%d')
            self._data_params['last_date'] = datetime.now().strftime('%Y-%m-%d')
            data = get_data(**self._data_params)
            if len(data) == 0:
                raise Exception('No data to process')
            self._online.update_with_data(data.index[-1], data.iloc[-1])
            positions = self._online.positions
            prev_positions = positions.iloc[-2]
            last_positions = positions.iloc[-1]
            diff_position = last_positions - prev_positions
            diff_position = diff_position[diff_position != 0]
            if len(diff_position) > 0:
                self.cancell_all_orders()

                weights = self._online.security_weights.iloc[-1]
                weights = (weights / weights.sum()).round(4)

                fee = self.get_fee()
                balance = self.get_acc_balance('USD') * fee * .99

                new_positions = balance * weights * np.sign(last_positions)
                new_positions = new_positions.dropna()

                current_positions = self.get_balance()
                total_amount = pd.Series(current_positions['total'])

                diff_amount = new_positions - total_amount
                diff_amount = diff_amount[diff_amount != 0].sort_values()
                for symbol, amount in diff_amount.items():
                    diff_amount[symbol] = np.round(amount, self.get_amount_precision(symbol))
                n = 0
                while len(diff_amount) and n < self._update_pos_tries:
                    order_errors = self._create_orders(diff_amount)
                    if len(order_errors) > 0:
                        self._msg_client.send_message('Unable to create orders in try {}\n{}'
                                                      .format(n + 1, '\n'.join(map(str, order_errors))))
                    # TODO very very very bad! change to non-blocking call
                    time.sleep(self._update_pos_delay)
                    n += 1

                if len(diff_amount) > 0:
                    raise Exception('Unable to create orders for {}'.format(', '.join(diff_amount.index)))
        except Exception as e:
            self._msg_client.send_message('Unable to update position. Check log file.\n{}'.format(e))

    def _create_orders(self, amounts):
        symbols_to_execute = amounts.copy().to_dict()
        errors = []
        try:
            for symbol, amount in symbols_to_execute.items():
                try:
                    self.market_order(amount, symbol, 'USD', 'buy' if amount > 0 else 'sell', raise_exception=True)
                    amounts.pop(symbol)
                except ccxt.errors.InsufficientFunds as ife:
                    logging.exception(ife)
                    errors.append(ife)
                    try:
                        free_balance = self.get_balance()['free'] * self.get_fee() * .99
                        price = self.exchange.fetch_tickers(symbol.upper() + '/USD')['last']
                        max_amount = free_balance / price
                        amounts[symbol] = np.round(np.min([amounts[symbol], max_amount]), 4)
                    except Exception as e:
                        logging.exception(e)
                        errors.append(e)
                        amounts[symbol] = np.round(amounts[symbol] * .98, self.get_amount_precision(symbol))
                except ccxt.errors.InvalidOrder as ioe:
                    logging.exception(ioe)
                    errors.append(ioe)
                    amounts.pop(symbol)
                except Exception as e:
                    logging.exception(e)
                    errors.append(e)
        except Exception as e:
            logging.exception(e)
            errors.append(e)

        return errors

    @property
    def msg_client(self):
        return self._msg_client

    @msg_client.setter
    def msg_client(self, client):
        self._msg_client = client

    def get_balance(self):
        logging.info('Loading status')
        balances = dict()
        try:
            fb = self.exchange.fetch_free_balance()
            ub = self.exchange.fetch_used_balance()
            tb = self.exchange.fetch_total_balance()
            balances = {
                'free': fb,
                'used': ub,
                'total': tb
            }
            currencies = self.exchange.currencies
            msg = ''
            if currencies is not None and len(currencies) > 0:
                for currency in currencies:
                    msg += currency + ':\n'
                    msg += '\tFree:  {}\n\tUsed:  {}\n\tTotal: {}\n'.format(
                        fb.get(currency, 0.0),
                        ub.get(currency, 0.0),
                        tb.get(currency, 0.0))
                logging.info('Status loaded:\n{}'.format(msg))
        except Exception as e:
            logging.exception(e)
            self.msg_client.send_message('Unable to load account status. Check logs.')
        return balances

    def get_acc_balance(self, quote='USD'):
        quote = quote.upper()
        try:
            amounts = self.exchange.fetch_total_balance()
            #TODO very very bad! create func for ticker fetch
            prices = dict()
            if hasattr(self.exchange, 'fetch_tickers'):
                tickers = self.exchange.fetch_tickers([s.upper() + '/' + quote for s in amounts.keys()])
                for ticker in tickers:
                    prices[ticker['symbol'].split('/')[0]] = ticker['last']
            elif hasattr(self.exchange, 'fetch_ticker'):
                fetch_ticker = getattr(self.exchange, 'fetch_ticker')
                for symbol in amounts.keys():
                    prices[symbol] = fetch_ticker(symbol + '/' + quote)['last']
            balance = amounts.pop(quote, 0.0)
            for symbol, amount in amounts.items():
                balance += prices[symbol] * amount
            return balance
        except Exception as e:
            logging.exception(e)

        return 0.0

    def get_fee(self, fee_type='taker'):
        logging.debug('Call fee')
        fee = 0.025
        try:
            if fee_type not in ['taker', 'maker']:
                logging.error('Wrong fee type')
                self.msg_client.send_message('Wrong fee type. Changed to "taker".')
                fee_type = 'taker'
            fee = bitstamp.fees[fee_type]
            logging.debug('{} fee is loaded: {}'.format(fee_type.capitalize(), fee))
        except Exception as e:
            logging.exception(e)
            self.msg_client.send_message('Unable to load {} fee. Check logs.'.format(fee_type))
        return float(1 - fee / 100)

    def market_order(self, amount, currency, quote, side, raise_exception=False):
        if amount == 0:
            logging.error('Unable to execute {} {}/{}'.format(amount, currency, quote))
            self.msg_client.send_message('Trying to execute 0 amount order. Check logs')
            return

        op = side.capitalize() + 'ing'
        symbol = '/'.join([currency, quote]).upper()

        try:
            amount = np.round(amount * self.get_fee(), 8)
            logging.info('{} {} {:.4f}'.format(op, currency, amount))
            response = self.exchange.create_order(symbol, 'market', side, amount, None)
            return response
        except Exception as error:
            logging.exception('{} order error:\n{}'.format(op, error))
            self.msg_client.send_message('Unable to execute order. Check logs.\n{}'.format(error))
            if raise_exception:
                raise error

    def buy_market(self, amount, currency, quote):
        return self.market_order(amount, currency, 'buy', quote)

    def sell_market(self, amount, currency, quote):
        return self.market_order(amount, currency, 'sell', quote)

    def iceberg_order(self, **kwargs):
        amount = kwargs.get('amount')
        currency = kwargs.get('currency')
        max_amount = kwargs.get('max_amount', 50000)
        green_zone = kwargs.get('green_zone')
        orange_zone = kwargs.get('orange_zone')
        side = kwargs.get('side')
        time_for_order = kwargs.get('time for order', 60)
        maximum_tries = kwargs.get('maximum tries', 5)
        base = kwargs.get('base', 'USD')
        precision = kwargs.get('precision', 8)
        try:
            book = self.exchange.fetch_order_book(currency.upper()+"/"+base)
        except:
            #TODO Telegram error message here
            return

        x = 0
        orders = []
        overall_amount = 0

        if side == 'Buy':
            book = book['asks']
            price = book[0][0]

        elif side == 'Sell':
            book = book['bids']
            price = book[0][0]

        else:
            raise Exception('Side can be either buy or sell')

        start_price = price
        while overall_amount < amount:
            if (price < book[0][0]*green_zone and side == 'Buy') or (price > book[0][0]*green_zone and side == 'Sell'):
                price = book[x][0]
                overall_amount += book[x][1]
                x += 1
            else:
                break

        overall_amount = min(overall_amount, amount)

        if overall_amount > 0:
            orders.append(self.order_exexuter_tryer(overall_amount, currency, 'limit', side, price, precision)['id'])

        time.sleep(time_for_order)
        if None in orders:
            remaining = overall_amount
        else:
            remaining = self.order_canceller(orders)
        max_amount /= start_price
        if overall_amount-remaining > 0.998*amount:
            return
        amount -= overall_amount + remaining
        amount = max(amount, 0)
        order_amount = 1
        order_value = amount
        while order_value >= max_amount:
            order_value = amount/order_amount
            order_amount += 1

        overall_remaining = 0
        for order_number in range(order_amount-1):
            orders = []
            remaining = order_value
            for _ in range(maximum_tries):
                book = exchange.fetch_order_book(currency.upper() + "/USD")
                price = (book['asks'][0][0]+book['bids'][0][0])/2
                if (price < start_price * orange_zone and side == 'Buy') or (
                        price > start_price * orange_zone and side == 'Sell'):
                    orders.append(self.order_exexuter_tryer(remaining, currency, 'limit', side, price, precision)['id'])
                    time.sleep(time_for_order)
                    remaining = self.order_canceller(orders)
                if remaining is None:
                    remaining = order_value
                    continue
                if remaining < order_value/10:
                    break
            overall_remaining += remaining

        self.order_exexuter_tryer(overall_remaining, currency, 'limit', side, start_price)

    def order_canceler(self, id_list,maximum_tries):
        remaining = 0
        for id in id_list:
            if id is None:
                continue
            id = int(id)
            order = self.exchange.fetch_order(id)
            if order['remaining'] is None:
                continue
            if order['remaining'] != 0:
                for _ in range(maximum_tries):
                    try:
                        if exchange.fetch_order_status(id) == 'canceled':
                            break
                        exchange.cancel_order(id)
                    except Exception as e:
                        if 'Order not found' in e.args[0]:
                            break

                remaining += order['remaining']
        return remaining

    def order_exexuter_tryer(self, amount, currency, type, side, price=None, recursion=None, precision=8):
        if type == 'Market' and price is not None:
            raise Exception('Market orders do not need a predifined price')
        amount = np.round(amount, precision)
        try:
            order = self.exchange.create_order(symbol=currency+'/USD', type=type, side=side, amount=amount, price=price)
            return order
        except Exception as e:

            print('e', e)
            if 'Invalid nonce' in e.args[0]:
                return {'id': None, 'Error': e}

            if 'Minimum order size' in e.args[0]:
                return {'id': None, 'Error': e}

            if 'You have only' in e.args[0]:
                start_ind = e.args[0].index('only')
                end_ind = e.args[0].index('available')
                try:
                    ind1 = e.args[0].index('need')
                    ind2 = e.args[0].index('to open')
                    amount = float(amount)*0.9975*float((e.args[0][start_ind+4:end_ind-5]))/float((e.args[0][ind1+4:ind2-5]))
                except:
                    amount = float((e.args[0][start_ind+4:end_ind-5]))*0.9975
                if amount != 0 and recursion is None:
                    try:
                        return self.order_exexuter_tryer(np.round(amount, precision),currency,type,side,price,1)
                    except:
                        return {'id': None,'Error':e}

            return {'id': None, 'Error': e}

    def cancell_all_orders(self):
        orders = exchange.fetch_open_orders()
        errors = []
        for i in range(len(orders)):
            try:
                exchange.cancel_order(orders[i]['id'])
            except Exception as e:
                logging.exception(e)
                errors.append(e)
        return errors


class GmailNotification:

    def __init__(self, amount=None, price=None, currency='BTC'):
        self.gmail_user = '-'  # cropped
        self.gmail_password = '-'  # cropped
        self.sent_from = self.gmail_user
        self.to = ['-']  # cropped
        self.amount = amount
        self.price = price
        self.currency = currency
        self.body = None
    def sending(self):
        try:
            '''
            server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
            server.ehlo()
            server.login(self.gmail_user, self.gmail_password)
            server.sendmail(self.sent_from, self.to, self.body)
            server.close()
            '''
            print('Email sent!')
        except:
            print('Something went wrong...')
    def sold_mail(self):
        self.body = 'Sold ' + str(self.amount) + self.currency + ' for $' + str(self.price) + '\nValue: $' + str(
            float(self.amount) * float(self.price))
        self.sending()

    def bought_mail(self):
        self.body = 'Bought ' + str(self.amount) + self.currency + ' for $' + str(self.price) + '\nValue: $' + str(
            float(self.amount) * float(self.price))
        self.sending()

    def error_mail(self, error='Error has occurred'):
        self.body = error
        self.sending()


class TelegramMessageClient(MessageClient):

    def __init__(self, name, id, hash, default_chat):
        self._default_chat = default_chat
        self._order_pattern = '^(?i)(buy|sell)\s*([0-9\.]*)\s*([a-z]*)\s*([a-z]*)$'
        self._order_regex = re.compile(self._order_pattern)
        self._bot = Bot(
            token=':'.join([str(id), str(hash)]),
            base_url='https://telegg.ru/orig/bot',  # vpn for telegram
        )
        self._updater = Updater(
            bot=self._bot,
        )
        start_handler = CommandHandler("start", self.say_hi)
        balance_chandler = CommandHandler("balance", self.balance)
        log_chandler = CommandHandler("log", self.send_log)
        hi_handler = MessageHandler(Filters.regex('^(?i)hello$'), self.say_hi)
        balance_handler = MessageHandler(Filters.regex('^(?i)balance$'), self.balance)
        order_handler = MessageHandler(Filters.regex(self._order_pattern), self.buy_sell)

        self._updater.dispatcher.add_handler(start_handler)
        self._updater.dispatcher.add_handler(hi_handler)
        self._updater.dispatcher.add_handler(balance_chandler)
        self._updater.dispatcher.add_handler(log_chandler)
        self._updater.dispatcher.add_handler(balance_handler)
        self._updater.dispatcher.add_handler(order_handler)

        self._updater.start_polling()

        self._trading_client = None

    @property
    def client(self):
        return self._client

    @property
    def trading_client(self):
        return self._trading_client

    @trading_client.setter
    def trading_client(self, client: TradingClient):
        self._trading_client = client

    def send_message(self, msg, chat_id=None):
        self._bot.send_message(
            chat_id=self._default_chat if chat_id is None else chat_id,
            text=msg,
        )

    def say_hi(self, bot: Bot, update: Update):
        bot.send_message(
            chat_id=update.message.chat_id,
            text='Hello!',
        )

    def balance(self, bot: Bot, update: Update):
        balances = self.trading_client.get_balance()
        data = dict()
        if len(balances) > 0:
            for bname, bdata in balances.items():
                for currency, cdata in bdata.items():
                    currency = currency.upper()
                    bname = bname.capitalize()
                    tmp_dict = data.get(currency, dict())
                    tmp_dict[bname] = cdata
                    data[currency] = tmp_dict
        msg = 'Account balances:\n'
        for currency, balance in data.items():
            msg += currency + ':\n'
            for name, value in balance.items():
                msg += '\t{:7}{}\n'.format(name, value)
        self.send_message(msg, update.effective_chat.id)

    def buy_sell(self, bot: Bot, update: Update):
        try:
            side, amount, currency, quote = self._order_regex.findall(update.message.text)[0]
            side = side.lower()
            amount = float(amount)
            currency = currency.upper()
            quote = quote.upper()
            order = self.trading_client.market_order(amount, currency, quote, side, True)
        except Exception as e:
            self.send_message('Unable to create order. Check logs.\n{}'.format(e), update.effective_chat.id)

    def send_log(self, bot: Bot, update: Update):
        try:
            bot.send_document(
                text="Your log file, my lord.",
                chat_id=update.effective_chat.id,
                filename=log_file,
                document=open(abspath('./' + log_file), 'rb')
            )
        except Exception as e:
            logging.exception(e)
            self.send_message('Unable to send log file.\n{}'.format(e), update.effective_chat.id)


if __name__ == '__main__':
    '''
    parser = argparse.ArgumentParser()

    parser.add_argument('-c', help='currency list', type=str, nargs='+')
    parser.add_argument('-l', help='limit in % or fixed', type=float, nargs='+')
    parser.add_argument('-p', help='use proxy flag', type=bool, nargs='?', default=False)

    args = parser.parse_args()

    if len(args.l) != 1 and len(args.l) != len(args.c):
        raise Exception('Wrong length of currency and limit lists')

    if len(args.l) == 1:
        args.l = np.repeat(args.l, len(args.c))

    pool = ProcessPoolExecutor(len(args.c))
    for c, l in zip(args.c, args.l):
        pool.submit(create_client, (c, l, args.p))
    '''
    '''
    params = {
        'skip_period': 0,
        'timeframe': '1H',
        'signal_params': {
            'curr_generators': {
                'BTC': {'BB': {'generator': signals.Random_SignalGenerator, 'params': {}}},
                'ETH': {'BB': {'generator': signals.Random_SignalGenerator, 'params': {}}},
                'XRP': {'BB': {'generator': signals.Random_SignalGenerator, 'params': {}}},
                'BCH': {'BB': {'generator': signals.Random_SignalGenerator, 'params': {}}},
                'LTC': {'BB': {'generator': signals.Random_SignalGenerator, 'params': {}}}
            }
        }
    }
    '''

    client = TelegramMessageClient('anon', TG_ID, TG_HASH, None)

    # BITSTAMP_USERNAME = '-'  # cropped
    # BITSTAMP_KEY = '-'  # cropped
    # BITSTAMP_SECRET = '-'  # cropped

    # params = {
    #     'apiKey': BITSTAMP_KEY,
    #     'secret': BITSTAMP_SECRET,
    #     'uid': BITSTAMP_USERNAME,
    # }
    # bitstamp = ccxt.bitstamp(params)
    #
    # trading_client = MarketTradingCllient(bitstamp, Online(), {})
    # client.trading_client = trading_client

    #Bitstamp('BTC', 500).new_online(params)

    exchange = ccxt.bitstamp({
        "apiKey": "-",  # cropped
        "secret": "-",  # cropped
        "uid": "-"  # cropped
    })
    #orders = exchange.fetch_balance()
    #print(orders)
    params = {'Amount': 0.002, 'Currency': 'BTC', 'Max_amount': 6, 'Green_zone': 1,
              'Orange_zone': 0.998, 'Side': 'Sell','Time for order': 10}



    #Online('BTC',1,1, exchange).iceberg_order(params)
    # print(exchange.fetch_balance())
    # x = exchange.fetch_open_orders()
    # exchange.create_order('BTC/USD', 'limit', 'buy', 1, 90)
    # orders = exchange.fetch_open_orders()
    # print('a', orders)
    # for i in range(len(orders)):
    #     id = orders[i]['id']
    #     exchange.cancel_order(id)
    # order = exchange.fetch_order(id)
    # print('canc', order)
    # exchange.cancel_order(orders[0]['id'])