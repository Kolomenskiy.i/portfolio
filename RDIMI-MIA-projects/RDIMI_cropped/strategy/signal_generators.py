# -*- encoding: utf-8 -*-
import numbers
import random
from abc import ABC, abstractmethod

import numpy as np
import pandas as pd
from scipy.signal import argrelextrema

from indicator import indicators as ind
from stats.stats import Charts
import pylab as plt
import copy
import scipy.stats as stats


class SignalGenerator(ABC):
    def __init__(self, params=None, start_date=None, end_date=None, charts=None):
        self._params = {} if params is None else params.copy()
        self._trade_types = params.get('trade_types', ['long']) if params is not None else ['long']
        params_values_list = [v for v in self._params.values() if isinstance(v, numbers.Number)]
        self._max_period = np.max(params_values_list) if len(params_values_list) > 0 else 0
        self._start_date = start_date
        self._end_date = end_date
        self._charts = charts

    def __call__(self, quotes):
        signals = self.generate_signals(quotes)
        if self._start_date is not None:
            signals = signals.loc[signals.index >= self._start_date]
        if self._end_date is not None:
            signals = signals.loc[signals.index <= self._end_date]

        if 'stop_loss' in self._params or 'trailing_stop_loss' in self._params:
            signals = self.stop_loss(signals, quotes, list(self._params.keys())[0])
            # TODO: [0] is not correct, it can be not stoploss but smth else
        if 'atr_stop_loss' in self._params or 'atr_trailing_stop_loss' in self._params:
            signals = self.atr_stop_loss(signals, quotes, list(self._params.keys())[0])
        # TODO: straight processing. If takeprofit is specified earlier it must be processed earlier than stoploss
        if 'take_profit' in self._params:  # takeprofit will be processed later than stoploss
            signals = self.take_profit(signals, quotes)
        if 'atr_take_profit' in self._params:
            signals = self.atr_take_profit(signals, quotes)

        return self.match_type(signals)
        # return signals

    @abstractmethod
    def generate_signals(self, quotes):
        pass

    @property
    def max_period(self):
        return self._max_period

    @property
    def params(self):
        return self._params

    @property
    def start_date(self):
        return self._start_date

    @start_date.setter
    def start_date(self, start_date):
        self._start_date = start_date

    @property
    def end_date(self):
        return self._start_date

    @end_date.setter
    def end_date(self, end_date):
        self._end_date = end_date

    @property
    def trade_types(self):
        return self._trade_types

    @property
    def charts(self):
        return self._charts

    def match_type(self, signals):
        sslice = signals.loc[signals != 0]
        temp_dict = {'long': 1, 'short': -1}
        if len(sslice) != 0:
            dropping_signal = temp_dict[self.trade_types[0]] if len(self.trade_types) == 1 else sslice[0]
        else:
            dropping_signal = None

        first = sslice.loc[sslice == dropping_signal]

        if len(first) == 0:
            return {}
        second = sslice.loc[sslice == -first.iloc[0]]

        cdate = first.index[0]
        ldate = sslice.index[-1]

        signals = {cdate: first.values[0]}

        try:
            while cdate <= ldate:
                second = second.loc[second.index >= cdate]
                cdate = second.index[0]
                signals[cdate] = second.values[0]
                first = first.loc[first.index >= cdate]
                cdate = first.index[0]
                signals[cdate] = first.values[0]
        except:
            pass
        # signals = signals.to_dict()

        return signals.copy()

    # TODO: stop_loss_no_instant_buy_after_sell
    def stop_loss(self, signals, quotes, stop_type):
        in_position = False
        entry_price = 0
        temp_dict = {'long': 1, 'short': -1}
        primary_signal = temp_dict[self.trade_types[0]] if len(self.trade_types) == 1 else signals[signals != 0][0]

        if primary_signal == 1:
            primary_compare_func = np.greater
            stop_loss = self._params.get(stop_type, 100)
        else:
            primary_compare_func = np.less
            stop_loss = -self._params.get(stop_type, 100)

        for i in range(len(signals)):
            if len(self.trade_types) == 2 and signals[i] != 0:
                primary_signal = signals[i]
                if primary_signal == 1:
                    primary_compare_func = np.greater
                    stop_loss = self._params.get(stop_type, 100)
                else:
                    primary_compare_func = np.less
                    stop_loss = -self._params.get(stop_type, 100)
            if stop_type == 'trailing_stop_loss' and in_position and entry_price < quotes.Open.values[i]:
                entry_price = quotes.Open.values[i]
            if not in_position and signals[i] == primary_signal:
                entry_price = quotes.Open.values[i]
                in_position = True
            elif signals[i] == -primary_signal or primary_compare_func([entry_price * (100.0 - stop_loss) / 100], [quotes.Open.values[i]]):
                entry_price = 0
                in_position = False
                signals[i] = -primary_signal

        return signals

    def atr_stop_loss(self, signals, quotes, stop_type):
        atr = ind.get_atr(quotes.High.values, quotes.Low.values, quotes.Close.values, 14)
        in_position = False
        entry_price = 0
        temp_dict = {'long': 1, 'short': -1}
        primary_signal = temp_dict[self.trade_types[0]] if len(self.trade_types) == 1 else signals[signals != 0][0]

        for i in range(len(signals)):
            if len(self.trade_types) == 2 and signals[i] != 0:
                primary_signal = signals[i]
            if primary_signal == 1:
                primary_compare_func = np.greater
                stop_loss = self._params.get(stop_type, 1) * atr[i]
            else:
                primary_compare_func = np.less
                stop_loss = -self._params.get(stop_type, 1) * atr[i]
            if stop_type == 'atr_trailing_stop_loss' and in_position and entry_price < quotes.Open.values[i]:
                entry_price = quotes.Open.values[i]
            if not in_position and signals[i] == primary_signal:
                entry_price = quotes.Open.values[i]
                in_position = True
            elif signals[i] == -primary_signal or primary_compare_func([entry_price - stop_loss], [quotes.Open.values[i]]):
                entry_price = 0
                in_position = False
                signals[i] = -primary_signal

        return signals

    def take_profit_no_instant_buy_after_sell(self, signals, quotes):  # proper! takeprofit but not profitable
        in_position = False
        temp_dict = {'long': 1, 'short': -1}
        primary_signal = temp_dict[self.trade_types[0]] if len(self.trade_types) == 1 else signals[signals != 0][0]

        if primary_signal == 1:
            primary_compare_func = np.less
            take_profit = self._params.get('take_profit', 100)
            entry_price = max(quotes['High'])
        else:
            primary_compare_func = np.greater
            take_profit = -self._params.get('take_profit', 100)
            entry_price = 0
        for i in range(len(signals)):
            if len(self.trade_types) == 2 and signals[i] != 0:
                primary_signal = signals[i]
                if primary_signal == 1:
                    primary_compare_func = np.less
                    take_profit = self._params.get('take_profit', 100)
                else:
                    primary_compare_func = np.greater
                    take_profit = -self._params.get('take_profit', 100)
            if not in_position and signals[i] == primary_signal:
                entry_price = quotes.Open.values[i]
                in_position = True
            elif signals[i] == -primary_signal:
                in_position = False
                signals[i] = -primary_signal
            elif primary_compare_func([entry_price * (100.0 + take_profit) / 100], [quotes.Open.values[i]]):
                entry_price = max(quotes['High']) if primary_signal == 1 else 0
                in_position = False
                signals[i] = -primary_signal
                # code below differs from original takeprofit + other lines which dont influence the result
                j = i + 1
                while signals[j] == 1:
                    signals[j] = 0
                    j += 1

        return signals

    def take_profit(self, signals, quotes):
        in_position = False
        entry_price = 0
        temp_dict = {'long': 1, 'short': -1}
        primary_signal = temp_dict[self.trade_types[0]] if len(self.trade_types) == 1 else signals[signals != 0][0]

        if primary_signal == 1:
            primary_compare_func = np.less
            take_profit = self._params.get('take_profit', 100)
        else:
            primary_compare_func = np.greater
            take_profit = -self._params.get('take_profit', 100)

        for i in range(len(signals)):
            if len(self.trade_types) == 2 and signals[i] != 0:
                primary_signal = signals[i]
                if primary_signal == 1:
                    primary_compare_func = np.less
                    take_profit = self._params.get('take_profit', 100)
                else:
                    primary_compare_func = np.greater
                    take_profit = -self._params.get('take_profit', 100)
            if not in_position and signals[i] == primary_signal:
                entry_price = quotes.Open.values[i]
                in_position = True
            elif signals[i] == -primary_signal or primary_compare_func([entry_price * (100.0 + take_profit) / 100], [quotes.Open.values[i]]):
                entry_price = 0
                in_position = False
                signals[i] = -primary_signal

        return signals

    def atr_take_profit(self, signals, quotes):
        atr = ind.get_atr(quotes.High.values, quotes.Low.values, quotes.Close.values, 14)
        in_position = False
        entry_price = 0
        temp_dict = {'long': 1, 'short': -1}
        primary_signal = temp_dict[self.trade_types[0]] if len(self.trade_types) == 1 else signals[signals != 0][0]

        for i in range(len(signals)):
            if len(self.trade_types) == 2 and signals[i] != 0:
                primary_signal = signals[i]
            if primary_signal == 1:
                primary_compare_func = np.less
                take_profit = self._params.get('atr_take_profit', 1) * atr[i]
            else:
                primary_compare_func = np.greater
                take_profit = -self._params.get('atr_take_profit', 1) * atr[i]
            if not in_position and signals[i] == primary_signal:
                entry_price = quotes.Open.values[i]
                in_position = True
            elif signals[i] == -primary_signal or primary_compare_func([entry_price + take_profit], [quotes.Open.values[i]]):
                entry_price = 0
                in_position = False
                signals[i] = -primary_signal

        return signals


class Buy_n_hold(SignalGenerator):

    def generate_signals(self, quotes):
        signals = pd.Series(0, index=quotes.index.values)
        signals[1] = 1.0
        signals[-1] = -1.0
        return signals


class Buy_n_hold_constantly(SignalGenerator):

    def generate_signals(self, quotes):
        signals = pd.Series(1, index=quotes.index.values)
        signals[-1] = -1.0
        return signals


class Random_SignalGenerator(SignalGenerator):

    def generate_signals(self, quotes):
        signals = pd.Series(0, index=quotes.index.values)
        for i in range(1, len(signals.values)):
            signals[i] = random.choice([1, -1, 0])
        return signals


class Last_Trade_BuyGenerator(SignalGenerator):

    def generate_signals(self, quotes):
        signals = pd.Series(0, index=quotes.index.values)
        signals[-1] = 1
        return signals


class BBSignalGenerator(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 500),
                          std_mult=self.params.get('std', 2.0),
                          ma_type='sma')
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 500),
                            std=self.params.get('std', 2.0),
                            ma_type='sma')

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)

        buy_list = np.where(quotes.Low.values > up_bb)[0] + buy_n
        buy_list = buy_list[buy_list < len(quotes)]
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))

        sell_list = np.where(quotes.High.values < low_bb)[0] + sell_n
        sell_list = sell_list[sell_list < len(quotes)]
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        # self._charts = [
        #     {'Data': up_bb, 'Color': 'black'},
        #     {'Data': low_bb, 'Color': 'black'},
        # ]

        return signals


class BBSignalGenerator_simplified(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 25))
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 25))

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []

        for i in range(len(quotes) - 1):
            if quotes.Low.values[i] > up_bb[i]:
                buy_list.append(i + buy_n)
            if quotes.High.values[i] < low_bb[i]:
                sell_list.append(i + sell_n)

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        # self._charts = [
        #     {'Data': up_bb, 'Color': 'black'},
        #     {'Data': low_bb, 'Color': 'black'},
        # ]

        return signals


class BBSignalGenerator_reverse(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 500), std_mult=self.params.get('std', 2.0))
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 500), std=self.params.get('std', 2.0))

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)

        buy_list = np.where(quotes.Low.values > up_bb)[0] + buy_n
        buy_list = buy_list[buy_list < len(quotes)]
        signals.update(pd.Series(-1, index=quotes.iloc[buy_list].index.values))

        sell_list = np.where(quotes.High.values < low_bb)[0] + sell_n
        sell_list = sell_list[sell_list < len(quotes)]
        signals.update(pd.Series(1, index=quotes.iloc[sell_list].index.values))

        return signals


class BB_strongest_trend_follow(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 500))

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)

        buy_list = np.where(quotes.Low.values > up_bb)[0] + buy_n
        buy_list = buy_list[buy_list < len(quotes)]
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))

        sell_list = np.where(quotes.High.values < up_bb)[0] + sell_n
        sell_list = sell_list[sell_list < len(quotes)]
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class BB_strong_trend_follow(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 50),
                          std_mult=self.params.get('up_std', 1.0))
        up_bb_safety = ind.up_bb(quotes.Close.values, self.params.get('safe_up_period', 25),
                                 std_mult=self.params.get('safe_up_std', 2.0))

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)

        buy_list = np.where((quotes.Low.values > up_bb) & (quotes.High.values < up_bb_safety))[0] + buy_n
        buy_list = buy_list[buy_list < len(quotes)]
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))

        sell_list = np.where(quotes.Low.values < up_bb)[0] + sell_n
        sell_list = sell_list[sell_list < len(quotes)]
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class BB_strong_trend_follow_LONGSHORT(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 50),
                          std_mult=self.params.get('up_std', 1.0))
        up_bb_safety = ind.up_bb(quotes.Close.values, self.params.get('safe_up_period', 50),
                                 std_mult=self.params.get('safe_up_std', 2.0))
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 50),
                            std=self.params.get('down_std', 1.0))
        low_bb_safety = ind.low_bb(quotes.Close.values, self.params.get('safe_low_period', 50),
                                   std=self.params.get('safe_down_std', 2.0))

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)

        buy_list = np.where((quotes.Low.values > up_bb) & (quotes.High.values < up_bb_safety))[0] + buy_n
        buy_list = buy_list[buy_list < len(quotes)]
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))

        sell_list = np.where((quotes.High.values < low_bb) & (quotes.Low.values > low_bb_safety))[0] + sell_n
        sell_list = sell_list[sell_list < len(quotes)]
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class BB_middle_trend_follow(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 50),
                          std_mult=self.params.get('up_std', 1.0))
        up_bb_safety = ind.up_bb(quotes.Close.values, self.params.get('safe_up_period', 50),
                                 std_mult=self.params.get('safe_up_std', 2.0))
        middle_bb = ind.mid_bb(quotes.Close.values, self.params.get('mid_period', 50))

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)

        buy_list = np.where((quotes.Low.values > up_bb) & (quotes.High.values < up_bb_safety))[0] + buy_n
        buy_list = buy_list[buy_list < len(quotes)]
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))

        sell_list = np.where(quotes.Low.values < middle_bb)[0] + sell_n
        sell_list = sell_list[sell_list < len(quotes)]
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class BB_middle_trend_follow_LONGSHORT(SignalGenerator):  # it's not exactly the same strat

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 50),
                          std_mult=self.params.get('up_std', 1.0))
        up_bb_safety = ind.up_bb(quotes.Close.values, self.params.get('safe_up_period', 50),
                                 std_mult=self.params.get('safe_up_std', 2.0))
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 50),
                            std=self.params.get('down_std', 1.0))
        low_bb_safety = ind.low_bb(quotes.Close.values, self.params.get('safe_low_period', 50),
                                   std=self.params.get('safe_down_std', 2.0))
        middle_bb = ind.mid_bb(quotes.Close.values, self.params.get('mid_period', 50))

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)
        exit_n = self.params.get('exit_n', 1)

        signals = pd.Series(0, index=quotes.index.values)

        long_signal = False
        short_signal = False
        for i in range(len(signals) - 1):
            if quotes.Low.values[i] > up_bb[i] and quotes.High.values[i] < up_bb_safety[i]:
                long_signal = True
            if quotes.Low.values[i] < middle_bb[i]:
                long_signal = False
            if quotes.Low.values[i] > low_bb_safety[i] and quotes.High.values[i] < low_bb[i]:
                short_signal = True
            if quotes.High.values[i] > middle_bb[i]:
                short_signal = False
            if long_signal:
                signals[i + buy_n] = 1
            elif short_signal:
                signals[i + sell_n] = -1
            else:
                signals[i + exit_n] = 0

        return signals


class BB_weak_trend_follow(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 500),
                          std_mult=self.params.get('up_std', 1.0))

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)

        buy_list = np.where(quotes.Close.values > up_bb)[0] + buy_n
        buy_list = buy_list[buy_list < len(quotes)]
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))

        sell_list = np.where(quotes.Low.values < up_bb)[0] + sell_n
        sell_list = sell_list[sell_list < len(quotes)]
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class BB_squeeze_catcher(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 500))
        # mid_bb = ind.mid_bb(quotes.Close.values, self.params.get('up_period', 500))
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 500))

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []

        for i in range(len(quotes) - 1):
            if quotes.Low.values[i] > up_bb[i]:
                sell_list.append(i + sell_n)
            elif quotes.Low.values[i] > low_bb[i] and quotes.Low.values[i] < 1.02 * low_bb[i]:
                buy_list.append(i + buy_n)

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        # self._charts = [
        #     {'Data': up_bb, 'Color': 'black'},
        #     {'Data': mid_bb, 'Color': 'black'},
        # ]

        return signals


class BB_and_rsi(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 25))
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 25))
        rsi_list = ind.rsi(quotes.Close.values, 14)

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        buy_flag = False
        sell_flag = False

        for i in range(len(quotes) - 1):
            if quotes.High.values[i] < low_bb[i] and rsi_list[i] < 20:
                buy_flag = True
                sell_flag = False
            if quotes.Low.values[i] > up_bb[i] and rsi_list[i] > 80:
                buy_flag = False
                sell_flag = True
            if buy_flag and rsi_list[i] > 20:
                buy_list.append(i + buy_n)
            elif sell_flag and rsi_list[i] < 80:
                sell_list.append(i + sell_n)

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class BB_Scared(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 500))  # 500
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 500))  # 500

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)

        buy_list = np.where(quotes.Low.values > up_bb)[0] + buy_n
        buy_list = buy_list[buy_list < len(quotes)]
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))

        sell_list = np.where(quotes.Low.values < low_bb)[0] + sell_n
        sell_list = sell_list[sell_list < len(quotes)]
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        # self._charts = [
        #     {'Data': up_bb, 'Color': 'black'},
        #     {'Data': low_bb, 'Color': 'black'},
        # ]

        return signals


class ZigZag(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 500))  # 100
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 500))  # 100
        zz_percent = self.params.get('zz_percent', 20) / 100  # 10

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        bought_flag = False
        bought_count = 0

        k = 0
        zz_last_peak_index = 0
        zz_last_peak_price = 0

        line_list = [0 for _ in range((len(quotes.Close.values)))]
        zz_price = copy.deepcopy(quotes.Close.values)

        for i in range(1000, len(quotes.Close.values) - 1):
            if quotes.Low.values[i] > up_bb[i]:
                zz_list = ind.zigzag.peak_valley_pivots(quotes.Close.values[i-1000: i], zz_percent, -zz_percent)
                zz_list[0], zz_list[-1] = 0, 0
                zz_extremums = pd.Series(zz_list, index=quotes.index.values[i-1000: i])
                zz_extremums = zz_extremums[zz_extremums != 0]
                if len(list(zz_extremums)) > 1 and zz_extremums[-1] == -1:
                    zz_last_peak_index = 1000-1
                    while zz_list[zz_last_peak_index] != 1:
                        zz_last_peak_index -= 1
                    zz_last_peak_price = quotes.High.loc[zz_extremums.index[-2]]
                    low_bb_breakout_price = low_bb[i]
                    k = (low_bb_breakout_price - zz_last_peak_price) / (i - zz_last_peak_index)

                    buy_list.append(i + buy_n)
                    bought_flag = True

                if i <= len(quotes.Close.values) - 1000:
                    for j in range(1000):
                        if zz_list[j] == 0:
                            zz_price[i + j] = 0.0

            elif bought_flag:
                bought_count += 1
                line_value = k * (i - zz_last_peak_index) + zz_last_peak_price
                line_list[i] = line_value
                if (bought_count <= i - zz_last_peak_index and low_bb[i] > line_value) or bought_count > i - zz_last_peak_index:
                    sell_list.append(i + sell_n)
                    bought_flag = False
                    bought_count = 0

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        # self._charts = [
        #     {'Data': zz_price, 'Type': 'Point_chart', 'Color': 'Orange'},
        #     {'Data': line_list, 'Type': 'Point_chart', 'Color': 'Red'},
        #     {'Data': up_bb, 'Color': 'Black'},
        #     {'Data': low_bb, 'Color': 'Black'},
        # ]

        return signals


class ZigZag_longs_shorts(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 100))
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 100))
        zz_percent = self.params.get('zz_percent', 10) / 100

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        bought_flag = False
        sell_flag = False
        bought_count = 0
        sell_count = 0

        k = 0
        zz_last_peak_index = 0
        zz_last_peak_price = 0
        zz_last_valley_index = 0
        zz_last_valley_price = 0

        line_list = [0 for x in range((len(quotes.Close.values)))]
        zz_price = copy.deepcopy(quotes.Close.values)

        for i in range(1000, len(quotes.Close.values) - 1):
            if quotes.Low.values[i] > up_bb[i]:
                zz_list = ind.zigzag.peak_valley_pivots(quotes.Close.values[i-1000: i], zz_percent, -zz_percent)
                zz_list[0], zz_list[-1] = 0, 0
                zz_extremums = pd.Series(zz_list, index=quotes.index.values[i-1000: i])
                zz_extremums = zz_extremums[zz_extremums != 0]
                if len(list(zz_extremums)) > 1:
                    if zz_extremums[-1] == -1:
                        zz_last_peak_index = 1000-1
                        while zz_list[zz_last_peak_index] != 1:
                            zz_last_peak_index -= 1
                        zz_last_peak_price = quotes.High.loc[zz_extremums.index[-2]]
                        low_bb_breakout_price = low_bb[i]
                        k = (low_bb_breakout_price - zz_last_peak_price) / (i - zz_last_peak_index)

                        buy_list.append(i + buy_n)
                        bought_flag = True

                if i <= len(quotes.Close.values) - 1000:
                    for j in range(1000):
                        if zz_list[j] == 0:
                            zz_price[i + j] = 0.0

            elif bought_flag:
                bought_count += 1
                line_value = k * (i - zz_last_peak_index) + zz_last_peak_price
                line_list[i] = line_value
                if (bought_count <= i - zz_last_peak_index and low_bb[i] > line_value) or bought_count > i - zz_last_peak_index:
                    sell_list.append(i + sell_n)
                    bought_flag = False
                    bought_count = 0

            elif quotes.High.values[i] < low_bb[i]:
                zz_list = ind.zigzag.peak_valley_pivots(quotes.Close.values[i-1000: i], zz_percent, -zz_percent)
                zz_list[0], zz_list[-1] = 0, 0
                zz_extremums = pd.Series(zz_list, index=quotes.index.values[i-1000: i])
                zz_extremums = zz_extremums[zz_extremums != 0]
                if len(list(zz_extremums)) > 1:
                    if zz_extremums[-1] == 1:
                        zz_last_valley_index = 1000 - 1
                        while zz_list[zz_last_valley_index] != -1:
                            zz_last_valley_index -= 1
                        zz_last_valley_price = quotes.Low.loc[zz_extremums.index[-2]]
                        up_bb_breakout_price = up_bb[i]
                        k = (up_bb_breakout_price - zz_last_valley_price) / (i - zz_last_valley_index)

                        sell_list.append(i + sell_n)
                        sell_flag = True

                if i <= len(quotes.Close.values) - 1000:
                    for j in range(1000):
                        if zz_list[j] == 0:
                            zz_price[i + j] = 0.0

            elif sell_flag:
                sell_count += 1
                line_value = k * (i - zz_last_valley_index) + zz_last_valley_price
                line_list[i] = line_value
                if (sell_count <= i - zz_last_valley_index and up_bb[i] < line_value) or sell_count > i - zz_last_valley_index:
                    buy_list.append(i + buy_n)
                    sell_flag = False
                    sell_count = 0

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        # self._charts = [
        #     {'Data': zz_price, 'Type': 'Point_chart', 'Color': 'Orange'},
        #     {'Data': line_list, 'Type': 'Point_chart', 'Color': 'Red'},
        #     {'Data': up_bb, 'Color': 'Black'},
        #     {'Data': low_bb, 'Color': 'Black'},
        # ]

        return signals


class ZigZag_shorts(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 100))
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 100))
        zz_percent = self.params.get('zz_percent', 10) / 100

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        sell_flag = False
        sell_count = 0

        k = 0
        zz_last_valley_index = 0
        zz_last_valley_price = 0

        line_list = [0 for _ in range((len(quotes.Close.values)))]
        zz_price = copy.deepcopy(quotes.Close.values)

        for i in range(1000, len(quotes.Close.values) - 1):
            if quotes.High.values[i] < low_bb[i]:
                zz_list = ind.zigzag.peak_valley_pivots(quotes.Close.values[i-1000: i], zz_percent, -zz_percent)
                zz_list[0], zz_list[-1] = 0, 0
                zz_extremums = pd.Series(zz_list, index=quotes.index.values[i-1000: i])
                zz_extremums = zz_extremums[zz_extremums != 0]
                if len(list(zz_extremums)) > 1:
                    if zz_extremums[-1] == 1:
                        zz_last_valley_index = 1000 - 1
                        while zz_list[zz_last_valley_index] != -1:
                            zz_last_valley_index -= 1
                        zz_last_valley_price = quotes.Low.loc[zz_extremums.index[-2]]
                        up_bb_breakout_price = up_bb[i]
                        k = (up_bb_breakout_price - zz_last_valley_price) / (i - zz_last_valley_index)

                        sell_list.append(i + sell_n)
                        sell_flag = True

                if i <= len(quotes.Close.values) - 1000:
                    for j in range(1000):
                        if zz_list[j] == 0:
                            zz_price[i + j] = 0.0

            elif sell_flag:
                sell_count += 1
                line_value = k * (i - zz_last_valley_index) + zz_last_valley_price
                line_list[i] = line_value
                if (sell_count <= i - zz_last_valley_index and up_bb[i] < line_value) or sell_count > i - zz_last_valley_index:
                    buy_list.append(i + buy_n)
                    sell_flag = False
                    sell_count = 0

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        # self._charts = [
        #     {'Data': zz_price, 'Type': 'Point_chart', 'Color': 'Orange'},
        #     {'Data': line_list, 'Type': 'Point_chart', 'Color': 'Red'},
        #     {'Data': up_bb, 'Color': 'Black'},
        #     {'Data': low_bb, 'Color': 'Black'},
        # ]

        return signals


class BB_select_period(SignalGenerator):  # TODO: terrible algorithm

    def generate_signals(self, quotes):
        # for i in range(1000, len(quotes.Close) - 1, 1000):
        #     period = 20
        #     out_candles_count = 0
        #     out_candles_ratio = 0
        #     too_much = False
        #     too_little = False
        #     flag = False
        #     while not flag:
        #         up_bb = ind.up_bb(quotes.Close.values, period)
        #         low_bb = ind.low_bb(quotes.Close.values, period)
        #         for j in range(i - 1000, i):
        #             if quotes.Low[j] > up_bb[j] or quotes.High[j] < low_bb[j]:
        #                 out_candles_count += 1
        #         if out_candles_ratio < out_candles_count / 1000:
        #             if too_much:
        #                 period -= 1
        #                 flag = True
        #         else:
        #             if too_little:
        #                 period += 1
        #                 flag = True
        #         out_candles_ratio = out_candles_count / 1000
        #         too_much = False
        #         too_little = False
        #         if out_candles_ratio > 0.055:  # too much
        #             too_much = True
        #             period += 1
        #         elif out_candles_ratio < 0.045:  # too little
        #             too_little = True
        #             period -= 1
        #         else:
        #             flag = True
        #         print('too_much', too_much, ',', 'too_little', too_little)
        #         print('out_candles_count', out_candles_count)
        #     print('period', period)
        # exit()

        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 20))
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 20))

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        BB_select_period = 1000
        # BB_start_period = 1
        # BB_end_period = 100
        # BB_period = BB_start_period

        for i in range(len(quotes) - 1):
            if i % BB_select_period == 0:
                period = 20
                out_candles_count = 0
                out_candles_ratio = 0
                too_much = False
                too_little = False
                flag = False
                while not flag:
                    up_bb = ind.up_bb(quotes.Close.values, period)
                    low_bb = ind.low_bb(quotes.Close.values, period)
                    for j in range(i - BB_select_period, i):
                        if quotes.Low[j] > up_bb[j] or quotes.High[j] < low_bb[j]:
                            out_candles_count += 1
                    if out_candles_ratio < out_candles_count / BB_select_period:
                        if too_much:
                            period -= 1
                            flag = True
                    else:
                        if too_little:
                            period += 1
                            flag = True
                    out_candles_ratio = out_candles_count / BB_select_period
                    too_much = False
                    too_little = False
                    if out_candles_ratio > 0.045:  # too much
                        too_much = True
                        period += 1
                    elif out_candles_ratio < 0.055:  # too little
                        too_little = True
                        period -= 1
                    else:
                        flag = True
                    print('too_much', too_much, ',', 'too_little', too_little)
                    print('out_candles_count', out_candles_count)
                print('period', period)
            # up_bb = ind.up_bb(quotes.Close.values, period + 5)
            # low_bb = ind.low_bb(quotes.Close.values, period + 5)
            if quotes.Low.values[i] > up_bb[i]:
                buy_list.append(i + buy_n)
            if quotes.High.values[i] < low_bb[i]:
                sell_list.append(i + sell_n)

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class BBChannelSignalGenerator(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 500))  # 20
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 500))  # 20

        signals = pd.Series(0, index=quotes.index.values)
        bb_diff = up_bb - low_bb

        order_length = self.params.get('order_length', 150)
        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        buy_list = []
        sell_list = []

        for i in range(order_length, len(quotes.Close.values) - 1):
            if i % order_length == 0:
                temp_BB_diff = bb_diff[i - 2 * order_length:i]
                temp_close_list = quotes.Close.values[i - 2 * order_length:i]
                max_idx = list(argrelextrema(temp_BB_diff, np.greater, order=order_length)[0])
                min_idx = list(argrelextrema(temp_BB_diff, np.less, order=order_length)[0])
                if len(max_idx) != 0 or len(min_idx) != 0:
                    if temp_close_list[-1] > temp_close_list[0]:
                        if len(buy_list) == 0 or buy_list[-1] != i:
                            buy_list.append(i)
                    else:
                        if len(sell_list) == 0 or sell_list[-1] != i:
                            sell_list.append(i)
            if quotes.Low.values[i] > up_bb[i]:
                buy_list.append(i + buy_n)
            if quotes.High.values[i] < low_bb[i]:
                sell_list.append(i + sell_n)
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))
        return signals


class BB_NarrowChannel_n_DecisiveBreak_SignalGenerator(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 25))
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 25))

        signals = pd.Series(0, index=quotes.index.values)
        # bb_diff = up_bb - low_bb
        bb_diff = ind.bbw(quotes.Close.values, self.params.get('up_period', 25))

        order_length = self.params.get('order_length', 5)
        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        buy_list = []
        sell_list = []

        for i in range(20 * order_length, len(quotes.Close.values) - 1):
            probable_min = bb_diff[i - order_length]
            min_flag = True
            for j in range(i - 2 * order_length, i):
                if probable_min > bb_diff[j]:
                    min_flag = False
                    break
            if min_flag:
                # visualize bbw mins
                # temp_BB_diff = bb_diff[i - 2 * order_length:i]
                # plt.plot(temp_BB_diff)
                # plt.scatter(len(temp_BB_diff) - order_length, temp_BB_diff[len(temp_BB_diff) - order_length], c='r')
                # plt.show()
                if quotes.Close.values[i] > up_bb[i]:
                    if len(buy_list) == 0 or buy_list[-1] != i:
                        buy_list.append(i + buy_n)
                elif quotes.Close.values[i] < low_bb[i]:
                    if len(sell_list) == 0 or sell_list[-1] != i:
                        sell_list.append(i + sell_n)
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))
        return signals


class BB_Bounce(SignalGenerator):

    def generate_signals(self, quotes):
        up_bb = ind.up_bb(quotes.Close.values, self.params.get('up_period', 25))
        low_bb = ind.low_bb(quotes.Close.values, self.params.get('low_period', 25))
        mid_bb = ind.mid_bb(quotes.Close.values, self.params.get('mid_period', 25))

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        BB_flag = False
        wait_to_return_to_sma = False
        temp_list = pd.Series(data=np.zeros(len(quotes.Close.values)), index=quotes.Close.index)

        for i in range(1, len(quotes.Close.values) - 1):
            if wait_to_return_to_sma:
                if quotes.Close.values[i] < 1.001 * mid_bb[i] and quotes.Close.values[i] > 0.999 * mid_bb[i]:
                    wait_to_return_to_sma = False
            if quotes.Low.values[i] > up_bb[i]:
                buy_list.append(i + buy_n)
                BB_flag = True
            elif quotes.High.values[i] < low_bb[i]:
                sell_list.append(i + sell_n)
                BB_flag = False
                wait_to_return_to_sma = True
            elif not BB_flag and not wait_to_return_to_sma:
                if quotes.Low.values[i] < low_bb[i]:
                    temp_list[i + 1] = quotes.Close[i + 1]
                    buy_list.append(i + buy_n)
                elif quotes.High.values[i] > up_bb[i]:
                    temp_list[i + 1] = quotes.Close[i + 1]
                    sell_list.append(i + sell_n)

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        # self._charts = [
        #     {'Data': up_bb, 'Color': 'red'},
        #     {'Data': low_bb, 'Color': 'green'},
        #     {'Data': mid_bb, 'Color': 'brown'},
        #     {'Data': temp_list, 'Type': 'Point_chart', 'Color': 'Orange'}
        # ]

        return signals


class SMA_diff(SignalGenerator):

    def generate_signals(self, quotes):
        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)
        buy_list = []
        sell_list = []

        sma_list = ind.SMA(quotes.Close.values, self.params.get('sma_period', 200))
        sma_diff = np.diff(sma_list, n=1)
        sma_diff = np.insert(sma_diff, 0, float('nan'))
        # sma_diff = np.insert(sma_diff, 0, float('nan'))
        # sma_diff = np.insert(sma_diff, 0, float('nan'))

        atr_list = ind.get_atr(quotes.High.values, quotes.Low.values, quotes.Close.values, 14)

        for i in range(len(quotes.Close.values) - 1):
            if sma_diff[i] > 0.1 * atr_list[i]:
                buy_list.append(i + buy_n)
            if sma_diff[i] < -0.1 * atr_list[i]:
            # else:
                sell_list.append(i + sell_n)

        signals = pd.Series(0, index=quotes.index.values)

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        self._charts = [
            {'Data': sma_list, 'Color': 'black'},
        ]

        return signals


class SymmetricalTrianglesWithLevelsSignalGenerator(SignalGenerator):

    def generate_signals(self, quotes):
        if len(self.params) == 0:
            ema_period, stock_min_order, stock_max_order = 10, 15, 15
        else:
            ema_period, stock_min_order, stock_max_order = self.params[0], int(self.params[1]), self.params[2]

        signals = pd.Series(0, index=quotes.index.values)

        ema_list = np.array(ind.ema(list(quotes.Close.values), ema_period))

        i = 0
        sell_list = []
        buy_list = []
        max_list = []
        min_list = []
        while i < len(ema_list)-1:
            if i > stock_max_order * 2 + 1:
                if max(ema_list[(i - stock_max_order * 2 - 1):i]) == ema_list[i - stock_max_order - 1]:
                    max_list.append(i - stock_max_order - 1)
            if i > stock_min_order * 2 + 1:
                if min(ema_list[(i - stock_min_order * 2 - 1):i]) == ema_list[i - stock_min_order - 1]:
                    min_list.append(i - stock_min_order - 1)
            id_list = max_list + min_list
            id_list.sort()
            check = False
            if len(max_list) > 2 and len(min_list) > 2:
                if min_list[-1] == id_list[-2] and max_list[-2] == id_list[-3] and min_list[-2] == id_list[-4]:
                    check = True
                if max_list[-1] == id_list[-2] and min_list[-2] == id_list[-3] and max_list[-2] == id_list[-4]:
                    check = True

                if check and ema_list[max_list[-2]] > ema_list[max_list[-1]] and ema_list[min_list[-1]] > ema_list[min_list[-2]]:
                    if ema_list[i] > (ema_list[max_list[-1]]):
                        buy_list.append(i + 1)
                    elif ema_list[i] < (ema_list[min_list[-1]]):
                        sell_list.append(i + 1)

            i += 1

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class TrianglesWithLevelsAndHeight(SignalGenerator):

    def generate_signals(self, quotes):
        if len(self.params) == 0:
            ema_period, stock_min_order, stock_max_order = 10, 15, 15
        else:
            ema_period, stock_min_order, stock_max_order = self.params[0], int(self.params[1]), self.params[2]

        signals = pd.Series(0, index=quotes.index.values)

        ema_list = np.array(ind.ema(list(quotes.Close.values), ema_period))

        i = 0
        sell_list = []
        buy_list = []
        max_list = []
        min_list = []
        selling_price = 0
        buying_price = 0
        while i < len(ema_list) - 1:
            if i > stock_max_order * 2 + 1:
                if max(ema_list[(i - stock_max_order * 2 - 1):i]) == ema_list[i - stock_max_order - 1]:
                    max_list.append(i - stock_max_order - 1)
            if i > stock_min_order * 2 + 1:
                if min(ema_list[(i - stock_min_order * 2 - 1):i]) == ema_list[i - stock_min_order - 1]:
                    min_list.append(i - stock_min_order - 1)
            id_list = max_list + min_list
            id_list.sort()
            check = False
            if len(max_list) > 2 and len(min_list) > 2:
                if max_list[-1] == id_list[-1] and min_list[-1] == id_list[-2] and max_list[-2] == id_list[-3] and \
                        min_list[-2] == id_list[-4]:
                    check = True
                if min_list[-1] == id_list[-1] and max_list[-1] == id_list[-2] and min_list[-2] == id_list[-3] and \
                        max_list[-2] == id_list[-4]:
                    check = True

                if check and ema_list[max_list[-2]] > ema_list[max_list[-1]] and ema_list[min_list[-1]] > ema_list[min_list[-2]]:
                    if ema_list[i] > (ema_list[max_list[-1]]):
                        buy_list.append(i + 1)
                        selling_price = ema_list[i] + ema_list[max_list[-2]] - ema_list[min_list[-2]]
                        buying_price = 0
                    elif ema_list[i] < (ema_list[min_list[-1]]):
                        sell_list.append(i + 1)
                        buying_price = ema_list[i] - ema_list[max_list[-2]] + ema_list[min_list[-2]]
                        selling_price = 0
            if selling_price != 0 and ema_list[i] < selling_price:
                sell_list.append(i + 1)
                selling_price = 0
            if buying_price != 0 and ema_list[i] < buying_price:
                buy_list.append(i + 1)
                buying_price = 0

            i += 1

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class TrianglesKeepingDirection(SignalGenerator):

    def generate_signals(self, quotes):
        if len(self.params) == 0:
            ema_period, stock_min_order, stock_max_order = 10, 15, 15
        else:
            ema_period, stock_min_order, stock_max_order = self.params[0], int(self.params[1]), self.params[2]

        signals = pd.Series(0, index=quotes.index.values)

        ema_list = np.array(ind.ema(list(quotes.Close.values), ema_period))

        i = 0
        sell_list = []
        buy_list = []
        max_list = []
        min_list = []
        while i < len(ema_list) - 1:
            if i > stock_max_order * 2 + 1:
                if max(ema_list[(i - stock_max_order * 2 - 1):i]) == ema_list[i - stock_max_order - 1]:
                    max_list.append(i - stock_max_order - 1)
            if i > stock_min_order * 2 + 1:
                if min(ema_list[(i - stock_min_order * 2 - 1):i]) == ema_list[i - stock_min_order - 1]:
                    min_list.append(i - stock_min_order - 1)
            id_list = max_list + min_list
            id_list.sort()
            check = False
            if len(max_list) > 2 and len(min_list) > 2:
                if min_list[-1] == id_list[-2] and max_list[-2] == id_list[-3] and min_list[-2] == id_list[-4]:
                    check = True
                if max_list[-1] == id_list[-2] and min_list[-2] == id_list[-3] and max_list[-2] == id_list[-4]:
                    check = True

                if check and ema_list[max_list[-2]] > ema_list[max_list[-1]] and ema_list[min_list[-1]] > ema_list[min_list[-2]]:
                    if min_list[-1] == id_list[-1]:
                        buy_list.append(i + 1)
                    elif max_list[-1] == id_list[-1]:
                        sell_list.append(i + 1)

            i += 1

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class TrianglesConsideringLength(SignalGenerator):

    def generate_signals(self, quotes):
        if len(self.params) == 0:
            ema_period, stock_min_order, stock_max_order, part_of = 10, 15, 15, 0.66
        else:
            ema_period, stock_min_order, stock_max_order, part_of = self.params[0], int(self.params[1]), self.params[2], self.params[3]

        signals = pd.Series(0, index=quotes.index.values)

        ema_list = np.array(ind.ema(list(quotes.Close.values), ema_period))

        i = 0
        sell_list = []
        buy_list = []
        max_list = []
        min_list = []
        while i < len(ema_list) - 1:
            if i > stock_max_order * 2 + 1:
                if max(ema_list[(i - stock_max_order * 2 - 1):i]) == ema_list[i - stock_max_order - 1]:
                    max_list.append(i - stock_max_order - 1)
            if i > stock_min_order * 2 + 1:
                if min(ema_list[(i - stock_min_order * 2 - 1):i]) == ema_list[i - stock_min_order - 1]:
                    min_list.append(i - stock_min_order - 1)
            id_list = max_list + min_list
            id_list.sort()
            check = False
            if len(max_list) > 2 and len(min_list) > 2:
                if min_list[-1] == id_list[-2] and max_list[-2] == id_list[-3] and min_list[-2] == id_list[-4]:
                    check = True
                if max_list[-1] == id_list[-2] and min_list[-2] == id_list[-3] and max_list[-2] == id_list[-4]:
                    check = True
                a1 = ema_list[max_list[-2]] - ema_list[max_list[-1]]
                a2 = ema_list[min_list[-2]] - ema_list[min_list[-1]]
                b1 = max_list[-1] - max_list[-2]
                b2 = min_list[-1] - min_list[-2]
                c1 = max_list[-1] * (ema_list[max_list[-2]] - ema_list[max_list[-1]]) + ema_list[max_list[-1]] * (
                            max_list[-1] - max_list[-2])
                c2 = min_list[-1] * (ema_list[min_list[-2]] - ema_list[min_list[-1]]) + ema_list[min_list[-1]] * (
                            min_list[-1] - min_list[-2])
                x = -1.0 * (c2 * b1 - c1 * b2) / (a1 * b2 - a2 * b1)
                if check and x > id_list[-1] and i - id_list[-4] < part_of * (x - id_list[-4]):
                    if ema_list[i] > (ema_list[max_list[-1]]):
                        buy_list.append(i + 1)
                    elif ema_list[i] < (ema_list[min_list[-1]]):
                        sell_list.append(i + 1)

            i += 1

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class TrianglesWithLevels(SignalGenerator):

    def generate_signals(self, quotes):
        if len(self.params) == 0:
            ema_period, stock_min_order, stock_max_order, another_order = 10, 15, 15, 1
        else:
            ema_period, stock_min_order, stock_max_order, another_order = self.params[0], int(self.params[1]), self.params[2], self.params[3]

        signals = pd.Series(0, index=quotes.index.values)

        ema_list = np.array(ind.ema(list(quotes.Close.values), ema_period))

        i = 0
        sell_list = []
        buy_list = []
        max_list = []
        min_list = []
        maximum_list = []
        minimum_list = []
        while i < len(ema_list) - 1:
            if i > stock_max_order * 2 + 1:
                if max(ema_list[(i - stock_max_order * 2 - 1):i]) == ema_list[i - stock_max_order - 1]:
                    max_list.append(i - stock_max_order - 1)
            if i > stock_min_order * 2 + 1:
                if min(ema_list[(i - stock_min_order * 2 - 1):i]) == ema_list[i - stock_min_order - 1]:
                    min_list.append(i - stock_min_order - 1)
            if i > another_order * 2 + 1:
                if max(ema_list[(i - another_order * 2 - 1):i]) == ema_list[i - another_order - 1]:
                    maximum_list.append(i - another_order - 1)
            if i > another_order * 2 + 1:
                if min(ema_list[(i - another_order * 2 - 1):i]) == ema_list[i - another_order - 1]:
                    minimum_list.append(i - another_order - 1)
            id_list = max_list + min_list
            id_list.sort()
            check = False
            if len(max_list) > 2 and len(min_list) > 2:
                if min_list[-1] == id_list[-2] and max_list[-2] == id_list[-3] and min_list[-2] == id_list[-4]:
                    check = True
                if max_list[-1] == id_list[-2] and min_list[-2] == id_list[-3] and max_list[-2] == id_list[-4]:
                    check = True

                if check and ema_list[max_list[-2]] - ema_list[min_list[-2]] > ema_list[max_list[-1]] - ema_list[min_list[-1]]:
                    if ema_list[i] < ema_list[min_list[-2]] and maximum_list[-1] > minimum_list[-1]:
                        buy_list.append(i + 1)
                    elif ema_list[i] > ema_list[max_list[-2]] and maximum_list[-1] < minimum_list[-1]:
                        sell_list.append(i + 1)

            i += 1

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class True_Divergence(SignalGenerator):

    def generate_signals(self, quotes):
        order_length = 20
        rsi_list = ind.rsi(quotes.Close.values, 14)
        # rsi_list = ind.fi(quotes.Close, quotes.Volume)
        #rsi_list = ind.macd(close_list)
        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []

        max_prices = []
        min_prices = []
        max_prices_copy = [None]*len(quotes.Open.values)
        min_prices_copy = [None]*len(quotes.Open.values)

        min_trend_line = [None]*len(quotes.Open.values)
        max_trend_line = [None]*len(quotes.Open.values)
        border_list = [None]*len(quotes.Open.values)

        max_rsi = []
        min_rsi = []
        max_rsi_copy = [None]*len(quotes.Open.values)
        min_rsi_copy = [None]*len(quotes.Open.values)
        max_rsi_trend_line = [None]*len(quotes.Open.values)
        min_rsi_trend_line = [None]*len(quotes.Open.values)
        for i in range(2*order_length, len(quotes.Open.values), 2*order_length):
            border_list[i] = quotes.Open.values[i]
            temp_open_list = quotes.Open.values[i-2*order_length:i]
            temp_rsi_list = rsi_list[i-2*order_length:i]
            max_prices_idx = list(argrelextrema(temp_open_list, np.greater, order=order_length)[0])
            min_prices_idx = list(argrelextrema(temp_open_list, np.less, order=order_length)[0])

            max_rsi_idx = list(argrelextrema(temp_open_list, np.greater, order=order_length)[0])
            min_rsi_idx = list(argrelextrema(temp_open_list, np.less, order=order_length)[0])
            for j in max_rsi_idx:
                max_rsi.append([i-2*order_length+j, temp_rsi_list[j]])
                max_rsi_copy[i-2*order_length+j] = temp_rsi_list[j]
            for j in min_rsi_idx:
                min_rsi.append([i-2*order_length+j, temp_rsi_list[j]])
                min_rsi_copy[i-2*order_length+j] = temp_rsi_list[j]

            for j in max_prices_idx:
                max_prices.append([i-2*order_length+j, temp_open_list[j]])
                max_prices_copy[i-2*order_length+j] = temp_open_list[j]
            for j in min_prices_idx:
                min_prices.append([i-2*order_length+j, temp_open_list[j]])
                min_prices_copy[i-2*order_length+j] = temp_open_list[j]

            if len(max_prices) >= 2 and len(min_prices) >= 2 and len(max_rsi) >= 2 and len(min_rsi) >= 2:
                if len(max_prices_idx) != 0:
                    for j in range(max_prices[-2][0], i+1):
                        max_trend_line[j] = (j-max_prices[-2][0])*(max_prices[-1][1]-max_prices[-2][1])/(max_prices[-1][0]-max_prices[-2][0])+max_prices[-2][1] #line equation
                if len(min_prices_idx) != 0:
                    for j in range(min_prices[-2][0], i+1):
                        min_trend_line[j] = (j-min_prices[-2][0])*(min_prices[-1][1]-min_prices[-2][1])/(min_prices[-1][0]-min_prices[-2][0])+min_prices[-2][1] #line equation

                if len(max_rsi_idx) != 0:
                    for j in range(max_rsi[-2][0], i+1):
                        max_rsi_trend_line[j] = (j-max_rsi[-2][0])*(max_rsi[-1][1]-max_rsi[-2][1])/(max_rsi[-1][0]-max_rsi[-2][0])+max_rsi[-2][1] #line equation

                    max_rsi_dif = max_rsi_trend_line[max_rsi[-1][0]]-max_rsi_trend_line[max_rsi[-2][0]]
                    max_prices_dif = max_trend_line[max_prices[-1][0]]-max_trend_line[max_prices[-2][0]]
                    if (max_rsi_dif > 0 and max_prices_dif < 0) or (max_rsi_dif < 0 and max_prices_dif > 0):
                        sell_list.append(i + 1)

                if len(min_rsi_idx) != 0:
                    for j in range(min_rsi[-2][0], i+1):
                        min_rsi_trend_line[j] = (j-min_rsi[-2][0])*(min_rsi[-1][1]-min_rsi[-2][1])/(min_rsi[-1][0]-min_rsi[-2][0])+min_rsi[-2][1] #line equation

                    min_rsi_dif = min_rsi_trend_line[min_rsi[-1][0]]-min_rsi_trend_line[min_rsi[-2][0]]
                    min_prices_dif = min_trend_line[min_prices[-1][0]]-min_trend_line[min_prices[-2][0]]
                    if (min_rsi_dif > 0 and min_prices_dif < 0) or (min_rsi_dif < 0 and min_prices_dif > 0):
                        buy_list.append(i + 1)
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class RSI_levels(SignalGenerator):

    def generate_signals(self, quotes):
        rsi_list = ind.rsi(quotes.Close.values, 14)
        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        for i in range(len(quotes.Close.values) - 1):
            if rsi_list[i] < 30:
                buy_list.append(i + 1)
            if rsi_list[i] > 70:
                sell_list.append(i + 1)
            # if rsi_list[i] < 40:
            #     buy_list.append(i + 1)
            # if rsi_list[i] > 60:
            #     sell_list.append(i + 1)
        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class Force_Index(SignalGenerator):

    def generate_signals(self, quotes):
        fi_list = ind.fi(quotes.Close, quotes.Volume, n=13)
        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        for i in range(len(quotes.Close.values) - 1):
            if fi_list[i] < 0:
                buy_list.append(i + 1)
            if fi_list[i] > 0:
                sell_list.append(i + 1)
        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        self._charts = [
            {'Data': fi_list, 'Priority': 'Secondary', 'Color': 'Orange'},
        ]

        return signals


class SMA_and_Price_cross(SignalGenerator):

    def generate_signals(self, quotes):
        sma_list = ind.SMA(quotes.Close.values, self.params.get('sma_period', 1000))  # 500
        green_zone = 1 + self.params.get('green_zone', 5) / 100
        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        for i in range(len(signals) - 1):
            if quotes.Close.values[i] > sma_list[i] * green_zone:
                buy_list.append(i + 1)
            if quotes.Close.values[i] * green_zone < sma_list[i]:
                sell_list.append(i + 1)
        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))
        return signals


class EMA_and_Price_cross(SignalGenerator):

    def generate_signals(self, quotes):
        ema_type = self.params.get('ema_type', 'ema')
        ema_functions = {'ema': ind.EMA, 'dema': ind.DEMA, 'tema': ind.TEMA}

        ema_list = ema_functions[ema_type](quotes.Close.values, self.params.get('ema_period', 500))  # 500
        green_zone = 1 + self.params.get('green_zone', 2) / 100  # 2
        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)
        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        for i in range(len(signals) - 1):
            if quotes.Close.values[i] > ema_list[i] * green_zone:
                buy_list.append(i + buy_n)
            if quotes.Close.values[i] * green_zone < ema_list[i]:
                sell_list.append(i + sell_n)
        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))
        return signals


class ADX_Trend_detection(SignalGenerator):

    def generate_signals(self, quotes):
        period = self.params.get('period', 14)
        trend_line = self.params.get('trend_line', 20)

        inputs = {
            'open': quotes.Open,
            'high': quotes.High,
            'low': quotes.Low,
            'close': quotes.Close
        }
        adx = ind.ADX(inputs, timeperiod=period)
        signals = pd.Series(0, index=quotes.index.values)

        trend_list = np.where(adx > trend_line)[0] + 1
        trend_list = trend_list[trend_list < len(quotes)]
        signals.update(pd.Series(1, index=quotes.iloc[trend_list].index.values))

        return signals


class ATR_Trend_detection(SignalGenerator):

    def generate_signals(self, quotes):
        period = self.params.get('period', 14)
        trend_line = self.params.get('trend_line', 100)
        atr = ind.get_atr(quotes.High.values, quotes.Low.values, quotes.Close.values, period)

        signals = pd.Series(0, index=quotes.index.values)
        trend_list = np.where(atr > trend_line)[0] + 1
        trend_list = trend_list[trend_list < len(quotes)]
        signals.update(pd.Series(1, index=quotes.iloc[trend_list].index.values))

        return signals


class ATR_Trend_detection_dynamic(SignalGenerator):

    def generate_signals(self, quotes):
        atr_period = self.params.get('atr_period', 14)
        sma_period = self.params.get('sma_period', 200)
        offset_n = self.params.get('offset_n', 1)
        atr = ind.get_atr(quotes.High.values, quotes.Low.values, quotes.Close.values, atr_period)
        sma_atr = ind.SMA(atr, sma_period)
        signals = pd.Series(0, index=quotes.index.values)
        trend_list = []

        for i in range(len(signals) - 1):
            if atr[i] > sma_atr[i]:
                trend_list.append(i + offset_n)

        signals.update(pd.Series(1, index=quotes.iloc[trend_list].index.values))

        return signals


class ATR_Trend_detection_mean(SignalGenerator):

    def generate_signals(self, quotes):
        period = self.params.get('period', 14)
        offset_n = self.params.get('offset_n', 1)
        atr = ind.get_atr(quotes.High.values, quotes.Low.values, quotes.Close.values, period)
        signals = pd.Series(0, index=quotes.index.values)
        trend_list = []

        for i in range(1000 + period, len(atr) - 1):
            mean_atr = np.mean(atr[i - 1000: i])
            if atr[i] > mean_atr:
                trend_list.append(i + offset_n)

        signals.update(pd.Series(1, index=quotes.iloc[trend_list].index.values))

        return signals


class NATR_Trend_detection(SignalGenerator):

    def generate_signals(self, quotes):
        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)
        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []

        period = self.params.get('period', 14)
        trend_line = self.params.get('trend_line', 2)
        green_zone = self.params.get('green_zone', 1) / 100 + 1
        natr = ind.get_natr(quotes.High.values, quotes.Low.values, quotes.Close.values, period)
        natr_huge = False

        for i in range(len(quotes.Close.values)):
            if natr[i] > trend_line:
                if not natr_huge and quotes.Close.values[i] > quotes.Open.values[i]:
                    buy_list.append(i + buy_n)
                if not natr_huge and quotes.Close.values[i] < quotes.Open.values[i]:
                    sell_list.append(i + sell_n)
                natr_huge = True
            elif natr[i] < 1:
                natr_huge = False

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class NATR_Trend_detection_channel(SignalGenerator):

    def generate_signals(self, quotes):
        atr_period = self.params.get('atr_period', 14)
        bb_period = self.params.get('bb_period', 20)  # 20
        offset_n = self.params.get('offset_n', 1)
        # atr = ind.get_atr(quotes.High.values, quotes.Low.values, quotes.Close.values, atr_period)
        atr = ind.get_natr(quotes.High.values, quotes.Low.values, quotes.Close.values, atr_period)
        up_bb, mid_bb, low_bb = ind.get_bbands(atr, period=bb_period, nbdevup=1, nbdevdn=1)
        signals = pd.Series(0, index=quotes.index.values)
        trend_list = []

        for i in range(len(signals) - 1):
            if atr[i] > mid_bb[i]:
                trend_list.append(i + offset_n)

        signals.update(pd.Series(1, index=quotes.iloc[trend_list].index.values))

        return signals


class SAR_and_Price(SignalGenerator):

    def generate_signals(self, quotes):
        sar_list = ind.SAR(quotes['High'], quotes['Low'], acceleration=self.params.get('acceleration', 0.005),
                           maximum=self.params.get('maximum', 0.05))
        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        for i in range(len(signals) - 1):
            if quotes.Close.values[i] > sar_list.values[i]:
                buy_list.append(i + 1)
            if quotes.Close.values[i] < sar_list.values[i]:
                sell_list.append(i + 1)
        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        # self._charts = [
        #     {'Data': sar_list, 'Color': 'black', 'Type': 'Point_chart'},
        # ]

        return signals


class GAP(SignalGenerator):  # sees the future because candle size uses open + close while buy_list appends open

    def generate_signals(self, quotes):
        period = self.params.get('period', 14)
        atr = ind.get_atr(quotes.High.values, quotes.Low.values, quotes.Close.values, period)
        buy_gap_list = pd.Series(0, index=quotes.index.values)
        sell_gap_list = pd.Series(0, index=quotes.index.values)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        buy_price = 0
        sell_price = np.float('+inf')

        for i in range(len(quotes.Close.values) - 1):
            candle_size = quotes.Close.values[i] - quotes.Open.values[i]

            if quotes.Close.values[i] <= buy_price:
                buy_list.append(i + 1)
            if quotes.Close.values[i] >= sell_price:
                sell_list.append(i + 1)

            if candle_size > 0 and candle_size > 2 * atr[i]:
                buy_price = quotes.Open.values[i]
                buy_gap_list[i] = buy_price
            if candle_size < 0 and -candle_size > 2 * atr[i]:
                sell_price = quotes.Open.values[i]
                sell_gap_list[i] = sell_price

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(-1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(1, index=quotes.iloc[sell_list].index.values))

        self._charts = [
            {'Data': buy_gap_list, 'Color': 'black', 'Type': 'Point_chart'},
            {'Data': sell_gap_list, 'Color': 'purple', 'Type': 'Point_chart'},
            # {'Data': quotes.Close, 'Color': 'blue'},
            {'Data': 2 * atr, 'Color': 'orange', 'Priority': 'Secondary'},
        ]

        return signals


class Market_profile(SignalGenerator):

    def generate_signals(self, quotes):
        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)
        window_interval = self.params.get('window_interval', 1000)  # 1000
        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        atr = ind.get_atr(quotes.High.values, quotes.Low.values, quotes.Close.values, 14)
        # buy_flag = False
        # sell_flag = False

        # mp = ind.MarketProfile(quotes, tick_size=self.params.get('bar_size', 100))

        for i in range(window_interval, len(quotes.Close.values) - 1, 10):
            atr_slice = atr[i - window_interval: i]
            mp = ind.MarketProfile(quotes, tick_size=self.params.get('bar_size', atr_slice[~np.isnan(atr_slice)].mean()))
            # mp_value_area = ind.get_mp(quotes[i - window_interval: i], bar_size=self.params.get('bar_size', 10))
            mp_slice = mp[quotes.index[i - window_interval]: quotes.index[i]]

            mp_data = mp_slice.profile

            # mp_data.plot(kind='bar')
            # fit = stats.norm.pdf(mp_data_sorted, np.mean(mp_data_sorted), np.std(mp_data_sorted))
            # plt.plot(mp_data_sorted, fit, '-o')
            # plt.hist(mp_data, normed=True)

            kurtosis = stats.kurtosis(mp_data)  # , fisher=False
            # plt.show()

            if kurtosis > 1:
                buy_list.append(i + buy_n)
            if kurtosis < -1:
                sell_list.append(i + sell_n)

            # mp_value_area = mp_slice.value_area
            # if quotes.Close.values[i] < mp_value_area[0]:
            #     buy_list.append(i + buy_n)
            # if quotes.Close.values[i] > mp_value_area[1]:
            #     sell_list.append(i + sell_n)
            # mp_poc_price = ind.get_mp(quotes[i - window_interval: i], bar_size=self.params.get('bar_size', 10))
            # if quotes.Close.values[i] < mp_poc_price:
            #     buy_list.append(i + buy_n)
            # if quotes.Close.values[i] > mp_poc_price:
            #     sell_list.append(i + sell_n)

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(-1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(1, index=quotes.iloc[sell_list].index.values))

        # self._charts = [
        #     {'Data': up_bb, 'Color': 'black'},
        # ]

        return signals


class SMA_death_cross(SignalGenerator):

    def generate_signals(self, quotes):
        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        sma50 = ind.SMA(quotes.Close.values, 50)
        sma200 = ind.SMA(quotes.Close.values, 200)

        for i in range(len(quotes.Close.values) - 1):
            if 1.05 * sma50[i] < sma200[i]:
                buy_list.append(i + buy_n)
            if 0.95 * sma50[i] > sma200[i]:
                sell_list.append(i + sell_n)

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(-1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(1, index=quotes.iloc[sell_list].index.values))

        return signals


class Fisher_trend_following(SignalGenerator):

    def generate_signals(self, quotes):
        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []
        fisher = ind.get_fish_rsi(quotes)

        for i in range(len(quotes.Close.values) - 1):
            if fisher[i] < -0.99:
                buy_list.append(i + buy_n)
            if fisher[i] > 0.99:
                sell_list.append(i + sell_n)

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(-1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(1, index=quotes.iloc[sell_list].index.values))

        return signals


class ZigZag_trend_detection(SignalGenerator):

    def generate_signals(self, quotes):
        zz_percent = self.params.get('zz_percent', 10) / 100  # 20

        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)
        buy_list = []
        sell_list = []

        for i in range(500, len(quotes.Close.values) - 1):
            zz_list = ind.zigzag.peak_valley_pivots(quotes.Close.values[i - 500: i], zz_percent, -zz_percent)
            zz_list[0], zz_list[-1] = 0, 0
            not_zero_index_list = [q for q, e in enumerate(zz_list) if e != 0]
            if len(not_zero_index_list) != 0:
                if zz_list[not_zero_index_list[-1]] == 1:
                    if quotes.Close.values[i - 500 + not_zero_index_list[-1]] < quotes.Close.values[i]:
                        buy_list.append(i + buy_n)
                if zz_list[not_zero_index_list[-1]] == -1:
                    if quotes.Close.values[i - 500 + not_zero_index_list[-1]] > quotes.Close.values[i]:
                        sell_list.append(i + sell_n)

        buy_list = list(set(buy_list))
        sell_list = list(set(sell_list))
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        # self._charts = [
        #     {'Data': zz_price, 'Type': 'Point_chart', 'Color': 'Orange'},
        #     {'Data': line_list, 'Type': 'Point_chart', 'Color': 'Red'},
        #     {'Data': up_bb, 'Color': 'Black'},
        #     {'Data': low_bb, 'Color': 'Black'},
        # ]

        return signals


class Test_strategy(SignalGenerator):

    def generate_signals(self, quotes):
        buy_n = self.params.get('buy_n', 1)
        sell_n = self.params.get('sell_n', 1)

        signals = pd.Series(0, index=quotes.index.values)

        buy_list = np.where(quotes.Close.values > quotes.Open.values)[0] + buy_n
        buy_list = buy_list[buy_list < len(quotes)]
        signals.update(pd.Series(1, index=quotes.iloc[buy_list].index.values))

        sell_list = np.where(quotes.Close.values < quotes.Open.values)[0] + sell_n
        sell_list = sell_list[sell_list < len(quotes)]
        signals.update(pd.Series(-1, index=quotes.iloc[sell_list].index.values))

        return signals


class CombineAlgo(SignalGenerator):
    def __init__(self, generator_list, params=None, trade_types=None):
        super().__init__(params, trade_types)
        self._generator_list = generator_list

    def generate_signals(self, quotes):
        signals_list = []
        for generator in self._generator_list:
            signals_list.append(generator.generate_signals(quotes))
        signals = self._combine_signals(signals_list)
        if isinstance(signals, dict):
            signals = pd.Series(signals)
        return signals

    @abstractmethod
    def _combine_signals(self, signals_list):
        pass


class CombineFuncOR(CombineAlgo):  # the first - the main

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _combine_signals(self, signals_list):
        signals_1 = signals_list[0]
        signals_2 = signals_list[1]
        signals_2.update(signals_1)
        return signals_2


class CombineFuncJOIN(CombineAlgo):  # the first - the main (it is immutable)
    # Strict join: signal lists dont intersect at all
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _combine_signals(self, signals_list):
        signals_1 = signals_list[0]
        signals_2 = signals_list[1]
        if len(signals_1) != len(signals_2):
            return

        # match type for series
        signals_1_new = signals_1.loc[signals_1 != 0]
        signals_1 = pd.Series([0 for _ in range(len(signals_1))], index=signals_1.index)
        signals_1.update(signals_1_new)
        signals_2_new = signals_2.loc[signals_2 != 0]
        signals_2 = pd.Series([0 for _ in range(len(signals_2))], index=signals_2.index)
        signals_2.update(signals_2_new)

        buy_signal = False
        for i in range(1, len(signals_1)):
            if signals_1[i - 1] == 1:
                buy_signal = True
            elif signals_1[i - 1] == -1:
                buy_signal = False
            else:
                if buy_signal:
                    signals_2[i] = 0

        signals_2.update(signals_1)
        return signals_2


class Trend_Filter_Func(CombineAlgo):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _combine_signals(self, signals_list):
        signals = signals_list[0]
        trend_signals = signals_list[1]
        signals = signals[signals != 0]
        trend_signals = pd.Series(SignalGenerator.match_type(self, trend_signals))

        for i in range(len(signals)):
            for j in range(len(trend_signals)):
                if trend_signals.index[j] > signals.index[i]:
                    if signals[i] != trend_signals[j - 1]:
                        signals[i] = 0
                    break
        return signals


class Flat_Filter_Func(CombineAlgo):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _combine_signals(self, signals_list):
        signals = signals_list[0]
        flat_signals = signals_list[1]

        # wait_list = np.where(flat_signals == 0)[0]  # works during flat
        wait_list = np.where(flat_signals == 1)[0]  # works during trend
        wait_list = wait_list[wait_list < len(flat_signals)]
        signals = pd.Series(signals.iloc[wait_list].values, index=flat_signals.iloc[wait_list].index.values)

        return signals


class Flat_Generator_Switch_Func(CombineAlgo):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _combine_signals(self, signals_list):
        trend_signals = signals_list[0]
        flat_signals = signals_list[1]
        trend_filter_signals = signals_list[2]
        signals = pd.Series(0, index=trend_signals.index.values)

        for i in range(len(trend_signals)):
            if trend_filter_signals[i] == 0:
                signals[i] = flat_signals[i]
            else:
                signals[i] = trend_signals[i]

        return signals


class Trend_Generator_Switch_Func(CombineAlgo):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _combine_signals(self, signals_list):
        uptrend_signals = signals_list[0]
        downtrend_signals = signals_list[1]
        trend_filter_signals = signals_list[2]
        signals = pd.Series(0, index=uptrend_signals.index.values)

        for i in range(len(uptrend_signals)):
            if trend_filter_signals[i] == 1:
                signals[i] = uptrend_signals[i]
            elif trend_filter_signals[i] == -1:
                signals[i] = downtrend_signals[i]

        return signals


class CombineByLastDates(CombineAlgo):

    def __init__(self, periods_dates, **kwargs):
        super().__init__(**kwargs)
        self._periods_dates = periods_dates

        start_date = periods_dates[0][0]
        end_date = periods_dates[-1][-1]

        for generator in self._generator_list:
            generator.start_date = start_date
            generator.end_date = end_date

    def __drop_signals(self, signals, next_signals, current_dates):

        if len(signals) == 0:
            return signals

        signals = signals.loc[(signals.index >= current_dates[0]) & (signals.index <= current_dates[1])]

        if len(signals) == 0:
            return signals

        if next_signals is None or len(next_signals) == 0:
            if signals.iloc[-1] == 1:
                signals[current_dates[1]] = -1
        else:
            cl_ix, cl_op = signals.index[-1], signals.iloc[-1]

            ns = next_signals.loc[next_signals.index <= current_dates[-1]]
            if len(ns) == 0:
                nl_op = 0
            else:
                nl_op = ns.iloc[-1]

            if cl_op == 1 and nl_op == -1:
                if cl_ix == current_dates[1]:
                    signals.pop(current_dates[1])
                else:
                    signals[current_dates[1]] = -1

        return signals

    def __adjust_signals(self, current_signals, previous_signals):

        if len(current_signals) > 0 and len(previous_signals):
            current_signals = current_signals.loc[current_signals.index > previous_signals.index[-1]]

        return current_signals

    def _combine_signals(self, signals_list):
        final_dict = {}

        signals_list = [pd.Series(v) for v in signals_list]

        for i in range(1, len(signals_list)):
            signals_list[i - 1] = self.__drop_signals(signals_list[i - 1], signals_list[i], self._periods_dates[i - 1])

        signals_list[-1] = self.__drop_signals(signals_list[-1], None, self._periods_dates[-1])

        for i in range(1, len(signals_list)):
            signals_list[i] = self.__adjust_signals(signals_list[i], signals_list[i - 1])

        for signals in signals_list:
            final_dict.update(signals.to_dict())

        new_final_dict = {}
        list_keys = list(final_dict.keys())
        list_keys.sort()
        for timestamp_key in list_keys:
            new_final_dict[timestamp_key] = final_dict.get(timestamp_key)

        return new_final_dict


class Assambley_SignalGenerator(CombineAlgo):

    def _combine_signals(self, signals_list):
        self.final = pd.Series()
        signals_list = [pd.Series(v) for v in signals_list]
        self.together = pd.concat(signals_list, axis=1).fillna(0)
        self.together.index = pd.to_datetime(self.together.index)
        last = np.zeros((len(signals_list)))
        for i in self.together.index:
            for b in range(len(signals_list)):
                if self.together.loc[i][b] != 0:
                    last[b] = self.together.loc[i][b]
            if all(x == last[0] for x in last):
                self.final[i] = last[0]
        return self.final


if __name__ == '__main__':

    print(__name__ + 'is imported')