file.html is the result file of a trading strategy backtest which contains statistics of a trading strategy.  

Index chart is a buy and hold strategy value chart of RDIMI fund's main trading pairs. Initial value is 100.  
Sold and bought points details can be shown by hovering over them (trade date, portfolio value, trade asset name).  
Strategy price chart is a trading strategy value chart.  

"Start" is a strategy's start date of working. The start date of a strategy after initialization is presented on a chart.  
"End" is a strategy's end date of working.  
"Index return" is a return of a buy and hold strategy.  
"Total return" is a strategy return.  
"Daily sharpe", "daily sortino", "CAGR", "max drawdown" are calculated based on the strategy value chart.  
"W/L trades" is a win trades number to loss trades number ratio.  
"Max drawdown" is a max drawdown calculated based on the index value chart.  


test.py is a configuration file where trading strategy can be built and backtested on datasets.  

The process of building and testing a trading strategy can be described as follows:
1) Basic strategy building (writing a signal generator in "signal_generators", in "strategy" folder)
2) Rebalance algorithms and other algos building (in "strategy_generators.py", in "strategy" folder)
3) Configuring test.py file and backtesting a strategy based on a hypothesis:  
    a) data timeframe  
    b) trade fee value/function  
    c) trading strategy and assets under its management:  
        c1) type of signal generator: "generator" means a simple signal generator to buy/sell/long/short an asset, "combine" means a generator which combines signals of other "generators"  
        c2) internal parameters of signal generators in "params" dictionary like "ema_type", "up_period" of a technical indicator value and others  
        c3) general parameters of signal generators in "params" dictionary like "take_profit" (and other take profit/stop loss types), "short" trade type and others  
    d) start and end date of a backtest  
    e) data obtaining parameters like trading pair, timeframe, exchange and others (in "test.py" and in "utils.py" in "data" folder)  
4) Internal and general parameters optimization of the generated strategy ("optimization" folder):  
    a) range of every variable parameter and parameter variation step  
    b) objective optimization function  
    c) optimization learn and test period  
    d) optimization method parameters  
5) Strategy signals execution ("trading" folder) applying an "iceberg" algorithm of a orders placement on crypto exchanges using ccxt ("limit.py") or in "interactive brokers" ("ib_limit.py")  
6) Notifications of results and portfolio status via telegram or mail ("messaging" folder)

The most used RDIMI fund's strategy and its parameters:  
    Exchange: Bitstamp.  
    Trading pairs: BTC, ETH, XRP, BCH, LTC to USD.  
    Rebalance algorithm: rebalance assets equally every new strategy signal and sell all assets if one is signaled to be sold.  
    Strategy:  
        ('BTC',):  
            [{'combine': signals.Trend_Generator_Switch_Func, 'params': {'take_profit': 10}},  
             {'generator': signals.BBSignalGenerator},  
             {'generator': signals.EMA_and_Price_cross, 'params': {'ema_type': 'tema'}},  
             {'generator': signals.EMA_and_Price_cross, 'params': {'ema_type': 'tema'}},  
             ],  
        ('ETH', 'XRP', 'BCH', 'LTC', ):  
            [{'combine': signals.Trend_Generator_Switch_Func, 'params': {'take_profit': 20}},  
             {'generator': signals.BBSignalGenerator},  
             {'generator': signals.EMA_and_Price_cross, 'params': {'ema_type': 'tema'}},  
             {'generator': signals.EMA_and_Price_cross, 'params': {'ema_type': 'tema'}},  
             ],  
    Signals execution: an "iceberg" algorithm of a orders placement.  
