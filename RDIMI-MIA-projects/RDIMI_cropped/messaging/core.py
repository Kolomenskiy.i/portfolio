# -*- encoding: utf-8 -*-

import pandas as pd

pd.core.common.is_list_like = pd.api.types.is_list_like

#from optimization.utils import load_generators
from abc import ABC, abstractmethod


class MessageClient(ABC):

    @abstractmethod
    def send_message(self, msg):
        pass


class DefaultMessageClient(MessageClient):

    def send_message(self, msg, chat_id=None):
        pass


