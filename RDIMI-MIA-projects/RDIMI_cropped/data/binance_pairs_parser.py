import bs4 as bs
import urllib.request
from urllib.parse import quote
import json


if __name__ == '__main__':
    # initial_search_string = 'USDT'

    url = 'https://www.binance.com/en/markets'

    source = urllib.request.urlopen(url).read()
    soup = bs.BeautifulSoup(source, 'lxml')

    all_pairs = list(
        json.loads(soup.find_all('script', id="__NEXT_DATA__")[0].string)['props']['initialState']['products'][
            'productMap'].keys())

    # get usd pairs
    usd_pairs = []
    for pair in all_pairs:
        if 'USDT' in pair:
            usd_pairs.append(pair)

    # delete up/down coins
    usd_pairs_cleaned = []
    for pair in usd_pairs:
        if 'DOWN' not in pair and 'UP' not in pair:
            usd_pairs_cleaned.append(pair)

    # get cryptos (not pairs)
    usd_cryptos = []
    for pair in usd_pairs_cleaned:
        if 'USD' not in pair[:5]:
            usd_cryptos.append(pair[:-4])

    # # get cryptos (not pairs)
    # usd_cryptos = []
    # for pair in usd_pairs_cleaned:
    #     usd_cryptos.append(pair[:-4])

    # delete usd coins to usd coins
    usd_cryptos_cleaned = []
    for crypto in usd_cryptos:
        if 'USD' not in crypto:
            usd_cryptos_cleaned.append(crypto)

    print('binance_pairs: ', all_pairs)
    print('usd_pairs: ', usd_pairs)
    print('usd_cryptos: ', usd_cryptos)
    print('usd_cryptos_cleaned: ', usd_cryptos_cleaned)
