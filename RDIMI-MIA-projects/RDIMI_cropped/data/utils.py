# -*- encoding: utf-8 -*-

from os import makedirs, remove
from os.path import exists, join, abspath
from calendar import timegm
from time import strptime, strftime, time
from enum import Enum
import pandas as pd
import numpy as np
from copy import deepcopy
from datetime import datetime
from io import StringIO
from frozendict import frozendict
from collections import namedtuple

from urllib.parse import urlencode
from urllib.request import urlopen


from filelock import Timeout, FileLock


from data.cryptocompare import get_historical_price_minute as get_minute_data, get_historical_price_hour as get_hour_data, get_historical_price_day as get_day_data
import csv
import re
import yfinance as yf

from api.binance_api import Binance

from time import mktime

from finam.export import Exporter, Market, LookupComparator

"""
Full-on example displaying up-to-date values of some important indicators
"""


def get_data(fpath, coin, market, timeframe, exchange='CCCAGG', max_period=None, **kwargs):

    sec_format = '%Y-%m-%d %H:%M:%S'
    timeframe = timeframe.lower()

    folder = join(abspath(fpath), coin + '_' + market)
    dfile = join(folder, '{}_{}_{}_{}.csv'.format(timeframe, exchange, coin, market))

    lock_path = str(dfile)+'.lock'
    lock = FileLock(lock_path)

    # try:
    #     lock.acquire(timeout=15, poll_intervall=0.5)
    # except:
    #     raise Exception('Failed acquire lock')

    try:
        start_date_orig = pd.Timestamp(kwargs.get('start_date', '2010-01-01 00:00:00')).strftime(sec_format)
        start_date = start_date_orig

        last_date = kwargs.get('last_date', None)
        last_date = pd.Timestamp(last_date).strftime(sec_format) if last_date is not None else None

        old_data = None
        current_data = pd.DataFrame()
        if exists(dfile):
            old_data = pd.read_csv(dfile, sep=',', parse_dates=True, index_col=0, engine='python')
            current_data = old_data.loc[old_data.index >= start_date]
            if last_date is not None:
                current_data = current_data.loc[current_data.index <= last_date]
            start_date = np.min([old_data.index[-1], pd.Timestamp(start_date)]).strftime(sec_format)
    except Exception as e:
        remove(dfile)
        raise Exception(f'Seems that file {dfile} is corrupted and has been deleted', e)

    update_data = kwargs.get('update_data', True)
    if update_data or len(current_data) == 0:
        start_date = strptime(start_date, sec_format)
        start_date_s = int(timegm(start_date))

        last_date_s = int(timegm(last_date) if last_date is not None else time())
        last_date = strptime(last_date, sec_format) if last_date is not None else strftime(sec_format)

        date_diff = last_date_s - start_date_s
        timeframe_list = list(timeframe)
        if exchange == 'Finam':
            tframe = Timeframe.H1
            # coin = {coin: coin_searcher(coin)}
            search = Exporter().lookup(code=coin)
            coin = {coin: [search.iloc[0]['market'], search.index[0]]}
            data = get_finam_data(coin, tframe, start_date=start_date, last_date=strptime(last_date, sec_format))
            if list(coin.values())[0][0] == 0:
                data = data.between_time('10:00', '18:00')
        else:
            if timeframe_list[1] == 'm':
                limit = date_diff * int(timeframe_list[0]) / 60
                get_data_func = get_minute_data
            elif timeframe_list[1] == 'h':
                limit = date_diff * int(timeframe_list[0]) / 60 / 60
                get_data_func = get_hour_data
            elif timeframe_list[1] == 'd':
                limit = date_diff * int(timeframe_list[0]) / 60 / 60 / 24
                get_data_func = get_day_data

            else:
                raise Exception('Unsupported timeframe exception: ' + timeframe)

            limit = int(limit) + 100

            if exchange == 'Binance':
                limit = 1000 if limit>1000 else limit
                bot = Binance(
                    API_KEY='-',  # cropped
                    API_SECRET='-'  # cropped
                )
                market = 'USDT' if market == 'USD' else market
                data_list = bot.klines(symbol=coin + market, interval=timeframe, limit=limit)
                data = {'Open': [], 'High': [], 'Low': [], 'Close': [], 'Volume': []}
                index = []
                for i in range(len(data_list)-1):
                    index.append(pd.to_datetime(data_list[i][0], unit='ms'))
                    data['Open'].append(float(data_list[i][1]))
                    data['High'].append(float(data_list[i][2]))
                    data['Low'].append(float(data_list[i][3]))
                    data['Close'].append(float(data_list[i][4]))
                    data['Volume'].append(float(data_list[i][5]))
                data = pd.DataFrame(data=data,index=index)
            else:
                data = get_data_func(coin=coin, curr=market, limit=limit, timestamp=last_date_s if last_date is not None else None, exchange=exchange)
                data['Volume'] = data.Volumeto
                data = data.drop('Volumeto', axis=1)
        data = data.loc[data.index >= strftime(sec_format, start_date)]

        idx = data.loc[data.Open != 0].index[0]
        data = data.loc[data.index >= idx]
        if old_data is not None and timeframe[1] != 'm':
            data = data.loc[~data.index.duplicated(keep='first')]
            old_data = old_data.to_dict(orient='index')
            old_data.update(data.to_dict(orient='index'))
            data = pd.DataFrame.from_dict(old_data, orient='index')
        data = data.loc[~data.index.duplicated(keep='first')]

        if not exists(folder):
            makedirs(folder)

        data = data.sort_index()

        # timeframe = ''.join(timeframe)
        # if timeframe[1]!='m':
        #     data = data.resample(timeframe).agg(
        #         {'Open': 'first',
        #          'High': 'max',
        #          'Low': 'min',
        #          'Close': 'last',
        #          'Volume': 'sum'})

        data.to_csv(dfile, sep=',')

        data = data.loc[data.index >= start_date_orig]
        if last_date is not None:
            try:
                last_date = last_date if isinstance(last_date, pd.Timestamp) else pd.Timestamp(last_date)
                last_date = last_date.strftime(sec_format)
            except:
                last_date = datetime.fromtimestamp(mktime(last_date))
            data = data.loc[data.index <= last_date]
    else:
        data = old_data.loc[old_data.index >= start_date]
        # data = data.loc[data.index.dayofweek < 5]  # remove weekend
        # data = data.between_time('18:00', '16:00')  # not working!
        if last_date is not None:
            data = data.loc[data.index <= last_date]
    # data = data.loc[data.index.dayofweek < 5]  # remove weekend

    del data['Conversiontype']
    del data['Conversionsymbol']
    
    lock.release()

    data = data.dropna()
    if max_period is not None and len(data) < max_period:
        raise NameError('{} data length is smaller than max period: {} < {}'.format(coin, len(data), max_period))

    return data.dropna()


def coin_searcher(coin):
    exporter = Exporter()
    MMVB = {'ABRD': 82460, 'AESL': 181867, 'AFKS': 19715, 'AFLT': 29, 'AGRO': 399716, 'AKRN': 17564, 'ALBK': 82616,
               'ALNU': 81882, 'ALRS': 81820, 'AMEZ': 20702, 'APTK': 13855, 'AQUA': 35238, 'ARMD': 19676, 'ARSA': 19915,
               'ASSB': 16452, 'AVAN': 82843, 'AVAZ': 39, 'AVAZP': 40, 'BANE': 81757, 'BANEP': 81758, 'BGDE': 175840,
               'BISV': 35242, 'BISVP': 35243, 'BLNG': 21078, 'BRZL': 81901, 'BSPB': 20066, 'CBOM': 420694,
               'CHEP': 20999, 'CHGZ': 81933, 'CHKZ': 21000, 'CHMF': 16136, 'CHMK': 21001, 'CHZN': 19960, 'CLSB': 16712,
               'CLSBP': 16713, 'CNTL': 21002, 'CNTLP': 81575, 'DASB': 16825, 'DGBZ': 17919, 'DIOD': 35363,
               'DIXY': 18564, 'DVEC': 19724, 'DZRD': 74744, 'DZRDP': 74745, 'ELTZ': 81934, 'ENRU': 16440,
               'EPLN': 451471, 'ERCO': 81935, 'FEES': 20509, 'FESH': 20708, 'FORTP': 82164, 'GAZA': 81997,
               'GAZAP': 81998, 'GAZC': 81398, 'GAZP': 16842, 'GAZS': 81399, 'GAZT': 82115, 'GCHE': 20125, 'GMKN': 795,
               'GRAZ': 16610, 'GRNT': 449114, 'GTLC': 152876, 'GTPR': 175842, 'GTSS': 436120, 'HALS': 17698,
               'HIMC': 81939, 'HIMCP': 81940, 'HYDR': 20266, 'IDJT': 388276, 'IDVP': 409486, 'IGST': 81885,
               'IGST03': 81886, 'IGSTP': 81887, 'IRAO': 20516, 'IRGZ': 9, 'IRKT': 15547, 'ISKJ': 17137, 'JNOS': 15722,
               'JNOSP': 15723, 'KAZT': 81941, 'KAZTP': 81942, 'KBSB': 19916, 'KBTK': 35285, 'KCHE': 20030,
               'KCHEP': 20498, 'KGKC': 83261, 'KGKCP': 152350, 'KLSB': 16329, 'KMAZ': 15544, 'KMEZ': 22525,
               'KMTZ': 81903, 'KOGK': 20710, 'KRKN': 81891, 'KRKNP': 81892, 'KRKO': 81905, 'KRKOP': 81906, 'KROT': 510,
               'KROTP': 511, 'KRSB': 20912, 'KRSBP': 20913, 'KRSG': 15518, 'KSGR': 75094, 'KTSB': 16284, 'KTSBP': 16285,
               'KUBE': 522, 'KUNF': 81943, 'KUZB': 83165, 'KZMS': 17359, 'KZOS': 81856, 'KZOSP': 81857, 'LIFE': 74584,
               'LKOH': 8, 'LNTA': 385792, 'LNZL': 21004, 'LNZLP': 22094, 'LPSB': 16276, 'LSNG': 31, 'LSNGP': 542,
               'LSRG': 19736, 'LVHK': 152517, 'MAGE': 74562, 'MAGEP': 74563, 'MAGN': 16782, 'MERF': 20947, 'MFGS': 30,
               'MFGSP': 51, 'MFON': 152516, 'MGNT': 17086, 'MGNZ': 20892, 'MGTS': 12984, 'MGTSP': 12983, 'MGVM': 81829,
               'MISB': 16330, 'MISBP': 16331, 'MNFD': 80390, 'MOBB': 82890, 'MOEX': 152798, 'MORI': 81944,
               'MOTZ': 21116, 'MRKC': 20235, 'MRKK': 20412, 'MRKP': 20107, 'MRKS': 20346, 'MRKU': 20402, 'MRKV': 20286,
               'MRKY': 20681, 'MRKZ': 20309, 'MRSB': 16359, 'MSNG': 6, 'MSRS': 16917, 'MSST': 152676, 'MSTT': 74549,
               'MTLR': 21018, 'MTLRP': 80745, 'MTSS': 15523, 'MUGS': 81945, 'MUGSP': 81946, 'MVID': 19737,
               'NAUK': 81992, 'NFAZ': 81287, 'NKHP': 450432, 'NKNC': 20100, 'NKNCP': 20101, 'NKSH': 81947,
               'NLMK': 17046, 'NMTP': 19629, 'NNSB': 16615, 'NNSBP': 16616, 'NPOF': 81858, 'NSVZ': 81929, 'NVTK': 17370,
               'ODVA': 20737, 'OFCB': 80728, 'OGKB': 18684, 'OMSH': 22891, 'OMZZP': 15844, 'OPIN': 20711, 'OSMP': 21006,
               'OTCP': 407627, 'PAZA': 81896, 'PHOR': 81114, 'PHST': 19717, 'PIKK': 18654, 'PLSM': 81241, 'PLZL': 17123,
               'PMSB': 16908, 'PMSBP': 16909, 'POLY': 175924, 'PRFN': 83121, 'PRIM': 17850, 'PRIN': 22806,
               'PRMB': 80818, 'PRTK': 35247, 'PSBR': 152320, 'QIWI': 181610, 'RASP': 17713, 'RBCM': 74779,
               'RDRB': 181755, 'RGSS': 181934, 'RKKE': 20321, 'RLMN': 152677, 'RLMNP': 388313, 'RNAV': 66644,
               'RODNP': 66693, 'ROLO': 181316, 'ROSB': 16866, 'ROSN': 17273, 'ROST': 20637, 'RSTI': 20971,
               'RSTIP': 20972, 'RTGZ': 152397, 'RTKM': 7, 'RTKMP': 15, 'RTSB': 16783, 'RTSBP': 16784, 'RUAL': 414279,
               'RUALR': 74718, 'RUGR': 66893, 'RUSI': 81786, 'RUSP': 20712, 'RZSB': 16455, 'SAGO': 445, 'SAGOP': 70,
               'SARE': 11, 'SAREP': 24, 'SBER': 3, 'SBERP': 23, 'SELG': 81360, 'SELGP': 82610, 'SELL': 21166,
               'SIBG': 436091, 'SIBN': 2, 'SKYC': 83122, 'SNGS': 4, 'SNGSP': 13, 'STSB': 20087, 'STSBP': 20088,
               'SVAV': 16080, 'SYNG': 19651, 'SZPR': 22401, 'TAER': 80593, 'TANL': 81914, 'TANLP': 81915, 'TASB': 16265,
               'TASBP': 16266, 'TATN': 825, 'TATNP': 826, 'TGKA': 18382, 'TGKB': 17597, 'TGKBP': 18189, 'TGKD': 18310,
               'TGKDP': 18391, 'TGKN': 18176, 'TGKO': 81899, 'TNSE': 420644, 'TORS': 16797, 'TORSP': 16798,
               'TRCN': 74561, 'TRMK': 18441, 'TRNFP': 1012, 'TTLK': 18371, 'TUCH': 74746, 'TUZA': 20716, 'UCSS': 175781,
               'UKUZ': 20717, 'UNAC': 22843, 'UNKL': 82493, 'UPRO': 18584, 'URFD': 75124, 'URKA': 19623, 'URKZ': 82611,
               'USBN': 81953, 'UTAR': 15522, 'UTII': 81040, 'UTSY': 419504, 'UWGN': 414560, 'VDSB': 16352,
               'VGSB': 16456, 'VGSBP': 16457, 'VJGZ': 81954, 'VJGZP': 81955, 'VLHZ': 17257, 'VRAO': 20958,
               'VRAOP': 20959, 'VRSB': 16546, 'VRSBP': 16547, 'VSMO': 15965, 'VSYD': 83251, 'VSYDP': 83252,
               'VTBR': 19043, 'VTGK': 19632, 'VTRS': 82886, 'VZRZ': 17068, 'VZRZP': 17067, 'WTCM': 19095,
               'WTCMP': 19096, 'YAKG': 81917, 'YKEN': 81766, 'YKENP': 81769, 'YNDX': 388383, 'YRSB': 16342,
               'YRSBP': 16343, 'ZHIV': 181674, 'ZILL': 81918, 'ZMZN': 556, 'ZMZNP': 603, 'ZVEZ': 82001}
    US_Stock = {'AMZN': 874205, 'AAPL': 20569, 'GOOGL': 874433, 'FB': 874399, 'NFLX': 874603, 'GS': 472568,
                'WMT': 18146,
                'V': 875041, 'MA': 489007, 'UBER': 897844, 'TIF': 875002, 'F': 489022, 'GPI': 874435, 'GM': 18058,
                'TSLA': 875014,
                'MSFT': 19068, 'ADBE': 20563, 'IBM': 18069, 'HPQ': 18069, 'INTU': 874486, 'MRO': 874582, 'MCD': 18080}
    US_otrasl = {'CBOT.$DJUSTC': 18995, 'CBOT.$DJUSHC': 18984}
    WORLD_idx = {
        'INX': 90,
        'INDEX.CSI200': 19594,
        'INDEX.CSI300': 22837
    }
    if coin in list(MMVB.keys()):
        result = [1, MMVB[coin]]
    elif coin in list(US_Stock.keys()):
        result = [25, US_Stock[coin]]
    elif coin in list(US_otrasl.keys()):
        result = [27, US_otrasl[coin]]
    elif coin in list(WORLD_idx.keys()):
        result = [6, WORLD_idx[coin]]
    else:
        result = None
    return result


def get_finam_data(ticker, timeframe, **kwargs):
    dt_format = '%Y-%m-%d'
    dt_rev_format = '%d.%m.%Y'

    fname = '{}_{}_{}'.format(timeframe, 'ENG', list(ticker.keys())[0])
    extfname = fname + '.csv'

    start_date = kwargs.get('start_date', '2000-01-01')
    last_date = kwargs.get('last_date', datetime.now().strftime(dt_format))

    # print("Market = " + market.value.name +
    #       "; Ticker = " + ticker.name +
    #       "; Period = " + str(timeframe.name) +
    #       "; Start = " + str(start_date) +
    #       "; End = " + str(last_date))

    # start_date = strptime(start_date_s, dt_format)
    start_date_rev = strftime(dt_rev_format, start_date)
    # last_date = strptime(last_date_s, dt_format)
    last_date_rev = strftime(dt_rev_format, last_date)

    #Все параметры упаковываем в единую структуру. Здесь есть дополнительные параметры, кроме тех, которые заданы в шапке. См. комментарии внизу:
    params = urlencode([
                        ('market', list(ticker.values())[0][0]), #на каком рынке торгуется бумага
                        ('em', list(ticker.values())[0][1]), #вытягиваем цифровой символ, который соответствует бумаге.
                        ('code', list(ticker.keys())[0]), #тикер нашей акции
                        ('apply', 0), #не нашёл что это значит.
                        ('df', start_date.tm_mday), #Начальная дата, номер дня (1-31)
                        ('mf', start_date.tm_mon - 1), #Начальная дата, номер месяца (0-11)
                        ('yf', start_date.tm_year), #Начальная дата, год
                        ('from', start_date_rev), #Начальная дата полностью
                        ('dt', last_date.tm_mday), #Конечная дата, номер дня
                        ('mt', last_date.tm_mon - 1), #Конечная дата, номер месяца
                        ('yt', last_date.tm_year), #Конечная дата, год
                        ('to', last_date_rev), #Конечная дата
                        ('p', timeframe.value.index), #Таймфрейм
                        ('f', fname), #Имя сформированного файла
                        ('e', ".csv"), #Расширение сформированного файла
                        ('cn', list(ticker.keys())[0]), #ещё раз тикер акции
                        ('dtf', 1), #В каком формате брать даты. Выбор из 5 возможных. См. страницу https://www.finam.ru/profile/moex-akcii/sberbank/export/
                        ('tmf', 1), #В каком формате брать время. Выбор из 4 возможных.
                        ('MSOR', 0), #Время свечи (0 - open; 1 - close)
                        # ('mstime', "on"), #Московское время
                        ('mstimever', 2), #Коррекция часового пояса
                        ('sep', 1), #Разделитель полей	(1 - запятая, 2 - точка, 3 - точка с запятой, 4 - табуляция, 5 - пробел)
                        ('sep2', 1), #Разделитель разрядов
                        ('datf', 1), #Формат записи в файл. Выбор из 6 возможных.
                        ('at', 1)]) #Нужны ли заголовки столбцов
    url = "http://export.finam.ru/" + extfname + '?' + params #урл составлен!
          # extfname + '?' + params #урл составлен!
    # try:
    #
    # except Exception as e:
    #     print(e)
    response = urlopen(url)
    txt = response.read().decode('utf-8')  # здесь лежит огромный массив данных, прилетевший с Финама.
    if len(txt) != 0:
        data = pd.read_csv(StringIO(txt))
        # print(start_date_s, last_date_s, data.shape)

        # for value in data['<DATE>'].map(str) + data['<TIME>'].map(str).map(lambda v: v.rjust(6, '0')):
        #     print(value)
        #     value = pd.to_datetime(value, format='%Y%m%d%H%M%S')
        #     print(value)

        data_idx = pd.DatetimeIndex(
            pd.to_datetime(data['<DATE>'].map(str) + data['<TIME>'].map(str).map(lambda v: v.rjust(6, '0')),
                           format='%Y%m%d%H%M%S'))
        data = data.set_index(data_idx)
        data = data.drop(['<DATE>', '<TIME>'], 1)
        data.columns = [re.sub('[^a-zA-Z]+', '', c).capitalize() for c in data.columns]
        data = data[['Open', 'High', 'Low', 'Close']]
        # data.to_csv(join('./', str(int(timegm(last_date))) + '_' + extfname))
    else:
        data = None
        # print(start_date_s, last_date_s, (0, 0))

    return data


def get_report(input_file="lvud6326_May_2020 copy.csv", output_file='RDI_Fund_May_2020.xlsx'):
    company = []
    trade_date = []
    settlement_date = []  # whats the difference between trade_date and this
    counterpart = []
    security = []
    number = []
    price = []
    currency = []
    commission = []
    other_charges = []
    trade_type = []

    def csv_reader(file_obj):
        reader = csv.reader(file_obj)
        flag = True
        for row in reader:
            if flag:
                flag = False
                continue
            line = row[0].split(",")
            print(line)
            company.append('RDI Fund')
            counterpart.append('Bitstamp')
            temp = line[1] + line[2] + line[3]
            trade_date.append(f'{temp}')
            security.append(re.sub(r'[^\w\s]+|[\d]+', r'', line[5]).strip())
            number.append(re.sub(r'[a-zA-Z]', r'', line[5]).strip())
            price.append(re.sub(r'[a-zA-Z]', r'', line[7]).strip())
            currency.append(re.sub(r'[^\w\s]+|[\d]+', r'', line[7]).strip())
            commission.append(re.sub(r'[a-zA-Z]', r'', line[8]).strip())
            other_charges.append('')
            if line[9] == 'Sell':
                trade_type.append('SH')
            elif line[9] == 'Buy':
                trade_type.append('BO')

    csv_path = input_file
    with open(csv_path, "r") as f_obj:
        csv_reader(f_obj)
    out_df = pd.DataFrame({
        'Company': company, 'Trade Date': trade_date, 'Settlement Date': trade_date, 'Counterpart': counterpart,
        'Security': security, 'Number': number, 'Price': price, 'Currency': currency, 'Commission': commission,
        'Other Charges': other_charges, 'Trade Type': trade_type
    })
    print('out_df', out_df)
    out_df.to_excel(output_file)


def split_data_by_volume(data):
    volume_count = 0
    volume_limit = data.Volume.iloc[0]
    new_data = pd.DataFrame(columns=['Close', 'High', 'Low', 'Open', 'Volume'])
    pd.DataFrame()
    for i in range(len(data.index)):
        volume_count += data.Volume.iloc[i]
        if volume_count >= 2 * volume_limit:
            df2 = pd.DataFrame([[data.iloc[i:i + 1].Close.values[0], data.iloc[i:i + 1].High.values[0],
                                 data.iloc[i:i + 1].Low.values[0], data.iloc[i:i + 1].Open.values[0], volume_count]],
                               columns=['Close', 'High', 'Low', 'Open', 'Volume'], index=data.index[i:i + 1])
            new_data = new_data.append(df2)
            volume_count = 0
            volume_limit = data.Volume.iloc[i]
    return new_data


def get_yahoo_data(ticker, period=None, timeframe="1d", start=None, end=None):
    # valid periods are: 1d, 5d, 1mo, 3mo, 6mo, 1y, 2y, 5y, 10y, ytd, max
    # valid intervals are: 1m, 2m, 5m, 15m, 30m, 60m, 90m, 1h, 1d, 5d, 1wk, 1mo, 3mo
    # get_yahoo_data("MSFT", period="max", timeframe="1d", start="2017-01-01", end="2020-01-01")

    msft = yf.Ticker(ticker)

    if start is not None or end is not None:
        period = None
    elif period is None:
        period = "max"

    hist = msft.history(period=period, interval=timeframe, start=start, end=end)

    # drop zeros in hist
    hist = hist.loc[hist['Open'] != 0.0]
    hist = hist.loc[hist['High'] != 0.0]
    hist = hist.loc[hist['Low'] != 0.0]
    hist = hist.loc[hist['Close'] != 0.0]

    # TODO: dirty code (universal code for multiple timeframes + hour variable)
    if timeframe == '1h':
        for i in range(len(hist.index)):
            if i == 0:
                hour = 9
            elif hist.index[i-1].day == hist.index[i].day:
                hour += 1
            else:
                hour = 9
            hist.index.values[i] = hist.index[i].replace(hour=hour)

    return hist


def get_kaggle_minute_data(coin, start_date=None, last_date=None):
    fpath = 'data/Minute_data/{}ust.csv'.format(coin)
    data = pd.read_csv(fpath, parse_dates=True, index_col=0)
    data.index = pd.to_datetime(data.index, unit='ms')
    data.columns = ['Open', 'Close', 'High', 'Low', 'Volume']
    data = data.loc[data.index >= start_date]
    data = data.loc[data.index <= last_date]
    return data


def generator_list_reader(generator):
    generator_new_dict = {}
    for i in list(generator.keys()):
        for a in list(i):
            generator_new_dict[a] = generator[i]
    return generator_new_dict

class NameValue:

    def __init__(self, name, value):
        self.__name = name
        self.__value = value

    @property
    def name(self):
        return self.__name

    @property
    def value(self):
        return self.__value


class Tickers(object):

    def __init__(self, tickers=None):
        tmp = deepcopy(tickers if tickers is not None else {})
        self.__tickers = frozendict({k: v for k, v in tmp.items()})
        for ticker, value in self.__tickers.items():
            self.__setattr__(ticker, NameValue(ticker, value))

# MarketValue = namedtuple("MarketValue", ['name', 'index', 'tickers'])
# MarketTickers = namedtuple('MarketTickers', ['name', 'ticker', 'index'])

class MarketValue(NameValue):

    def __init__(self, name, value, tickers=None):
        super().__init__(name, value)
        self.__tickers = Tickers(tickers)

    @property
    def tickers(self):
        return self.__tickers



class Timeframe(Enum):
    TimeframeValue = namedtuple('TimeframeValue', ['name', 'index', 'limit'])
    # TODO Tick и MN1 считаются неправильно
    tick = TimeframeValue('Tick', 1, -1)
    M1 = TimeframeValue('M1', 2, 60)
    M5 = TimeframeValue('M5', 3, 60 * 5)
    M10 = TimeframeValue('M10', 4, 60 * 10)
    M15 = TimeframeValue('M15', 5, 60 * 15)
    M30 = TimeframeValue('M30', 6, 60 * 30)
    H1 = TimeframeValue('H1', 7, 60 * 60)
    D1 = TimeframeValue('D1', 8, 60 * 60 * 24)
    W1 = TimeframeValue('W1', 9, 60 * 60 * 24 * 7)
    MN1 = TimeframeValue('MN1', 10, 60 * 60 * 24 * 30)
from finam.export import Exporter, Market, LookupComparator

# get_re