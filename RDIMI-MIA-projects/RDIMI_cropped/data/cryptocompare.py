import requests
import time
import datetime
from functools import wraps
from calendar import timegm
import logging

import pandas as pd
from pandas.io.json import json_normalize
import numpy as np

# API
URL_COIN_LIST = 'https://www.cryptocompare.com/api/data/coinlist/'
URL_PRICE = 'https://min-api.cryptocompare.com/data/pricemulti?fsyms={}&tsyms={}'
URL_PRICE_MULTI = 'https://min-api.cryptocompare.com/data/pricemulti?fsyms={}&tsyms={}'
URL_PRICE_MULTI_FULL = 'https://min-api.cryptocompare.com/data/pricemultifull?fsyms={}&tsyms={}'
URL_HIST_PRICE = 'https://min-api.cryptocompare.com/data/pricehistorical?fsym={}&tsyms={}&ts={}&e={}'
URL_HIST_PRICE_DAY = 'https://min-api.cryptocompare.com/data/histoday?fsym={}&tsym={}&e={}&limit={}' # &toTs={}
URL_HIST_PRICE_HOUR = 'https://min-api.cryptocompare.com/data/histohour?fsym={}&tsym={}&e={}&limit={}' # &toTs={}
URL_HIST_PRICE_MINUTE = 'https://min-api.cryptocompare.com/data/histominute?fsym={}&tsym={}&e={}&limit={}' # &toTs={}
URL_AVG = 'https://min-api.cryptocompare.com/data/generateAvg?fsym={}&tsym={}&e={}'
URL_EXCHANGES = 'https://www.cryptocompare.com/api/data/exchanges'

# FIELDS
PRICE = 'PRICE'
HIGH = 'HIGH24HOUR'
LOW = 'LOW24HOUR'
VOLUME = 'VOLUME24HOUR'
CHANGE = 'CHANGE24HOUR'
CHANGE_PERCENT = 'CHANGEPCT24HOUR'
MARKETCAP = 'MKTCAP'

# DEFAULTS
CURR = 'USD'
LIMIT = 2000
###############################################################################

def multi_coins(func):

    @wraps(func)
    def multi_coins_wrap(*args, **kwargs):
        args = list(args)
        coin = args[0]
        if isinstance(coin, str):
            return func(*args, **kwargs)
        else:
            data = {}
            for ccoin in coin:
                args[0] = ccoin
                args = tuple(args)
                data[coin] = func(*args, **kwargs)
                time.sleep(1)
            return data

    return multi_coins_wrap

def high_limit(func):
    
    mult = 60
    if 'day' in str(func):
        mult = 60 * 60 * 60
    elif 'hour' in str(func):
        mult = 60 * 60

    @wraps(func)
    def high_limit_wrap(*args, **kwargs):
        timestamp = kwargs.get('timestamp', None)
        if isinstance(timestamp, datetime.datetime):
            timestamp = time.mktime(timestamp.timetuple())
        limit = kwargs.get('limit', 2000)
        if limit > 2000:
            kwargs['limit'] = 2000
            if timestamp is None:
                kwargs.pop('timestamp', None)
            json = func(*args, **kwargs)
            lrange = [2000] * int(np.floor(limit / 2000))
            lrange[-1] = limit - np.sum(lrange)
            lrange = [v for v in lrange if v > 0]
            timestamp = int(kwargs.get('timestamp', time.time()))
            for climit in lrange:
                timestamp -= 2000 * mult
                kwargs['timestamp'] = timestamp
                kwargs['limit'] = climit
                njson = func(*args, **kwargs)
                json['Data'].extend(njson['Data'])
            return json
        
        return func(*args, **kwargs)

    return high_limit_wrap

def df_respsone(func):

    @wraps(func)
    def df_respsone_wrap(*args, **kwargs):
        data = func(*args, **kwargs)
        if kwargs.get('df', True):
            data = json_normalize(data['Data'])
            data = data.set_index(pd.DatetimeIndex(pd.to_datetime(data.time, unit='s')))
            data = data.drop('time', axis=1)
            data = data.loc[data.index.drop_duplicates()].sort_index(ascending=True)
            data = data.iloc[1:]
            data.columns = [c.capitalize() for c in data.columns]
            return data

    return df_respsone_wrap

def query_cryptocompare(url,errorCheck=True):
    try:
        try:
            logging.getLogger('TradeLogger').debug('get_data URL: {}'.format(url))
        except:
            pass
        response = requests.get(url).json()
    except Exception as e:
        print('Error getting coin information. %s' % str(e))
        raise e
    if errorCheck and (response.get('Response') == 'Error'):
        print('[ERROR] %s' % response.get('Message'))
        raise Exception(response.get('Message'))
    return response

def format_parameter(parameter):
    if isinstance(parameter, list):
        return ','.join(parameter)
    else:
        return parameter


def add_to_ts(url, timestamp):
    if timestamp is not None:
        url = '{}&toTs={}'.format(url, int(timestamp))
    return url

###############################################################################

def get_coin_list(format=False):
    response = query_cryptocompare(URL_COIN_LIST, False)['Data']
    if format:
        return list(response.keys())
    else:
        return response

# TODO: add option to filter json response according to a list of fields
def get_price(coin, curr=CURR, full=False):
    if full:
        return query_cryptocompare(URL_PRICE_MULTI_FULL.format(format_parameter(coin),
            format_parameter(curr)))
    if isinstance(coin, list):
        return query_cryptocompare(URL_PRICE_MULTI.format(format_parameter(coin),
            format_parameter(curr)))
    else:
        return query_cryptocompare(URL_PRICE.format(coin, format_parameter(curr)))

def get_historical_price(coin, curr=CURR, timestamp=time.time(), exchange='CCCAGG'):
    if isinstance(timestamp, datetime.datetime):
        timestamp = time.mktime(timestamp.timetuple())
    return query_cryptocompare(URL_HIST_PRICE.format(coin, format_parameter(curr),
        int(timestamp), format_parameter(exchange)))

@df_respsone
@high_limit
def get_historical_price_day(coin, curr=CURR, limit=LIMIT, timestamp=None, exchange='CCCAGG'):
    if isinstance(timestamp, datetime.datetime):
        timestamp = time.mktime(timestamp.timetuple())
    return query_cryptocompare(add_to_ts(URL_HIST_PRICE_DAY.format(coin, format_parameter(curr), format_parameter(exchange), limit), timestamp))

@df_respsone
@high_limit
def get_historical_price_hour(coin, curr=CURR, limit=LIMIT, timestamp=None, exchange='CCCAGG'):
    if isinstance(timestamp, datetime.datetime):
        timestamp = time.mktime(timestamp.timetuple())

    return query_cryptocompare(add_to_ts(URL_HIST_PRICE_HOUR.format(coin, format_parameter(curr), format_parameter(exchange), limit), timestamp))

@df_respsone
@high_limit
def get_historical_price_minute(coin, curr=CURR, limit=LIMIT, timestamp=None, exchange='CCCAGG'):
    if isinstance(timestamp, datetime.datetime):
        timestamp = time.mktime(timestamp.timetuple())
    return query_cryptocompare(add_to_ts(URL_HIST_PRICE_MINUTE.format(coin, format_parameter(curr), format_parameter(exchange), limit), timestamp))


def get_avg(coin, curr=CURR, exchange='CCCAGG'):
    response = query_cryptocompare(URL_AVG.format(coin, curr, format_parameter(exchange)))
    if response:
        return response['RAW']


def get_exchanges():
    response = query_cryptocompare(URL_EXCHANGES)
    if response:
        return response['Data']
