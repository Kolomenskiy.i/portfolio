import csv

import matplotlib.dates as mdates
import mpl_finance as mpl_f
import numpy as np
import pandas as pd
import progressbar
import pylab as plt
import requests
from datetime import datetime
import time
import os, shutil
import calendar

from api.binance_api import Binance

np.set_printoptions(threshold=np.inf) #full output list
np.set_printoptions(suppress=True)

bot = Binance(
    API_KEY='-',  # cropped
    API_SECRET='-'  # cropped
)

pair1 = 'BTCUSDT'
   
def old_get_binance_data(data_type, pair, data_timeframe):
    candles_amount=1000
    slov = bot.tickerBookTicker(symbol=pair1)
    if data_type == 'ask':
            return slov.get('askPrice')
    if data_type == 'bid':
            return slov.get('bidPrice')
    if data_type == 'ask_volume':
            return slov.get('askQty')
    if data_type == 'bid_volume':
            return slov.get('bidQty')
    if pair == 'BTCUSDT':
        x = bot.klines(symbol=pair1, interval=data_timeframe, limit=candles_amount)
    if pair == 'EOS':
        x = bot.klines(symbol=pair1, interval=data_timeframe, limit=candles_amount)
    data=[0 for x in range(candles_amount)]
    i = 0
    while i < candles_amount:
        if data_type == 'Close':
            data[i] = float(x[i][4])         
        if data_type == 'Open':
            data[i] = float(x[i][1])
        if data_type == 'Volume':
            data[i] = float(x[i][5])
        i+=1
    return data  

def get_orderbook():
    FILENAME = 'Poloniex_BTCETH_OrderBookFlow_Sample.csv'
    start = True
    with open(FILENAME, 'r', newline='') as file:
        reader = csv.reader(file)
        data = np.empty((4, 5054971-1))
        i = 0
        for row in reader:
            if start:
                start = False       
            else:
                data[0][i] = row[3]#price
                data[1][i] = row[4]#volume
                data[2][i] = row[2]#buy or sell
                data[3][i] = row[7]#timestamp
                i+=1
    return data

def get_btc_orderbook():
    FILENAME = 'btc.csv'
    with open(FILENAME, 'r', newline='') as file:
        reader = csv.reader(file)
        data = np.empty((4, 1190525))
        i = 0
        bar = progressbar.ProgressBar(maxval=1190525).start() 
        for row in reader:
            bar.update(i)
            if row[0] == 'BTC/USD' and row[1] == 'kraken':
                data[0][i] = row[3]#ask
                data[1][i] = row[5]#bid
                data[2][i] = row[4]#ask_volume
                data[3][i] = row[6]#bid_volume
                i+=1
        bar.finish()
    return data
 
def stop_loss_order(btc_balance, stop_price):
    x = float("{0:.6f}".format(btc_balance))
    stop_price = float("{0:.6f}".format(stop_price))
    result = bot.createOrder(
        symbol=pair1,
        recvWindow=5000,
        side='SELL',
        type='STOP_LOSS_LIMIT',
        timeInForce='GTC',
        quantity=x,
        stopPrice=stop_price,
        price=stop_price,
        newOrderRespType='FULL'
    )
    return result

def no_repeat(div_list):#deletes first sells (i.e. 2) and last buys (i.e. 1)
    p = 0
    last_action = 0
    last_action_index = 0
    start = True
    start2 = True
    while p < len(div_list):
        if start:
            start = False
        else:
            if last_action == div_list[p]:
                div_list[p] = 0
        if div_list[p] != 0:
            if start2:
                if div_list[p] == 1:
                    start2 = False
                else:
                    div_list[p] = 0
            #else:
            last_action = div_list[p]
            last_action_index = p
        p+=1
    if last_action == 1:
        div_list[last_action_index] = 0
    return div_list
  
def param_search(start_index, stop_index, step, stop_step):
    start_index_copy = start_index
    stop_index_copy = stop_index
    max_profit = -100
    start_index1 = start_index
    stop_index1 = stop_index
    start_step = step
    step1 = start_step
    best_param = start_index
    best_param1 = start_index
    while step >= stop_step:
        for j in np.arange(start_index, stop_index, step):
            while step1 >= stop_step:
                for i in np.arange(start_index1, stop_index1, step1):
                    profit1 = simple_offline('stylus', i, j)
                    if max_profit < profit1:
                        max_profit = profit1
                        best_param = j
                        best_param1 = i
                    print('i: ', i, 'j: ', j, '\n')
                start_index1 = best_param1 - step1
                stop_index1 = best_param1 + step1
                step1/=10
            i = 0
            step1 = start_step
            start_index1 = start_index_copy
            stop_index1 = stop_index_copy
        start_index = best_param - step
        stop_index = best_param + step
        step/=10
    print('Max profit: ', max_profit)
    print('Stop_loss: ', best_param)
    print('Take_profit: ', best_param1)
  
def one_param_search(start_index, stop_index, step, stop_step, div_list, offset):
    max_profit = -100
    best_param = start_index
    while step >= stop_step:
        for i in np.arange(start_index, stop_index, step):
            profit = simple_offline(div_list, i, offset)
            if max_profit < profit:
                max_profit = profit
                best_param = i
        start_index = best_param - step
        stop_index = best_param + step
        step/=10
    #if simple_offline('stylus', 1000000) >= max_profit:
        #print('Stop loss is not needed')
    #else:
    print('Max profit: ', max_profit)
    print('Best param: ', best_param)
    return best_param

def simple_offline(div_list, param, offset):
    #div_list = globals()[name](param)
    balance = 1000000
    btc_balance = 0
    i = 0
    deals_count = 0
    div_list = no_repeat(div_list)
    bought_price = 0
    bought = False
    while i < len(div_list):
        '''
        if i > 3:#take profit
            if close_list[i-4]*param<close_list[i-2] and close_list[i]<close_list[i-1] and div_list[i] == 0:
                div_list[i] = 2
        '''
        if div_list[i] == 1 and bought == False:#bought
            btc_balance = (float(btc_balance) + float(balance)/float(close_list[i]))*0.997
            balance = 0
            deals_count+=1
            bought_price = close_list[i]
            bought = True
        if div_list[i] == 2 and bought == True:#sold
            balance = (float(balance) + float(btc_balance)*float(close_list[i]))*0.997
            btc_balance = 0
            deals_count+=1
            bought = False
        if bought_price > close_list[i+offset]*param  and bought == True: #stop loss param = 1.02 
            balance = (float(balance) + float(btc_balance)*float(close_list[i]))*0.997
            btc_balance = 0
            deals_count+=1
            bought = False
        i+=1     
    if (balance == 0):
        balance = float(btc_balance) * float(open_list[len(open_list)-1]) #!!!!!!!!len(open list) -> close list 
    trading_profit = float(balance)/float(1000000)*float(100)
    return trading_profit-100

def adaptive_stop_loss(start_index, stop_index, step, stop_step, div_list, close_list1):
    max_profit = -100
    best_param = start_index
    while step >= stop_step:
        for i in np.arange(start_index, stop_index, step):
            balance = 1000000
            btc_balance = 0
            j = 0
            deals_count = 0
            div_list = no_repeat(div_list)
            bought_price = 0
            bought = False
            while j < len(div_list):
                if bought_price*i > close_list1[j] and bought == True:
                    balance = (float(balance) + float(btc_balance)*float(close_list1[j]))*0.997
                    btc_balance = 0
                    deals_count+=1
                    bought = False
                if div_list[j] == 1 and bought == False:#bought
                    btc_balance = (float(btc_balance) + float(balance)/float(close_list1[j]))*0.997
                    balance = 0
                    deals_count+=1
                    bought_price = close_list1[j]
                    bought = True
                if div_list[j] == 2 and bought == True:#sold
                    balance = (float(balance) + float(btc_balance)*float(close_list1[j]))*0.997
                    btc_balance = 0
                    deals_count+=1
                    bought = False
                j+=1     
            if balance == 0:
                balance = float(btc_balance) * float(open_list[len(open_list)-1])
            trading_profit = float(balance)/float(1000000)*float(100)
            profit = trading_profit-100

            if max_profit < profit:
                max_profit = profit
                best_param = i
            #print('max profit: ', max_profit)
            #print('i: ', i)
        start_index = best_param - step
        stop_index = best_param + step
        step/=10
    print('Max profit: ', max_profit)
    print('Best param: ', best_param)
    return best_param

def adaptive_stop_loss_and_trailing(start_index, stop_index, step, stop_step, name, div_list = [-1], index_list = -1):
    if div_list[0] == -1:
        try:
            div_list = globals()[name]()
        except TypeError:
            div_list = globals()[name]
    max_profit = -100
    best_param = start_index
    while step >= stop_step:
        bar = progressbar.ProgressBar(maxval=stop_index).start() 
        for i in np.arange(start_index, stop_index, step):
            bar.update(i)
            balance = 1000000
            btc_balance = 0
            j = 0
            deals_count = 0
            div_list = no_repeat(div_list)
            bought_price = 0
            bought = False
            while j < len(close_list):
                if bought_price < close_list[j] and bought == True:
                    bought_price = close_list[j]
                if div_list[j] == 1 and bought == False:#bought
                    btc_balance = (float(btc_balance) + float(balance)/float(close_list[j]))*0.997
                    balance = 0
                    deals_count+=1
                    bought_price = close_list[j]
                    bought = True
                if (div_list[j] == 2 or bought_price*i > close_list[j]) and bought == True:#sold
                    balance = (float(balance) + float(btc_balance)*float(close_list[j]))*0.997
                    btc_balance = 0
                    deals_count+=1
                    bought = False
                    bought_price = 0
                j+=1       
            if balance == 0:
                balance = float(btc_balance) * float(open_list[len(open_list)-1])
            trading_profit = float(balance)/float(1000000)*float(100)
            profit = trading_profit-100

            if max_profit < profit:
                max_profit = profit
                best_param = i
        bar.finish()
        start_index = best_param - step
        stop_index = best_param + step
        step/=10
    print('Final stop loss: ', best_param)
    bar = progressbar.ProgressBar(maxval=len(close_list)).start() 
    balance = 1000000
    btc_balance = 0
    start_btc_price = open_list[0]
    j = 0
    deals_count = 0
    div_list = no_repeat(div_list)
    bought_price = 0
    bought = False
    bought_list = np.zeros(len(close_list))
    sold_list = np.zeros(len(close_list))
    sold_list_stoploss = np.zeros(len(close_list))
    while j < len(close_list):
        bar.update(j)
        if bought_price < close_list[j] and bought == True:
            bought_price = close_list[j]
        if bought_price*best_param > close_list[j] and bought == True:
            print('Stop loss is done')
            balance = (float(balance) + float(btc_balance)*float(close_list[j]))*0.997
            btc_balance = 0
            deals_count+=1
            sold_list_stoploss[j] = close_list[j]
            bought = False
            bought_price = 0
        if div_list[j] == 1 and bought == False:#bought
            btc_balance = (float(btc_balance) + float(balance)/float(close_list[j]))*0.997
            balance = 0
            deals_count+=1
            bought_price = close_list[j]
            bought_list[j] = close_list[j]
            bought = True
        if div_list[j] == 2 and bought == True:#sold
            balance = (float(balance) + float(btc_balance)*float(close_list[j]))*0.997
            btc_balance = 0
            deals_count+=1
            sold_list[j] = close_list[j]
            bought = False
            bought_price = 0
        j+=1    
    bar.finish()   
    print(name)
    holding_profit = float(open_list[len(open_list)-1])/float(start_btc_price)*float(100) #!!!!!!!!len(open list) -> close list 
    print('Holding profit: ' + str(holding_profit-100))
    if (balance == 0):
        balance = float(btc_balance) * float(open_list[len(open_list)-1]) #!!!!!!!!len(open list) -> close list 
    trading_profit = float(balance)/float(1000000)*float(100)
    print('Trading profit: ' + str(trading_profit-100))
    print('Deals count: ', deals_count, '\n')
    #plt.style.use('dark_background')
    #plt.ion()
    fig, ax = plt.subplots(figsize=(16,9))
    ax.set_title(name + ' ' + timeframe)
    ax.set_xlabel('Holding profit: ' + str(holding_profit-100) + '\n' + 'Trading profit: ' + str(trading_profit-100) + '\n' + 'Deals count: ' + str(deals_count)
    + '\n' + 'Index list: ' + str(index_list))
    #ax.plot(close_list, 'bo')
    ax.plot(sold_list, 'ro')
    ax.plot(sold_list_stoploss, 'yo')
    ax.plot(bought_list, 'go')
    ax.plot(close_list)
    ax.axis([0, len(close_list), min(close_list), max(close_list)])
    fig.tight_layout()
    plt.show()

def dynamic_stop_loss_and_trailing(name, div_list = [-1], index_list = -1):
    if div_list[0] == -1:
        try:
            div_list = globals()[name]()
        except TypeError:
            div_list = globals()[name]
    balance = 1000000
    btc_balance = 0
    stop_loss = 0
    start_btc_price = open_list[0]
    j = 0
    deals_count = 0
    div_list = no_repeat(div_list)
    bought_price = 0
    bought = False
    bought_list = np.zeros(len(close_list))
    sold_list = np.zeros(len(close_list))
    sold_list_stoploss = np.zeros(len(close_list))
    bar = progressbar.ProgressBar(maxval=len(close_list)).start() 
    while j < len(close_list):
        bar.update(j)
        
        if (j+1)%100 == 0:
            #prev_stop_loss = stop_loss
            stop_loss = adaptive_stop_loss(0.6, 1.0, 0.01, 0.01, div_list[j-99:j+1], close_list[j-99:j+1])
            stop_loss = round(stop_loss, 2)
            if stop_loss == 0.6 or stop_loss == 1.0:
                stop_loss = 0
            print('Stop loss: ', stop_loss)
        
        if bought_price < close_list[j] and bought == True:
            bought_price = close_list[j]
        if bought_price*stop_loss > close_list[j] and bought == True:
            print('Stop loss is done')
            balance = (float(balance) + float(btc_balance)*float(close_list[j]))*0.997
            btc_balance = 0
            deals_count+=1
            sold_list_stoploss[j] = close_list[j]
            bought = False
            bought_price = 0
        if div_list[j] == 1 and bought == False:#bought
            btc_balance = (float(btc_balance) + float(balance)/float(close_list[j]))*0.997
            balance = 0
            deals_count+=1
            bought_price = close_list[j]
            bought_list[j] = close_list[j]
            bought = True
        if div_list[j] == 2 and bought == True:#sold
            balance = (float(balance) + float(btc_balance)*float(close_list[j]))*0.997
            btc_balance = 0
            deals_count+=1
            sold_list[j] = close_list[j]
            bought = False
            bought_price = 0
        j+=1    
    bar.finish()   
    print(name)
    holding_profit = float(open_list[len(open_list)-1])/float(start_btc_price)*float(100) #!!!!!!!!len(open list) -> close list 
    print('Holding profit: ' + str(holding_profit-100))
    if (balance == 0):
        balance = float(btc_balance) * float(open_list[len(open_list)-1]) #!!!!!!!!len(open list) -> close list 
    trading_profit = float(balance)/float(1000000)*float(100)
    print('Trading profit: ' + str(trading_profit-100))
    print('Deals count: ', deals_count, '\n')
    #plt.style.use('dark_background')
    #plt.ion()
    fig, ax = plt.subplots(figsize=(16,9))
    ax.set_title(name + ' ' + timeframe)
    ax.set_xlabel('Holding profit: ' + str(holding_profit-100) + '\n' + 'Trading profit: ' + str(trading_profit-100) + '\n' + 'Deals count: ' + str(deals_count)
    + '\n' + 'Index list: ' + str(index_list))
    #ax.plot(close_list, 'bo')
    ax.plot(sold_list, 'ro')
    ax.plot(sold_list_stoploss, 'yo')
    ax.plot(bought_list, 'go')
    ax.plot(close_list)
    ax.axis([0, len(close_list), min(close_list), max(close_list)])
    fig.tight_layout()
    plt.show()

def trailing_stop(name, div_list = [-1], index_list = -1):
    if div_list[0] == -1:
        try:
            div_list = globals()[name]()
        except TypeError:
            div_list = globals()[name]
    balance = 1000000
    btc_balance = 0
    start_btc_price = open_list[0]
    i = 0
    deals_count = 0
    bought_list = np.zeros(len(close_list))
    sold_list = np.zeros(len(close_list))
    div_list = no_repeat(div_list)
    bought_price = 0
    bought = False
    sold_list_stoploss = np.zeros(len(close_list))
    stop_loss = 0
    bar = progressbar.ProgressBar(maxval=len(close_list)).start() 
    while i < len(close_list):
        bar.update(i)
        
        if (i+1)%100 == 0:
            stop_loss = adaptive_stop_loss(0.7, 1.0, 0.01, 0.01, div_list[i-99:i+1], close_list[i-99:i+1])
            stop_loss = round(stop_loss, 2)
            if stop_loss == 0.7 or stop_loss == 1.0:
                stop_loss = 0.9
            print('Stop loss: ', stop_loss)
        
        if bought_price < close_list[i] and bought == True:
            bought_price = close_list[i]
        if div_list[i] == 1 and bought == False:#bought
            btc_balance = (float(btc_balance) + float(balance)/float(close_list[i]))*0.997
            balance = 0
            bought_list[i] = close_list[i]
            deals_count+=1
            bought_price = close_list[i]
            bought = True
        if (div_list[i] == 2 or bought_price*stop_loss > close_list[i]) and bought == True:#sold
            balance = (float(balance) + float(btc_balance)*float(close_list[i]))*0.997
            btc_balance = 0
            sold_list[i] = close_list[i]
            deals_count+=1
            bought = False
            bought_price = 0
        i+=1    
    bar.finish()
    print(name)
    holding_profit = float(open_list[len(open_list)-1])/float(start_btc_price)*float(100) #!!!!!!!!len(open list) -> close list 
    print('Holding profit: ' + str(holding_profit-100))
    if (balance == 0):
        balance = float(btc_balance) * float(open_list[len(open_list)-1]) #!!!!!!!!len(open list) -> close list 
    trading_profit = float(balance)/float(1000000)*float(100)
    print('Trading profit: ' + str(trading_profit-100))
    print('Deals count: ', deals_count, '\n')
    #plt.style.use('dark_background')
    #plt.ion()
    fig, ax = plt.subplots(figsize=(16,9))
    ax.set_title(name + ' ' + timeframe)
    ax.set_xlabel('Holding profit: ' + str(holding_profit-100) + '\n' + 'Trading profit: ' + str(trading_profit-100) + '\n' + 'Deals count: ' + str(deals_count)
    + '\n' + 'Index list: ' + str(index_list))
    #ax.plot(close_list, 'bo')
    ax.plot(sold_list, 'ro')
    ax.plot(sold_list_stoploss, 'yo')
    ax.plot(bought_list, 'go')
    ax.plot(close_list)
    ax.axis([0, len(close_list), min(close_list), max(close_list)])
    fig.tight_layout()
    plt.show()

def get_binance_data(candles_amount=1000):
        open_list, close_list, low_list, high_list, time_list = np.empty((candles_amount)), np.empty((candles_amount)), np.empty(
            (candles_amount)), np.empty((candles_amount)), np.empty((candles_amount))

        bot = Binance(
            API_KEY='-',  # cropped
            API_SECRET='-'  # cropped
        )

        x = bot.klines(symbol='BTCUSDT', interval='1h', limit=candles_amount)

        for i in range(candles_amount):
            time_list[i] = x[i][0]
            open_list[i] = float(x[i][1])
            high_list[i] = float(x[i][2])
            low_list[i] = float(x[i][3])
            close_list[i] = float(x[i][4])

        df = pd.DataFrame()
        df['Open'] = open_list
        df['High'] = high_list
        df['Low'] = low_list
        df['Close'] = close_list
        df.index = pd.to_datetime(time_list)

        return df

def get_new_data(date):
    """ Query the API for 2000 days historical price data starting from "date". """
    url = "https://min-api.cryptocompare.com/data/histoday?fsym=BTC&tsym=USD&e=Bitstamp&limit=2000&toTs={}".format(date)
    r = requests.get(url)
    ipdata = r.json()
    return ipdata

def get_df(from_date, to_date):
    """ Get historical price data between two dates. """
    date = to_date
    holder = []
    # While the earliest date returned is later than the earliest date requested, keep on querying the API
    # and adding the results to a list. 
    while date > from_date:
        data = get_new_data(date)
        holder.append(pd.DataFrame(data['Data']))
        date = data['TimeFrom']
    # Join together all of the API queries in the list.    
    df = pd.concat(holder, axis=0)
    # Remove data points from before from_date
    df = df[df['time'] > from_date]
    # Convert timestamp to readable date format
    df['time'] = pd.to_datetime(df['time'], unit='s')   
    # Make the DataFrame index the time
    df.set_index('time', inplace=True)                  
    # And sort it so its in time order 
    df.sort_index(ascending=False, inplace=True)      
    df = df.iloc[::-1] #reverse df !!!!
    df.drop_dublicates()
    '''#write data to csv
    q = get_df(1417176000, 1547424000) #h:1417176000, d:1417132800
    print(q)
    q.to_csv('1h_BTC_USD_Bitstamp.csv')
    '''
    return df

def crypto_compare_data(timeframe, sym1, sym2, exchange, from_date, to_date):
    """
    timeframe='hour', sym1='BTC', sym2='USD', exchange='Bitstamp', from_date=1417176000, to_date=1547424000
    """
    date = to_date
    holder = []
    timeframe_dict = {'1h': 'hour', '1d': 'day'}
    # While the earliest date returned is later than the earliest date requested, keep on querying the API
    # and adding the results to a list. 
    while date > from_date:
        #data = get_new_data(date)

        """ Query the API for 2000 days historical price data starting from "date". """
        url = "https://min-api.cryptocompare.com/data/histo{}?fsym={}&tsym={}&e={}&limit=2000&toTs={}".format(
            timeframe_dict[timeframe], sym1, sym2, exchange, date)
        r = requests.get(url)
        data = r.json()

        holder.append(pd.DataFrame(data['Data']))
        date = data['TimeFrom']
    # Join together all of the API queries in the list.    
    df = pd.concat(holder, axis=0)
    # Remove data points from before from_date
    df = df[df['time'] > from_date]
    # Convert timestamp to readable date format
    df['time'] = pd.to_datetime(df['time'], unit='s')
    # Make the DataFrame index the time
    df.set_index('time', inplace=True)
    # And sort it so its in time order 
    df.sort_index(ascending=False, inplace=True)
    df.index.names = ['Time']
    df.columns = ['Close', 'High', 'Low', 'Open', 'Volumefrom', 'Volume']
    df = df.iloc[::-1] #reverse df
    df = df.drop_duplicates()

    folder = '../data/{}/{}/{}'.format(sym1 + '_' + sym2, timeframe, exchange)

    if not os.path.exists(folder):
        os.makedirs(folder)

    if os.path.exists(folder):
        for the_file in os.listdir(folder):
            file_path = os.path.join(folder, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                # elif os.path.isdir(file_path): shutil.rmtree(file_path)
            except Exception as e:
                print(e)
    start_datetime = datetime.utcfromtimestamp(from_date).strftime('%Y-%m-%d_%H-%M')
    end_datetime = datetime.utcfromtimestamp(to_date).strftime('%Y-%m-%d_%H-%M')
    filename = start_datetime + '_' + end_datetime + '.csv'
    df.to_csv(folder + '/' + filename)
    return df


def get_data(sym1, sym2, timeframe, exchange, **kwargs):
    folder = '../data/{}/{}/{}'.format(sym1 + '_' + sym2, timeframe, exchange)
    update_data = kwargs.get('update_data', False)
    if not update_data and os.path.exists(folder):
        for dirpath, dirnames, files in os.walk(folder):
            if files:
                df = pd.read_csv(folder + '/' + files[0], sep=',', parse_dates=True, index_col=0)
                start_date = kwargs.get('start_date', df.index[0])
                last_date = kwargs.get('last_date', df.index[-1])
                return df[start_date:last_date]
    else:
        start_date = int(calendar.timegm(time.strptime(kwargs['start_date'], '%Y-%m-%d %H:%M:%S')))
        if 'last_date' in kwargs:
            last_date = int(calendar.timegm(time.strptime(kwargs['last_date'], '%Y-%m-%d %H:%M:%S')))
        else:
            last_date = int(time.time())
        return crypto_compare_data(timeframe, sym1, sym2, exchange, start_date, last_date)


def candle_graph():
    del df['volumeto']
    df['time'] = pd.to_datetime(df['time'])
    df['time'] = mdates.date2num(df['time'])

    ax1 = plt.subplot2grid((6,1), (0,0), rowspan=5, colspan=1)
    ax2 = plt.subplot2grid((6,1), (5,0), rowspan=1, colspan=1, sharex=ax1)
    ax1.xaxis_date()

    mpl_f.candlestick_ohlc(ax1, df.values, width = 0.6/(1*1), colordown='g', colorup='r')
    #ax2.bar(df['time'], df['volumefrom'].values, facecolor='y')
    ax2.fill_between(df['time'], df['volumefrom'], 0)
    plt.show()


#timeframe = '1h' 
'''
open_list = get_data('open',pair1, timeframe)
close_list = get_data('close', pair1, timeframe)
min_list = get_data('min', pair1, timeframe)
max_list = get_data('max', pair1, timeframe)
volume_list = get_data('volume', pair1, timeframe)
'''
'''
df = pd.read_csv('1h_BTC_USD_Bitstamp.csv', sep=',')
open_list = df['open'].values
close_list = df['close'].values
min_list = df['low'].values
max_list = df['high'].values
time_list = df['time'].values
volume_list = df['volumefrom'].values
'''

#print(crypto_compare_data('hour', 'BTC', 'USD', 'Bitstamp', 1417176000, 1550955229)) #h:1417176000, d:1417132800
#eth coinbase 1h: 1464318000
