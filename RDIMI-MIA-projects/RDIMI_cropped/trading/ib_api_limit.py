from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.contract import Contract
from ibapi.order import *
from ibapi.account_summary_tags import AccountSummaryTags

from ccxt.base.exchange import Exchange

import threading
import time


class IBapi(EWrapper, EClient):  # overloading of methods
    def __init__(self):
        self.order_status_dict = None
        self.nextorderId = None
        self.reqId = None
        self.account = None
        self.tag = None
        self.value = None
        self.currency = None
        self.balance = None
        # Update portfolio variables
        self.contract = None
        self.position = None
        self.marketPrice = None
        self.marketValue = None
        self.averageCost = None
        self.unrealizedPNL = None
        self.realizedPNL = None
        self.accountName = None

        self.balance_dict = {}

        EClient.__init__(self, self)

    def tickPrice(self, reqId, tickType, price, attrib):
        if tickType == 2 and reqId == 1:
            print('The current ask price is: ', price)

    def nextValidId(self, orderId: int):
        super().nextValidId(orderId)
        self.nextorderId = orderId

    def orderStatus(self, orderId, status, filled, remaining, avgFullPrice, permId, parentId, lastFillPrice, clientId,
                    whyHeld, mktCapPrice):
        self.order_status_dict = {'orderId': orderId, 'status': status, 'filled': filled, 'remaining': remaining,
                                  'lastFillPrice': lastFillPrice}

    def accountSummary(self, reqId: int, account: str, tag: str, value: str, currency: str):
        super().accountSummary(reqId, account, tag, value, currency)
        self.reqId = reqId
        self.account = account
        self.tag = tag
        self.value = value
        self.currency = currency

    def updatePortfolio(self, contract, position, marketPrice, marketValue, averageCost, unrealizedPNL, realizedPNL, accountName):
        super().updatePortfolio(contract, position, marketPrice, marketValue, averageCost, unrealizedPNL, realizedPNL, accountName)
        self.contract = contract
        self.position = position
        self.marketPrice = marketPrice
        self.marketValue = marketValue
        self.averageCost = averageCost
        self.unrealizedPNL = unrealizedPNL
        self.realizedPNL = realizedPNL
        self.accountName = accountName

        self.balance_dict[contract.localSymbol] = {'asset_count': position, 'market_value': marketValue}


class IBExchange(Exchange):
    def __init__(self, host='127.0.0.1', port=7496, master_id=123, account_name='-'):  # cropped
        super().__init__()
        self.host = host
        self.port = port
        self.master_id = master_id
        self.account_name = account_name
        self.app = IBapi()
        self.app.nextorderId = None
        self.order = None
        self.contract = None
        self.connect()

    def run_loop(self):
        self.app.run()

    def fx_order(self, symbol):
        # EURUSD contract example
        # self.contract = Contract()
        # self.contract.symbol = symbol[:3]
        # self.contract.secType = 'CASH'
        # self.contract.exchange = 'IDEALPRO'
        # self.contract.currency = symbol[3:]

        # Stocks contract example
        self.contract = Contract()
        self.contract.symbol = symbol
        self.contract.secType = 'STK'
        self.contract.exchange = 'SMART'
        self.contract.currency = 'USD'

        # China contract example (not working!)
        # self.contract = Contract()
        # self.contract.symbol = symbol
        # self.contract.secType = 'CFD'
        # self.contract.exchange = 'SMART'
        # self.contract.currency = 'AUD'

        return self.contract

    def connect(self):
        self.app.connect(self.host, self.port, self.master_id)
        api_thread = threading.Thread(target=self.run_loop, daemon=True)
        api_thread.start()

        while not isinstance(self.app.nextorderId, int):
            print('Waiting for connection... (launch TWS and relaunch the program)')
            time.sleep(1)
        print('Connection established!')

    def disconnect(self):
        self.app.disconnect()

    def create_order(self, symbol, order_type, side, amount, price=None, params={}):
        self.order = Order()
        self.order.action = side
        self.order.totalQuantity = amount
        self.order.orderType = order_type

        if order_type == 'LMT':
            if price is None:
                print('Error! Please specify limit price')
                exit()
            else:
                self.order.lmtPrice = price

        self.app.placeOrder(self.app.nextorderId, self.fx_order(symbol), self.order)
        # waiting for order to be placed
        st = time.time()
        while self.app.order_status_dict is None:
            if time.time() - st > 60:
                raise Exception('Failed to create IB order')
            pass

        st = time.time()
        while self.app.order_status_dict['status'] != 'Submitted' and self.app.order_status_dict['status'] != 'Filled':
            if time.time() - st > 60:
                raise Exception('Failed to create IB order')
            pass

    def cancel_order(self, order_id, symbol=None, params={}):  # why do you need symbol in ccxt?
        self.app.cancelOrder(order_id)
        # waiting for order to be canceled
        while self.app.order_status_dict is None:
            pass
        while self.app.order_status_dict['status'] != 'Cancelled' and self.app.order_status_dict['status'] != 'Filled':
            pass

    def cancel_all_orders(self, params={}):  # there's no ccxt function to cancel all orders
        self.app.reqGlobalCancel()
        # waiting for orders to be canceled
        while self.app.order_status_dict is None:
            pass
        while self.app.order_status_dict['status'] != 'Cancelled' and self.app.order_status_dict['status'] != 'Filled':
            pass

    def fetch_order_status(self, order_id, symbol=None, params={}):  # TODO: order_id should be used
        return self.app.order_status_dict

    def fetch_balance(self, params={}):  # TODO: automatically get account name
        self.app.balance_dict = {}
        self.app.value = None
        self.app.contract = None
        balance_type_dict = {'cash_balance': AccountSummaryTags.TotalCashValue,
                             'total_balance': AccountSummaryTags.NetLiquidation,
                             'assets_market_value': AccountSummaryTags.GrossPositionValue,
                             }

        # fetch cash balance
        request_id = 9001
        self.app.reqAccountSummary(request_id, "All", balance_type_dict['cash_balance'])
        start_time = time.time()
        while self.app.value is None:  # wait for cash balance
            if time.time() - start_time > 60:
                raise Exception('Failed to get cash balance from IB')
        self.app.balance_dict[self.app.currency] = {'asset_count': self.app.value, 'market_value': self.app.value}

        # fetch market value of assets
        self.app.value = None
        request_id += 1
        self.app.reqAccountSummary(request_id, "All", balance_type_dict['assets_market_value'])
        start_time = time.time()
        while self.app.value is None:  # wait for assets market value
            if time.time() - start_time > 60:
                print('Balance dict: ', self.app.balance_dict)
                raise Exception('Failed to get assets market value from IB')

        # fetch assets balance
        if int(float(self.app.value)) != 0:
            self.app.reqAccountUpdates(True, self.account_name)
            start_time = time.time()
            while self.app.contract is None:
                if time.time() - start_time > 60:
                    print('Balance dict: ', self.app.balance_dict)
                    raise Exception('Failed to get assets balance from IB')
            time.sleep(1)  # wait for balance dict to be filled

        return self.app.balance_dict


# if __name__ == '__main__':
#     IB_obj = IBExchange(port=7497, account_name='-')  # cropped
#     # IB_obj.create_order('AAPL', 'LMT', 'BUY', 1000, price=1000)
#     # print('order_status_dict 0: ', IB_obj.fetch_order_status(1))
#     # IB_obj.create_order('AMZN', 'MKT', 'BUY', 1)
#     # # IB_obj.create_order('EURUSD', 'MKT', 'BUY', 100000)
#     # # IB_obj.create_order('EURUSD', 'LMT', 'BUY', 100000, price=10)
#     # print('order_status_dict 1: ', IB_obj.fetch_order_status(1))
#     # IB_obj.cancel_all_orders()
#     # print('order_status_dict 2: ', IB_obj.fetch_order_status(1))
#     # print('Balance dict: ', IB_obj.fetch_balance())
#     #
#     # IB_obj.create_order('MAIL', 'MKT', 'BUY', 100)
#     IB_obj.create_order('AAPL', 'MKT', 'BUY', 100)
#     print('order_status_dict: ', IB_obj.fetch_order_status(1))
#     print('Balance dict: ', IB_obj.fetch_balance())
