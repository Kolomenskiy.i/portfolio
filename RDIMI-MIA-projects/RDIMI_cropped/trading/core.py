# -*- encoding: utf-8 -*-

from datetime import datetime
from datetime import timedelta

import numpy as np
import pandas as pd

pd.core.common.is_list_like = pd.api.types.is_list_like
import bt
from bt import algos as btalgos

from cbt.core_custom import Online, StrategyOHLC
from cbt import base_algos as balgos

from abc import ABC, abstractmethod
import ccxt

import logging

from messaging.core import DefaultMessageClient

from data.utils import get_data, get_yahoo_data


class TradingClient(ABC):

    @abstractmethod
    def start(self):
        pass

    @abstractmethod
    def stop(self):
        pass

    @abstractmethod
    def _execute(self):
        pass

    @abstractmethod
    def get_balance(self):
        pass

    @abstractmethod
    def get_prices(self, symbols, quote='USD', price_type='last'):
        pass


class DefaultTradingClient(TradingClient):

    def get_balance(self):
        return {'Total': {'USD': 0.0}}


class BaseTradingClient(TradingClient):

    def __init__(self, exchange, online_params, data_params, **kwargs):

        self._logger_name = kwargs['logger_name']

        self._symbols_to_execute = dict()
        self._update_pos_tries = kwargs.get('update_pos_tries', 5)
        self._update_pos_delay = kwargs.get('update_pos_delay', 300)

        # TODO надо передавать параметры и создавать объект из параметров тут
        self._data_params = data_params
        self._online_params = online_params
        self.logger.info("TEST CORE")
        self._trading_currencies = list(online_params['signal_gen_params']['signal_params']['curr_generators'].keys())
        self.exchange = exchange
        self._msg_client = DefaultMessageClient()

        # markets = self.exchange.fetch_markets()
        # self._symbols_precision = dict()
        # for market in markets:
        #     if market['quote'] == 'USD':
        #         self._symbols_precision[market['base'].upper()] = market['precision']['amount']

        self._sched = None

    @property
    def logger(self):
        return logging.getLogger(self._logger_name)

    def init_online(self, base_params, sginal_gen_params, data_params):

        self.logger.debug("INIT TRY DEBUG")
        self.logger.info("INIT TRY INFO")

        signal_params = {'curr_generators': {}}

        def iterate_generator(gen_params):
            generators = []
            combine = None
            combine_params = {}
            for current_dict in gen_params:
                if 'combine' in current_dict.keys():
                    combine = current_dict['combine']
                    combine_params = current_dict.get('params', {})
                else:
                    if isinstance(current_dict['generator'], list):
                        generators.append(iterate_generator(current_dict['generator']))
                    else:
                        generators.append(current_dict['generator'](current_dict.get('params', {})))

            if combine is not None:
                generator = combine(generator_list=generators, params=combine_params)
            else:
                generator = generators[0]

            return generator

        for currency, gen_params in sginal_gen_params['signal_params']['curr_generators'].items():
            signal_params['curr_generators'][currency] = iterate_generator(gen_params)

        data_dict = dict.fromkeys(list(signal_params['curr_generators'].keys()))
        tmp_data_params = data_params.copy()
        tmp_data_params['start_date'] = (datetime.now() - timedelta(days=100)).strftime('%Y-%m-%d')
        self.logger.debug(f'get_data params:\n{tmp_data_params}')
        # tmp_data_params['last_date'] = datetime.utcnow().strftime('%Y-%m-%d %H-%M')
        # start_date = str((start_time - timedelta(hours=25)).replace(minute=0,second=0,microsecond=0))
        for currency in data_dict.keys():
            tmp_data_params['coin'] = currency
            for i in range(3):
                try:
                    data_dict[currency] = get_data(**tmp_data_params).iloc[-2001:]

                    # new_tmp_data_params = {}
                    # new_tmp_data_params['ticker'] = tmp_data_params['coin']
                    # new_tmp_data_params['start'] = tmp_data_params['start_date']
                    # new_tmp_data_params['timeframe'] = tmp_data_params['timeframe']
                    # data_dict[currency] = get_yahoo_data(**new_tmp_data_params).iloc[-2001:]

                    break
                except Exception as e:
                    self.logger.error(e)
                    if i == 2:
                        raise Exception("I'll try to stop it all!", e)

        timeframe_dict = {'1H': balgos.RunHourly, '1D': btalgos.RunDaily, '1W': btalgos.RunWeekly,
                          '1M': btalgos.RunMonthly, 'DT': balgos.RunDataTimeframe}
        timeframe_algo = timeframe_dict.get(data_params.get('period', 'DT'), balgos.RunDataTimeframe)(
            run_on_first_date=base_params.get('run_on_first_date', True),
            run_on_end_of_period=base_params.get('run_on_end_of_period', True),
            run_on_last_date=base_params.get('run_on_last_date', True)
        )
        algos = [
            timeframe_algo,
            balgos.SkipPeriod(sginal_gen_params['skip_period']),
            balgos.SelectAll(),
            balgos.MultiSignalAlgo(**signal_params),
            balgos.CheckForAction(),
            balgos.SelectSignalAlgo(),
            balgos.AddSelectedPosition(),
            balgos.DistinctSelectedAlgo(),
            balgos.DropSelectedSignal(),
            # balgos.WeighEqually_Sell_on_BTC(),
            balgos.WeighEqually_Sell_All_if_sell(),
            btalgos.Rebalance(),
        ]

        strategy = StrategyOHLC(base_params.get('name', 'strategy'), algos,
                                sec_amount_rounding=base_params.get('sec_amount_rounding', None))
        online_bt = Online(strategy, data_dict,
                                integer_positions=base_params.get('integer_positions', True),
                                commissions=base_params.get('commissions', None),
                                initial_capital=base_params.get('initial_capital', 100000.0),
                                progress_bar=base_params.get('progress_bar', False))

        return online_bt

    def get_amount_precision(self, base, quote='USD'):
        return self._symbols_precision.get(base.upper(), 2)

    @property
    def msg_client(self):
        return self._msg_client

    @msg_client.setter
    def msg_client(self, client):
        self._msg_client = client

    def get_prices(self, symbols, quote='USD', price_type='last'):
        quote = 'USDT' if str(self.exchange) == 'Binance' and quote =='USD' else quote
        prices = dict()
        if len(symbols) > 0:
            symbols = [s for s in symbols if s != quote]
            fetch_ticker = getattr(self.exchange, 'fetch_ticker')
            for symbol in symbols:
                try:
                    prices[symbol] = fetch_ticker(symbol + '/' + quote)[price_type]
                except Exception as ex:
                    self.logger.exception(ex)

        return prices

    def get_balance(self):
        self.logger.info('Loading status')
        balances = dict()
        try:
            fb = self.exchange.fetch_free_balance()
            ub = self.exchange.fetch_used_balance()
            tb = self.exchange.fetch_total_balance()
            balances = {
                'free': fb,
                'used': ub,
                'total': tb
            }
            currencies = self.exchange.currencies
            msg = ''
            if currencies is not None and len(currencies) > 0:
                for currency in currencies:
                    msg += currency + ':\n'
                    msg += '\tFree:  {}\n\tUsed:  {}\n\tTotal: {}\n'.format(
                        fb.get(currency, 0.0),
                        ub.get(currency, 0.0),
                        tb.get(currency, 0.0))
                self.logger.info('Status loaded:\n{}'.format(msg))
        except Exception as e:
            self.logger.exception(e)
            self.msg_client.send_message('Unable to load account status. Check logs.')
        return balances

    def get_acc_balance(self, quote='USD'):
        quote = 'USDT' if str(self.exchange) == 'Binance' and quote =='USD' else quote
        quote = quote.upper()
        try:
            amounts = self.exchange.fetch_total_balance()
            amounts = {k: v for k, v in amounts.items() if v != 0}
            #TODO very very bad! create func for ticker fetch
            prices = self.get_prices([coin for coin in amounts.keys() if coin != quote], quote)
            balance = amounts.pop(quote, 0.0)
            for symbol, amount in amounts.items():
                balance += prices[symbol] * amount
            return balance
        except Exception as e:
            self.logger.exception(e)

        return 0.0

    def get_fee(self, fee_type='taker'):
        self.logger.debug('Call fee')
        fee = 0.0025
        try:
            if fee_type not in ['taker', 'maker']:
                self.logger.error('Wrong fee type')
                self.msg_client.send_message('Wrong fee type. Changed to "taker".')
                fee_type = 'taker'
            fee = self.exchange.fees['trading'][fee_type]
            self.logger.debug('{} fee is loaded: {}'.format(fee_type.capitalize(), fee))
        except Exception as e:
            self.logger.exception(e)
            self.msg_client.send_message('Unable to load {} fee. Check logs.'.format(fee_type))
        return float(1 - fee)

    def liquidate(self, quote='USD'):
        try:
            balances = self.get_balance()['total']
            for coin, balance in balances.items():
                try:
                    order = self.sell_market(balance, coin, quote, False)
                    self.msg_client.send_message('ORDER\n{}'.format(order))
                except Exception as e:
                    self.msg_client.send_message('Unable to liquidate {}'.format(coin))
        except Exception as e:
            self.logger.exception(e)
            self.msg_client.send_message('Unable to liquidate position. Check logs.\n{}'.format(e))

        try:
            balances = self.get_balance()['total']
            balances.pop(quote, None)
            coins = {coin: value for coin, value in balances.items() if value != 0}
            self.msg_client.send_message('Unable to liauidate position:\n{}'.format(coins))
        except Exception as e:
            self.logger.exception(e)
            self.msg_client.send_message('Unable to liquidate position. Check logs.\n{}'.format(e))

    def market_order(self, amount, currency, quote, side, raise_exception=False):
        if amount == 0:
            self.logger.error('Unable to execute {} {}/{}'.format(amount, currency, quote))
            self.msg_client.send_message('Trying to execute 0 amount order. Check logs')
            return

        op = side.capitalize() + 'ing'
        symbol = '/'.join([currency, quote]).upper()

        try:
            amount = np.abs(np.round(amount * self.get_fee(), 8))
            self.logger.info('{} {} {:.4f}'.format(op, currency, amount))
            response = self.exchange.create_order(symbol, 'market', side, amount, None)
            return response
        except Exception as error:
            self.logger.exception('{} order error:\n{}'.format(op, error))
            if raise_exception:
                raise error
            self.msg_client.send_message('Unable to execute order. Check logs.\n{}'.format(error))
            return None

    def buy_market(self, amount, currency, quote, raise_exception=False):
        return self.market_order(amount, currency, 'buy', quote, raise_exception)

    def sell_market(self, amount, currency, quote, raise_exception=False):
        return self.market_order(amount, currency, 'sell', quote, raise_exception)
