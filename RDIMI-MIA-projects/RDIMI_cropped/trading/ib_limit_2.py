""" A Simple Order Routing Mechanism """
from ib.ext.Contract import Contract
from ib.ext.Order import Order
from ib.opt import Connection
# from ib.opt.message import NextValidId
from ib.ext.EWrapperMsgGenerator import EWrapperMsgGenerator


class IB:
    def __init__(self, tws_order_id, tws_port=7497, tws_client_id=100):
        self._tws_conn = Connection.create(port=tws_port, clientId=tws_client_id)
        self._tws_order_id = tws_order_id

    def error_handler(self, msg):
        print("Server Error:", msg)

    def server_handler(self, msg):
        print("Server Msg:", msg.typeName, "-", msg)
        print("type:", type(msg))
        print("str:", str(msg))
        if str(type(msg)) == "<class 'ib.opt.message.NextValidId'>":
            orderid = ''
            oid_flag = False
            for i in str(msg):
                if i == '=':
                    oid_flag = True
                    continue
                elif i == '>':
                    oid_flag = False
                    continue
                if oid_flag:
                    orderid += i
            print("orderID:", int(orderid))
            self._tws_order_id = int(orderid)

    def create_contract(self, symbol, sec_type, exch, prim_exch, curr):
        contract = Contract()
        contract.m_symbol = symbol
        contract.m_secType = sec_type
        contract.m_exchange = exch
        contract.m_primaryExch = prim_exch
        contract.m_currency = curr
        return contract

    def create_order(self, order_type, quantity, action):
        order = Order()
        order.m_orderType = order_type
        order.m_totalQuantity = quantity
        order.m_action = action
        return order

    def main(self):
        self._tws_conn.connect()
        self._tws_conn.register(self.error_handler, 'Error')
        self._tws_conn.registerAll(self.server_handler)

        aapl_contract = self.create_contract('AAPL', 'STK', 'SMART', 'SMART', 'USD')
        aapl_order = self.create_order('MKT', 100, 'SELL')

        self._tws_conn.placeOrder(self._tws_order_id, aapl_contract, aapl_order)
        # self._tws_conn.placeOrder(self._tws_order_id, aapl_contract, aapl_order)


ib_object = IB(132)
ib_object.main()


# def error_handler(msg):
#     print("Server Error:", msg)
#
#
# def server_handler(msg):
#     print("Server Msg:", msg.typeName, "-", msg)
#     # print("type:", type(msg))
#     # print("str:", str(msg))
#     if str(type(msg)) == "<class 'ib.opt.message.NextValidId'>":
#         orderid = ''
#         oid_flag = False
#         for i in str(msg):
#             if i == '=':
#                 oid_flag = True
#                 continue
#             elif i == '>':
#                 oid_flag = False
#                 continue
#             if oid_flag:
#                 orderid += i
#         print("orderID:", int(orderid))
#
#
# def create_contract(symbol, sec_type, exch, prim_exch, curr):
#     contract = Contract()
#     contract.m_symbol = symbol
#     contract.m_secType = sec_type
#     contract.m_exchange = exch
#     contract.m_primaryExch = prim_exch
#     contract.m_currency = curr
#     return contract
#
#
# def create_order(order_type, quantity, action):
#     order = Order()
#     order.m_orderType = order_type
#     order.m_totalQuantity = quantity
#     order.m_action = action
#     return order
#
#
# if __name__ == "__main__":
#     client_id = 100
#     order_id = 129
#     port = 7497
#     tws_conn = None
#     try:
#         # Establish connection to TWS.
#         tws_conn = Connection.create(port=port, clientId=client_id)
#         tws_conn.connect()
#
#         # Assign error handling function.
#         tws_conn.register(error_handler, 'Error')
#
#         # Assign server messages handling function.
#         tws_conn.registerAll(server_handler)
#
#         # Create AAPL contract and send order
#         aapl_contract = create_contract('AAPL', 'STK', 'SMART', 'SMART', 'USD')
#
#         # Go long 100 shares of AAPL
#         aapl_order = create_order('MKT', 100, 'SELL')
#
#         # Place order on IB TWS.
#         tws_conn.placeOrder(order_id, aapl_contract, aapl_order)
#     finally:
#         # Disconnect from TWS
#         if tws_conn is not None:
#             tws_conn.disconnect()
