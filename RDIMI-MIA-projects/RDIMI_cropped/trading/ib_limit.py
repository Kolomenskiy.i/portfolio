# -*- encoding: utf-8 -*-

import sys
from multiprocessing import Process
import os
from os.path import pardir, join, abspath
root_dir = abspath(join('./', pardir))
sys.path.append(root_dir)
for fldr in ['api', 'cbt', 'data', 'indicator', 'stats', 'strategy', 'optimization', 'trading', 'messaging']:
    sys.path.append(join(root_dir, fldr))
print(sys.path)

import logging
from logging.handlers import TimedRotatingFileHandler
import time
from datetime import datetime
from datetime import timedelta
from functools import partial
from os.path import join, abspath, pardir, exists
from os import makedirs

import numpy as np
import pandas as pd

from data.utils import generator_list_reader

import copy

pd.core.common.is_list_like = pd.api.types.is_list_like

import ccxt


from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.schedulers.background import BackgroundScheduler
import threading
from timeloop.job import Job
import platform

from trading.core import BaseTradingClient

from messaging.telegram import TelegramMessageClient

import bt

from strategy import signal_generators as signals
from data.utils import get_data
from cbt.core_custom import fee_func

import argparse
import yaml

from trading.ib_api_limit import IBExchange


class LimitTradingCllient(BaseTradingClient):

    def start(self):
        self.logger.debug(f"in start func sched is {self._sched}")
        # try:
        if self._sched is None:
            self._sched_job = None
            self.logger.info('PREPARE strategy')
            if platform.system() == 'Windows':
                # job = Job(timedelta(minutes=1), self._execute)
                # self.logger.info('Trading bot is launched')
                # self._msg_client.send_message("Trading client is launched")
                # print('Trading bot is launched')
                # nowdt = datetime.now()
                # nxtdt = nowdt.replace(second=0, microsecond=0) + timedelta(minutes=1)
                # print('sleep ', nowdt, nxtdt, (nxtdt - nowdt).total_seconds())
                # time.sleep((nxtdt - nowdt).total_seconds())
                # job.run()
                self._sched = BackgroundScheduler()
                self._msg_client.send_message("Trading client is launched")
                self._execute()
            else:
                self._sched = BackgroundScheduler()
                # self._sched_job = self._sched.add_job(self._execute, 'cron', hour='*/2')
                # self._sched_job = self._sched.add_job(self._execute, 'cron', minute=0)
                # self.logger.info('START JOB {} {}'.format(self._sched_job.id, self._sched_job))
                # self.logger.info('Trading bot is launched')
                # self._sched.start()
                self._msg_client.send_message("Trading client is launched")
                self._execute()
            self.msg_client.send_message("Let's blow this market up, bro!")
            self.logger.info('START TRADING')
        else:
            self.msg_client.send_message("Stahp it u! I'm trying to earn some money!")
        # except Exception as e:
        #     self.logger.error(e)
        #     self.msg_client.send_message(f'Unable to execute start\n{e}')

    def stop(self):
        self.logger.debug(f"in stop func sched is {self._sched}")
        if self._sched is not None:
            self.logger.info('STOP JOB {} {}'.format(self._sched_job.id, self._sched_job))
            self._sched.remove_job(self._sched_job.id)
            self._sched = None
            self._sched_job = None
            self.logger.info('STOP TRADING')
            self.msg_client.send_message("Fuck this shit! I'm tired")
        else:
            self.msg_client.send_message("I'm chilling! What's wrong with you?")

    def _execute(self):
        try:
            self._online = self.init_online(self._online_params['base_params'],
                                            self._online_params['signal_gen_params'], self._data_params)
            self.logger.info('INIT strategy')
            bt.run(self._online)
            positions = self._online.positions
            tmp_pos = self._online.positions.iloc[-5:]
            self.logger.debug(f'positions before:\n{tmp_pos}')

            prev_idx = (datetime.utcnow().replace(microsecond=0, second=0, minute=0)-timedelta(hours=4)).strftime('%Y-%m-%d %H:%M:%S')
            last_idx = (datetime.utcnow().replace(microsecond=0, second=0, minute=0)-timedelta(hours=5)).strftime('%Y-%m-%d %H:%M:%S')

            # prev_positions = positions.loc[prev_idx].fillna(0.0)
            # last_positions = positions.loc[last_idx].fillna(0.0)
            prev_positions = positions.iloc[-1].fillna(0.0)
            last_positions = positions.iloc[-2].fillna(0.0)
            diff_position = last_positions - prev_positions
            # diff_position['YNDX'] = 100
            # TODO Delete after testing
            diff_position = diff_position[diff_position != 0]
            self.logger.debug('diff_position:\n{}'.format(diff_position))
            if len(diff_position) > 0:
                total_amount = self._ib_get_balance()
                prices = pd.Series(self._online.data.iloc[-1])
                prices['USD'] = 1.0
                self.logger.debug('prices:\n{}'.format(prices))
                amount_balance = (pd.Series(total_amount) / prices).fillna(0)
                self.logger.debug('amount_balance:\n{}'.format(amount_balance))

                sell_coins = diff_position[diff_position < 0].index
                drop_coins = amount_balance[amount_balance == 0]
                drop_coins = drop_coins[sell_coins].dropna()
                self.logger.debug(f'sell coins:\n{sell_coins}\nzero balance:\n{amount_balance[amount_balance == 0]}\ndrop coins:\n{drop_coins}')

                weights = self._online.security_weights.iloc[-1]
                # weights['YNDX'] = 100
                # TODO Delete after testing
                self.logger.debug('weights: {}'.format(weights))
                weights = weights.drop(drop_coins.index)
                weights = weights.fillna(0.0)
                self.logger.debug('weights:\n{}\nsum:\n{}\nnew weights:\n{}'.format(weights, weights.sum(), (weights / weights.sum()).round(4)))
                weights = (weights / weights.sum()).round(4)

                # fee = self.get_fee()
                fee = 0.995
                new_positions = (amount_balance.sum() * fee * .99 * weights * np.sign(last_positions))
                # new_positions['YNDX'] = 100
                self.logger.debug('new_positions:\n{}'.format(new_positions))
                new_positions = new_positions.fillna(0.0)

                dust_positions = new_positions[np.abs(new_positions) <= 5.0]
                dust_positions = dust_positions[dust_positions != 0]
                self.logger.debug('dust_positions:\n{}'.format(dust_positions))
                new_positions[dust_positions.index] = amount_balance[dust_positions.index]
                self.logger.debug('new new_positions:\n{}'.format(new_positions))

                diff_amount = new_positions - amount_balance
                diff_amount = diff_amount.dropna()
                diff_amount = diff_amount[np.abs(diff_amount) > 5.0]
                self.logger.debug('diff_amount:\n{}'.format(diff_amount))

                if len(diff_amount) == 0:
                    return

                # asks = pd.Series(self.get_prices(diff_amount[diff_amount < 0].index, price_type='ask'))
                # bids = pd.Series(self.get_prices(diff_amount[diff_amount > 0].index, price_type='bid'))
                # prices = pd.concat([asks, bids])
                self.logger.debug('prices:\n{}'.format(prices))

                # diff_amount = diff_amount / prices
                diff_amount = diff_amount.sort_values()

                sell_dict = {}
                buy_dict = {}

                sell_list = {}
                buy_list = {}

                for symbol, amount in diff_amount.items():
                    diff_amount[symbol] = int(amount)
                    self.logger.debug('symb\n{}'.format(symbol))
                    self.logger.debug('amount:\n{}'.format(amount))

                    if amount < 0:
                        sell_list[symbol] = amount
                    else:
                        buy_list[symbol] = amount

                self.logger.debug('Sell list:\n{}'.format(sell_list))
                self.logger.debug('Buy list:\n{}'.format(buy_list))


                # TODO Cancell all orders
                # try:
                #     b = self.cancell_all_orders()
                #     self.logger.debug('Cancell all orders return:\n{}'.format(b))
                # except Exception as e:
                #     self.logger.debug('Failed to cancel orders:\n{}'.format(e))

                self.msg_client.send_message("I'll try to make some IB orders:\n{}".format(diff_amount))
                # process_list = []
                for symbol, amount in sell_list.items():
                    sell_dict['currency'] = symbol
                    sell_dict['amount'] = amount
                    sell_dict['side'] = 'Sell'
                    self.logger.debug('Sell dict:\n{}'.format(sell_dict))
                    # process_list.append(Process(target=self._exchange.create_order, args=(symbol,'MKT','SELL',amount)))
                    self.exchange.create_order(symbol, 'MKT', 'SELL', amount)
                # for i in process_list:
                #     i.start()
                #     time.sleep(10)
                # for i in process_list:
                #     i.join()
                # process_list = []
                for symbol, amount in buy_list.items():
                    buy_dict['currency'] = symbol
                    buy_dict['amount'] = amount
                    buy_dict['side'] = 'Buy'
                    self.logger.debug('Buy dict:\n{}'.format(buy_dict))
                    # process_list.append(
                    # Process(target=self._exchange.create_order, args=(symbol, 'MKT', 'BUY', amount)))
                    self.exchange.create_order(symbol, 'MKT', 'BUY', amount)
                    # IBExchange(port=7497, account_name='-').create_order(symbol, 'MKT', 'BUY', amount)  # cropped

                # for i in process_list:
                #     i.start()
                #     time.sleep(10)

        except Exception as e:
            self._msg_client.send_message('Unable to update position. Check log file.\n{}'.format(e))

    def _ib_get_balance(self):
        self.logger.info('Loading status')
        for _ in range(5):
            try:
                current_positions = self.exchange.fetch_balance()
                balances = pd.Series()
                for i in list(current_positions.keys()):
                    balances[i] = float(current_positions[i]['market_value'])
                return balances
            except:
                time.sleep(5)
        raise Exception('Unable to get account status')


    def _create_orders(self, amounts):
        symbols_to_execute = amounts.copy().to_dict()
        errors = []
        try:
            for symbol, amount in symbols_to_execute.items():
                try:
                    self.market_order(amount, symbol, 'USD', 'buy' if amount > 0 else 'sell', raise_exception=True)
                    amounts.pop(symbol)
                except ccxt.errors.InsufficientFunds as ife:
                    logging.exception(ife)
                    errors.append(ife)
                    try:
                        free_balance = self.get_balance()['free'] * self.get_fee() * .99
                        price = self.exchange.fetch_tickers(symbol.upper() + '/USD')['last']
                        max_amount = free_balance / price
                        amounts[symbol] = np.round(np.min([amounts[symbol], max_amount]), 4)
                    except Exception as e:
                        logging.exception(e)
                        errors.append(e)
                        amounts[symbol] = np.round(amounts[symbol] * .98, self.get_amount_precision(symbol))
                except ccxt.errors.InvalidOrder as ioe:
                    logging.exception(ioe)
                    errors.append(ioe)
                    amounts.pop(symbol)
                except Exception as e:
                    logging.exception(e)
                    errors.append(e)
        except Exception as e:
            logging.exception(e)
            errors.append(e)

        return errors

    def iceberg_order(self, kwargs):
        amount = abs(float(kwargs.get('amount')))
        currency = kwargs.get('currency')
        max_amount = kwargs.get('max_amount', 50000)
        green_zone = kwargs.get('green_zone')
        orange_zone = kwargs.get('orange_zone')
        side = kwargs.get('side')
        time_for_order = kwargs.get('time for order', 60)
        maximum_tries = kwargs.get('maximum tries', 5)
        base = kwargs.get('base', 'USD')
        precision = kwargs.get('precision', 8)
        recursion = kwargs.get('recursion', 0)
        self.logger.debug('In iceberg order')
        for t in range(3):
            try:
                book = self.exchange.fetch_order_book(currency.upper() + "/" + base)
                break
            except Exception as e:
                if t<3:
                    time.sleep(5)
                    continue
                else:
                    self._msg_client.send_message('Unable to fetch order book. Iceberg order has failed\n{}'.format(e))
                    return

        x = 0
        orders = []
        overall_amount = 0

        if side == 'Buy':
            book = book['asks']
            price = book[0][0]

        elif side == 'Sell':
            book = book['bids']
            price = book[0][0]

        else:
            raise Exception('Side can be either buy or sell')

        if amount*price<=25:
            return
        start_price = price
        while overall_amount < amount:
            print('Book:',book[0][0])
            print('Green zone:', green_zone)
            if (price < book[0][0] * green_zone and side == 'Buy') or (
                    price > book[0][0] * green_zone and side == 'Sell'):
                price = book[x][0]
                overall_amount += book[x][1]
                x += 1
            else:
                break

        overall_amount = min(overall_amount, amount)
        self.logger.debug('Currency:\n{}'.format(currency))
        self.logger.debug('Market order amount:\n{}'.format(overall_amount))
        self.logger.debug('Start price:\n{}'.format(start_price))
        self.logger.debug('Market order price:\n{}'.format(price))

        if overall_amount > 0:
            orders.append(self.order_exexuter_tryer(overall_amount, currency, 'limit', side, price, None, precision,base)['id'])
            self.logger.debug('Order list:\n{}'.format(orders))

            time.sleep(time_for_order)
        if None in orders:
            remaining = overall_amount
        else:
            remaining = self.order_canceler(orders, maximum_tries)
        max_amount /= start_price
        self.logger.debug('Remaining amount:\n{}'.format(orders))
        if overall_amount - remaining > 0.998 * amount:
            return
        amount -= overall_amount + remaining
        amount = max(amount, 0)
        order_amount = 1
        order_value = amount
        while order_value >= max_amount:
            order_value = amount / order_amount
            order_amount += 1
        overall_remaining = 0
        for order_number in range(order_amount):
            orders = []
            remaining = order_value
            for _ in range(maximum_tries):
                book = self.exchange.fetch_order_book(currency.upper() + base)
                price = (book['asks'][0][0] + book['bids'][0][0]) / 2
                if (price < start_price * orange_zone and side == 'Buy') or (
                        price > start_price * orange_zone and side == 'Sell'):
                    orders.append(self.order_exexuter_tryer(remaining, currency, 'limit', side, price, precision,base)['id'])
                    time.sleep(time_for_order)
                    remaining = self.order_canceler(orders, maximum_tries)
                    self.logger.debug('Remaining:\n{}'.format(remaining))
                if remaining is None:
                    remaining = order_value
                    continue
                if remaining < order_value / 10:
                    break
            overall_remaining += remaining

        self.logger.debug('Overall remaining:\n{}'.format(overall_remaining))
        if abs(overall_remaining) > abs(float(kwargs.get('amount'))) / 20:
            if recursion <= 5:
                kwargs['amount'] = abs(overall_remaining)
                kwargs['recursion'] = recursion + 1
                self.iceberg_order(kwargs)

            else:
                self.order_exexuter_tryer(overall_remaining, currency, 'limit', side, start_price)

    def order_canceler(self, id_list, maximum_tries):
        remaining = 0
        for id in id_list:
            if id is None:
                continue
            id = int(id)
            order = self.exchange.fetch_order(id)
            if order['remaining'] is None:
                continue
            if order['remaining'] != 0:
                for _ in range(maximum_tries):
                    try:
                        if self.exchange.fetch_order_status(id) == 'canceled':
                            break
                        self.exchange.cancel_order(id)
                    except Exception as e:
                        if 'Order not found' in e.args[0]:
                            break

                remaining += order['remaining']
        return remaining

    def order_exexuter_tryer(self, amount, currency, type, side, price=None, recursion=None, precision=8,base='USD'):
        if type == 'Market' and price is not None:
            raise Exception('Market orders do not need a predifined price')
        amount = np.round(amount, precision)
        self.logger.debug('Tryer amount:\n{}'.format(amount))
        try:
            order = self.exchange.create_order(symbol=currency + '/'+base, type=type, side=side, amount=amount,
                                                price=price)
            return order

        except Exception as e:
            self.logger.debug('Exception on tryer:\n{}'.format(e))
            if "Ensure this value" in e.args[0]:
                return {'id': None, 'Error': e}

            if 'Invalid nonce' in e.args[0]:
                return {'id': None, 'Error': e}

            if 'Minimum order size' in e.args[0]:
                return {'id': None, 'Error': e}

            if 'You have only' in e.args[0]:
                start_ind = e.args[0].index('only')
                end_ind = e.args[0].index('available')
                try:
                    ind1 = e.args[0].index('need')
                    ind2 = e.args[0].index('to open')
                    amount = float(amount) * 0.9975 * float((e.args[0][start_ind + 4:end_ind - 5])) / float(
                        (e.args[0][ind1 + 4:ind2 - 5]))
                    self.logger.debug('New amount:\n{}'.format(amount))
                    print('Ama', amount)
                except:
                    amount = float((e.args[0][start_ind + 4:end_ind - 5])) * 0.9975
                    self.logger.debug('New amount2:\n{}'.format(amount))
                    print('Ama2', amount)
                if amount != 0 and recursion is None:
                    try:
                        return self.order_exexuter_tryer(np.round(amount, precision), currency, type, side, price, 1)
                    except:
                        return {'id': None, 'Error': e}

            return {'id': None, 'Error': e}

    def cancell_all_orders(self):
        orders = self.exchange.fetch_open_orders()
        errors = []
        for i in range(len(orders)):
            try:
                self.exchange.cancel_order(orders[i]['id'])
            except Exception as e:
                logging.exception(e)
                errors.append(e)
        return errors


if __name__ == '__main__':
    # parser = argparse.ArgumentParser()
    # parser.add_argument('-c', help='select bitstamp account', type=str, default='test_account_500.yaml')
    # parser.add_argument('-b', help='select telegram bot', type=str, default='test_bot.yaml')
    # args = parser.parse_args()
    # with open(args.c) as f:
    #     account_info = yaml.safe_load(f)
    # with open(args.b) as f:
    #     bot_info = yaml.safe_load(f)

    BITSTAMP_USERNAME = '-'  # cropped
    BITSTAMP_KEY = '-'  # cropped
    BITSTAMP_SECRET = '-'  # cropped

    # BITSTAMP_USERNAME = account_info['BITSTAMP_USERNAME']
    # BITSTAMP_KEY = account_info['BITSTAMP_KEY']
    # BITSTAMP_SECRET = account_info['BITSTAMP_SECRET']
    # LOG_NAME = account_info['LOG_NAME']
    # TG_ID = bot_info['TG_ID']
    # TG_HASH = bot_info['TG_HASH']
    # USER_DICT = {'Admins': bot_info['ADMINS'], 'Clients': bot_info['CLIENTS']}
    USER_DICT = {'Admins': [0], 'Clients': [0]}  # cropped

    TG_ID = 0  # cropped
    TG_HASH = '-'  # cropped
    TG_DCHAT = '-'  # cropped

    LOG_NAME = 'Test'

    if not exists('./logs'):
        makedirs(abspath('./logs'))

    log_file = f'{LOG_NAME}_trade_log.log'
    log_path = abspath(f'./logs/{log_file}')
    logging.basicConfig(
        level=logging.DEBUG,
        datefmt='%Y-%m-%d %H:%M:%S',
        format=u'%(levelname)s - %(asctime)s - ' + BITSTAMP_USERNAME + ':\n%(message)s\n',
        handlers=[
            TimedRotatingFileHandler(abspath(f'./logs/{LOG_NAME}_debug_log.log'), encoding='utf-8',
                                     when="d",
                                     interval=1,
                                     backupCount=50)
        ]
    )
    logger = logging.getLogger(f'TradeLogger_{LOG_NAME}')
    handler = TimedRotatingFileHandler(log_path,
                                       when="d",
                                       interval=1,
                                       backupCount=50)
    formatter = logging.Formatter(fmt=u'%(levelname)s - %(asctime)s - ' + BITSTAMP_USERNAME + ':\n%(message)s\n',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)


    logging._defaultFormatter = logging.Formatter(u"%(message)s")

    msg_client = TelegramMessageClient('anon', TG_ID, TG_HASH, TG_DCHAT,USER_DICT, log_file=log_path, teleg=False)

    params = {
        'apiKey': BITSTAMP_KEY,
        'secret': BITSTAMP_SECRET,
        'uid': BITSTAMP_USERNAME,
        'nonce': lambda: ccxt.Exchange.microseconds()
    }

    # params = {
    #     'apiKey': BINANCE_KEY,
    #     'secret': BINANCE_SECRET,
    #     # 'nonce': lambda: ccxt.Exchange.microseconds()
    # }

    # bitstamp = ccxt.bitstamp(params)
    # bitstamp = ccxt.bittrex()
    ib = IBExchange(account_name='-', port=7497)  # cropped

    timeframe = '1h'
    data_params = {
        'timeframe': timeframe,
        'fpath': abspath(join(abspath('./'), pardir, 'data')),
        'market': 'USD',
        'exchange': 'Finam'
    }

    max_period = 500
    params = {
        'skip_period': max_period,
        'timeframe': '1H',
        'signal_params': {
            'curr_generators': {
                # ('AAPL', 'FB', 'AMZN', 'NVDA', 'F', 'ADBE'): [{'generator': signals.Test_strategy}],
                # ('YNDX',): [{'generator': signals.Test_strategy}],  # 'MAIL', 'AFLT',
                # ('AAPL', ): [{'generator': signals.Test_strategy}],
                # ('BNTX', 'HTBX', 'ALT', 'CYDY', 'INO', 'NVAX', 'ARCT', '6185.HK'):
                #     [{'generator': signals.Test_strategy}],
                # ('MSFT', 'GOOGL', 'GOOG', 'FB', 'ADBE',
                #  'BRK-B', 'BRK-A', 'V', 'JPM', 'MA',
                #  'JNJ', 'NVS', 'PFE', 'MRK', 'ABBV',
                #  'AAPL', 'TSM', 'INTC', 'NVDA', 'AVGO',
                #  'AMZN', 'BABA', 'WMT', 'HD', 'COST'):
                #     [{'generator': signals.Test_strategy}],
                # ('601398.SS', '601939.SS', '601318.SS', '601288.SS', '601628.SS',
                #  '002475.SZ',  '601138.SS', '603501.SS', '000063.SZ', '600745.SS',
                #  '300750.SZ', '601766.SS', '600031.SS', '000338.SZ',  '600346.SS',
                #  '600276.SS', '300760.SZ', '603259.SS', '000661.SZ', '300122.SZ',
                #  '600519.SS', '000858.SZ', '603288.SS', '002304.SZ', '000568.SZ'):
                #     [{'generator': signals.Test_strategy}],
                # 'GAZP': [{'generator': signals.Test_strategy}],
                # 'XRP': [{'generator': signals.Test_strategy}],
                # 'BCH': [{'generator': signals.Test_strategy}],
                # 'ETH': [{'generator': signals.Test_strategy}],
                # 'ETH': [{'combine': signals.Trend_Generator_Switch_Func, 'params': {'qwerty':2000, 'take_profit': 20,'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.BBSignalGenerator, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.EMA_and_Price_cross, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.EMA_and_Price_cross, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         ],
                # 'BCH': [{'combine': signals.Trend_Generator_Switch_Func, 'params': {'qwerty':2000, 'take_profit': 20,'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.BBSignalGenerator, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.EMA_and_Price_cross, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.EMA_and_Price_cross, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         ],
                # 'XRP': [{'combine': signals.Trend_Generator_Switch_Func, 'params': {'qwerty':2000, 'take_profit': 20,'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.BBSignalGenerator, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.EMA_and_Price_cross, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.EMA_and_Price_cross, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         ],
                # 'LTC': [{'combine': signals.Trend_Generator_Switch_Func, 'params': {'qwerty':2000, 'take_profit': 20,'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.BBSignalGenerator, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.EMA_and_Price_cross, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         {'generator': signals.EMA_and_Price_cross, 'params': {'buy_n': 0, 'sell_n': 0}},
                #         ],
                # ('ETHUSD', 'XRPUSD', 'BCHUSD', 'LTCUSD', ):  # 'XLM'
                # [{'generator': signals.Test_strategy}],
                # ('BTCUSD', ):
                #     [{'generator': signals.Test_strategy}],
                ('YNDX',):
                    [{'generator': signals.Test_strategy}],
            },
        }
    }
    if isinstance(list(params['signal_params']['curr_generators'].keys())[0], tuple):
        params['signal_params']['curr_generators'] = generator_list_reader(params['signal_params']['curr_generators'])


    base_params = {
        'name': 'online_strategy',
        'integer_positions': False,
        'commissions': partial(fee_func, ff=0, pf=0),
        'sec_amount_rounding': {
            'BTC': 2,
            'ETH': 2,
            'BCH': 2,
            'LTC': 2,
            'XRP': 5
        }
    }
    online_params = {
        'base_params': base_params,
        'signal_gen_params': params
    }
    trading_client = LimitTradingCllient(ib, online_params, data_params, logger_name=logger.name)
    trading_client.msg_client = msg_client
    msg_client.trading_client = trading_client
    trading_client.start()
