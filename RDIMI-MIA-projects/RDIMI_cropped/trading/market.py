# -*- encoding: utf-8 -*-

import sys
import os
from os.path import pardir, join, abspath
root_dir = abspath(join('./', pardir))
sys.path.append(root_dir)
for fldr in ['api', 'cbt', 'data', 'indicator', 'stats', 'strategy', 'optimization', 'trading', 'messaging']:
    sys.path.append(join(root_dir, fldr))
print(sys.path)

import logging
from logging.handlers import TimedRotatingFileHandler
import time
from datetime import datetime
from datetime import timedelta
from functools import partial
from os.path import join, abspath, pardir, exists
from os import makedirs

import numpy as np
import pandas as pd

pd.core.common.is_list_like = pd.api.types.is_list_like

import ccxt


from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.schedulers.background import BackgroundScheduler
import threading
from timeloop.job import Job
import platform

from trading.core import BaseTradingClient
from trading.params import TradingParams

from messaging.telegram import TelegramMessageClient

import bt

from strategy import signal_generators as signals
from data.utils import get_data
from cbt.core_custom import fee_func

import argparse
import yaml

class MarketTradingCllient(BaseTradingClient):

    def start(self):
        self.logger.debug(f"in start func sched is {self._sched}")
        try:
            if self._sched is None:
                self._sched_job = None
                self._online = self.init_online(self._online_params['base_params'], self._online_params['signal_gen_params'], self._data_params)
                self.msg_client.send_message('Strategy was initialized')
                self.logger.info('INIT strategy')
                bt.run(self._online)
                self.msg_client.send_message('Strategy was prepared')
                self.logger.info('PREPARE strategy')
                if platform.system() == 'Windows':
                    job = Job(timedelta(minutes=1), self._execute)
                    self.logger.info('Trading bot is launched')
                    self._msg_client.send_message("Trading client is launched")
                    print('Trading bot is launched')
                    nowdt = datetime.now()
                    nxtdt = nowdt.replace(second=0, microsecond=0) + timedelta(minutes=1)
                    print('sleep ', nowdt, nxtdt, (nxtdt - nowdt).total_seconds())
                    time.sleep((nxtdt - nowdt).total_seconds())
                    job.run()
                else:
                    self._sched = BackgroundScheduler()
                    self._sched_job = self._sched.add_job(self._execute, 'cron', minute=0)
                    self.logger.info('START JOB {} {}'.format(self._sched_job.id, self._sched_job))
                    self.logger.info('Trading bot is launched')
                    self._sched.start()
                    self._msg_client.send_message("Trading client is launched")
                    print('Trading bot is launched')
                self.msg_client.send_message("Let's blow this market up, bro!")
                self.logger.info('START TRADING')
            else:
                self.msg_client.send_message("Stahp it u! I'm trying to earn some money!")
        except Exception as e:
            self.logger.error(e)
            self.msg_client.send_message(f'Unable to execute start\n{e}')

    def stop(self):
        self.logger.debug(f"in stop func sched is {self._sched}")
        if self._sched is not None:
            self.logger.info('STOP JOB {} {}'.format(self._sched_job.id, self._sched_job))
            self._sched.remove_job(self._sched_job.id)
            self._sched = None
            self._sched_job = None
            self.logger.info('STOP TRADING')
            self.msg_client.send_message("Fuck this shit! I'm tired")
        else:
            self.msg_client.send_message("I'm chilling! What's wrong with you?")

    def _execute(self):
        try:
            self.logger.info("I'm trying to update + " + str(datetime.now()))
            tmp_data_params = self._data_params.copy()
            start_dt = self._online.positions.index[-10]
            tmp_data_params['start_date'] = start_dt.strftime('%Y-%m-%d %H:%M')
            data = dict()
            first_add_dt = self._online.positions.index[-1] + timedelta(hours=1)
            target_dt = pd.Timestamp((datetime.utcnow() - timedelta(hours=1)).replace(microsecond=0, second=0, minute=0))
            logger.debug(f"get_data dict:\n{tmp_data_params}\nfirst_add_dt: {first_add_dt}\ntarget_dt: {target_dt}")
            for coin in self._trading_currencies:
                for _ in range(3):
                    try:
                        tmp_data_params['coin'] = coin
                        rtime_utc = datetime.utcnow()
                        cdata = get_data(**tmp_data_params)
                        self.logger.debug('data:\n{}'.format(cdata))
                        if cdata is not None:
                            self.logger.debug(f'first_dt: {first_add_dt}\n'\
                                              f'target_dt: {target_dt}\n'\
                                              f'request utc: {rtime_utc}')
                            cdata = cdata.loc[cdata.index >= first_add_dt]
                            cdata = cdata.loc[cdata.index <= target_dt]
                            self.logger.debug('data:{}'.format(cdata))
                            data[coin] = cdata
                            break
                    except Exception as e:
                        self.logger.exception(e)
                        self._msg_client.send_message('Cryptocompare error.\n{}'.format(e))
                    data[coin] = None
                    time.sleep(60)
                if data[coin] is None:
                    raise Exception('No data to process')
            if len(data) == 0:
                raise Exception('Final no data to process')

            tmp_pos = self._online.positions.iloc[-5:]
            self.logger.debug(f'positions after:\n{tmp_pos}')

            try:
                for idx in data[coin].index:
                    idx_data = {k: v.loc[idx] for k, v in data.items()}
                    self.logger.debug(f"Update data for {idx}\n{idx_data}")
                    self._online.update_with_data(idx, idx_data)
            except Exception as e:
                self.msg_client.send_message(f'Unablt to update position\n{e}')

            if target_dt not in self._online.positions.iloc[-5:].index:
                self._online = self.init_online(self._online_params['base_params'], self._online_params['signal_gen_params'], self._data_params)
                bt.run(self._online)
                tmp_pos = self._online.positions.iloc[-5:]
                self.logger.debug(f'recreated positions:\n{tmp_pos}')

            positions = self._online.positions

            tmp_pos = self._online.positions.iloc[-5:]
            self.logger.debug(f'positions final:\n{tmp_pos}')

            prev_idx = first_add_dt - timedelta(hours=1)
            last_idx = target_dt

            prev_positions = positions.loc[prev_idx].fillna(0.0)
            last_positions = positions.loc[target_dt].fillna(0.0)
            diff_position = last_positions - prev_positions
            diff_position = diff_position[diff_position != 0]
            self.logger.debug('diff_position:\n{}'.format(diff_position))
            if len(diff_position) > 0:
                current_positions = self.get_balance()
                total_amount = pd.Series(current_positions['total'])
                prices = self.get_prices(total_amount.index)
                prices['USD'] = 1.0
                self.logger.debug('prices:\n{}'.format(prices))
                amount_balance = total_amount * pd.Series(prices)
                self.logger.debug('amount_balance:\n{}'.format(amount_balance))

                # sell_coins = diff_position[diff_position < 0].index
                # drop_coins = amount_balance[amount_balance == 0]
                # drop_coins = drop_coins[sell_coins].dropna()
                # self.logger.debug(f'sell coins:\n{sell_coins}\nzero balance:\n{amount_balance[amount_balance == 0]}\ndrop coins:\n{drop_coins}')

                weights = self._online.security_weights.iloc[-1]
                self.logger.debug('weights: {}'.format(weights))
                # weights = weights.drop(drop_coins.index)
                weights = weights.fillna(0.0)
                self.logger.debug('weights:\n{}\nsum:\n{}\nnew weights:\n{}'.format(weights, weights.sum(), (weights / weights.sum()).round(4)))
                weights = (weights / weights.sum()).round(4)

                fee = self.get_fee()
                new_positions = (amount_balance.sum() * fee * .99 * weights * np.sign(last_positions))
                self.logger.debug('new_positions:\n{}'.format(new_positions))
                new_positions = new_positions.fillna(0.0)

                dust_positions = new_positions[np.abs(new_positions) <= 5.0]
                dust_positions = dust_positions[dust_positions != 0]
                self.logger.debug('dust_positions:\n{}'.format(dust_positions))
                new_positions[dust_positions.index] = amount_balance[dust_positions.index]
                self.logger.debug('new new_positions:\n{}'.format(new_positions))

                diff_amount = new_positions - amount_balance
                diff_amount = diff_amount.dropna()
                diff_amount = diff_amount[np.abs(diff_amount) > 5.0]
                self.logger.debug('diff_amount:\n{}'.format(diff_amount))

                if len(diff_amount) == 0:
                    return

                asks = pd.Series(self.get_prices(diff_amount[diff_amount < 0].index, price_type='ask'))
                bids = pd.Series(self.get_prices(diff_amount[diff_amount > 0].index, price_type='bid'))
                prices = pd.concat([asks, bids])
                self.logger.debug('prices:\n{}'.format(prices))

                diff_amount = diff_amount / prices
                diff_amount = diff_amount.sort_values()
                for symbol, amount in diff_amount.items():
                    diff_amount[symbol] = np.round(amount, self.get_amount_precision(symbol))
                diff_amount = diff_amount[diff_amount != 0]
                self.logger.debug('diff_amount:\n{}'.format(diff_amount))
                if self.logger.level == logging.DEBUG:
                    self.msg_client.send_message("I'll try to create some orders:\n{}".format(diff_amount))

                # close current orders
                # TODO close open orders

                n = 0
                while len(diff_amount) and n < self._update_pos_tries:
                    order_errors = self._create_orders(diff_amount)
                    if len(order_errors) > 0:
                        # self._msg_client.send_message('Unable to create orders in try {}\n{}'
                        #                               .format(n + 1, '\n'.join(map(str, order_errors))))
                        time.sleep(self._update_pos_delay)
                        n += 1
                    else:
                        break
                    # TODO very very very bad! change to non-blocking call

                if len(diff_amount) > 0:
                    error_message = ''
                    if self.logger.level == logging.DEBUG and len(order_errors) == 0:
                        error_message = '\n'.join(['ERROR: {}'.format(e) for e in order_errors])
                    raise Exception('Unable to create orders for {}\n{}'.format(', '.join(diff_amount.index), error_message))

            if len(self._online.positions) >= 2250:
                self._online = self.init_online(self._online_params['base_params'],
                                                self._online_params['signal_gen_params'], self._data_params)
                bt.run(self._online)

        except Exception as e:
            self.logger.exception(e)
            self._msg_client.send_message('Unable to update position. Check log file.\n{}'.format(e))

    def _create_orders(self, amounts):
        symbols_to_execute = amounts.copy().to_dict()
        errors = []
        try:
            for symbol, amount in symbols_to_execute.items():
                op = 'buy' if amount > 0 else 'sell'
                order_header = 'ORDER: #{} {} {} {}'
                try:
                    order = self.market_order(amount, symbol, 'USD', op, raise_exception=True)
                    self.logger.info(order)
                    self.msg_client.send_message(order_header.format(order['id'], op.upper(), amount, symbol))
                    amounts.pop(symbol)
                except ccxt.errors.InsufficientFunds as ife:
                    self.logger.exception(ife)
                    errors.append(ife)
                    try:
                        free_balance = self.get_balance()['free'] * self.get_fee() * .99
                        price = self.exchange.fetch_tickers(symbol.upper() + '/USD')['last']
                        max_amount = free_balance / price
                        amounts[symbol] = np.round(np.min([amounts[symbol], max_amount]), 4)
                    except Exception as e:
                        self.logger.exception(e)
                        errors.append(e)
                        amounts[symbol] = np.round(amounts[symbol] * .98, self.get_amount_precision(symbol))
                    if logger.level == logging.DEBUG:
                        self.msg_client.send_message('{}\nnew amount: {}'.format(ife, amounts[symbol]))
                except ccxt.errors.InvalidOrder as ioe:
                    self.logger.exception(ioe)
                    if self.logger.level == logging.DEBUG:
                        self.msg_client.send_message(ioe)
                    errors.append(ioe)
                    amounts.pop(symbol)
                except Exception as e:
                    self.logger.exception(e)
                    errors.append(e)
        except Exception as e:
            self.logger.exception(e)
            errors.append(e)

        return errors


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-c', help='select bitstamp account', type=str, default='test_account_500.yaml')
    args = parser.parse_args()
    with open(args.c) as f:
        account_info = yaml.safe_load(f)

    BITSTAMP_USERNAME = account_info['BITSTAMP_USERNAME']
    BITSTAMP_KEY = account_info['BITSTAMP_KEY']
    BITSTAMP_SECRET = account_info['BITSTAMP_SECRET']
    LOG_NAME = account_info['LOG_NAME']

    TG_ID = 0  # cropped
    TG_HASH = '-'  # cropped
    TG_DCHAT = '-'  # cropped

    if not exists('./logs'):
        makedirs(abspath('./logs'))

    params = TradingParams()

    log_file = f'{LOG_NAME}_trade_log.log'
    log_path = abspath(f'./logs/{log_file}')
    params.logger_path = log_path
    logging.basicConfig(
        level=logging.DEBUG,
        datefmt='%Y-%m-%d %H:%M:%S',
        format='%(levelname)s - %(asctime)s - ' + BITSTAMP_USERNAME + ':\n%(message)s\n',
        handlers=[
            TimedRotatingFileHandler(abspath(f'./logs/{LOG_NAME}_debug_log.log'),
                                     when="d",
                                     interval=1,
                                     backupCount=50)
        ]
    )
    logger_name = f'TradeLogger_{LOG_NAME}'
    params.logger_name = logger_name
    logger = logging.getLogger(logger_name)
    handler = TimedRotatingFileHandler(log_path,
                                     when="d",
                                     interval=1,
                                     backupCount=50)
    formatter = logging.Formatter(fmt='%(levelname)s - %(asctime)s - ' + BITSTAMP_USERNAME + ':\n%(message)s\n',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    msg_client = TelegramMessageClient('anon', TG_ID, TG_HASH, TG_DCHAT, log_file=log_path, teleg=False)

    params = {
        'apiKey': BITSTAMP_KEY,
        'secret': BITSTAMP_SECRET,
        'uid': BITSTAMP_USERNAME,
	    'nonce': lambda: ccxt.Exchange.microseconds()
    }

    bitstamp = ccxt.bitstamp(params)
    # bitstamp = ccxt.bittrex()
    timeframe = '1H'
    data_params = {
        'timeframe': timeframe,
        'fpath': abspath(join(abspath('./'), pardir, 'data')),
        'market': 'USD',
        'exchange': 'Bitstamp'
    }
    signal_params = {
        'skip_period': 0,
        'timeframe': timeframe,
        'signal_params': {
            'curr_generators': {
                'BTC': {'BB': {'generator': signals.BBSignalGenerator, 'params': {'up_period': 25, 'low_period': 25, 'buy_n': 0, 'sell_n': 0}}},
                'ETH': {'BB': {'generator': signals.BBSignalGenerator, 'params': {'up_period': 25, 'low_period': 25, 'buy_n': 0, 'sell_n': 0}}},
                'XRP': {'BB': {'generator': signals.BBSignalGenerator, 'params': {'up_period': 25, 'low_period': 25, 'buy_n': 0, 'sell_n': 0}}},
                'BCH': {'BB': {'generator': signals.BBSignalGenerator, 'params': {'up_period': 25, 'low_period': 25, 'buy_n': 0, 'sell_n': 0}}},
                'LTC': {'BB': {'generator': signals.BBSignalGenerator, 'params': {'up_period': 25, 'low_period': 25, 'buy_n': 0, 'sell_n': 0}}}
            }
        }
    }
    base_params = {
        'name': 'online_strategy',
        'integer_positions': False,
        'commissions': partial(fee_func, ff=0, pf=0.01),
        'sec_amount_rounding': {
            'BTC': 2,
            'ETH': 2,
            'BCH': 2,
            'LTC': 2,
            'XRP': 5
        }
    }
    online_params = {
        'base_params': base_params,
        'signal_gen_params': signal_params
    }
    trading_client = MarketTradingCllient(bitstamp, online_params, data_params, logger_name=logger.name)
    trading_client.msg_client = msg_client
    msg_client.trading_client = trading_client

    trading_client.start()
