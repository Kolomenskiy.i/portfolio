# -*- encoding: utf-8 -*-

import logging


class TradingParams(object):

    __instance = None
    logger_name = None
    logger_path = None

    def __new__(cls):
        if not cls.__instance:
            cls.__instance = super(TradingParams, cls).__new__(cls)
        return cls.__instance

    def get_logger(self) -> logging.Logger:
        return logging.getLogger(self.logger_name)