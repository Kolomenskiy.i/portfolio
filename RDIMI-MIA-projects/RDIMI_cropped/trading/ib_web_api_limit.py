# import requests
#
#
# if __name__ == "__main__":
#     URL = "https://localhost:5000/v1/portal/fyi/unreadnumber"
#
#     PARAMS = {}
#
#     r = requests.get(url=URL, params=PARAMS, verify=False)  # , headers={'User-Agent': 'Custom'}
#
#     print(r)
#
#     data = r.json()
#
#     print(data)

from ibw.client import IBClient

REGULAR_ACCOUNT = 'MY_ACCOUNT_NUMBER'
REGULAR_USERNAME = 'MY_ACCOUNT_USERNAME'

# Create a new session of the IB Web API.
ib_client = IBClient(username=REGULAR_USERNAME, account=REGULAR_ACCOUNT)

# create a new session.
ib_client.create_session()

# grab the account data.
account_data = ib_client.portfolio_accounts()

# print the data.
print(account_data)

# Grab historical prices.
aapl_prices = ib_client.market_data_history(conid=['-'], period='1d', bar='5min')  # cropped

# print the prices.
print(aapl_prices)

# close the current session.
ib_client.close_session()
