import React, { memo } from 'react';
import { connect } from 'react-redux';
import { addRekvest as addRekvestServer } from '../../models/AppModel';
import {
	addRekvestAction
} from '../../store/action';
import Rekvest from '../Rekvest/Rekvest';

const Tester = ({
	testerName,
	testerId,
	rekvestes,
	addRekvestDispatch
}) => {
	const addRekvest = async () => {
		let rekvestName = prompt('Введите название заявки на тестирование');

		if (!rekvestName) return;

		rekvestName = rekvestName.trim();

		if (!rekvestName) return;

    const info = addRekvestServer({ testerId, rekvestName });
    console.log(info);
		addRekvestDispatch({ rekvestName, testerId });
	};

	return(<div className="ra-tester">
      <header className="ra-tester-header">
        {testerName}
      </header>
      <div className="ra-tester-rekvestes">
      	{rekvestes.map((rekvest, index) => (
      		<Rekvest
      			rekvestName = {rekvest}
      			rekvestId = {index}
      			testerId = {testerId}
      			key = {`list${testerId}-rekvest${index}`}
      		/>
      	))}
      </div>
      <footer
      	className="ra-tester-add-rekvest"
      	onClick = {addRekvest}
      >
        Добавить заявку на тестирование
      </footer>
    </div>);
};

const mapDispatchToProps = dispatch => ({
	addRekvestDispatch: ({ testerId, rekvestName }) => 
		dispatch(addRekvestAction({ rekvestName, testerId }))
})

export default connect(
	null,
	mapDispatchToProps
)(memo(Tester));
