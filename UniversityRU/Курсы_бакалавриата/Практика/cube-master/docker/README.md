На компьютере должен быть установлен Docker. 

Для развертки бд необходимо:
1) Запулить репозиторий гитхаб, в терминале перейти в папку docker и выполнить команду:
docker-compose build
2) Запустить файл docker-compose, который создает бд по sql скриптам из папки db:
docker-compose up
3) Дождаться создания бд (не более 5 минут)

В случае успеха возможно подключиться к cubedb через localhost на порту 5432. 
Пользователь: postgres. Пароль: postgres.
Для этого можно вопользоваться командой:

psql -h localhost -p 5432 -d cubedb -U postgres --password

Пароль: postgres


Полезные команды Docker.

Остановка всех контейнеров:
docker stop $(docker ps -a -q)

Удаление всех контейнеров:
docker rm $(docker ps -a -q)

Удаление всех образов:
docker rmi -f $(docker images -a -q)
