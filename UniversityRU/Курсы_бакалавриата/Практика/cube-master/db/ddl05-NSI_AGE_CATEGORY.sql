-- auto-generated definition
create table NSI_AGE_CATEGORY
(
    ID       BIGINT not null
        constraint NSI_AGE_CATEGORY_PK
            primary key,
    ISACTUAL CHAR,
    CODE     BIGINT,
    AGE_TO   BIGINT,
    NAME     VARCHAR(100),
    AGE_FROM BIGINT,
    SORT     BIGINT
);

