-- Database: olap

-- DROP DATABASE olap;

CREATE DATABASE olap

    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;