-- auto-generated definition
create table mrtb_index_values_route_period
(
    INDEX_ID                      BIGINT,
    TRANSPORT_TYPE_ID             BIGINT,
    OPERATOR_ID                   BIGINT,
    ROUTE_NAME                    VARCHAR(2048),
    DEPARTURE_ID                  BIGINT,
    ARRIVAL_ID                    BIGINT,
    VALUE                         BIGINT,
    VERSION_ID                    BIGINT,
    PERIOD_ID                     BIGINT,
    DEPARTURE_ID_COUNTRY          BIGINT,
    ARRIVAL_ID_COUNTRY            BIGINT,
    DEPARTURE_ID_LEVEL2           BIGINT,
    ARRIVAL_ID_LEVEL2             BIGINT,
    DEPARTURE_ID_LEVEL3           BIGINT,
    ARRIVAL_ID_LEVEL3             BIGINT,
    DEPARTURE_ID_TRANSPORT_REGION BIGINT,
    ARRIVAL_ID_TRANSPORT_REGION   BIGINT,
    DEPARTURE_ID_CITY             BIGINT,
    ARRIVAL_ID_CITY               BIGINT,
    SEX_ID                        SMALLINT,
    AGE_RANGE_ID                  SMALLINT,
    YEAR                          SMALLINT ,
    MONTH                         SMALLINT ,
    DEPARTURE_ID_MO               VARCHAR(1000),
    ARRIVAL_ID_MO                 VARCHAR(1000),
    ROUTE_NUMBER                  VARCHAR(38),
    CITIZENSHIP_ID                INT,
    check (year >= 0),
    check (MONTH >= 0)
)
;

comment on column mrtb_index_values_route_period.INDEX_ID is 'Идентификатор показателя'
;

comment on column mrtb_index_values_route_period.TRANSPORT_TYPE_ID is 'Идентификатор типа транспорта'
;

comment on column mrtb_index_values_route_period.OPERATOR_ID is 'Идентификатор перевозчика'
;

comment on column mrtb_index_values_route_period.ROUTE_NAME is 'Номер маршрута'
;

comment on column mrtb_index_values_route_period.DEPARTURE_ID is 'Идентификатор точки отправления'
;

comment on column mrtb_index_values_route_period.ARRIVAL_ID is 'Идентификатор точки прибытия'
;

comment on column mrtb_index_values_route_period.VALUE is '	Значение показателя'
;

comment on column mrtb_index_values_route_period.VERSION_ID is 'Идентификатор версии данных'
;

comment on column mrtb_index_values_route_period.PERIOD_ID is 'Идентификатор периода из справочника «Временные периоды»'
;

comment on column mrtb_index_values_route_period.DEPARTURE_ID_COUNTRY is 'Идентификатор страны отправления'
;

comment on column mrtb_index_values_route_period.ARRIVAL_ID_COUNTRY is 'Идентификатор страны прибытия'
;

comment on column mrtb_index_values_route_period.DEPARTURE_ID_LEVEL2 is 'Идентификатор остановочного пункта отправления (уровень 2)'
;

comment on column mrtb_index_values_route_period.ARRIVAL_ID_LEVEL2 is 'Идентификатор остановочного пункта прибытия (уровень 2)'
;

comment on column mrtb_index_values_route_period.DEPARTURE_ID_LEVEL3 is 'Идентификатор остановочного пункта отправления (уровень 3)'
;

comment on column mrtb_index_values_route_period.ARRIVAL_ID_LEVEL3 is 'Идентификатор остановочного пункта прибытия (уровень 3)'
;

comment on column mrtb_index_values_route_period.DEPARTURE_ID_TRANSPORT_REGION is 'Идентификатор транспортного региона отправления'
;

comment on column mrtb_index_values_route_period.ARRIVAL_ID_TRANSPORT_REGION is 'Идентификатор транспортного региона прибытия'
;

comment on column mrtb_index_values_route_period.DEPARTURE_ID_CITY is 'Идентификатор города отправления'
;

comment on column mrtb_index_values_route_period.ARRIVAL_ID_CITY is 'Идентификатор города прибытия'
;

comment on column mrtb_index_values_route_period.SEX_ID is 'Идентификатор пола'
;

comment on column mrtb_index_values_route_period.AGE_RANGE_ID is 'Идентификатор возрастной категории'
;

comment on column mrtb_index_values_route_period.YEAR is 'Год'
;

comment on column mrtb_index_values_route_period.MONTH is 'Месяц'
;

comment on column mrtb_index_values_route_period.DEPARTURE_ID_MO is 'Идентификатор МО пункта отправления'
;

comment on column mrtb_index_values_route_period.ARRIVAL_ID_MO is 'Идентификатор МО пункта прибытия'
;

comment on column mrtb_index_values_route_period.ROUTE_NUMBER is 'Номер маршрута'
;

comment on column mrtb_index_values_route_period.CITIZENSHIP_ID is 'Идентификатор гражданства'
;

create index MRTB_IND_VAL_RT_PER_SRCH7
    on mrtb_index_values_route_period (INDEX_ID, TRANSPORT_TYPE_ID, PERIOD_ID)
;

create index MRTB_IND_VAL_RT_PER_SRCH5
    on mrtb_index_values_route_period (ROUTE_NAME, DEPARTURE_ID_CITY, ARRIVAL_ID_CITY, OPERATOR_ID)
;

create index MRTB_IND_VAL_RT_PER_SRCH3
    on mrtb_index_values_route_period (TRANSPORT_TYPE_ID, ARRIVAL_ID)
;

create index MRTB_IND_VAL_RT_PER_SRCH9
    on mrtb_index_values_route_period (INDEX_ID, TRANSPORT_TYPE_ID, ROUND(PERIOD_ID / 1000), MOD(PERIOD_ID, 100))
;

create index MRTB_IND_VAL_RT_PER_SRCH2
    on mrtb_index_values_route_period (TRANSPORT_TYPE_ID, DEPARTURE_ID)
;

create index I_IND_VAL_RT_PER_SH6
    on mrtb_index_values_route_period (ROUTE_NAME, INDEX_ID, TRANSPORT_TYPE_ID, OPERATOR_ID, PERIOD_ID, DEPARTURE_ID,
                                       ARRIVAL_ID)
;

create index I_IND_VAL_RT_PER_SH13
    on mrtb_index_values_route_period (VERSION_ID, INDEX_ID, ROUND(PERIOD_ID / 1000), TRANSPORT_TYPE_ID)
;

create index MRTB_IND_VAL_RT_PER_SRCH1
    on mrtb_index_values_route_period (INDEX_ID)
;

create index I_IND_VAL_RT_PER_SH10
    on mrtb_index_values_route_period (DEPARTURE_ID, INDEX_ID, TRANSPORT_TYPE_ID, ROUND(PERIOD_ID / 1000), VERSION_ID)
;

create index I_IND_VAL_RT_PER_SH11
    on mrtb_index_values_route_period (ARRIVAL_ID, INDEX_ID, TRANSPORT_TYPE_ID, ROUND(PERIOD_ID / 1000), VERSION_ID)
;

create index MRTB_IND_VAL_RT_PER_SRCH4
    on mrtb_index_values_route_period (INDEX_ID, PERIOD_ID)
;

create index I_IND_VAL_RT_PER_SH12
    on mrtb_index_values_route_period (VERSION_ID, INDEX_ID, ROUND(PERIOD_ID / 1000))
;

create index MRTB_IND_VAL_RT_PER_SRCH8
    on mrtb_index_values_route_period (INDEX_ID, TRANSPORT_TYPE_ID, ROUND(PERIOD_ID / 1000))
;

