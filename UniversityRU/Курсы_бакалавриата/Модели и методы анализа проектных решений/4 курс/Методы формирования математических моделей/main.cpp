#include <iostream> 
#include <vector> 
#include <cmath> 
#include <string> 
#include <map> 
#include <fstream> 
#include <math.h>

#define	_USE_MATH_DEFINES 
#define INTEGRATION_TIME	1e-3
#define INTEGRATION_STEP	1e-16
#define	ACCURACY			1e-4
#define	MODEL_SIZE			30
#define	REACT_ELEM_COUNT	0
#define	COMMON_ELEM_COUNT	15
#define	C					1e-6	//C3
#define	R_1					1000.
#define	R_2					20.
#define	R_3					1000000.
#define	R_4					1000000.
#define	R_5					20.
#define	R_6					1000.
#define	Rd					1e6
#define	Rb					20.
#define	L					0.1
#define	It					1e-12
#define	TAU					5e-9
#define	MFT					0.026
#define	E_1					1
#define	E_2					5
#define E_3					5
#define Cd					2e-12	//C1, C2

using namespace std;

vector<double>		vDelta;
map<string, double> vResult; 
map<string, double> vPrevResult;
map<string, double> vPrevPrevResult;

double	MMatrix[8][7] = {	{ 0, 0,	0,	0,	-1, 0, 0},
							{ 1, 0,	1,	-1,	0, 1, 0},
							{ 0, 0,	0,	0,	-1, 0, 0},
							{ 1, 0,	1,	-1,	0, 1, 0},
							{ -1, 1,	0,	1,	1, 0, 0},
							{ 1, 0,	1,	-1,	0, 1, 0},
							{ 0, 0, 0,	0,	0, 0, -1},
							{ -1, 0,	0,	1,	0, 0, 1}
						};

double	integrationStep = 1e-8;
double	correctedStep = integrationStep;
double	prevIntegrStep = integrationStep;
double	integrationAcr = ACCURACY;
double	epsMax = 1e-2;
double	epsMin = 25e-3;
double	minStep = 1e-16;
double	modelingTime = 0;
ofstream	file;

vector<string> variableMapping; 
double**	aMatrix;

void setFirstApproximation();
void initializeModelMatrix(); 
void fillModelMatrix(double); 
void gaussSolver();
bool checkConverge(); 
void printSolution(); 
void zeroModelMatrix(); 
void printModel();
void Solution();


int main()
{
	file.open("output.dat");

	//making array with string values for vector of residuals 
	variableMapping.push_back(string("Uc_1"));//1 
	variableMapping.push_back(string("Uc_2"));//2 
	variableMapping.push_back(string("Ui_1"));//3 
	variableMapping.push_back(string("Ui_2"));//4 
	variableMapping.push_back(string("Ur_2"));//5 
	variableMapping.push_back(string("Ur_4"));//6 
	variableMapping.push_back(string("Ur_6"));//7 
	variableMapping.push_back(string("Ul"));//8 
	variableMapping.push_back(string("Ie_1"));//9 
	variableMapping.push_back(string("Ie_2"));//10 
	variableMapping.push_back(string("Ie_3"));//11 
	variableMapping.push_back(string("Ir_1"));//12 
	variableMapping.push_back(string("ir_3"));//13 
	variableMapping.push_back(string("Ir_5"));//14 
	variableMapping.push_back(string("Ic_3"));//15 
	variableMapping.push_back(string("Ic_1"));//16 
	variableMapping.push_back(string("Ic_2"));//17 
	variableMapping.push_back(string("Ii_1"));//18 
	variableMapping.push_back(string("Ii_2"));//19 
	variableMapping.push_back(string("Ir_2"));//20 
	variableMapping.push_back(string("Ir_4"));//21 
	variableMapping.push_back(string("Ir_6"));//22 
	variableMapping.push_back(string("Il"));//23 
	variableMapping.push_back(string("Ue_1"));//24 
	variableMapping.push_back(string("Ue_2"));//25 
	variableMapping.push_back(string("Ue_3"));//26 
	variableMapping.push_back(string("Ur_1"));//27 
	variableMapping.push_back(string("Ur_3"));//28 
	variableMapping.push_back(string("Ur_5"));//29 
	variableMapping.push_back(string("Uc_3"));//30
	for (int i = 0; i < MODEL_SIZE; i++) {
		vPrevResult[variableMapping[i]] = 0;
		vResult[variableMapping[i]] = 0;
		vPrevPrevResult[variableMapping[i]] = 0;
	}
	initializeModelMatrix();
	while (modelingTime < INTEGRATION_TIME) {
		integrationStep = correctedStep; 
		bool convergeSystem = false;
		int newtonIterCount = 0; 
		while (!convergeSystem) {
			zeroModelMatrix();//init matrix values 
			setFirstApproximation(); 
			fillModelMatrix(modelingTime); 
			gaussSolver();
			for (int i = 0; i < MODEL_SIZE; i++) {
				vResult[variableMapping[i]] += vDelta[i];
			}
			convergeSystem = checkConverge(); 
			newtonIterCount++;
			if(newtonIterCount > MODEL_SIZE && !convergeSystem) {
				newtonIterCount = 0;
				integrationStep /= 2;
				if (integrationStep < minStep) {
					integrationStep = minStep;
				}
				for (int i = 0; i < MODEL_SIZE; i++) {
					vResult[variableMapping[i]] = vPrevResult[variableMapping[i]];
				}
			}
		} //Newton
		cout << "Newton iter count " << newtonIterCount	<< "; Step " << integrationStep << endl;
		double curEps = 0;
		for (int i = REACT_ELEM_COUNT; i < MODEL_SIZE; i++) {
			double eps = 0.5*fabs(correctedStep/(correctedStep + prevIntegrStep) * ((vResult[variableMapping[i]] - vPrevResult[variableMapping[i]]) - correctedStep/prevIntegrStep * (vPrevResult[variableMapping[i]] - vPrevPrevResult[variableMapping[i]])));
			curEps = (eps > curEps) ? eps : curEps;
		}

		if (curEps > epsMax) {
			correctedStep /= 2;
			for (int i = 0; i < MODEL_SIZE; i++) {
				vResult[variableMapping[i]] = vPrevResult[variableMapping[i]];
			}
		}
		else {
			for (int i = 0; i < MODEL_SIZE; i++) {
				vPrevPrevResult[variableMapping[i]] = vPrevResult[variableMapping[i]];
				vPrevResult[variableMapping[i]] = vResult[variableMapping[i]];
			}
			prevIntegrStep = integrationStep; modelingTime += integrationStep; printSolution();
			if (curEps < epsMin || correctedStep < minStep) {
				correctedStep *= 2;
			}
			else {
				correctedStep = INTEGRATION_STEP;
			}
		}
	}
	return 0;
}

void setFirstApproximation() {
	for(int i = 0; i < MODEL_SIZE; i++) {
		vDelta.push_back(0);
	}
}

void initializeModelMatrix() {
	aMatrix = new double* [MODEL_SIZE]; 
	for (int j = 0; j < MODEL_SIZE; ++j) {
		aMatrix[j] = new double [MODEL_SIZE + 1];
	}
}

void fillModelMatrix(double modelingTime) {
	//matrix A values
	 
	//string 1
	aMatrix[0][0] = 1.0;
	aMatrix[0][27] = -1.0;
	//string 2
	aMatrix[1][1] = 1.0;
	aMatrix[1][23] = 1.0;
	aMatrix[1][25] = 1.0;
	aMatrix[1][26] = -1.0;
	aMatrix[1][28] = 1.0;
	//string 3
	aMatrix[2][2] = 1.0;
	aMatrix[2][27] = -1.0;
	//string 4
	aMatrix[3][3] = 1.0;
	aMatrix[3][23] = 1.0;
	aMatrix[3][25] = 1.0;
	aMatrix[3][26] = -1.0;
	aMatrix[3][28] = 1.0;
	//string 5
	aMatrix[4][4] = 1.0;
	aMatrix[4][23] = -1.0;
	aMatrix[4][24] = 1.0;
	aMatrix[4][26] = 1.0;
	aMatrix[4][27] = 1.0;
	//string 6
	aMatrix[5][5] = 1.0;
	aMatrix[5][23] = 1.0;
	aMatrix[5][25] = 1.0;
	aMatrix[5][26] = -1.0;
	aMatrix[5][28] = 1.0;
	//string 7
	aMatrix[6][6] = 1.0;
	aMatrix[6][29] = -1.0;
	//string 8
	aMatrix[7][7] = 1.0;
	aMatrix[7][23] = -1.0;
	aMatrix[7][26] = 1.0;
	aMatrix[7][29] = 1.0;
	//string 9
	aMatrix[8][8] = 1.0;
	aMatrix[8][16] = -1.0;
	aMatrix[8][18] = -1.0;
	aMatrix[8][19] = 1.0;
	aMatrix[8][20] = -1.0;
	aMatrix[8][22] = 1.0;
	//string 10
	aMatrix[9][9] = 1.0;
	aMatrix[9][19] = -1.0;
	//string 11
	aMatrix[10][10] = 1.0;
	aMatrix[10][16] = -1.0;
	aMatrix[10][18] = -1.0;
	aMatrix[10][20] = -1.0;
	//string 12
	aMatrix[11][11] = 1.0;
	aMatrix[11][16] = 1.0;
	aMatrix[11][18] = 1.0;
	aMatrix[11][19] = -1.0;
	aMatrix[11][20] = 1.0;
	aMatrix[11][22] = -1.0;
	//string 13
	aMatrix[12][12] = 1.0;
	aMatrix[12][15] = 1.0;
	aMatrix[12][17] = 1.0;
	aMatrix[12][19] = -1.0;
	//string 14
	aMatrix[13][13] = 1.0;
	 
	aMatrix[13][16] = -1.0;
	aMatrix[13][18] = -1.0;
	aMatrix[13][20] = -1.0;
	//string 15
	aMatrix[14][14] = 1.0;
	aMatrix[14][21] = 1.0;
	aMatrix[14][22] = -1.0;
	//string 16
	aMatrix[15][0] = -Cd/integrationStep;
	aMatrix[15][15] = 1.0;
	//string 17
	aMatrix[16][1] = -Cd/integrationStep;
	aMatrix[16][16] = 1.0;
	//string 18
	aMatrix[17][2] = -(It/MFT)*exp(vResult["Ui_1"]/MFT);
	aMatrix[17][17] = 1.0;
	//string 19
	aMatrix[18][3] = -(It/MFT)*exp(vResult["Ui_2"]/MFT);
	aMatrix[18][18] = 1.0;
	//string 20
	aMatrix[19][4] = 1.0; aMatrix[19][19] = -R_2;
	//string 21
	aMatrix[20][5] = 1.0; aMatrix[20][20] = -R_4;
	//string 22
	aMatrix[21][6] = 1.0; aMatrix[21][21] = -R_6;
	//string 23
	aMatrix[22][7] = 1.0;
	aMatrix[22][22] = -L/integrationStep;
	//string 24
	aMatrix[23][23] = 1.0;
	//string 25
	aMatrix[24][24] = 1.0;
	//string 26
	aMatrix[25][25] = 1.0;
	//string 27 
	aMatrix[26][11] = -R_1; 
	aMatrix[26][26] = 1.0;
	//string 28 
	aMatrix[27][12] = -R_3; 
	aMatrix[27][27] = 1.0;
	//string 29 
	aMatrix[28][13] = -R_5; 
	aMatrix[28][28] = 1.0;
	//string 30
	aMatrix[29][14] = 1.0; 
	aMatrix[29][29] = -C/integrationStep;

	//vector B values
	//string 1
	aMatrix[0][MODEL_SIZE] = -(vResult["Uc_1"] - vResult["Ur_3"]);
	//string 2
	aMatrix[1][MODEL_SIZE] = -(vResult["Uc_2"] + vResult["Ue_1"] + vResult["Ue_3"] - vResult["Ur_1"] + vResult["Ur_5"]);
	//string 3
	aMatrix[2][MODEL_SIZE] = -(vResult["Ui_1"] - vResult["Ur_3"]);
	//string 4
	aMatrix[3][MODEL_SIZE] = -(vResult["Ui_2"] + vResult["Ue_1"] + vResult["Ue_3"] - vResult["Ur_1"] + vResult["Ur_5"]);
	//string 5
	aMatrix[4][MODEL_SIZE] = -(vResult["Ur_2"] - vResult["Ue_1"] + vResult["Ue_2"] + vResult["Ur_1"] + vResult["Ur_3"]);
	//string 6
	aMatrix[5][MODEL_SIZE] = -(vResult["Ur_4"] + vResult["Ue_1"] + vResult["Ue_3"] - vResult["Ur_1"] + vResult["Ur_5"]);
	//string 7
	aMatrix[6][MODEL_SIZE] = -(vResult["Ur_6"] - vResult["Uc_3"]);
	//string 8
	aMatrix[7][MODEL_SIZE] = -(vResult["Ul"] - vResult["Ue_1"] + vResult["Ur_1"] + vResult["Uc_3"]);
	//string 9
	aMatrix[8][MODEL_SIZE] = -(vResult["Ie_1"] - vResult["Ic_2"] - vResult["Ii_2"] + vResult["Ir_2"] - vResult["Ir_4"] + vResult["Il"]);
	//string 10
	aMatrix[9][MODEL_SIZE] = -(vResult["Ie_2"] - vResult["Ir_2"]);
	//string 11
	aMatrix[10][MODEL_SIZE] = -(vResult["Ie_3"] - vResult["Ic_2"] - vResult["Ii_2"] - vResult["Ir_4"]);
	//string 12
	aMatrix[11][MODEL_SIZE] = -(vResult["Ir_1"] + vResult["Ic_2"] + vResult["Ii_2"] - vResult["Ir_2"] + vResult["Ir_4"] - vResult["Il"]);
	//string 13
	aMatrix[12][MODEL_SIZE] = -(vResult["Ir_3"] + vResult["Ic_1"] + vResult["Ii_1"] - vResult["Ir_2"]);//?
	//string 14
	aMatrix[13][MODEL_SIZE] = -(vResult["Ir_5"] - vResult["Ic_2"] - vResult["Ii_2"] - vResult["Ir_4"]);
	//string 15
	aMatrix[14][MODEL_SIZE] = -(vResult["Ic_3"] + vResult["Ir_6"] - vResult["Il"]);
	//string 16
	aMatrix[15][MODEL_SIZE] = -(vResult["Ic_1"] - Cd*(vResult["Uc_1"] - vPrevResult["Uc_1"])/integrationStep);
	//string 17
	aMatrix[16][MODEL_SIZE] = -(vResult["Ic_2"] - Cd*(vResult["Uc_2"] - vPrevResult["Uc_2"])/integrationStep);
	//string 18
	aMatrix[17][MODEL_SIZE] = -(vResult["Ii_1"] - It * (exp(vResult["Ui_1"]/MFT) - 1));
	//string 19
	aMatrix[18][MODEL_SIZE] = -(vResult["Ii_2"] - It * (exp(vResult["Ui_2"]/MFT) - 1));
	//string 20
	aMatrix[19][MODEL_SIZE] = -(vResult["Ur_2"] - vResult["Ir_2"]*R_2);
	//string 21
	aMatrix[20][MODEL_SIZE] = -(vResult["Ur_4"] - vResult["Ir_4"]*R_4);
	//string 22
	aMatrix[21][MODEL_SIZE] = -(vResult["Ur_6"] - vResult["Ir_6"]*R_6);
	//string 23
	aMatrix[22][MODEL_SIZE] = -(vResult["Ul"] - L*(vResult["Il"]- vPrevResult["Il"])/integrationStep);
	//string 24
	aMatrix[23][MODEL_SIZE] = -(vResult["Ue_1"] - 10 * sin(2 * M_PI * modelingTime/(1e-4)));
	//string 25
	aMatrix[24][MODEL_SIZE] = -(vResult["Ue_2"] - E_2);
	//string 26
	aMatrix[25][MODEL_SIZE] = -(vResult["Ue_3"] - E_3);
	//string 27
	aMatrix[26][MODEL_SIZE] = -(vResult["Ur_1"] - vResult["Ir_1"]*R_1);
	//string 28
	aMatrix[27][MODEL_SIZE] = -(vResult["Ur_3"] - vResult["Ir_3"]*R_3);
	//string 29
	aMatrix[28][MODEL_SIZE] = -(vResult["Ur_5"] - vResult["Ir_5"]*R_5);
	//string 30
	aMatrix[29][MODEL_SIZE] = -(vResult["Ic_3"] - C*(vResult["Uc_3"]- vPrevResult["Uc_3"])/integrationStep);
}

bool checkConverge() {
	for (int i = REACT_ELEM_COUNT; i < MODEL_SIZE; i++) {
		if (fabs(vDelta[i]) > integrationAcr) {
			return false;
		}
	}
	return true;
}

void printSolution() {
	file << modelingTime;
	file << "\t" << vResult["Ue_1"] - vResult["Ur_1"]; 
	file << "\t" << vResult["Ur_6"];
	file << endl;
}


void Solution() {
	cout << "modeling time:[" << modelingTime << "]"; 
	cout << "\t" << vResult["Ue_1"];
	cout << "\t" << vResult["Ie_1"]; 
	cout << "\t" << vResult["Ur_1"]; 
	cout << "\t" << vResult["Ir_1"]; 
	cout << "\t" << vResult["Ue_2"]; 
	cout << "\t" << vResult["Ie_2"]; 
	cout << "\t" << vResult["Ur_2"]; 
	cout << "\t" << vResult["Ir_2"]; 
	cout << "\t" << vResult["Ui_1"]; 
	cout << "\t" << vResult["Ii_1"]; 
	cout << "\t" << vResult["Ui_2"]; 
	cout << "\t" << vResult["Ii_2"]; 
	cout << "\t" << vResult["Ul"]; 
	cout << "\t" << vResult["Il"]; 
	cout << "\t" << vResult["Uc"]; 
	cout << "\t" << vResult["Ic"]; 
	cout << "\t" << vResult["Ur_6"]; 
	cout << "\t" << vResult["Ir_6"]; 
	cout << endl;
}

void zeroModelMatrix() {
	for (int i = 0; i < MODEL_SIZE; i++) {
		for (int j = 0; j <= MODEL_SIZE; j++) {
			aMatrix[i][j] = 0;
		}
	}
}

void printModel() {
	for (int i = 0; i < MODEL_SIZE; i++) {
		for (int j = 0; j < MODEL_SIZE; j++) {
			cout << aMatrix[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void gaussSolver(void) {
	int	i, j, k, maxInd;
	double	max;//gonna keep diagonal element with max value for better solving accuracy
	double	tmp;//gonna be using for swapping matrix strings 
	double	eps = 0.000001;//accuracy
	//Gauss forward solution with pre-corrections
	for(i = 0; i < MODEL_SIZE; i++) {
		//correction BEGIN
		max = fabs(aMatrix[i][i]); 
		maxInd = i;
		 
		// LF max value of diagonal element for the lowest error in calculation
		for(k = i + 1; k < MODEL_SIZE; k++) {
			if(fabs(aMatrix[k][i]) > max) {
				maxInd = k;
				max = fabs(aMatrix[k][i]);
			}
		}
		// if max value is lower then accuracy
		if(max < eps) {
			puts("Can't solve system of equations: zero column"); 
			exit(5);
		}
		// Swapping current matrix string with max one
		for(j = 0; j < MODEL_SIZE + 1; j++) {
			tmp = aMatrix[maxInd][j]; 
			aMatrix[maxInd][j] = aMatrix[i][j]; 
			aMatrix[i][j] = tmp;
		}//correction END
		// Divide each element in string with diagonal element 
		tmp = aMatrix[i][i];
		for(j = i; j < MODEL_SIZE + 1; j++) {
			aMatrix[i][j] /= tmp;
		}
		// Subtraction of chosen multiplied line from rest lines
		for(k = i + 1; k < MODEL_SIZE; k++) {
			tmp = aMatrix[k][i];
			for(j = i; j < MODEL_SIZE + 1; j++) {
				aMatrix[k][j] -= aMatrix[i][j] * tmp;
			}
		}
	}
	// Backward Gauss solution
	for(i = MODEL_SIZE - 1; i >= 0; i--) {
		double tmp = aMatrix[i][MODEL_SIZE+1]; 
		for(j = 0; j < i; j++) {
			aMatrix[j][MODEL_SIZE+1] -= aMatrix[j][i] * tmp;
		}
	}
	//Writing solution of vector of residuals into array vDelta in following code
	for (int i = 0; i < MODEL_SIZE; i++) {
		vDelta[i] = aMatrix[i][MODEL_SIZE];
	}
	return ;
}
