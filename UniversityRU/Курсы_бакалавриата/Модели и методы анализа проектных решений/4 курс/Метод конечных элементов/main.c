#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define EPS 1e-16
#define START_X 0.0
#define END_X 10.0

#define U 5.0
#define DU 10.0

#define CUBE_MATRIX_DIM 4
#define LINEAR_MATRIX_DIM 2

// #define CUBE 1


/* Simple matrix and vector realization */
typedef struct matrix_t {
	double** data;
	size_t cols;
	size_t rows;
} matrix_t;

typedef struct vector_t {
	double* data;
	size_t size;
} vector_t;

matrix_t* create_matrix(size_t rows, size_t cols) {
	matrix_t* m = (matrix_t*) calloc(1, sizeof(matrix_t));

	if (m == NULL) {
		return NULL;
	}

	m->rows = rows;
	m->cols = cols;
	m->data = (double**) calloc(rows, sizeof(double*));

	if (m->data == NULL) {
		free(m);
		return NULL;
	}

	for (size_t i = 0; i < m->rows; i++) {
		m->data[i] = (double*) calloc(m->cols, sizeof(double));
		if (m->data[i] == NULL) {
			for (size_t j = 0; j < i; j++) {
				free(m->data[i]);
			}
			free(m->data);
			free(m);
			return NULL;
		}
	}
	return m;
}

vector_t* create_vector(size_t size) {
	vector_t* v = (vector_t*) calloc(1, sizeof(vector_t));

	if (v == NULL) {
		return NULL;
	}

	v->size = size;
	v->data = (double*) calloc(size, sizeof(double));

	if (v->data == NULL) {
		free(v->data);
		free(v);
		return NULL;
	}

	return v;
}

void free_matrix(matrix_t* m) {
	if (m == NULL) {
		return;
	}
	if (m->data == NULL) {
		free(m);
		return;
	}
	for (size_t i = 0; i < m->rows; i++) {
		free(m->data[i]);
	}
	free(m->data);
	free(m);
}

void free_vector(vector_t* v) {
	if (v == NULL) {
		return;
	}
	if (v->data == NULL) {
		free(v);
		return;
	}
	free(v->data);
	free(v);
}

/* Gauss method */
void gauss_straight(matrix_t* A, vector_t* b) {
	double pivot = 0.0;
	for (size_t i = 0; i < A->rows; i++) {
		for (size_t j = i + 1; j < A->cols; j++) {
			if (fabs(A->data[j][i]) < EPS) {
				continue;
			}

			pivot = A->data[j][i] / A->data[i][i];
			b->data[j] -= pivot * b->data[i];

			for (size_t k = 0; k < A->rows; k++) {
				A->data[j][k] -= pivot * A->data[i][k];
			}
		}
	}
}

vector_t* gauss_backward(matrix_t* A, vector_t* b) {
	vector_t* x = create_vector(b->size);
	for (int i = b->size - 1; i >= 0; i--) {
		x->data[i] = b->data[i];
		for (size_t j = i + 1; j < b->size; j++) {
			x->data[i] -= x->data[j] * A->data[i][j];
		}
		x->data[i] /= A->data[i][i];
	}
	return x;
}

/* Integrate block */
double accurate_solution(double x) {
	// return 3.252198835e-12 * exp(sqrt(8) * x) - 3.535533906 * exp(-sqrt(8) * x) - 10.0 / 8.0;
	return -(5*exp(-2*sqrt(2)*x)/(4*(1+exp(40*sqrt(2))))*(exp(2*sqrt(2)*x)-2*sqrt(2)*exp(4*sqrt(2)*x)-5*exp(4*sqrt(2)*(x+5))+exp(2*sqrt(2)*(x + 20))-5*exp(20*sqrt(2))+2*sqrt(2)*exp(40*sqrt(2))));
}

vector_t* prec_accurate_solution(vector_t* x_vec) {
	vector_t* y_vec = create_vector(x_vec->size);
	for (size_t i = 0; i < x_vec->size; i++) {
		y_vec->data[i] = accurate_solution(x_vec->data[i]);
	}

	printf("Accurate solution\n");
	printf("%lf\t", y_vec->data[0]);
	printf("%lf\t", y_vec->data[1]);
	printf("%lf\n", y_vec->data[2]);

	return y_vec;
}

vector_t* prec_linear_solution(size_t elem_count) {
	double L = (END_X - START_X) / elem_count;
	size_t dots_cnt = elem_count + 1;
	matrix_t* A = create_matrix(dots_cnt, dots_cnt);
	vector_t* b = create_vector(dots_cnt);
	double local[LINEAR_MATRIX_DIM][LINEAR_MATRIX_DIM] =
	{{8.0 * L / 3.0 + 1.0 / L, 8.0 * L / 6.0 - 1.0 / L},
	{8.0 * L / 6.0 - 1.0 / L, 8.0 * L / 3.0 + 1.0 / L}};

	for (size_t i = 0; i < elem_count; i++) {
		for (size_t j = 0; j < LINEAR_MATRIX_DIM; j++) {
			for (size_t k = 0; k < LINEAR_MATRIX_DIM; k++) {
				A->data[i + j][i + k] += local[j][k];
			}
		}
	}

	// Bound
	// A->data[0][0] = 1.0;
	// A->data[1][0] = 0.0;
	// b->data[0] = 2 * L - local[0][0] * U;
	// b->data[1] = 4 * L - local[1][0] * U;
	//My bound
	A->data[0][0] = DU;
	A->data[1][0] = U;

	// b->data[0] = -5 * L + local[0][0] * U;
	// b->data[1] = -10 * L + local[1][0] * U;

	// best 40 elems
	// b->data[0] = -40 * L - local[0][0] * U;
	// b->data[1] = -80 * L + local[1][0] * U;

	// best 20 elems
	b->data[0] = -75 * L - local[0][0] * U;
	b->data[1] = -80 * L + local[1][0] * U;

	for (size_t i = 2; i < b->size - 1; i++) {
		// b->data[i] = 4 * L;
		b->data[i] = -10 * L;
	}

	// b->data[b->size - 1] = 2 * L + DU;

	// b->data[b->size - 1] = -10 * L + DU;
	// b->data[b->size - 2] = -5 * L + DU;
	b->data[b->size - 1] = 1.8*U;
	b->data[b->size - 2] = 1.8*U;

	gauss_straight(A, b);
	vector_t* res = gauss_backward(A, b);

	// res->data[0] = -U;
	// res->data[0] = -4.785534;

	res->data[res->size - 1] = U;

	printf("%lf\t", res->data[res->size - 3]);
	printf("%lf\t", res->data[res->size - 2]);
	printf("%lf\n", res->data[res->size - 1]);

	return res;
}

vector_t* prec_cube_solution(size_t elem_count) {
	double L = (END_X - START_X) / elem_count;

	// Локальная полная матрица жесткости
	double local[CUBE_MATRIX_DIM][CUBE_MATRIX_DIM] =
	{{37. / (10 * L) + 8 * 8 * L / 105., -189. / (40 * L) + 8 * 33 * L / 560.,
	27. / (20 * L) - 8 * 3 * L / 140., -13. / (40 * L) + 8 * 19 * L / 1680.},
	{-189. / (40 * L) + 8 * 33 * L / 560., 54. / (5 * L) + 8 * 27 * L / 70.,
	-297. / (40 * L) - 8 * 27 * L / 560., 27. / (20 * L) - 8 * 3 * L / (140.)},
	{27. / (20 * L) - 8 * 3 * L / 140., -297. / (40 * L) - 8 * 27 * L / 560.,
	54. / (5 * L) + 8 * 27 * L / 70., -189. / (40 * L) + 8 * 33 * L / 560.},
	{-13. / (40 * L) + 8 * 19 * L / 1680., 27. / (20 * L) - 8 * 3 * L / 140.,
	-189. / (40 * L) + 8 * 33 * L / 560., 37. / (10 * L) + 8 * 8 * L / 105.}};

	// Локальный полный вектор нагрузок
	double local_b[CUBE_MATRIX_DIM] = {-10. * L / 8., -10. * 3 * L / 8., -10. * 3 * L / 8., -10. * L / 8.};

	// Проходим методом Гаусса, чтобы исключить внутренние элементы
	double pivot = 0.0;
	for (size_t i = 1; i < CUBE_MATRIX_DIM - 1; i++) {
		for (size_t j = 0; j < CUBE_MATRIX_DIM; j++) {
			if (fabs(local[j][i]) < EPS) {
				continue;
			}
			if (i == j) {
				continue;
			}
			pivot = local[j][i] / local[i][i];
			local_b[j] -= pivot * local_b[i];
			for (size_t k = 0; k < CUBE_MATRIX_DIM; k++) {
				local[j][k] -= pivot * local[i][k];
			}
		}
	}

	// Локальная матрица жесткости для граничных узлов
	double local_min[LINEAR_MATRIX_DIM][LINEAR_MATRIX_DIM] = {{local[0][0], local[0][3]}, {local[3][0], local[3][3]}};
	
	// Локальная матрица нагрузок для граничных узлов
	double local_min_b[LINEAR_MATRIX_DIM] = {local_b[0], local_b[3]};

	size_t dots_cnt = elem_count + 1;
	matrix_t* A = create_matrix(dots_cnt, dots_cnt);
	vector_t* b = create_vector(dots_cnt);

	// Заполняем A
	for (size_t i = 0; i < elem_count; i++) {
		for (size_t j = 0; j < LINEAR_MATRIX_DIM; j++) {
			for (size_t k = 0; k < LINEAR_MATRIX_DIM; k++) {
				A->data[i + j][i + k] += local_min[j][k];
			}
		}
	}

	// граничные условия
	A->data[0][0] = DU;
	A->data[1][0] = U;

	// initial b
	// b->data[0] = local_min_b[0] - local_min[0][0] * U;
	// b->data[1] = local_min_b[1] + local_min_b[0] - local_min[1][0] * U;

	b->data[0] = -2*local_min[0][0] - local_min[0][0] - local_min[0][0] * DU;
	b->data[1] = 1.5*local_min[1][0] - local_min[0][0] * DU;

	// best 40 elems
	// b->data[0] = local_min[1][0] - local_min[0][0] + local_min_b[1] * DU;
	// b->data[1] = -local_min_b[1] - local_min[0][0] * DU;

	// good 20 elems (file ideal_cube20.c is the best)
	// b->data[0] = -local_min_b[1] + local_min[0][0] + local_min_b[1] * DU;
	// b->data[1] = local_min_b[0] + local_min_b[0] - local_min[0][0] * DU;

	printf("local min b: [0]: %lf\t[1]: %lf\n", local_min_b[0], local_min_b[1]);
	printf("local min: [0][0]: %lf\t[1][0]: %lf\n", local_min[0][0], local_min[1][0]);
	printf("U: %lf\tDU: %lf\n\n", U, DU);
	// printf("%lf\t", local_min_b[0]);
	// printf("%lf\t", local_min_b[1]);
	// printf("%lf\t", local_min[0][0]);
	// printf("%lf\t", local_min[1][0]);
	// printf("%lf\t", U);
	// printf("%lf\n", DU);

	// Заполняем b
	for (size_t i = 2; i < b->size - 1; i++) {
		b->data[i] = local_min_b[0] + local_min_b[0];
	}

	// b->data[b->size - 1] = local_min_b[1] + DU;

	b->data[b->size - 1] = local_min_b[0] + DU;
	b->data[b->size - 2] = local_min_b[1] + local_min_b[1] + DU;

	gauss_straight(A, b);
	vector_t* res = gauss_backward(A, b);
	// res->data[0] = -U;
	res->data[res->size - 1] = U;

	printf("Cube solution\n");
	printf("%lf\t", res->data[0]);
	printf("%lf\t", res->data[1]);
	printf("%lf\n", res->data[2]);

	// printf("%lf\t", res->data[res->size - 3]);
	// printf("%lf\t", res->data[res->size - 2]);
	// printf("%lf\n", res->data[res->size - 1]);

	return res;
}

/* Metric block */
double calc_absolute_error(vector_t* y_real, vector_t* y_pred) {
	double max_err = 0.0;
	for (size_t i = 0; i < y_real->size; i++) {
		double err = fabs(y_real->data[i] - y_pred->data[i]);
		if (err > max_err) {
			max_err = err;
		}
	}
	return max_err;
}

double calc_mean_squared_error(vector_t* y_real, vector_t* y_pred) {
	double sum = 0.0;
	for (size_t i = 0; i < y_real->size; i++) {
		double err = fabs(y_real->data[i] - y_pred->data[i]);
		sum += err * err;
	}
	return sum / y_real->size;
}

int main() {
	size_t elem_count = 20;
	// size_t elem_count = 40;
	// size_t elem_count = 500;
	// size_t elem_count = 3000;
	double L = (END_X - START_X) / elem_count;
	vector_t* x = create_vector(elem_count + 1);

	printf("x:\n");
	for (size_t i = 0; i < x->size; i++) {
		x->data[i] = START_X + i * L;
		printf("%lf\n", x->data[i]);
	}
	#ifdef CUBE
	vector_t* y = prec_cube_solution(elem_count);
	#else
	vector_t* y = prec_linear_solution(elem_count);
	#endif

	printf("predict:\n");
	FILE* gp = popen("gnuplot -persist", "w");
	fprintf(gp, "$predict << EOD\n");
	for (size_t i = 0; i < x->size; i++) {
		fprintf(gp, "%lf %lf\n", x->data[i], y->data[i]);
		printf("%lf\n", y->data[i]);
	}
	fprintf(gp, "EOD\n");

	printf("accurate:\n");
	vector_t* y_real = prec_accurate_solution(x);
	fprintf(gp, "$real << EOD\n");
	for (size_t i = 0; i < x->size; i++) {
		fprintf(gp, "%lf %lf\n", x->data[i], y_real->data[i]);
		printf("%lf\n", y_real->data[i]);
	}
	fprintf(gp, "EOD\n");
	fprintf(gp, "plot '$predict' using 1:2 with lines title 'MFE (%zu elem.)', '$real' using 1:2 with lines title 'accurate (%zu elem)',\n", elem_count, elem_count);
	//fprintf(gp, "plot '$predict' using 1:2 with lines title 'accurate (%zu elem.)',\n", elem_count);
	printf("Absolute error: %lf\n", calc_absolute_error(y_real, y));
	printf("Mean-Squared Error: %lf\n", calc_mean_squared_error(y_real, y));
}