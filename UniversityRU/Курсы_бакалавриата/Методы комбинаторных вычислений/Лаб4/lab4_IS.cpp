#include <stdio.h>
#include <math.h>
#include <stdlib.h>


#define P_ARG(k) (3*(k)*(k)-(k))/2

#define N 18
#define M 9


int vectorOut(int* vector, int size) {

    for (int i = 0; i < size; i++) {
        
        printf("%d ", vector[i]);
    }
    printf("\n");
    return 0;
}


int partitionSum(int number) {

    int k = 1;
    int sum = 0;

    if(number == 0) {
        return 1;
    }

    while((P_ARG(-k) <= number)||(P_ARG(k) <= number)) {

        if (P_ARG(k) <= number) {
            sum += (partitionSum(number - P_ARG(k)) * pow(-1, k-1));
        }
        if (P_ARG(-k) <= number) {
            sum += (partitionSum(number - P_ARG(-k)) * pow(-1, k-1));
        }
        k++;

    }

    return sum;
}


int Hindenburg(int number) {

    int m = 1;
    int j = 0;
    int sum = 0;
    int new_value = 0;

    int* partition = (int*)malloc(m * sizeof(int));

    partition[0] = number;

    vectorOut(partition, m);


    while(partition[m-1] != 1) {

        sum = 0;
        j = 0;

        if(partition[m-1] - partition[0] <= 1) {

            m++;
            partition = (int*)realloc(partition, m * sizeof(int));
            for (int i = 0; i < m-1; i++) {
                partition[i] = 1;
            }
            partition[m-1] = number - (m-1);
            vectorOut(partition, m);
            continue;
        }

        int i = m-2;
        while(partition[m-1] - partition[i] < 2) {
            i--;
        }
        j = i;
        new_value = partition[j] + 1;

        for (int i = j; i < m-1; i++) {
            partition[i] = new_value; 
        }
        for (int i = 0; i < m-1; i++) {
            sum += partition[i];
        }
        partition[m-1] = number - sum;

        vectorOut(partition, m);

    }

    free(partition);

    return 0;
}


int getMultiSum(int* k, int size) {

    int multi_sum = 0;
    for (int i = 0; i < size; i++) {
        multi_sum += k[i] * (i+1);
    }
    return multi_sum;
}


int searchForLast(int* k, int size, int* last, int* second_last) {

    for (int i = 0; i < size; i++) {
        if(k[i]) {
            (*last) = i;
            break;
        }
    }
    (*second_last) = (*last);

    for (int i = (*last) + 1; i < size; i++) {
        if(k[i]) {
            (*second_last) = i;
            break;
        }
    }
    return 0;
}


int output(int* k, int size) {
    for (int i = size-1; i >= 0; i--) {
        if(k[i]) {

            printf(" %d*%d +", k[i], i+1);
        }
    }
    printf("\b \n");

    return 0;
}


void Ehrlich(int number) {

    int* k = (int*)malloc(number * sizeof(int)); // vector of multipliers
    int m = 0; // index of the last multinumber
    int prev = 0; // index of the second last multinumber
    int multi_sum = 0;

    for (int i = 1; i < number; i++) {
        k[i] = 0;
    }
    k[0] = number;

    output(k, number);

    while(k[number-1] != 1) {

        if(k[m] > 1) {
            k[m] = 0;
            k[m+1] += 1;
            searchForLast(k, number, &m, &prev);
        } else {

            if(k[m] == 1) {
                searchForLast(k, number, &m, &prev);
                k[prev+1] += 1;
                k[m] = 0;
                k[prev] = 0;
                searchForLast(k, number, &m, &prev);
            }
        }

        if((multi_sum = getMultiSum(k, number)) != number) {
            k[0] += number - multi_sum;
            searchForLast(k, number, &m, &prev);
        }
        output(k, number);
    }
}


int main(int argc, char** argv)
{
    int number = N;
    int partition_sum = partitionSum(number);

    printf("Количество разбиений для числа %d: %d\n", number, partition_sum);

    number = M;
    printf("Алгоритм Гинденбурга для числа 9\n");
    Hindenburg(number);

    printf("Алгоритм Эрлиха для числа 9\n");
    Ehrlich(number);

    return 0;
}
