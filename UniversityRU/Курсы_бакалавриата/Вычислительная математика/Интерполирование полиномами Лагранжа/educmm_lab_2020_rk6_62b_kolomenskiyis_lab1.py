import numpy as np
import matplotlib.pyplot as plt
import math


def l_i(i, x, x_nodes):  # i = x index
    chisl = 1
    znam = 1
    for j in range(len(x_nodes)):
        if i != j:
            chisl *= x - x_nodes[j]
            znam *= x_nodes[i] - x_nodes[j]
    return chisl / znam


def L(x, x_nodes, y_nodes):
    int_pol = 0
    for i in range(len(y_nodes)):
        int_pol += y_nodes[i] * l_i(i, x, x_nodes)
    return int_pol


def e_func(x):
    if isinstance(x, (list, np.ndarray)):
        res = []
        for i in x:
            res.append(np.exp(-i**2))
        return res
    else:
        return np.exp(-x ** 2)


def cheb_value(i, n):
    return math.cos(math.pi / n * (0.5 + i))


def piecewise_linear_approx(x, x_nodes, y_nodes):
    for i in range(len(x_nodes) - 1):
        if x_nodes[i] <= x <= x_nodes[i + 1]:
            k = (y_nodes[i] - y_nodes[i + 1]) / (x_nodes[i] - x_nodes[i + 1])
            b = y_nodes[i + 1] - k * x_nodes[i + 1]
            return k * x + b


def erf_func(N, x_nodes, y_nodes):
    h = 4. / N
    appr_sum = (piecewise_linear_approx(x_nodes[0], x_nodes, y_nodes) + piecewise_linear_approx(x_nodes[-1], x_nodes,
                                                                                                y_nodes)) / 2
    for i in range(1, N):
        appr_sum += piecewise_linear_approx((-2. + i * h), x_nodes, y_nodes)

    return appr_sum * h


if __name__ == '__main__':
    x_min = -5
    x_max = 5
    nodes_min = 4
    nodes_max = 20
    n_chart = 200
    linear_x_list = np.linspace(x_min, x_max, n_chart)
    linear_y_list = e_func(linear_x_list)

    # 1) интерполянты Лагранжа
    for i in range(nodes_min, nodes_max + 1):
        x = np.linspace(x_min, x_max, i)
        y = e_func(x)
        lagrange_y_list = [L(i, x, y) for i in linear_x_list]

        plt.plot(x, y, 'o', linear_x_list, linear_y_list, linear_x_list, lagrange_y_list)
        plt.legend(("f(x)⋂L(x)", "f(x)", "L(x), N={}".format(i)), loc='best')
        plt.grid()
        plt.xlabel('x')
        plt.ylabel('y')
        plt.show()

    list_x = []
    list_y = []
    polinom_y_list = []
    distance = [0] * (nodes_max - nodes_min + 1)
    Num = [0] * (nodes_max - nodes_min + 1)

    for i in range(nodes_min, nodes_max + 1):
        Num[i - nodes_min] = i
        list_x = np.linspace(x_min, x_max, i)
        list_y = e_func(list_x)
        lagrange_y_list = [L(j, list_x, list_y) for j in linear_x_list]
        distance[i - nodes_min] = 0
        for j in range(n_chart):
            if abs(linear_y_list[j] - lagrange_y_list[j]) > distance[i - nodes_min]:
                distance[i - nodes_min] = abs(linear_y_list[j] - lagrange_y_list[j])

    plt.plot(Num, distance)
    plt.legend(("Численно найденная погрешность аппроксимации",), loc='best')
    plt.grid()
    plt.xlabel('Количество узлов N')
    plt.ylabel('Вычисленное значение')
    plt.show()

    list_of_coeff = [0] * (nodes_max + 1)
    list_of_coeff[0] = [0] * (nodes_max + 2)
    list_of_coeff[0][0] = 1
    for i in range(1, nodes_max + 2):
        list_of_coeff[0][i] = 0
    for i in range(1, nodes_max + 1):
        list_of_coeff[i] = [0] * (nodes_max + 2)
        for j in range(nodes_max + 1):
            list_of_coeff[i][j] = (j + 1) * list_of_coeff[i - 1][j + 1] - 2 * (list_of_coeff[i - 1][j - 1])

    maxes_dif = [0] * (nodes_max - nodes_min + 1)

    for m in range(nodes_min, nodes_max + 1):
        i = m - nodes_min
        maxes_dif[i] = 0
        dif_y = 0
        for j in range(n_chart):
            for k in range(nodes_max + 2):
                dif_y += list_of_coeff[m][k] * (linear_x_list[j] ** k)
            dif_y *= e_func(linear_x_list[j])
            if abs(dif_y) > maxes_dif[i]:
                maxes_dif[i] = abs(dif_y)

    list_of_factorial = []
    for i in range(nodes_max + 1):
        list_of_factorial.append(math.factorial(i))

    for m in range(nodes_min, nodes_max + 1):
        i = m - nodes_min
        maxes_dif[i] /= list_of_factorial[m]

    x_opt = [0] * (nodes_max - nodes_min + 1)

    list_x = []
    list_y = []
    polinom_y_list = []

    for m in range(nodes_min, nodes_max + 1):
        i = m - nodes_min
        x_opt[i] = 0
        list_x = np.linspace(x_min, x_max, m)
        list_y = e_func(list_x)
        lagrange_y_list = [L(j, list_x, list_y) for j in linear_x_list]
        for j in linear_x_list:
            tmp = 1
            for k in list_x:
                tmp = tmp * (j - k)
            if abs(tmp) > x_opt[i]:
                x_opt[i] = abs(tmp)

    linear_ost = [0] * (nodes_max - nodes_min + 1)
    for i in range(nodes_max - nodes_min + 1):
        linear_ost[i] = maxes_dif[i] * x_opt[i]

    plt.plot(Num, linear_ost)
    plt.legend(("Аналитически найденная погрешность аппроксимации",), loc='best')
    plt.grid()
    plt.xlabel('Количество узлов N')
    plt.ylabel('Вычисленное значение')
    plt.show()

    plt.semilogy(Num, linear_ost, Num, distance)
    plt.legend(("Аналитически найденная погрешность аппроксимации",
                "Численно найденная погрешность аппроксимации"), loc='best')
    plt.grid()
    plt.xlabel('Количество узлов N')
    plt.ylabel('Вычисленное значение')
    plt.show()

    # 2) интерполянты Лагранжа при оптимальном расположении узлов
    for i in range(nodes_min, nodes_max + 1):
        cheb_values_by_x = np.array([cheb_value(j, i) for j in range(i)])
        cheb_values_by_x = ((x_min - x_max) / 2) * cheb_values_by_x + (x_min + x_max) / 2
        cheb_values_by_y = e_func(cheb_values_by_x)
        cheb_y_list = [L(j, cheb_values_by_x, cheb_values_by_y) for j in linear_x_list]

        plt.plot(cheb_values_by_x, cheb_values_by_y, 'o',
                 linear_x_list, linear_y_list,
                 linear_x_list, cheb_y_list)
        plt.legend(("f(x)⋂L(x)", "f(x)", "L(x), N={}".format(i)), loc='best')
        plt.grid()
        plt.xlabel('x')
        plt.ylabel('y')
        plt.show()

    cheb_list_x = []
    cheb_list_y = []
    cheb_distance = [0] * (nodes_max - nodes_min + 1)
    Num = [0] * (nodes_max - nodes_min + 1)

    for i in range(nodes_min, nodes_max + 1):
        Num[i - nodes_min] = i
        cheb_list_x = np.array([cheb_value(j, i) for j in range(i)])
        cheb_list_x = ((x_min - x_max) / 2) * cheb_list_x + (x_min + x_max) / 2
        cheb_list_y = e_func(cheb_list_x)
        lagrange_y_list = [L(j, cheb_list_x, cheb_list_y) for j in linear_x_list]
        cheb_distance[i - nodes_min] = 0
        for j in range(n_chart):
            if abs(linear_y_list[j] - lagrange_y_list[j]) > cheb_distance[i - nodes_min]:
                cheb_distance[i - nodes_min] = abs(linear_y_list[j] - lagrange_y_list[j])

    plt.plot(Num, cheb_distance)
    plt.legend(("Численно найденная погрешность аппроксимации",), loc='best')
    plt.grid()
    plt.xlabel('Количество узлов N')
    plt.ylabel('Вычисленное значение')
    plt.show()

    list_of_power = [0] * (nodes_max + 1)
    list_of_power[0] = 1

    for i in range(1, nodes_max + 1):
        list_of_power[i] = list_of_power[i - 1] * 2

    chebishev_ost = [0] * (nodes_max - nodes_min + 1)
    for i in range(nodes_max - nodes_min + 1):
        chebishev_ost[i] = maxes_dif[i] / list_of_power[i + 4]

    plt.semilogy(Num, chebishev_ost)
    plt.legend(("Аналитически найденная погрешность аппроксимации",), loc='best')
    plt.grid()
    plt.xlabel('Количество узлов N')
    plt.ylabel('Вычисленное значение')
    plt.show()

    plt.plot(Num, chebishev_ost, Num, cheb_distance)
    plt.legend(("Аналитически найденная погрешность аппроксимации",
                "Численно найденная погрешность аппроксимации"), loc='best')
    plt.grid()
    plt.xlabel('Количество узлов N')
    plt.ylabel('Вычисленное значение')
    plt.show()

    # 3) кусочно-линейная интерполяция
    for i in range(nodes_min, nodes_max + 1):
        x = np.linspace(x_min, x_max, i)
        y = e_func(x)
        piecewise_linear_approx_y_list = [piecewise_linear_approx(i, x, y) for i in linear_x_list]

        plt.plot(x, y, 'o', linear_x_list, linear_y_list, linear_x_list, piecewise_linear_approx_y_list)
        plt.legend(("f(x)⋂P(x)", "f(x)", "P(x), N={}".format(i)), loc='best')
        plt.grid()
        plt.xlabel('x')
        plt.ylabel('y')
        plt.show()

    line_list_x = []
    line_list_y = []
    line_y_list = []
    piecewise_linear_approx_distance = [0] * (nodes_max - nodes_min + 1)
    Num = [0] * (nodes_max - nodes_min + 1)

    for i in range(nodes_min, nodes_max + 1):
        Num[i - nodes_min] = i
        line_list_x = np.linspace(x_min, x_max, i)
        line_list_y = e_func(line_list_x)
        line_y_list = [piecewise_linear_approx(j, line_list_x, line_list_y) for j in linear_x_list]
        piecewise_linear_approx_distance[i - nodes_min] = 0
        for j in range(n_chart):
            if abs(linear_y_list[j] - line_y_list[j]) > piecewise_linear_approx_distance[i - nodes_min]:
                piecewise_linear_approx_distance[i - nodes_min] = abs(linear_y_list[j] - line_y_list[j])

    plt.plot(Num, piecewise_linear_approx_distance)
    plt.legend(("Численно найденная погрешность аппроксимации",), loc='best')
    plt.grid()
    plt.xlabel('Количество узлов N')
    plt.ylabel('Вычисленное значение')
    plt.show()

    piecewise_linear_approx_ost = [0] * (nodes_max - nodes_min + 1)

    for i in range(nodes_min, nodes_max + 1):
        piecewise_linear_approx_ost[i - nodes_min] = ((x_max - x_min) * (x_max - x_min)) / ((i - 1) * (i - 1) * 4)

    plt.plot(Num, piecewise_linear_approx_ost)
    plt.legend(("Аналитически найденная погрешность аппроксимации",), loc='best')
    plt.grid()
    plt.xlabel('Количество узлов N')
    plt.ylabel('Вычисленное значение')
    plt.show()

    plt.plot(Num, piecewise_linear_approx_ost, Num, piecewise_linear_approx_distance)
    plt.legend(("Аналитически найденная погрешность аппроксимации",
                "Численно найденная погрешность аппроксимации"), loc='best')
    plt.grid()
    plt.xlabel('Количество узлов N')
    plt.ylabel('Вычисленное значение')
    plt.show()

    plt.semilogy(Num, piecewise_linear_approx_distance, Num, cheb_distance, Num, distance)
    plt.legend(("Погрешность кусочно-линейной аппроксимации",
                "Погрешность при чебышевском распределении",
                "Погрешность при равномерном распределении"), loc='best')
    plt.grid()
    plt.xlabel('Количество узлов N')
    plt.ylabel('Расстояние в лебеговом пространстве')
    plt.show()

    N = [3, 5, 7, 9]
    erf_list = np.zeros(len(N))
    for i in range(len(erf_list)):
        x_nodes = np.linspace(-2., 2., N[i])
        y_nodes = np.exp(-x_nodes**2)
        erf_list[i] = erf_func(N[i], x_nodes, y_nodes) / math.sqrt(math.pi)
    plt.plot(N, erf_list, 'o')
    plt.legend(("Приближенное значение функции ошибок при x=2",), loc='best')
    plt.grid()
    plt.xlabel('Количество узлов N')
    plt.ylabel('Значение функции erf(x)')
    plt.show()
