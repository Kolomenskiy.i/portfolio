#include <stdio.h>


int calc_mod(int base, int pow, int mod) {
	int result = base;
	for (int i = 2; i <= pow; i++) {
		result = result * base % mod;
	}
	return result;
}

/* расширенный алгоритм Эвклида */
int extended_euclid(int a, int b) {
	int d = 0;
	int x1 = 0, x2 = 1, y1 = 1;
	int temp_b = b;
	while (a > 0) {
		int temp1 = temp_b / a;
		int temp2 = temp_b - temp1 * a;
		temp_b = a;
		a = temp2;

		int x = x2 - temp1 * x1;
		int y = d - temp1 * y1;

		x2 = x1;
		x1 = x;
		d = y1;
		y1 = y;
	}
	if (d <= 0) {
		return d + b;
	}
	return d;
}

/* Алгоритм Эвклида для вычисления НОД */
int gcd(int a, int b) {
	while (b != 0) {
		int temp = a;
		a = b;
		b = temp % b;
	}
	return a;
}

int choose_coprime(int n) {
	for (int i = 2; i * i <= n; i++) {
		if (gcd(n, i) == 1) {
			return i;
		}
	}
	return 1;
}

int main() {
	// Выбираем два простых числа (в реальности они должны быть
	// большими)
	int p = 17;
	int q = 19;
	// Модуль сравнения
	int n = p * q;
	// Значение функции Эйлера
	int euler = (p - 1) * (q - 1);
	// Выбираем взаимнопростое с euler (открытый ключ)
	// Опять же в реальности среди всех простых выбирается одно
	// Случайным образом
	int y = choose_coprime(euler);
	// Секретный ключ выбираем по расширенному алгоритму Эйлера
	int x = extended_euclid(y, euler);
	printf("Public key: %d, private key: %d\n", y, x);
	// В качестве примера зашифруем число меньшее n с помощью
	// открытого ключа y и расшифруем с помощью x
	// В реальности сообщение бьется на блоки меньшие n и блоки 
	// шифруются отдельно
	int message = 101;
	int encoded = calc_mod(message, y, n);
	int decoded = calc_mod(encoded, x, n);
	printf("Message: %d Encoded: %d Decoded: %d\n", message, encoded, decoded);
}