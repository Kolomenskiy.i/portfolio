#include <stdio.h>

int calc_mod(int base, int pow, int mod) {
	int result = base;
	for (int i = 2; i <= pow; i++) {
		result = result * base % mod;
	}
	return result;
}

int main() {
	// У обоих сторон есть публичные ключи, которые можно
	// передать в чистом виде
	int public1 = 101;
	int public2 = 57;
	// И секретные ключи
	int secret1 = 257;
	int secret2 = 513;

	// Вычисляем числа которые мы будем передавать другой стороне
	int A = calc_mod(public1, secret1, public2);
	int B = calc_mod(public1, secret2, public2);

	// теперь у обоих сторон есть A, B и публичные части ключей
	// Вычисляем общий секретный ключ на одной стороне
	int key1 = calc_mod(B, secret1, public2);
	int key2 = calc_mod(A, secret2, public2);
	printf("Key1:%d Key2:%d\n", key1, key2);
}