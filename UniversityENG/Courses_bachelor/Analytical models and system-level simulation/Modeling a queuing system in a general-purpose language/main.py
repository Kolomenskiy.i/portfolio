import numpy as np
import matplotlib.pyplot as plt
import random as rnd

from matplotlib import rc
 
font = {'family': 'verdana',
        'weight': 'normal'}
rc('font', **font)

modelingTime = 180 # 3 часа в минутах
deltaT = 0.01 # Шаг по времени 0.01 минуты
deligationMin = 3 # Минимальное количество человек в делегации
deligationMax = 5 # Максимальное еоличество человек в делегации
cup = deligationMax * 4.0 # Общее количество чашек
Tdeligation = 6 # Среднее время прихода делигации
Ttea = 6 # Среднее время обслуживания делигации
Twash = 10 # Среднее время мытья чашек
cupDirty = 0.5 # Отношение грязных чашек к чистым, при котором грязные чашки забирают мыть



class GeneratorTime: # Генератор времени прихода делегации
    def __init__(self, T):
        self.T = T
    def generate(self, time):
        timeDistribution = [0]
        while timeDistribution[-1] < time:
            timeDistribution.append(timeDistribution[-1] + np.random.normal(loc=self.T, scale=(1+self.T*0.05)))
        return timeDistribution

class GeneratorManCount:
    def __init__(self, min, max):
        self.min = min
        self.max = max
    def generate(self, length):
        manDistribution = [rnd.randint(self.min, self.max) for i in range(length)]
        return manDistribution

class Cup:
    def __init__(self, count):
        self.count = count
        self.washingTime = Twash

class Deligation:
    def __init__(self, count):
        self.count = count
        self.cupTime = np.random.normal(loc=Ttea, scale=(1+Ttea*0.05))

class SYGNAL:
    def __init__(self, text):
        self.text = text

class CEC: # Current Event Chian
    def __init__(self):
        self.currentCup = []
        self.currentDeligation = []

    def step(self, delta):
        dirtyCups = 0
        for index, deligation in enumerate(self.currentDeligation):
            deligation.cupTime -= delta
            if (deligation.cupTime <= 0):
                dirtyCups += deligation.count
                self.currentDeligation.pop(index)

        clearCups = 0
        for index, cup in enumerate(self.currentCup):
            cup.washingTime -= delta
            if (cup.washingTime <= 0):
                clearCups += cup.count
                self.currentCup.pop(index)

        return dirtyCups, clearCups

    def addDeligation(self, deligation, currentClearCup):
        if(deligation.count > currentClearCup):
            return SYGNAL("All cups are dirty")
        self.currentDeligation.append(deligation)

    def addCup(self, cup):
        self.currentCup.append(cup)


class FEC: # Future Event Chain
    def __init__(self):
        self.queue = []

    def step(self, delta, cec, cups):
        err = None
        if(len(cec.currentDeligation) == 0):
            if(len(self.queue) > 0):
                err = cec.addDeligation(self.queue[0], cups)
                self.queue.pop(0)
        return err
    
    def add(self, deligation):
        self.queue.append(deligation)



def model(maxTime):

    time = GeneratorTime(Tdeligation).generate(modelingTime)
    man = GeneratorManCount(deligationMin, deligationMax).generate(len(time))
    index = 0
    fec = FEC()
    cec = CEC()
    currentClearCup = cup
    currentTime = 0

    times = []
    cupsDirty = []
    cupsClear = []
    deligations = []

    while currentTime < maxTime:
        currentTime += deltaT
        if(currentTime > time[index]):
            deligation = Deligation(man[index])
            index += 1
            fec.add(deligation)

        dirty, clear = cec.step(deltaT)
        currentClearCup += clear - dirty
        if(((cup - currentClearCup)/cup >= cupDirty) and (len(cec.currentCup) == 0)):
            cec.addCup(Cup(cup-currentClearCup))

        err = fec.step(deltaT, cec, currentClearCup)
        if(err is not None):
            times.append(currentTime)
            cupsDirty.append(cup - currentClearCup)
            cupsClear.append(currentClearCup)
            if(len(cec.currentDeligation) > 0):
                deligations.append(cec.currentDeligation[0].count)
            else:
                deligations.append(0)
            return times, cupsDirty, cupsClear, deligations

        times.append(currentTime)
        cupsDirty.append(cup - currentClearCup)
        cupsClear.append(currentClearCup)
        if(len(cec.currentDeligation) > 0):
            deligations.append(cec.currentDeligation[0].count)
        else:
            deligations.append(0)

    return times, cupsDirty, cupsClear, deligations

time, dirtyCups, clearCups, deligation = model(modelingTime)

fig, ax = plt.subplots()
ax.plot(time, dirtyCups, label='Грязные чашки')
ax.plot(time, clearCups, label='Чистых чашки')
ax.plot(time, deligation, label='Человек в комнате')
ax.set_xlabel('Время')
ax.set_ylabel('Количество')
ax.grid(True)
ax.legend()
plt.show()