import matplotlib
import matplotlib.pyplot as plt
import math as math
import mpmath as mpmath


dpi_size = 100


def find_p0(lamb, mu, n):
    p_i = lamb / mu
    result = 1
    for i in range(2, n + 1):
        p_i, result = p_i * lamb / (mu * i), result + p_i
    return 1 / (result + p_i)


def find_p_N_k(lamb, mu, n):
    P = [0]*(n + 1)
    P[0] = find_p0(lamb, mu, n)
    N = 0
    for i in range(1, n + 1):
        P[i] = P[i - 1] * lamb / (mu * i)
        N += i * P[i]
    k = N / n
    s = 0
    for i in range(n + 1):
        s += P[i]
    #print(s)
    return (P[n], N, k)


def show_without_queue(lamb, mu, p_lim):
    start = find_p_N_k(lamb, mu, 1)
    p_refuse = [start[0]]
    N = [start[1]]
    k = [start[2]]
    amount = [1]
    i = 1
    while p_refuse[i - 1] > p_lim:
        start = find_p_N_k(lamb, mu, i + 1)
        p_refuse.append(start[0])
        N.append(start[1])
        k.append(start[2])
        amount.append(i + 1)
        i += 1
    xlabel = "Кол-во операторов"
    ylabel = "Вероятность отказа"
    font_size = 14
    fig1, axis_P = plt.subplots(nrows=1, ncols=1, figsize=(16, 8))
    axis_P.plot(amount, p_refuse)
    axis_P.scatter(amount, p_refuse)
    axis_P.set_xlabel(xlabel, fontsize = font_size)
    axis_P.set_ylabel(ylabel, fontsize = font_size)
    axis_P.grid()
    plt.xlim(1, amount[-1])
    plt.ylim(0, 1.1)
    fig1.savefig("exersize_1_P_ref.png", dpi = dpi_size)
    
    ylabel = "Мат. ожидание кол-ва занятых операторов"
    fig2, axis_N = plt.subplots(nrows=1, ncols=1, figsize=(16, 8))
    axis_N.plot(amount, N)
    axis_N.scatter(amount, N)
    axis_N.set_xlabel(xlabel, fontsize = font_size)
    axis_N.set_ylabel(ylabel, fontsize = font_size)
    axis_N.grid()
    plt.xlim(1, amount[-1])
    plt.ylim(0, 1.1 * max(N))
    fig2.savefig("exersize_1_N.png", dpi = dpi_size)
    
    ylabel = "Коэффициент загрузки операторов"
    fig3, axis_k = plt.subplots(nrows=1, ncols=1, figsize=(16, 8))
    axis_k.plot(amount, k)
    axis_k.scatter(amount, k)
    axis_k.set_xlabel(xlabel, fontsize = font_size)
    axis_k.set_ylabel(ylabel, fontsize = font_size)
    axis_k.grid()
    plt.xlim(1, amount[-1])
    plt.ylim(0, 1.1)
    fig3.savefig("exersize_1_k.png", dpi = dpi_size)
    
    print("amount | P_refuse |    N    |    k    |")
    print("---------------------------------------")
    for i in range(len(amount)):
        print(" %4d  | %.5f  | %.5f | %.5f |" % (amount[i], p_refuse[i], N[i], k[i]))
    return amount[-1]


T_c = 29
T_s = 221
mu = 1 / T_s
lamb = 1 / T_c
p_lim = 0.01
operator_amount = show_without_queue(lamb, mu, p_lim)


def find_p0_with_queue(lamb, mu, n, m):
    p_i = lamb / mu
    result = 1 + p_i
    for i in range(2, n + 1):
        p_i = p_i * lamb / (mu * i)
        result += p_i
    for i in range(m):
        p_i = p_i * lamb / (mu * n)
        result += p_i
    return 1 / result


def find_pref_pqueue_N_kN_Q_kQ(lamb, mu, n, m):
    P = [0]*(n + m + 1)
    P[0] = find_p0_with_queue(lamb, mu, n, m)
    N = 0
    Q = 0
    P_queue = 0
    for i in range(1, n + 1):
        P[i] = P[i - 1] * lamb / (mu * i)
        N += i * P[i]
    for i in range(1, m + 1):
        P[n + i] = P[n + i - 1] * lamb / (mu * n)
        N += n * P[n + i]
        Q += i * P[n + i]
        P_queue += P[n + i]
    k_N = N / n
    k_Q = Q / m
    s = 0
    for i in range(n + m + 1):
        s += P[i]
    #print(s)
    return (P[n + m], P_queue, N, k_N, Q, k_Q)


def show_with_queue(lamb, mu, op_amount):
    p_refuse_op = [[0] * op_amount for i in range(op_amount)]
    p_queue_op = [[0] * op_amount for i in range(op_amount)]
    N_op = [[0] * op_amount for i in range(op_amount)]
    k_N_op = [[0] * op_amount for i in range(op_amount)]
    Q_op = [[0] * op_amount for i in range(op_amount)]
    k_Q_op = [[0] * op_amount for i in range(op_amount)]
    p_refuse_qu = [[0] * op_amount for i in range(op_amount)]
    p_queue_qu = [[0] * op_amount for i in range(op_amount)]
    N_qu = [[0] * op_amount for i in range(op_amount)]
    k_N_qu = [[0] * op_amount for i in range(op_amount)]
    Q_qu = [[0] * op_amount for i in range(op_amount)]
    k_Q_qu = [[0] * op_amount for i in range(op_amount)]
    amount = [i for i in range(1, op_amount + 1)]
    for i in range(op_amount):
        for j in range(op_amount):
            helper = find_pref_pqueue_N_kN_Q_kQ(lamb, mu, j + 1, i + 1)
            p_refuse_op[i][j] = helper[0]
            p_queue_op[i][j] = helper[1]
            N_op[i][j] = helper[2]
            k_N_op[i][j] = helper[3]
            Q_op[i][j] = helper[4]
            k_Q_op[i][j] = helper[5]
            p_refuse_qu[j][i] = helper[0]
            p_queue_qu[j][i] = helper[1]
            N_qu[j][i] = helper[2]
            k_N_qu[j][i] = helper[3]
            Q_qu[j][i] = helper[4]
            k_Q_qu[j][i] = helper[5]
            
    xlabel_qu = "Число мест в очереди"
    size_x = 16
    size_y = 8
    font_size = 14
    fig1, axis_P_ref_qu = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_P_ref_qu.plot(amount, p_refuse_qu[i], "-o", label = "n = {}".format(i + 1))
    axis_P_ref_qu.set_xlabel(xlabel_qu, fontsize = font_size)
    axis_P_ref_qu.set_ylabel("Вероятность отказа", fontsize = font_size)
    axis_P_ref_qu.legend(fontsize = 12)
    axis_P_ref_qu.grid()
    plt.xlim(1, op_amount)
    plt.ylim(0, 1.1)
    fig1.savefig("exersize_1_2_qu_P_ref.png", dpi = dpi_size)

    fig2, axis_P_queue_qu = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_P_queue_qu.plot(amount, p_queue_qu[i],"-o" , label = "n = {}".format(i + 1))
    axis_P_queue_qu.set_xlabel(xlabel_qu, fontsize = font_size)
    axis_P_queue_qu.set_ylabel("Вероятность существования очереди", fontsize = font_size)
    axis_P_queue_qu.legend(fontsize = 12)
    axis_P_queue_qu.grid()
    plt.xlim(1, op_amount)
    plt.ylim(0, 1.1)
    fig2.savefig("exersize_1_2_qu_P_queue.png", dpi = dpi_size)
    
    fig3, axis_N_qu = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_N_qu.plot(amount, N_qu[i], "-o", label = "n = {}".format(i + 1))
    axis_N_qu.set_xlabel(xlabel_qu, fontsize = font_size)
    axis_N_qu.set_ylabel("Мат. ожидание числа занятых операторов", fontsize = font_size)
    axis_N_qu.legend(fontsize = 12)
    axis_N_qu.grid()
    plt.xlim(1, op_amount)
    max_N_qu = 0
    for i in range(op_amount):
        for j in range(op_amount):
            if (max_N_qu < N_qu[i][j]):
                max_N_qu = N_qu[i][j]
    plt.ylim(0, 1.1 * max_N_qu)
    fig3.savefig("exersize_1_2_qu_N.png", dpi = dpi_size)
    
    fig4, axis_k_N_qu = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_k_N_qu.plot(amount, k_N_qu[i], "-o", label = "n = {}".format(i + 1))
    axis_k_N_qu.set_xlabel(xlabel_qu, fontsize = font_size)
    axis_k_N_qu.set_ylabel("Коэффициент занятости операторов", fontsize = font_size)
    axis_k_N_qu.legend(fontsize = 12)
    axis_k_N_qu.grid()
    plt.xlim(1, op_amount)
    plt.ylim(0, 1.1)
    fig4.savefig("exersize_1_2_qu_k_N.png", dpi = dpi_size)
    
    fig5, axis_Q_qu = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_Q_qu.plot(amount, Q_qu[i], "-o", label = "n = {}".format(i + 1))
    axis_Q_qu.set_xlabel(xlabel_qu, fontsize = font_size)
    axis_Q_qu.set_ylabel("Мат. ожидание длины очереди", fontsize = font_size)
    axis_Q_qu.legend(fontsize = 12)
    axis_Q_qu.grid()
    plt.xlim(1, op_amount)
    max_Q_qu = 0
    for i in range(op_amount):
        for j in range(op_amount):
            if (max_Q_qu < Q_qu[i][j]):
                max_Q_qu = Q_qu[i][j]
    plt.ylim(0, 1.1 * max_Q_qu)
    fig5.savefig("exersize_1_2_qu_Q.png", dpi = dpi_size)
    
    fig6, axis_k_Q_qu = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_k_Q_qu.plot(amount, k_Q_qu[i], "-o", label = "n = {}".format(i + 1))
    axis_k_Q_qu.set_xlabel(xlabel_qu, fontsize = font_size)
    axis_k_Q_qu.set_ylabel("Коэффициент занятости мест в очереди", fontsize = font_size)
    axis_k_Q_qu.legend(fontsize = 12)
    axis_k_Q_qu.grid()
    plt.xlim(1, op_amount)
    plt.ylim(0, 1.1)
    fig6.savefig("exersize_1_2_qu_k_Q.png", dpi = dpi_size)
    
    xlabel_op = "Кол-во операторов"
    fig7, axis_P_ref_op = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_P_ref_op.plot(amount, p_refuse_op[i], "-o", label = "m = {}".format(i + 1))
    axis_P_ref_op.set_xlabel(xlabel_op, fontsize = font_size)
    axis_P_ref_op.set_ylabel("Вероятность отказа", fontsize = font_size)
    axis_P_ref_op.legend(fontsize = 12)
    axis_P_ref_op.grid()
    plt.xlim(1, op_amount)
    plt.ylim(0, 1.1)
    fig7.savefig("exersize_1_2_op_P_ref.png", dpi = dpi_size)
    
    fig8, axis_P_queue_op = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_P_queue_op.plot(amount, p_queue_op[i], "-o", label = "m = {}".format(i + 1))
    axis_P_queue_op.set_xlabel(xlabel_op, fontsize = font_size)
    axis_P_queue_op.set_ylabel("Вероятность существования очереди", fontsize = font_size)
    axis_P_queue_op.legend(fontsize = 12)
    axis_P_queue_op.grid()
    plt.xlim(1, op_amount)
    plt.ylim(0, 1.1)
    fig8.savefig("exersize_1_2_op_P_queue.png", dpi = dpi_size)
    
    fig9, axis_N_op = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_N_op.plot(amount, N_op[i], "-o", label = "m = {}".format(i + 1))
    axis_N_op.set_xlabel(xlabel_op, fontsize = font_size)
    axis_N_op.set_ylabel("Мат. ожидание числа занятых операторов", fontsize = font_size)
    axis_N_op.legend(fontsize = 12)
    axis_N_op.grid()
    plt.xlim(1, op_amount)
    max_N_op = 0
    for i in range(op_amount):
        for j in range(op_amount):
            if (max_N_op < N_op[i][j]):
                max_N_op = N_op[i][j]
    plt.ylim(0, 1.1 * max_N_op)
    fig9.savefig("exersize_1_2_op_N.png", dpi = dpi_size)
    
    fig10, axis_k_N_op = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_k_N_op.plot(amount, k_N_op[i], "-o", label = "m = {}".format(i + 1))
    axis_k_N_op.set_xlabel(xlabel_op, fontsize = font_size)
    axis_k_N_op.set_ylabel("Коэффициент занятости операторов", fontsize = font_size)
    axis_k_N_op.legend(fontsize = 12)
    axis_k_N_op.grid()
    plt.xlim(1, op_amount)
    plt.ylim(0, 1.1)
    fig10.savefig("exersize_1_2_op_k_N.png", dpi = dpi_size)
    
    fig11, axis_Q_op = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_Q_op.plot(amount, Q_op[i], "-o", label = "m = {}".format(i + 1))
    axis_Q_op.set_xlabel(xlabel_op, fontsize = font_size)
    axis_Q_op.set_ylabel("Мат. ожидание длины очереди", fontsize = font_size)
    axis_Q_op.legend(fontsize = 12)
    axis_Q_op.grid()
    plt.xlim(1, op_amount)
    max_Q_op = 0
    for i in range(op_amount):
        for j in range(op_amount):
            if (max_Q_op < Q_op[i][j]):
                max_Q_op = Q_op[i][j]
    plt.ylim(0, 1.1 * max_Q_op)
    fig11.savefig("exersize_1_2_op_Q.png", dpi = dpi_size)
    
    fig12, axis_k_Q_op = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    for i in range(op_amount):
        axis_k_Q_op.plot(amount, k_Q_op[i], "-o", label = "m = {}".format(i + 1))
    axis_k_Q_op.set_xlabel(xlabel_op, fontsize = font_size)
    axis_k_Q_op.set_ylabel("Коэффициент занятости мест в очереди", fontsize = font_size)
    axis_k_Q_op.legend(fontsize = 12)
    axis_k_Q_op.grid()
    plt.xlim(1, op_amount)
    plt.ylim(0, 1.1)
    fig12.savefig("exersize_1_2_op_k_Q.png", dpi = dpi_size)


show_with_queue(lamb, mu, operator_amount)


def find_p0_with_infinite_queue(lamb, mu, n):
    if (n * mu < lamb):
        return 0
    p_i = lamb / mu
    result = 1
    for i in range(2, n + 1):
        p_i, result = p_i * lamb / (mu * i), result + p_i
    return 1 / (result + p_i * (1 + (lamb / (n * mu - lamb))))


def find_p_N_k_Q_with_infinite_queue(lamb, mu, n):
    if (n * mu < lamb):
        return (1, n, 1, math.inf)
    P = [0]*(n + 1)
    P[0] = find_p0_with_infinite_queue(lamb, mu, n)
    for i in range(1, n + 1):
        P[i] = P[i - 1] * lamb / (mu * i)
    N = sum(i * P[i] for i in range(1, n + 1)) + n * P[n] * lamb / (n * mu - lamb)
    return (P[n] * lamb / (n * mu - lamb), N, N / n, P[n] * lamb * n * mu / ((lamb - n * mu) ** 2))


def show_with_infinite_queue(lamb, mu, op_amount):
    start = int(lamb // mu)
    p_queue = [0] * (op_amount - start)
    N = [0] * (op_amount - start)
    k = [0] * (op_amount - start)
    Q = [0] * (op_amount - start)
    amount = [i for i in range(start + 1, op_amount + 1)]
    for i in range(op_amount - start):
        p_queue[i], N[i], k[i], Q[i] = find_p_N_k_Q_with_infinite_queue(lamb, mu, i + start + 1)
        
    print("amount | P_queue |    N    |    k    |    Q    |")
    print("------------------------------------------------")
    for i in range(len(amount)):
        print(" %4d  | %.5f | %.5f | %.5f | %7.4f |" % (amount[i], p_queue[i], N[i], k[i], Q[i]))
    
    xlabel = "Кол-во операторов"
    size_x = 16
    size_y = 8
    font_size = 14
    
    ylabel = "Вероятность существования очереди"
    fig1, axis_P = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_P.plot(amount, p_queue, "-o")
    plt.xlim(start + 1, op_amount)
    plt.ylim(0, 1.1)
    axis_P.set_xlabel(xlabel,fontsize = font_size)
    axis_P.set_ylabel(ylabel,fontsize = font_size)
    axis_P.grid()
    fig1.savefig("exersize_1_3_P_queue.png", dpi = dpi_size)
    
    ylabel = "Мат. ожидание числа занятных операторов"
    fig2, axis_N = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_N.plot(amount, N, "-o")
    plt.xlim(start + 1, op_amount)
    plt.ylim(0, 1.1 * max(N))
    axis_N.set_xlabel(xlabel,fontsize = font_size)
    axis_N.set_ylabel(ylabel,fontsize = font_size)
    axis_N.grid()
    fig2.savefig("exersize_1_3_N.png", dpi = dpi_size)
    
    ylabel = "Коэффициент загрузки операторов"
    fig3, axis_k = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_k.plot(amount, k, "-o")
    plt.xlim(start + 1, op_amount)
    plt.ylim(0, 1.1)
    axis_k.set_xlabel(xlabel,fontsize = font_size)
    axis_k.set_ylabel(ylabel,fontsize = font_size)
    axis_k.grid()
    fig3.savefig("exersize_1_3_k_n.png", dpi = dpi_size)
    
    ylabel = "Мат. ожидание длины очереди"
    fig4, axis_Q = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_Q.plot(amount, Q, "-o")
    plt.xlim(start + 1, op_amount)
    plt.ylim(0, 1.1 * max(Q))
    axis_Q.set_xlabel(xlabel,fontsize = font_size)
    axis_Q.set_ylabel(ylabel,fontsize = font_size)
    axis_Q.grid()
    # fig1.savefig("exersize_1_3_Q.png", dpi = dpi_size)
    fig4.savefig("exersize_1_3_Q.png", dpi = dpi_size)


T_c = 29
T_s = 221
mu = 1 / T_s
lamb = 1 / T_c
show_with_infinite_queue(lamb, mu, operator_amount)


def find_p0_with_infinite_queue_and_waiting(lamb, mu, vi, n):
    p_i = lamb / mu
    result = 1 + p_i
    for i in range(2, n + 1):
        p_i = p_i * lamb / (mu * i)
        result += p_i
    helper = (mpmath.gammainc(1 + n * mu / vi, a = 0, b = lamb / vi)) * p_i
    result += math.exp(lamb / vi) * ((lamb / vi) ** (-n * mu / vi)) * helper
    return float(1 / result)


def find_p_N_k_Q_with_infinite_queue_and_waiting(lamb, mu, vi, n):
    P = [0]*(n + 1)
    P[0] = find_p0_with_infinite_queue_and_waiting(lamb, mu, vi, n)
    for i in range(1, n + 1):
        P[i] = P[i - 1] * lamb / (mu * i)
    p_queue = 1 - sum(P)
    N = sum(i * P[i] for i in range(1, n + 1)) + n * p_queue
    Q = (lamb - N * mu) / vi
    k = N / n
    return (p_queue, N, k, Q)


def show_with_infinite_queue_and_waiting(lamb, mu, vi, op_amount):
    start = 0
    p_queue = [0] * (op_amount)
    N = [0] * (op_amount)
    k = [0] * (op_amount)
    Q = [0] * (op_amount)
    amount = [i for i in range(1, op_amount + 1)]
    for i in range(op_amount - start):
        p_queue[i], N[i], k[i], Q[i] = find_p_N_k_Q_with_infinite_queue_and_waiting(lamb, mu, vi, i + 1)
        
    print("amount | P_queue |     N    |    k    |    Q    |")
    print("-------------------------------------------------")
    for i in range(len(amount)):
        print(" %4d  | %.5f | %8.5f | %.5f | %7.4f |" % (amount[i], p_queue[i], N[i], k[i], Q[i]))
    
    xlabel = "Кол-во операторов"
    size_x = 16
    size_y = 8
    font_size = 14
    
    ylabel="Вероятность существования очереди"
    fig1, axis_P = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_P.plot(amount, p_queue, "-o")
    plt.xlim(start + 1, op_amount)
    plt.ylim(0, 1.1)
    axis_P.set_xlabel(xlabel, fontsize = font_size)
    axis_P.set_ylabel(ylabel, fontsize = font_size)
    axis_P.grid()
    fig1.savefig("exersize_1_4_P_queue.png", dpi = dpi_size)
    
    ylabel="Мат. ожидание числа занятых операторов"
    fig2, axis_N = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_N.plot(amount, N, "-o")
    plt.xlim(start + 1, op_amount)
    plt.ylim(0, 1.1 * max(N))
    axis_N.set_xlabel(xlabel, fontsize = font_size)
    axis_N.set_ylabel(ylabel, fontsize = font_size)
    axis_N.grid()
    fig2.savefig("exersize_1_4_N.png", dpi = dpi_size)
    
    ylabel="Коэффициент загрузки операторов"
    fig3, axis_k = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_k.plot(amount, k, "-o")
    plt.xlim(start + 1, op_amount)
    plt.ylim(0, 1.1)
    axis_k.set_xlabel(xlabel, fontsize = font_size)
    axis_k.set_ylabel(ylabel, fontsize = font_size)
    axis_k.grid()
    fig3.savefig("exersize_1_4_k.png", dpi = dpi_size)
    
    ylabel="Мат. ожидание длины очереди"
    fig4, axis_Q = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_Q.plot(amount, Q, "-o")
    plt.xlim(start + 1, op_amount)
    plt.ylim(0, 1.1 * max(Q))
    axis_Q.set_xlabel(xlabel, fontsize = font_size)
    axis_Q.set_ylabel(ylabel, fontsize = font_size)
    axis_Q.grid()
    fig4.savefig("exersize_1_4_Q.png", dpi = dpi_size)


T_c = 29
T_s = 221
T_w = 531
mu = 1 / T_s
lamb = 1 / T_c
vi = 1 / T_w
show_with_infinite_queue_and_waiting(lamb, mu, vi, operator_amount)


def find_p0_for_stank(lamb, mu, N, n):
    p_i = N * lamb / mu
    result = 1 + p_i
    for i in range(2, n + 1):
        p_i = p_i * (N - i + 1) * lamb / (mu * i)
        result += p_i
    for i in range(n + 1, N + 1):
        p_i = p_i * (N - i + 1) * lamb / (mu * n)
        result += p_i
    return 1 / result

def find_all_for_stank(lamb, mu, N, n):
    P = [0]*(N + 1)
    P[0] = find_p0_for_stank(lamb, mu, N, n)
    M_wait = 0
    M_not_work = 0
    P_wait = 0
    N_worker = 0
    k_N = 0
    for i in range(1, n + 1):
        P[i] = P[i - 1] * (N - i + 1) * lamb / (mu * i)
        N_worker += i * P[i]
        M_not_work += i * P[i]
    for i in range(n + 1, N + 1):
        P[i] = P[i - 1] * (N - i + 1) * lamb / (mu * n)
        N_worker += n * P[i]
        M_not_work += i * P[i]
        M_wait += (i - n) * P[i]
        P_wait += P[i]
    k_N = N_worker / n
    return (M_not_work, M_wait, P_wait, N_worker, k_N)


def show_with_stank(lamb, mu, N):
    M_wait = [0] * N
    M_nw = [0] * N
    P_wait = [0] * N
    N_work = [0] * N
    k_N = [0] * N
    amount = [i for i in range(1, N + 1)]
    
    for i in range(N):
        M_nw[i], M_wait[i], P_wait[i], N_work[i], k_N[i] = find_all_for_stank(lamb, mu, N, i + 1)
        
    print("amount | M_not_work |  M_wait  |  P_wait  |  N_worker  |   k_N   |")
    print("------------------------------------------------------------------")
    for i in range(N):
        print(" %4d  | %10.7f | %8.4f | %8.4f | %10.6f | %7.3f |"%(amount[i],M_nw[i],M_wait[i],P_wait[i],N_work[i],k_N[i]))
    
    xlabel = "Количество наладчиков"
    size_x = 16
    size_y = 8
    font_size = 14
    
    ylabel="Мат. ожидание числа простаивающих станков"
    fig1, axis_M_nw = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_M_nw.plot(amount, M_nw, "-o")
    axis_M_nw.set_xlabel(xlabel, fontsize = font_size)
    axis_M_nw.set_ylabel(ylabel, fontsize = font_size)
    plt.xlim(1, N)
    plt.ylim(0, 1.1 * max(M_nw))
    axis_M_nw.grid()
    fig1.savefig("exersize_2_M_not_work.png", dpi = dpi_size)
    
    ylabel="Мат. ожидание числа станков, ожидающих обслуживания"
    fig2, axis_M_wait = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_M_wait.plot(amount, M_wait, "-o")
    axis_M_wait.set_xlabel(xlabel, fontsize = font_size)
    axis_M_wait.set_ylabel(ylabel, fontsize = font_size)
    plt.xlim(1, N)
    plt.ylim(0, 1.1 * max(M_wait))
    axis_M_wait.grid()
    fig2.savefig("exersize_2_M_wait.png", dpi = dpi_size)
    
    ylabel="Вероятность ожидания обслуживания"
    fig3, axis_P_wait = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_P_wait.plot(amount, P_wait, "-o")
    axis_P_wait.set_xlabel(xlabel, fontsize = font_size)
    axis_P_wait.set_ylabel(ylabel, fontsize = font_size)
    plt.xlim(1, N)
    plt.ylim(0, 1.1 * max(P_wait))
    axis_P_wait.grid()
    fig3.savefig("exersize_2_P_wait.png", dpi = dpi_size)
    
    ylabel="Мат. ожидание числа занятых наладчиков"
    fig4, axis_N_work = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_N_work.plot(amount, N_work, "-o")
    axis_N_work.set_xlabel(xlabel, fontsize = font_size)
    axis_N_work.set_ylabel(ylabel, fontsize = font_size)
    plt.xlim(1, N)
    plt.ylim(0, 1.1 * max(N_work))
    axis_N_work.grid()
    fig4.savefig("exersize_2_N_work.png", dpi = dpi_size)
    
    ylabel="Коэффициент занятости наладчиков"
    fig5, axis_k_N = plt.subplots(nrows=1, ncols=1, figsize=(size_x, size_y))
    axis_k_N.plot(amount, k_N, "-o")
    axis_k_N.set_xlabel(xlabel, fontsize = font_size)
    axis_k_N.set_ylabel(ylabel, fontsize = font_size)
    plt.xlim(1, N)
    plt.ylim(0, 1.1 * max(k_N))
    axis_k_N.grid()
    fig5.savefig("exersize_2_k_N.png", dpi = dpi_size)


N = 41
T_s = 20
T_c = 360
lamb = 1 / T_c
mu = 1 / T_s
print("Limit for not working stanks: {} \n\n".format((N * (T_s)) / (  (T_c + T_s))))
show_with_stank(lamb, mu, N)
