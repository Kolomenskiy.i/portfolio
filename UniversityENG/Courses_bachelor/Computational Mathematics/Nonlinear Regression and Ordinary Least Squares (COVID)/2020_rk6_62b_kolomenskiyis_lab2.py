import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import basinhopping
from math import ceil


def exponential(I0, chi, t):
    return I0 * np.exp(chi * t)


def SIS(I0, I_inf, chi, t):
    return I_inf / (1 + (I_inf / I0 - 1) * np.exp(-chi * t))


def error(args):
    I0, I_inf = args
    Y_approx = SIS(I0, I_inf, chi, X).astype('int64')
    Y_real = true_data.total_cases
    return np.sqrt((Y_real - Y_approx).pow(2).sum() / Y.size)


def lebeg_error(Y_real, Y_approx):
    return (Y_real - Y_approx).abs().max()


def l2_error(Y_real, Y_approx):
    return np.sqrt((Y_real - Y_approx).pow(2).sum() / Y.size)


def max_pandemia_days(I0, I_inf, chi):
    return -np.log(0.01 / ((I_inf / I0 - 1) * (1 - 1.01 * np.exp(-chi)))) / chi


data = pd.read_csv('owid-covid-data.csv', usecols=['location', 'date', 'total_cases'], parse_dates=True)
data = data[data.location == 'Canada']
data = data[data.total_cases > 0].reset_index()
data.head()

_, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 6))
data.plot(x='date', y='total_cases', ax=axes, logy=True, style='r.-')
axes.set_xlabel('Дата')
axes.set_ylabel('Количество инфицированных')
axes.grid()
axes.legend(['Количество инфицированных'], loc='best')
plt.show()

day_cnt = 16  # Сколько дней описывают экспоненциальную зависимость
I0 = 60  # Начальное число инфицированных
true_data = data[data.total_cases >= I0].reset_index()
Y = np.log(true_data.total_cases[0:day_cnt] / I0)
X = pd.Series(np.arange(0, Y.size))
chi = (1 / X.T.dot(X)) * X.T.dot(Y)  # Значение параметра 'хи' в соответствии с нормальным уравнением
print("Коэффициент χ = {}".format(chi))

aprox_y = exponential(I0, chi, X).astype('int64')

exponential_data = true_data[0:day_cnt]

_, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 6))
aprox_y.plot(ax=axes, logy=True, style='b.-')
exponential_data.plot(y='total_cases', ax=axes, logy=True, style='r.-')
axes.set_xlabel('Количество дней после начала эпидемии')
axes.set_ylabel('Количество инфицированных')
axes.grid()
axes.legend(['Приближенные данные', 'Исходные данные'], loc='best')
plt.show()

X = np.exp(pd.Series(np.arange(0, true_data.shape[0])) * (-chi))
Y = 1 / true_data.total_cases
X = pd.concat([pd.Series(np.ones(X.shape)), X], axis=1)  # Добавляем единичный столбец
A = np.linalg.inv(X.T.dot(X)).dot(X.T).dot(Y)  # Получаем коэффициенты
I_inf = int(round(np.abs(1 / A[0])))
I0 = int(round(np.abs(1 / (A[1] + A[0]))))
print(I_inf, I0)

days_range = np.arange(0, true_data.shape[0])
X = pd.Series(days_range, index=days_range)
aprox_y = SIS(I0, I_inf, chi, X).astype('int64')

_, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 6))
aprox_y.plot(ax=axes, logy=True, style='b.-')
true_data.plot(y='total_cases', ax=axes, logy=True, style='r.-')
axes.set_xlabel('Количество дней после начала эпидемии')
axes.set_ylabel('Количество инфицированных')
axes.grid()
axes.legend(['Приближенные данные', 'Исходные данные'], loc='best')
plt.show()

sol = basinhopping(error, [I0, I_inf])

I0_opt, I_inf_opt = sol.x
print(I_inf_opt, I0_opt)
I0_opt, I_inf_opt = int(round(I0_opt)), int(round(I_inf_opt))
aprox_y_opt = SIS(I0_opt, I_inf_opt, chi, X).astype('int64')

_, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 6))
aprox_y_opt.plot(ax=axes, logy=True, style='b.-')
true_data.plot(y='total_cases', ax=axes, logy=True, style='r.-')
axes.set_xlabel('Количество дней после начала эпидемии')
axes.set_ylabel('Количество инфицированных')
axes.grid()
axes.legend(['Приближенные данные', 'Исходные данные'], loc='best')
plt.show()

leb_error = lebeg_error(true_data.total_cases, aprox_y)  # Ошибка в пространстве с лебеговой нормой
print("Погрешность в L_inf норме без оптимизации: {}".format(leb_error))
leb_error = lebeg_error(true_data.total_cases, aprox_y_opt)
print("Погрешность в L_inf норме c оптимизацией: {}".format(leb_error))

l2_err = l2_error(true_data.total_cases, aprox_y)  # Ошибка в пространстве с нормой L2
print("Погрешность в L2 норме без оптимизации: {}".format(l2_err))
l2_err = l2_error(true_data.total_cases, aprox_y_opt)
print("Погрешность в L2 норме с оптимизацией: {}".format(l2_err))

exp_b_list = []
for i in range(1, true_data.total_cases.size):
    exp_b_list.append(true_data.total_cases[i] / true_data.total_cases[i - 1])

deriv = []
for i in range(1, len(exp_b_list)):
    deriv.append(np.abs(exp_b_list[i] - exp_b_list[i - 1]))

otn = []
for i in range(1, len(deriv)):
    otn.append(np.mean(deriv[0:i]) / deriv[i])

_, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 6))
axes.set_xlabel('Количество дней после начала эпидемии')
axes.set_ylabel('Отношение изменения $e^{\chi}$')
axes.grid()
plt.plot(range(0, len(otn)), otn)
plt.show()

max_days = ceil(max_pandemia_days(I0, I_inf, chi))
print("Количество дней до окончания эпидемии: {}".format(max_days))
print("Количество инфицированных при t → ∞: {}".format(I_inf))
