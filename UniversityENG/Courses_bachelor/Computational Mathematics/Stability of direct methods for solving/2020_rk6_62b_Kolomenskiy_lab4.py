import matplotlib.pyplot as plt
import numpy as np
import math as math
import copy


def gauss(A, b, pivoting):
    A = copy.deepcopy(A)
    b = copy.deepcopy(b)
    n = len(A)
    x = np.zeros(n)
    if not pivoting:
        for i in range(n - 1):
            for j in range(i + 1, n):
                m = A[j][i] / A[i][i]
                for k in range(i, n):
                    A[j][k] -= A[i][k] * m
                b[j] -= m * b[i]
        for i in range(n - 1, 0, -1):
            for j in range(i):
                b[j] -= b[i] * A[j][i] / A[i][i]
        for i in range(n):
            x[i] = b[i] / A[i][i]
        return x
    elif pivoting:
        pre_x = np.zeros(n)
        index = [0] * n
        for i in range(n):
            index[i] = i
        for i in range(n - 1):
            max_i = 0
            max_j = 0
            maximum = 0
            for j in range(i, n):
                for k in range(i, n):
                    if abs(A[j][k]) > maximum:
                        maximum = abs(A[j][k])
                        max_i = j
                        max_j = k
            tmp = copy.deepcopy(A[max_i])
            A[max_i] = A[i]
            A[i] = tmp
            tmp = b[max_i]
            b[max_i] = b[i]
            b[i] = tmp
            index[max_j], index[i] = index[i], index[max_j]
            for j in range(n):
                tmp = A[j][max_j]
                A[j][max_j] = A[j][i]
                A[j][i] = tmp
            for j in range(i + 1, n):
                m = A[j][i] / A[i][i]
                for k in range(i, n):
                    A[j][k] -= A[i][k] * m
                b[j] -= m * b[i]
        for i in range(n - 1, 0, -1):
            for j in range(i):
                b[j] -= b[i] * A[j][i] / A[i][i]
        for i in range(n):
            pre_x[i] = b[i] / A[i][i]
        for i in range(n):
            x[index[i]] = pre_x[i]
        return x


def thomas(A, b):
    b = copy.deepcopy(b)
    n = len(b)
    ac = np.zeros(n - 1)
    bc = np.zeros(n)
    cc = np.zeros(n - 1)
    for i in range(n):
        if i == 0:
            bc[i] = A[i][i]
            cc[i] = A[i][i + 1]
        elif i == (n - 1):
            ac[i - 1] = A[i][i - 1]
            bc[i] = A[i][i]
        else:
            ac[i - 1] = A[i][i - 1]
            bc[i] = A[i][i]
            cc[i] = A[i][i + 1]
            
    for i in range(1, n):
        mc = ac[i - 1] / bc[i - 1]
        bc[i] = bc[i] - mc * cc[i - 1] 
        b[i] = b[i] - mc * b[i - 1]
    x = bc
    x[-1] = b[-1]/bc[-1]

    for i in range(n-2, -1, -1):
        x[i] = (b[i] - cc[i] * x[i + 1]) / bc[i]

    return x


def cholesky(A, b):
    n = len(b)
    chol_A = np.zeros((n, n))
    
    for i in range(n):
        for j in range(i):
            l_ij_sum = 0
            for k in range(j):
                l_ij_sum += chol_A[i][k] * chol_A[j][k]
            chol_A[i][j] = (A[i][j] - l_ij_sum) / chol_A[j][j]
        l_ii_sum = 0
        for j in range(i):
            l_ii_sum += chol_A[i][j] ** 2
        chol_A[i][i] = math.sqrt(A[i][i] - l_ii_sum)
    
    b = copy.deepcopy(b)
    x = np.zeros(n)
    y = np.zeros(n)
    for i in range(n - 1):
        for j in range(i + 1, n):
            b[j] -= b[i] * chol_A[j][i] / chol_A[i][i]
    for i in range(n):
        y[i] = b[i] / chol_A[i][i]
    for i in range(n - 1, 0, -1):
        for j in range(i):
            y[j] -= y[i] * chol_A[i][j] / chol_A[i][i]
    for i in range(n):
        x[i] = y[i] / chol_A[i][i]
    return x


def diagonal_dominant_matrix(A):
    A = A.copy()
    new_diag = np.abs(A).sum(axis=1)
    for i, new_diag_element in enumerate(new_diag):
        A[i, i] = new_diag_element * (1 - 2 * np.random.randint(0, 2))
    A = A / (new_diag.max() + new_diag.min())
    return A


def generate_matrix(mode, amt):
    if mode == 0:  # default
        norm = False
        while not norm:
            A_all = np.random.uniform(-1, 1, (amt, 4, 4))
            for i in range(amt):
                if np.linalg.det(A_all[i]) == 0:
                    break
                else:
                    norm = True
    if mode == 1:  # D dominant
        norm = False
        while not norm:
            A_all = np.random.uniform(-1, 1, (amt, 4, 4))
            A_all_diag = []
            for i in range(amt):
                A_all_diag.append(diagonal_dominant_matrix(A_all[i]))
            A_all = np.stack(A_all_diag, axis=0)
            for i in range(amt):
                if np.linalg.det(A_all[i]) == 0:
                    break
                else:
                    norm = True
        
    if mode == 2:  # triangle D
        norm = False
        while not norm:
            A_all = np.random.uniform(-1, 1, (amt, 4, 4))
            for i in range(amt):
                A_all[i][0][2], A_all[i][0][3], A_all[i][1][3], A_all[i][2][0], A_all[i][3][0], A_all[i][3][
                    1] = 0, 0, 0, 0, 0, 0
            for i in range(amt):
                if np.linalg.det(A_all[i]) == 0:
                    break
                else:
                    norm = True
    if mode == 3:  # positive define matrix
        norm = False
        while not norm:
            A_all = np.random.uniform(-1, 1, (amt, 4, 4))
            A_all[:, 0, 1:] = 0
            A_all[:, 1, 2:] = 0
            A_all[:, 2, 3:] = 0
            for i in range(amt):
                A_all[i] = np.dot(A_all[i], A_all[i].transpose())
            for i in range(amt):
                if np.linalg.det(A_all[i]) == 0:
                    break
                else:
                    norm = True
    return A_all


B = np.ones(4)
PERFECT = gauss


# default matrix
A_all = generate_matrix(0, 1000)


spectr_radius = []
eigvals = np.linalg.eigvals(A_all)
for i in range(len(eigvals)):
    spectr_radius.append(np.max(np.abs(eigvals[i])))
    
spectr_radius_val = np.array(spectr_radius)

cond = np.linalg.cond(A_all)


min_vals = []
for i in range(len(eigvals)):
    min_vals.append(np.min(np.abs(eigvals[i])))
min_vals = np.array(min_vals)


np.max(spectr_radius_val)


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(spectr_radius_val, bins=100)
axes.grid()
axes.set_title("Спектральные радиусы матриц общего вида")
axes.set_xlabel("Значения радиусов")
axes.set_ylabel("Количество")
plt.show()


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(cond, bins=100)
axes.grid()
axes.set_title("Числа обусловленности матриц общего вида")
axes.set_xlabel("Значения чисел обусловленности")
axes.set_ylabel("Количество")
plt.show()


# Решения для матриц общего вида:
res_0_perfect = []
for i in range(1000):
    res_0_perfect.append(PERFECT(A_all[i], B, True))
res_0_perfect = np.array(res_0_perfect)

res_0_default = []
for i in range(1000):
    res_0_default.append(gauss(A_all[i], B, False))
res_0_default = np.array(res_0_default)


sum_l2 = np.zeros(1000)
for k in range(1000):
    for i in range(4):
        sum_l2[k] += (res_0_default[k][i] - res_0_perfect[k][i]) ** 2
    
x_l2 = np.zeros(1000)
for k in range(1000):
    for i in range(4):
        x_l2[k] += res_0_perfect[k][i] ** 2
        
l2_0 = np.sqrt(sum_l2) / np.sqrt(x_l2)

max_inf = np.zeros(1000)
for k in range(1000):
    max_inf[k] = np.max(np.abs(res_0_default[k] - res_0_perfect[k]))

x_inf = np.zeros(1000)
for k in range(1000):
    x_inf[k] = np.max(np.abs(res_0_perfect[k]))
    
l_inf_0 = max_inf / x_inf


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(l2_0, bins=100)
axes.grid()
axes.set_title("Погрешности по среднеквадратичной норме")
axes.set_xlabel("Значения погрешностей")
axes.set_ylabel("Количество")
plt.show()


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(l_inf_0, bins=100)
axes.grid()
axes.set_title("Погрешности по супремум-норме")
axes.set_xlabel("Значения погрешностей")
axes.set_ylabel("Количество")
plt.show()


maxmin_vals = spectr_radius_val / min_vals


cor_array = np.vstack((maxmin_vals, l2_0))


R_xy = np.corrcoef(cor_array)


R = np.corrcoef([maxmin_vals, l2_0])


# Матрицы со строгим диагональным преобладанием
A_all = generate_matrix(1, 1000)

spectr_radius = []
eigvals = np.linalg.eigvals(A_all)
for i in range(len(eigvals)):
    spectr_radius.append(np.max(np.abs(eigvals[i])))
    
spectr_radius_val = np.array(spectr_radius)

cond = np.linalg.cond(A_all)

min_vals = []
for i in range(len(eigvals)):
    min_vals.append(np.min(np.abs(eigvals[i])))
min_vals = np.array(min_vals)


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(spectr_radius_val, bins=100)
axes.grid()
axes.set_title("Спектральные радиусы матриц со строгим диагональным преобладанием")
axes.set_xlabel("Значения радиусов")
axes.set_ylabel("Количество")
plt.show()


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(cond, bins=100)
axes.grid()
axes.set_title("Числа обусловленности матриц со строгим диагональным преобладанием")
axes.set_xlabel("Значения чисел обусловленности")
axes.set_ylabel("Количество")
plt.show()


res_1_perfect = []
for i in range(1000):
    res_1_perfect.append(PERFECT(A_all[i], B, True))
res_1_perfect = np.array(res_1_perfect)

res_1_default = []
for i in range(1000):
    res_1_default.append(gauss(A_all[i], B, False))
res_1_default = np.array(res_1_default)


sum_l2 = np.zeros(1000)
for k in range(1000):
    for i in range(4):
        sum_l2[k] += (res_1_default[k][i] - res_1_perfect[k][i]) ** 2
    
x_l2 = np.zeros(1000)
for k in range(1000):
    for i in range(4):
        x_l2[k] += res_1_perfect[k][i] ** 2
        
l2_0 = np.sqrt(sum_l2) / np.sqrt(x_l2)

max_inf = np.zeros(1000)
for k in range(1000):
    max_inf[k] = np.max(np.abs(res_1_default[k] - res_1_perfect[k]))

x_inf = np.zeros(1000)
for k in range(1000):
    x_inf[k] = np.max(np.abs(res_1_perfect[k]))
    
l_inf_0 = max_inf / x_inf

maxmin_vals = spectr_radius_val / min_vals 


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(l2_0, bins=100)
axes.grid()
axes.set_title("Погрешности по среднеквадратичной норме")
axes.set_xlabel("Значения погрешностей")
axes.set_ylabel("Количество")
plt.show()


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(l_inf_0, bins=100)
axes.grid()
axes.set_title("Погрешности по супремум-норме")
axes.set_xlabel("Значения погрешностей")
axes.set_ylabel("Количество")
plt.show()


R = np.corrcoef([maxmin_vals, l2_0])[1, 0]


R = np.corrcoef([spectr_radius_val, l2_0])[1, 0]


# Трехдиагональные матрицы
A_all = generate_matrix(2, 1000)

spectr_radius = []
eigvals = np.linalg.eigvals(A_all)
for i in range(len(eigvals)):
    spectr_radius.append(np.max(np.abs(eigvals[i])))
    
spectr_radius_val = np.array(spectr_radius)

cond = np.linalg.cond(A_all)

min_vals = []
for i in range(len(eigvals)):
    min_vals.append(np.min(np.abs(eigvals[i])))
min_vals = np.array(min_vals)


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(spectr_radius_val, bins=100)
axes.grid()
axes.set_title("Спектральные радиусы трехдиагональных матриц")
axes.set_xlabel("Значения радиусов")
axes.set_ylabel("Количество")
plt.show()


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(cond, bins=100)
axes.grid()
axes.set_title("Числа обусловленности трехдиагональных матриц")
axes.set_xlabel("Значения чисел обусловленности")
axes.set_ylabel("Количество")
plt.show()


res_1_perfect = []
for i in range(1000):
    res_1_perfect.append(PERFECT(A_all[i], B, True))
res_1_perfect = np.array(res_1_perfect)

res_1_default = []
for i in range(1000):
    res_1_default.append(thomas(A_all[i], B))
res_1_default = np.array(res_1_default)


sum_l2 = np.zeros(1000)
for k in range(1000):
    for i in range(4):
        sum_l2[k] += (res_1_default[k][i] - res_1_perfect[k][i]) ** 2
    
x_l2 = np.zeros(1000)
for k in range(1000):
    for i in range(4):
        x_l2[k] += res_1_perfect[k][i] ** 2
        
l2_0 = np.sqrt(sum_l2) / np.sqrt(x_l2)

max_inf = np.zeros(1000)
for k in range(1000):
    max_inf[k] = np.max(np.abs(res_1_default[k] - res_1_perfect[k]))

x_inf = np.zeros(1000)
for k in range(1000):
    x_inf[k] = np.max(np.abs(res_1_perfect[k]))
    
l_inf_0 = max_inf / x_inf

maxmin_vals = spectr_radius_val / min_vals 


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(l2_0, bins=100)
axes.grid()
axes.set_title("Погрешности по среднеквадратичной норме")
axes.set_xlabel("Значения погрешностей")
axes.set_ylabel("Количество")
plt.show()


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(l_inf_0, bins=100)
axes.grid()
axes.set_title("Погрешности по супремум-норме")
axes.set_xlabel("Значения погрешностей")
axes.set_ylabel("Количество")
plt.show()


R = np.corrcoef([spectr_radius_val, l2_0])[1, 0]


R = np.corrcoef([maxmin_vals, l2_0])[1, 0]


R = np.corrcoef([cond, l2_0])[1, 0]


# Положительно определенные матрицы
A_all = generate_matrix(3, 1000)

spectr_radius = []
eigvals = np.linalg.eigvals(A_all)
for i in range(len(eigvals)):
    spectr_radius.append(np.max(np.abs(eigvals[i])))
    
spectr_radius_val = np.array(spectr_radius)

cond = np.linalg.cond(A_all)

min_vals = []
for i in range(len(eigvals)):
    min_vals.append(np.min(np.abs(eigvals[i])))
min_vals = np.array(min_vals)


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(spectr_radius_val, bins=100)
axes.grid()
axes.set_title("Спектральные радиусы положительно определенных матриц")
axes.set_xlabel("Значения радиусов")
axes.set_ylabel("Количество")
plt.show()


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(cond, bins=100)
axes.grid()
axes.set_title("Числа обусловленности положительно определенных матриц")
axes.set_xlabel("Значения чисел обусловленности")
axes.set_ylabel("Количество")
plt.show()


res_1_perfect = []
for i in range(1000):
    res_1_perfect.append(PERFECT(A_all[i], B, True))
res_1_perfect = np.array(res_1_perfect)

res_1_default = []
for i in range(1000):
    res_1_default.append(cholesky(A_all[i], B))
res_1_default = np.array(res_1_default)


def squared_dif(array1, array2):
    returnum = 0
    length = 0
    n = len(array1)
    for i in range(n):
        returnum += (array1[i]-array2[i]) ** 2
        length += array1[i] ** 2
    returnum = math.sqrt(returnum / (length))
    return returnum


for k in range(10):
    print(squared_dif(res_1_perfect[k], res_1_default[k]))


sum_l2 = np.zeros(1000)
for k in range(1000):
    for i in range(4):
        sum_l2[k] += (res_1_default[k][i] - res_1_perfect[k][i]) ** 2
    
x_l2 = np.zeros(1000)
for k in range(1000):
    for i in range(4):
        x_l2[k] += res_1_perfect[k][i] ** 2
        
l2_0 = np.sqrt(sum_l2) / np.sqrt(x_l2)

max_inf = np.zeros(1000)
for k in range(1000):
    max_inf[k] = np.max(np.abs(res_1_default[k] - res_1_perfect[k]))

x_inf = np.zeros(1000)
for k in range(1000):
    x_inf[k] = np.max(np.abs(res_1_perfect[k]))
    
l_inf_0 = max_inf / x_inf

maxmin_vals = spectr_radius_val / min_vals 


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(l2_0, bins=100)
axes.grid()
axes.set_title("Погрешностей по среднеквадратичной норме")
axes.set_xlabel("Значения погрешностей")
axes.set_ylabel("Количество")
plt.show()


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
axes.hist(l_inf_0, bins=100)
axes.grid()
axes.set_title("Погрешности по супремум-норме")
axes.set_xlabel("Значения погрешностей")
axes.set_ylabel("Количество")
plt.show()


R = np.corrcoef([maxmin_vals, l2_0])[1, 0]


R = np.corrcoef([spectr_radius_val, l2_0])[1, 0]


np.var(l_inf_0)


np.var(l2_0)
