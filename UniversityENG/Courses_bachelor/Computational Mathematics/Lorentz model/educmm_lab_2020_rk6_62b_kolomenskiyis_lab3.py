import matplotlib.pyplot as plt
import numpy as np
import time
from scipy.optimize import root
from mpl_toolkits import mplot3d


sigma = 10
b = 8/3


def f1(x, y):
    return sigma * (y - x)


def f2(x, y, z):
    return x * (r - z) - y


def f3(x, y, z):
    return x * y - b * z


def f(w):
    return np.array([f1(w[0], w[1]), f2(w[0], w[1], w[2]), f3(w[0], w[1], w[2])])


def euler(x_0, t_n, f, h):
    m = int(t_n / h)
    w = np.zeros((m, 3))
    w[0] = x_0
    for i in range(1, m):
        w[i] = w[i - 1] + h * f(w[i - 1])
    return w


def implicit_euler(x_0, t_n, f, h):
    m = int(t_n / h)
    w = np.zeros((m, 3))
    w[0] = x_0

    def non_linear_eq(w_i1):
        return w[i - 1] + h * f(w_i1) - w_i1

    for i in range(1, m):
        sol = root(non_linear_eq, [0, 0, 0])
        w[i] = sol.x

    return w


def adams_bashforth_moulton(x_0, t_n, f, h):
    m = int(t_n / h)
    w = np.zeros((m, 3))
    w[0] = x_0
    w[1] = implicit_euler(w[0], 2, f, 1)[1]
    w[2] = implicit_euler(w[1], 2, f, 1)[1]
    w[3] = implicit_euler(w[2], 2, f, 1)[1]
    
    for i in range(4, m):
        w_tilda = w[i - 1] + (h / 24) * (55 * f(w[i - 1]) - 59 * f(w[i - 2]) + 37 * f(w[i - 3]) - 9* f(w[i - 4]))
        w[i] = w[i - 1] + (h / 24) * (9 * f(w_tilda) + 19 * f(w[i - 1]) - 5 * f(w[i - 2]) + f(w[i - 3]))
        
    return w


METHOD_DEFAULT = adams_bashforth_moulton

r = 0
trajectory_count = 20

begin_array_xy = np.random.uniform(-50, 50, (100, 2))
begin_array_z = np.random.uniform(0, 70, (100, 1))
begin_array = np.zeros((100, 3))

for i in range(trajectory_count):
    begin_array[i] = np.append(begin_array_xy[i], begin_array_z[i])

start_time = time.time()

result = []
# result2 = []
# result3 = []
for i in range(trajectory_count):
    result_row = np.array(METHOD_DEFAULT(begin_array[i], 100, f, 0.01))
    # result2_row = np.array(euler(begin_array[i], 100, f, 0.01))
    # result3_row = np.array(implicit_euler(begin_array[i], 100, f, 0.01))
    result.append(result_row)
    # result2.append(result2_row)
    # result3.append(result3_row)

result = np.array(result)
# result2 = np.array(result2)
# result3 = np.array(result3)

t = time.time() - start_time
print(t)

fig = plt.figure(figsize=(8, 8))
ax = plt.axes(projection="3d")
for i in range(trajectory_count):
    ax.plot3D(result[i, :, 0], result[i, :, 1], result[i, :, 2], linewidth=0.8)
    
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")
# fig.show()
plt.show()

# fig2 = plt.figure(figsize=(8, 8))
# ax2 = plt.axes(projection="3d")
# for i in range(trajectory_count):
#     ax2.plot3D(result2[i, :, 0], result2[i, :, 1], result2[i, :, 2], linewidth=0.8)
# ax2.set_xlabel("x")
# ax2.set_ylabel("y")
# ax2.set_zlabel("z")
# fig2.show()
#
# fig3 = plt.figure(figsize=(8, 8))
# ax3 = plt.axes(projection="3d")
# for i in range(trajectory_count):
#     ax3.plot3D(result3[i, :, 0], result3[i, :, 1], result3[i, :, 2], linewidth=0.8)
# ax3.set_xlabel("x")
# ax3.set_ylabel("y")
# ax3.set_zlabel("z")
# plt.show()
