-- auto-generated definition
create table NSI_ACBPDP_TRANSPORT_TYPES
(
    ID                  BIGINT not null
        constraint NSI_ACBPDP_TRANSPORT_TYPES_PK
            primary key,
    TRANSPORT_TYPE_ID   BIGINT,
    TRANSPORT_TYPE_NAME VARCHAR(200),
    CODE                BIGINT,
    SORT                BIGINT,
    ISACTUAL            CHAR,
    TRANSPORT_TYPE_UO   BIGINT
);

