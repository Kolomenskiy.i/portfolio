CREATE TABLE CITIZENSHIP
(
    ID   INT not null,
    CODE INT not null,
    NAME VARCHAR(500) NOT NULL,
    SORT INT NOT NULL
);

alter table CITIZENSHIP
    add constraint CITIZEN_PK
        primary key (ID);