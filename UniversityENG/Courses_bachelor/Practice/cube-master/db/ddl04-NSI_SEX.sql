-- auto-generated definition
create table NSI_SEX
(
    ID       BIGINT not null
        constraint NSI_SEX_PK
            primary key,
    ISACTUAL CHAR,
    NAME     VARCHAR(20),
    SORT     BIGINT,
    CODE     BIGINT
);

