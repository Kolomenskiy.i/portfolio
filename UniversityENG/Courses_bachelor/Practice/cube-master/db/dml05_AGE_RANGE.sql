insert into NSI_AGE_CATEGORY (ID, ISACTUAL, CODE, AGE_TO, NAME, AGE_FROM, SORT) values (1596605, 'Y', -1, -1, 'Не указан', -1, 0);
insert into NSI_AGE_CATEGORY (ID, ISACTUAL, CODE, AGE_TO, NAME, AGE_FROM, SORT) values (1596606, 'Y', 1, 23, 'До 23 лет', 0, 1);
insert into NSI_AGE_CATEGORY (ID, ISACTUAL, CODE, AGE_TO, NAME, AGE_FROM, SORT) values (1596607, 'Y', 2, 60, 'От 23 до 60 лет', 23, 2);
insert into NSI_AGE_CATEGORY (ID, ISACTUAL, CODE, AGE_TO, NAME, AGE_FROM, SORT) values (1596608, 'Y', 3, 999, 'Старше 60 лет', 60, 3);
