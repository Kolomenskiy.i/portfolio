-- auto-generated definition
create table REGIONS
(
    REGION_ID         INT          not null,
    PARENT_REGION_ID  INT,
    REGION_LEVEL      SMALLINT     not null,
    REGION_NAME       VARCHAR(500) not null,
    SHORT_REGION_NAME VARCHAR(500) not null,
    REGION_PREFIX     VARCHAR(20),
    FORMAL_NAME       VARCHAR(200)
)
    ;

comment on column REGIONS.REGION_ID is 'Идентификатор региона' ;

comment on column REGIONS.PARENT_REGION_ID is 'Родительский идентификатор региона' ;

comment on column REGIONS.REGION_LEVEL is 'Уровень региона' ;

comment on column REGIONS.REGION_NAME is 'Название региона' ;

comment on column REGIONS.SHORT_REGION_NAME is 'Сокращенное название региона' ;

comment on column REGIONS.REGION_PREFIX is 'Префикс региона' ;

comment on column REGIONS.FORMAL_NAME is 'Официальное название региона' ;

create unique index REGIONS_NEW_PK
    on REGIONS (REGION_ID)
    ;

create index REGIONS_NAME
    on REGIONS (REGION_NAME)
    ;

create index I_REGIONS_PRN
    on REGIONS (PARENT_REGION_ID)
    ;

create index I_REGIONS_PRN_USHORT
    on REGIONS (PARENT_REGION_ID, UPPER(SHORT_REGION_NAME))
    ;

create index I_REGIONS_USHORT_PRN
    on REGIONS (UPPER(SHORT_REGION_NAME), PARENT_REGION_ID)
    ;

alter table REGIONS
    add constraint REGIONS_PK
        primary key (REGION_ID)
    ;

