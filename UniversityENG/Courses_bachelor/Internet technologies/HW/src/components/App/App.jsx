import React, { Fragment, PureComponent } from 'react';
import { connect } from 'react-redux';
import { addTester, getTesters } from '../../models/AppModel';
import {
	addTesterAction,
	downloadTestersAction
} from '../../store/action';
import Tester from '../Tester/Tester';
import './App.css'

class App extends PureComponent {
	state = {
		isInputShown: false,
		inputValue: ''
	};

	async componentDidMount() {
		const testers = await getTesters();
		this.props.downloadTestersDispatch(testers);
	}

	showInput = () => this.setState({ isInputShown: true });

	onInputChange = ({ target: { value } }) => this.setState({
		inputValue: value
	});

	onKeyDown = async (event) => {
		if (event.key === 'Escape') {
			this.setState({
				isInputShown: false,
				inputValue: ''
			});
			return;
		}

		if (event.key === 'Enter') {
			if (this.state.inputValue) {
				const info = await addTester({
					testerName: this.state.inputValue,
					rekvestes: []
				});
				console.log(info);

				this.props.addTesterDispatch(this.state.inputValue);
			}

			this.setState({
				isInputShown: false,
				inputValue: ''
			})
		}
	};

	render() {
		const { isInputShown, inputValue } = this.state;
		const { testers } = this.props;

		return (
			<Fragment>
				<header id="main-header">
					Личный кабинет
					<div id="author">
						Руководитель отдела тестирования
						<div className="avatar"></div>
					</div>
				</header>
				<main id="ra-container">
					{testers.map((tester, index) => (
						<Tester
							testerName = {tester.testerName}
							testerId = {index}
							rekvestes={tester.rekvestes}
							key = {`list${index}`}
						/>
					))}
					<div className="ra-tester">
						{!isInputShown && (
							<header 
								className="ra-tester-header"
								onClick = {this.showInput}
							>
								Добавить тестировщика
							</header>
						)}
						{isInputShown && (
							<input
								type="text"
								id="add-tester-input"
								placeholder="Имя повара"
								value = { inputValue }
								onChange = {this.onInputChange}
								onKeyDown={this.onKeyDown}
							/>
						)}
					</div>
				</main>
			</Fragment>
		)
	}
}

const mapStateToProps = ({ testers }) => ({ testers });

const mapDispatchToProps = dispatch => ({ 
	addTesterDispatch: (testerName) => dispatch(addTesterAction(testerName)),
	downloadTestersDispatch: (testers) => dispatch(downloadTestersAction(testers))
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App);
