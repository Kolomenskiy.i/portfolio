const getTesters = async () => {
	const response = await fetch('http://localhost:4321/testers');
	const testers = await response.json();

	return testers;
}

const addTester = async (tester) => {
	const response = await fetch('http://localhost:4321/testers', {
		method: 'POST',
		body: JSON.stringify(tester),
		headers: {
			'Content-Type': 'application/json'
		}
	});

	const { info } = await response.json();

	return info;
};

const addRekvest = async ({ testerId, rekvestName }) => {
	const response = await fetch(`http://localhost:4321/testers/${testerId}/rekvestes`, {
		method: 'POST',
		body: JSON.stringify({ rekvestName }),
		headers: {
			'Content-Type': 'application/json'
		}
	});

	const { info } = await response.json();

	return info;
};

const editRekvest = async ({ testerId, rekvestId, newRekvestName }) => {
	const response = await fetch(`http://localhost:4321/testers/${testerId}/rekvestes/${rekvestId}`, {
		method: 'PATCH',
		body: JSON.stringify({ newRekvestName }),
		headers: {
			'Content-Type': 'application/json'
		}
	});

	const { info } = await response.json();

	return info;
};

const removeRekvest = async ({ testerId, rekvestId }) => {
	const response = await fetch(`http://localhost:4321/testers/${testerId}/rekvestes/${rekvestId}`, {
		method: 'DELETE',
	});

	const { info } = await response.json();

	return info;
};

const moveRekvest = async ({ testerId, rekvestId, destTesterId }) => {
	const response = await fetch(`http://localhost:4321/testers/${testerId}`, {
		method: 'PATCH',
		body: JSON.stringify({ rekvestId, destTesterId }),
		headers: {
			'Content-Type': 'application/json'
		}
	});

	if (response.status !== 200) {
		const { error } = await response.json();
		return Promise.reject(error);
	}

	const { info } = await response.json();

	return info;
};

export {
	getTesters,
	addTester,
	addRekvest,
	editRekvest,
	removeRekvest,
	moveRekvest
};